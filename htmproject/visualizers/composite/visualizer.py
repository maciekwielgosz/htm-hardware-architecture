from htmproject.interfaces.visualizer import Visualizer


class CompositeVisualizer(Visualizer):
    def __init__(self, analyzer, config, visualizers=None, **kwargs):
        super(CompositeVisualizer, self).__init__(analyzer, config)

        visualizers = visualizers if visualizers is not None else []
        self.__visualizers = [
            v(self._analyzer, self._config, **kwargs) for v in visualizers
        ]

    def visualize_complete(self, prefix='', iterations_offset=0):
        for v in self.__visualizers:
            v.visualize_complete(prefix, iterations_offset)
