from htmproject.interfaces.mapper import Mapper


class ScalarMapper(Mapper):
    def __init__(self):
        pass

    def map(self, list_to_map):
        return [self.extract_name(v) for v in list_to_map]

    @staticmethod
    def extract_name(name):
        return int(float(name))
