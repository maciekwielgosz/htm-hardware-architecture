#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, print_function, absolute_import
import argparse
import sys

from htmproject.statistics.instance_generator import InstanceGenerator
from htmproject.statistics.stats_getter import StatsGetter
from htmproject.statistics.stats_renderer import StatsRenderer


def add_common_args(parser):
    parser.add_argument(
        '-c',
        '--config',
        help='config template file path'
    )

    parser.add_argument(
        '-s',
        '--s-config',
        help='statistics gathering config file path'
    )

    viz_parser = parser.add_mutually_exclusive_group(required=False)
    viz_parser.add_argument(
        '--visualizations',
        dest='visualizations',
        action='store_true',
        help='render visualizations'
    )
    viz_parser.add_argument(
        '--no-visualizations',
        dest='visualizations',
        action='store_false',
        help='don\'t render visualizations'
    )
    parser.set_defaults(visualizations=True)


def add_generator_args(parser):
    add_common_args(parser)


def add_stats_args(parser):
    add_generator_args(parser)

    pickles_parser = parser.add_mutually_exclusive_group(required=False)
    pickles_parser.add_argument(
        '--save-pickles',
        dest='save_pickles',
        action='store_true',
        help='save htm & classifier pickles'
    )
    pickles_parser.set_defaults(save_pickles=False)

    pickles_parser.add_argument(
        '--use-pickles',
        dest='use_pickles',
        action='store_true',
        help='use htm & classifier pickles instead of running learning'
    )
    pickles_parser.set_defaults(use_pickles=False)

    parser.add_argument(
        '--max-processes',
        type=int,
        help='number of processor cores used',
        default=0
    )

    parser.add_argument(
        '--instance-log',
        help='single instance logging level',
        choices=[
            'debug',
            'info',
            'warning',
            'error',
            'critical'
        ],
        default='info'
    )

    histograms_parser = parser.add_mutually_exclusive_group(required=False)
    histograms_parser.add_argument(
        '--histograms',
        dest='histograms',
        action='store_true',
        help='make histograms dumps'
    )
    histograms_parser.add_argument(
        '--no-histograms',
        dest='histograms',
        action='store_false',
        help='don\'t make histograms dumps'
    )
    parser.set_defaults(histograms=False)


def add_recover_args(parser):
    parser.add_argument(
        'dir',
        help='dir path containing generated stats files'
    )

    add_common_args(parser)


def add_subparsers(subparsers):
    stats_parser = subparsers.add_parser('stats', help='perform statistical tests')
    add_stats_args(stats_parser)

    recover_parser = subparsers.add_parser('recover', help='save what can be saved after failed stats gathering')
    add_recover_args(recover_parser)

    generate_parser = subparsers.add_parser('generate', help='dry-run for stats (setup sets and configs)')
    add_generator_args(generate_parser)


def parse_args(args):
    parser = argparse.ArgumentParser(description="HTM statistics gathering utility")

    add_stats_args(parser)

    return parser.parse_args(args)


def main(args):
    if args.mode == 'recover':
        recover(args)
    elif args.mode == 'generate':
        generate(args)
    else:
        stats(args)


def recover(args):
    sr = StatsRenderer(args.dir, args.config, args.s_config, args.visualizations)
    sr.render()


def stats(args):
    sg = StatsGetter(
        args.config,
        args.s_config,
        args.save_pickles,
        args.use_pickles,
        args.max_processes,
        args.instance_log,
        args.histograms,
        args.visualizations
    )
    sg.run()


def generate(args):
    ig = InstanceGenerator(args.config, args.s_config, False)
    for _ in ig:
        pass


def run(args):
    main(args)

if __name__ == "__main__":
    run(parse_args(sys.argv[1:]))
