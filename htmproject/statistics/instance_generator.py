import copy
import glob
import logging
import os
import random

import numpy
from htmproject.interfaces.cli import Dumper
from htmproject.interfaces.cli.cli_params import params as cli_params
from htmproject.configs.json import JSONConfig
from htmproject.utils.class_from_path import get_class_from_path


class InstanceGenerator(object):
    def __init__(self, config_path, statistics_config_path, use_pickles):
        self.__logger = logging.getLogger('htm')

        self.__generator_config = JSONConfig(statistics_config_path, core_defaults=False)

        self.__config_template = JSONConfig(config_path)
        self.__config_template.add_default_params_section('cli', cli_params)

        # import and create tmp wrapper object to get required default config values
        wrapper_class = get_class_from_path(self.__config_template['cli.wrapper_path'], 'Wrapper')
        wrapper_class(self.__config_template, dry_run=True)

        self.__instances = int(self.__generator_config['instances'])
        self.__instances_params = self.__generator_config.get('instances_params', {})
        self.__setup = str(self.__generator_config['setup']).lower()
        self.__params = self.__generator_config['params']

        self.__learning_set_size = int(self.__generator_config.get('learning_set_size', 0))
        self.__testing_set_size = int(self.__generator_config.get('testing_set_size', 0))

        self.__learning_dir = self.__config_template.get('wrapper.learning_dir', None)
        self.__testing_dir = self.__config_template.get('wrapper.testing_dir', None)

        self.__learning_set_files = self.__get_files_list(self.__learning_dir)
        self.__testing_set_files = self.__get_files_list(self.__testing_dir)

        self.__mode = str(self.__generator_config['mode']).lower()
        self.__random_trials = int(self.__generator_config['random_trials']) if self.__mode == "random" else None

        self.__use_pickles = use_pickles

    @staticmethod
    def __get_files_list(source_path):
        if source_path is None:
            return {}
        subdirs = [
            name for name in os.listdir(source_path) if os.path.isdir(os.path.join(source_path, name))
        ]
        files = {}
        for name in subdirs:
            files[name] = glob.glob(
                os.path.join(os.path.join(source_path, name), '*.*')
            )
        return files

    def __prepare_dir(self, source_set, size):
        if not size:
            return None

        dir_path = os.path.abspath(os.path.join(
            self.__config_template.get_root_path(),
            'set-%s' % (numpy.random.randint(2**32), )
        ))

        os.mkdir(dir_path, 0o700)

        for cls in source_set.keys():
            cls_dir = os.path.join(dir_path, cls)
            os.mkdir(cls_dir, 0o700)
            numpy.random.shuffle(source_set[cls])
            if len(source_set[cls]) < size:
                self.__logger.warning("Not enough files in class %s (%i instead of %i)" % (
                    cls, len(source_set[cls]), size
                ))
            for sample in source_set[cls][:size]:
                os.symlink(sample, os.path.join(cls_dir, os.path.basename(sample)))

        return dir_path

    def __prepare_learning_dir(self):
        path = self.__prepare_dir(
            self.__learning_set_files,
            self.__learning_set_size
        )
        return path if path is not None else self.__learning_dir

    def __prepare_testing_dir(self):
        path = self.__prepare_dir(
            self.__testing_set_files,
            self.__testing_set_size
        )
        return path if path is not None else self.__testing_dir

    def __iter__(self):
        if self.__mode == "random":
            return self.__random_iter()
        else:
            return self.__sweep_iter()

    def __sweep_iter(self):
        for param_name, param_config in self.__params.iteritems():
            param_value = float(param_config['begin'])
            stop = float(param_config['end'])
            step = float(param_config['step'])
            multiplier = float(param_config['multiplier'])

            while param_value <= stop:
                config = self.__prepare_sweep_config(param_name, param_value)

                i = 0
                while i < self.__instances:
                    yield self.__get_instance(config, i, '%s-%s' % (param_name, param_value))
                    i += 1

                param_value += step
                param_value *= multiplier

    def __random_iter(self):
        t = 0
        while t < self.__random_trials:
            config = self.__prepare_random_config()

            i = 0
            while i < self.__instances:
                yield self.__get_instance(config, i, 'random-%s' % (t, ))
                i += 1
            t += 1

    def __get_instance(self, config, instance_index, filename_suffix):
        local_config = copy.deepcopy(config)

        testing_dir = self.__prepare_testing_dir()
        local_config['wrapper.testing_dir'] = testing_dir

        learning_dir = None
        if not self.__use_pickles:
            learning_dir = self.__prepare_learning_dir()
            local_config['wrapper.learning_dir'] = learning_dir

        config_path = self.__get_path_for_instance('config', filename_suffix, instance_index)
        stats_path = self.__get_path_for_instance('stats', filename_suffix, instance_index)
        htm_path = self.__get_path_for_instance('htm', filename_suffix, instance_index, 'pickle')
        classifier_path = self.__get_path_for_instance('classifier', filename_suffix, instance_index, 'pickle')
        out_path = self.__get_path_for_instance('out', filename_suffix, instance_index, 'log')
        histograms_path = self.__get_path_for_instance('%s-histograms', filename_suffix, instance_index)
        visualizer_dir = self.__get_path_for_instance('visualizations', filename_suffix, instance_index, None)

        local_config['visualizer.plot.output_dir'] = visualizer_dir

        for key, values in self.__instances_params.items():
            local_config[key] = values[instance_index]

        dumper = Dumper()
        dumper.dump(config_path, local_config['all'])

        return {
            'config_path': config_path,
            'stats_path': stats_path,
            'learning_dir': learning_dir,
            'testing_dir': testing_dir,
            'htm_pickle_path': htm_path,
            'classifier_pickle_path': classifier_path,
            'out_path': out_path,
            'histograms_path': histograms_path,
            'setup': self.__setup
        }

    def __get_path_for_instance(self, name, suffix, instance_index, ext='json'):
        if ext:
            filename = '%s-%s-%s.%s' % (name, suffix, instance_index, ext)
        else:
            filename = '%s-%s-%s' % (name, suffix, instance_index)
        path = os.path.abspath(
            os.path.join(self.__config_template.get_root_path(), filename)
        )
        return path

    def __prepare_config(self, params):
        local_config = copy.deepcopy(self.__config_template)

        for key, value in params.iteritems():
            if key in local_config:
                local_config[key] = int(value) if int(value) == value else value
            else:
                raise KeyError(key)

        return local_config

    def __prepare_sweep_config(self, param_name, param_value):
        return self.__prepare_config({str(param_name): param_value})

    def __prepare_random_config(self):
        params = {}

        for param_name, param_config in self.__params.iteritems():
            start = float(param_config['begin'])
            stop = float(param_config['end'])
            step = float(param_config['step'])

            value, _ = divmod((stop - start + step) * random.random(), step)

            params[str(param_name)] = start + value

        return self.__prepare_config(params)

    def get_config_root(self):
        return self.__config_template.get_root_path()
