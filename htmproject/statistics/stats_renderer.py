import copy
import glob
import json
import logging
import os
import warnings
import math
import numpy
import operator

import re

from htmproject.configs.json import JSONConfig
from htmproject.interfaces.cli import Dumper


class StatsRenderer(object):
    def __init__(self, dir_path=None, config_path=None, statistics_config_path=None, visualizations=True):
        self.__logger = logging.getLogger('htm')
        self.__results = {}

        if dir_path is None:
            self.__dir_path = os.getcwd()
        else:
            self.__dir_path = dir_path

        if config_path is None:
            config_path = os.path.join(self.__dir_path, 'config.json')

        if statistics_config_path is None:
            statistics_config_path = os.path.join(self.__dir_path, 'statistics_config.json')

        self.__results_path = os.path.join(self.__dir_path, 'results.json')
        self.__visualization_path = os.path.join(self.__dir_path, 'results_visualization.pdf')

        self.__generator_config = JSONConfig(statistics_config_path, core_defaults=False)

        self.__mode = str(self.__generator_config['mode']).lower()

        self.__config_template = JSONConfig(config_path)
        self.__render_visualisations = visualizations and (self.__mode == "sweep")

        self.__params = self.__generator_config['params']

    def __get_stats_files(self, param_name):
        paths = {}

        files = glob.glob(
            os.path.join(self.__dir_path, 'stats-%s-*.json' % (param_name, ))
        )

        for f in files:
            m = re.match('stats-[\w_.]+-([\d.]+)-[\d.]+\.json', os.path.basename(f), re.I)
            if m is not None:
                value = float(m.group(1))
                paths.setdefault(value, []).append(os.path.abspath(f))

        return paths

    def render(self):
        if self.__mode == "sweep":
            results = self.__get_sweep_results()
        else:
            results = self.__get_results()

        self.__dump_stats(results)

        if self.__render_visualisations:
            self.__plot_stats(results)

    def __get_sweep_results(self):
        sweep_results = {}
        for param_name, param_config in self.__params.iteritems():
            stats_files = self.__get_stats_files(param_name)

            if ('begin' not in param_config) or ('end' not in param_config):
                values = stats_files.keys()
                values.sort()
                param_config['begin'] = values[0]
                param_config['end'] = values[-1]

            results = {}
            for param_value in stats_files.keys():
                results[param_value] = self.__accumulate_stats(stats_files, param_value)

            sweep_results[param_name] = results
        return sweep_results

    def __get_results(self):
        stats_files = self.__get_stats_files('random')

        results = []
        for trial in stats_files.keys():
            results.append({
                'params': self.__get_random_params(trial),
                'results': self.__accumulate_stats(stats_files, trial)
            })

        return results

    def __get_random_params(self, trial):
        config_path = os.path.join(self.__dir_path, 'config-random-%i-0.json' % (trial, ))
        config = JSONConfig(config_path)

        params = {}
        for key in self.__params.iterkeys():
            params[key] = config[key]

        return params

    @staticmethod
    def __accumulate_stats(stats_files, param_value):
        learning = []
        testing = []

        for sts in stats_files[param_value]:
            with open(sts, 'r') as sts_fd:
                try:
                    result = json.load(sts_fd)
                except ValueError:
                    pass
                else:
                    if result['learning'] is not None:
                        learning.append(result['learning']['f1_score'])
                    if result['testing'] is not None:
                        testing.append(result['testing']['f1_score'])

        learning = numpy.array(learning)
        testing = numpy.array(testing)

        results = {
            'learning': {
                'mean': None,
                'variance': None
            },
            'testing': {
                'mean': None,
                'variance': None
            }
        }

        if len(learning):
            results['learning']['mean'] = learning.mean()
            results['learning']['variance'] = learning.var()

        if len(testing):
            results['testing']['mean'] = testing.mean()
            results['testing']['variance'] = testing.var()

        return results

    def __dump_stats(self, results):
        dumper = Dumper()

        dumper.dump(self.__results_path, results)

    def __plot_stats(self, sweep_results):
        import matplotlib.pyplot as plt
        from matplotlib.backends.backend_pdf import PdfPages

        self.__logger.info('Rendering plots to %s file...' % (self.__visualization_path, ))

        with PdfPages(self.__visualization_path) as pdf:
            for param_name, results in sweep_results.iteritems():
                self.__render_plot(param_name, results, pdf, plt)

            plt.close('all')

            self.__render_config(self.__config_template, pdf, plt)
            self.__render_config(self.__generator_config, pdf, plt)

        self.__logger.info('Plots rendered')

    @staticmethod
    def __render_config(config, pdf, plt):
        config_json = json.dumps(config['all'], sort_keys=True, indent=4).split('\n')
        m = None
        for m in xrange(int(math.ceil(len(config_json) / 68.0))):
            if not m:
                fig = plt.figure(figsize=(11.69, 8.27), dpi=96)
            fig.text(
                0.07 + (m * 0.43),
                0.95,
                '\n'.join(config_json[m * 68:(m + 1) * 68]),
                size='xx-small',
                verticalalignment='top'
            )
            if m:
                pdf.savefig(fig)
        if not m and m is not None:
            pdf.savefig(fig)

    def __render_plot(self, param_name, results, pdf, plt):
        keys = results.keys()
        keys.sort()

        learning_mean = self.__get_data(results, keys, 'learning', 'mean')
        testing_mean = self.__get_data(results, keys, 'testing', 'mean')
        learning_var = self.__get_data(results, keys, 'learning', 'variance')
        testing_var = self.__get_data(results, keys, 'testing', 'variance')

        fig = plt.figure(figsize=(11.69, 8.27), dpi=96)
        fig.add_subplot(1, 1, 1)
        plt.subplots_adjust(left=0.125, bottom=0.1, right=0.9, top=0.9, wspace=0.2, hspace=0.4)

        if learning_mean[0] is not None:
            self.__add_plot_and_variance(keys, learning_mean, learning_var, 'learning', 'blue', plt)

        self.__add_plot_and_variance(keys, testing_mean, testing_var, 'testing', 'green', plt)

        if learning_mean[0] is not None:
            self.__add_trend_line(keys, learning_mean, plt)

        self.__add_trend_line(keys, testing_mean, plt)

        plt.legend(loc='best')
        plt.ylabel('F1-score')
        plt.xlabel(param_name)
        plt.ylim((-0.05, 1.05))

        pdf.savefig(fig)

        return fig

    @staticmethod
    def __get_data(results, keys, set_name, data_key):
        return numpy.array([
            results[k][set_name][data_key]
            for k in keys
        ])

    @staticmethod
    def __add_plot_and_variance(values, scores, var, label, color, plt):
        plt.plot(
            values,
            scores,
            'o',
            label=label,
            color=color
        )

        plt.fill_between(
            values,
            scores+var,
            scores-var,
            facecolor=color,
            alpha=0.5
        )

    @staticmethod
    def __add_trend_line(values, scores, plt):
        if isinstance(scores[0], tuple) or isinstance(scores[0], list):
            scores = map(operator.itemgetter(-1), scores)

        with warnings.catch_warnings():
            warnings.filterwarnings('error')
            try:
                z = numpy.polyfit(values, scores, 4)
                p = numpy.poly1d(z)
                plt.plot(values, p(values), 'r--')
            except (numpy.RankWarning, RuntimeWarning):
                pass
