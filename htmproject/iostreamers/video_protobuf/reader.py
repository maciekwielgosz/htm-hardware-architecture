from htmproject.interfaces.iostreamer.protobuf import ProtoBufReader
from .iostreamer_params import params as iostreamer


class VideoProtoBufReader(ProtoBufReader):

    def __init__(self, config):
        super(VideoProtoBufReader, self).__init__(config)

        config.add_default_params_section('iostreamer', iostreamer)
