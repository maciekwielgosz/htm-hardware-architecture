import json
import os

from htmproject.interfaces.iostreamer.writer import Writer
from .iostreamer_params import params as iostreamer


class JSONWriter(Writer):
    """
    Saves data to file. To be optimized for frequent saves.
    """
    def __init__(self, config):
        config.add_default_params_section('iostreamer', iostreamer)

        self.output_file = config.get("iostreamer.output_file")
        self.content = []
        self.file = None

    def save_data_item(self, item):
        self.content.append(list(item))
        try:
            self.file.seek(0)
            self.file.truncate()
            json.dump(self.content, self.file)
            return True
        except (TypeError, OverflowError):
            return False

    def open(self, reset_file=True):
        if not reset_file and os.path.exists(self.output_file):
            r_file = open(self.output_file)
            self.content = json.load(r_file)
            r_file.close()

        self.file = open(self.output_file, mode='w')

    def close(self):
        self.file.close()


