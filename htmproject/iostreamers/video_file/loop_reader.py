import cv2

from htmproject.iostreamers.video_file.reader import VideoFileReader


class VideoFileLoopReader(VideoFileReader):
    """
    Reads frames from *.avi video files in a loop
    """

    def _read_files(self):
        """

        :return:
        """
        frame = None

        while True:
            captured = False
            flag = True

            while not captured:
                while self._capture is None or not self._capture.isOpened() or not flag:
                    if self._capture is not None:
                        self._capture.release()
                        self._capture = None
                        flag = True
                    self._current_file = str(self._files_list.next())
                    self._capture = cv2.VideoCapture(self._current_file)

                flag, frame = self._capture.read()

                captured = flag

            yield frame
