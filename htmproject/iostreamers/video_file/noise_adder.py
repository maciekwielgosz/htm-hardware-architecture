import argparse
import logging
import os
import sys

import cv2
import re

import math
from skvideo.io import VideoWriter

from htmproject.configs.root import RootConfig
from htmproject.iostreamers.video_file import VideoFileChunkReader
from htmproject.iostreamers.video_file.noise_generator import NoiseGenerator

logging.basicConfig(
    format='%(asctime)s : %(levelname)s : %(message)s',
    level=logging.INFO,
    stream=sys.stdout
)


def parse_args(args):
    parser = argparse.ArgumentParser(description="Helper script to add noise to videos")

    parser.add_argument(
        'input_dir'
    )

    parser.add_argument(
        'output_dir'
    )

    parser.add_argument(
        '-r',
        '--reduction-rate',
        type=int,
        default=1
    )

    parser.add_argument(
        '-t',
        '--type',
        choices=[
            'gauss'
        ],
        default='gauss'
    )

    gauss_parser = parser.add_argument_group('Gauss noise params')

    gauss_parser.add_argument(
        '-m',
        '--mu',
        type=float,
        default=0
    )

    gauss_parser.add_argument(
        '-s',
        '--sigma',
        type=float,
        default=8.5
    )

    return parser.parse_args(args)


def run(args):
    logger = logging.getLogger('htm')

    abs_input_dir = os.path.abspath(args.input_dir)

    config = RootConfig()
    config.set("iostreamer.in_dir", abs_input_dir)

    try:
        os.mkdir(args.output_dir, 0o711)
    except OSError as e:
        if e.errno == 17:
            logger.critical('Output dir already exists. Not overwriting!')
            return
        else:
            raise e

    reader = VideoFileChunkReader(config, shuffling=False)
    reader.open()

    noise_generator = NoiseGenerator()

    chunk = reader.next_chunk()
    while chunk is not None:
        basename = os.path.basename(chunk['name'])
        subdir_re = re.compile(abs_input_dir + os.path.sep + '([\w_-]+)' + os.path.sep + basename)

        try:
            subdir = subdir_re.match(chunk['name']).group(1)
            abs_subdir = os.path.join(args.output_dir, subdir)
            if not os.path.exists(abs_subdir):
                os.mkdir(abs_subdir, 0o711)
        except AttributeError:
            subdir = ''

        logger.info('Processing %s...' % (basename, ))

        width = int(reader.get_capture_prop(cv2.cv.CV_CAP_PROP_FRAME_WIDTH))
        height = int(reader.get_capture_prop(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT))

        if args.reduction_rate != 1:
            width = int(math.floor(width/args.reduction_rate))
            height = int(math.floor(height / args.reduction_rate))

        ## need to be divisible by 2
        width -= width % 2
        height -= height % 2

        writer = VideoWriter(
            os.path.join(args.output_dir, subdir, basename),
            fourcc='H264',
            fps=reader.get_capture_prop(cv2.cv.CV_CAP_PROP_FPS),
            frameSize=(
                width,
                height
            )
        )
        writer.open()

        for i in xrange(chunk['size']):
            frame = reader.get_next_data_item()

            if args.reduction_rate != 1:
                frame = cv2.resize(frame, (width, height))

            noisy = noise_generator.add_normal_noise(frame, args.sigma, args.mu)
            writer.write(noisy)

        writer.release()
        chunk = reader.next_chunk()

    reader.close()

if __name__ == "__main__":
    run(parse_args(sys.argv[1:]))
