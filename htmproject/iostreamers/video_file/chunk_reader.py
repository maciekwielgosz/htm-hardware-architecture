import cv2

from htmproject.interfaces.iostreamer.chunk.reader import ChunkReader
from htmproject.iostreamers.video_file.reader import VideoFileReader


class VideoFileChunkReader(VideoFileReader, ChunkReader):
    """
    Reads frames from *.avi video files file-by-file
    """

    def _read_files(self):
        """

        :return:
        """
        while True:
            flag, frame = self._capture.read()
            if flag:
                yield frame
            else:
                raise StopIteration()

    def next_chunk(self):
        """
        :rtype: dict|None
        :return: Chunk (file) name and size (frames count) or None if no more files available
        """
        try:
            flag = False
            while self._capture is None or not self._capture.isOpened() or not flag:
                if self._capture is not None:
                    self._capture.release()
                    self._capture = None
                self._current_file = str(self._files_list.next())
                self._capture = cv2.VideoCapture(self._current_file)
                flag = True

            self._frames = self._read_files()

            return {
                'name': self._current_file,
                'size': int(self._capture.get(self._CAP_PROP_FRAME_COUNT))
            }
        except StopIteration:
            return None

    def reset_chunk(self):
        # following seems not to work on linux, so reset whole capture instead
        # self._capture.set(cv2.cv.CV_CAP_PROP_POS_FRAMES, 0)

        self._capture.release()
        self._capture = cv2.VideoCapture(self._current_file)
