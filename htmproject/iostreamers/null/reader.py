from htmproject.interfaces.iostreamer.reader import Reader


class NullReader(Reader):
    def __init__(self, size):
        self.__size = size
        self.__current = None
        self.__data = self.__data_generator()

    def __data_generator(self):
        i = 0
        while i < self.__size:
            yield  # self.__current
            i += 1

    def get_next_data_item(self):
        return self.__data.next()

    def get_data_set_size(self):
        return self.__size

    def open(self):
        pass

    def close(self):
        pass

    def get_current_data_item(self):
        return self.__current
