import os
import cv2
import shutil

from htmproject.interfaces.iostreamer.reader import Reader
from .iostreamer_params import params as iostreamer


class VideoCameraReader(Reader):
    """
    Reads frames from built-in camera.
    """

    def __init__(self, config):
        config.add_default_params_section('iostreamer', iostreamer)

        self.index = 0
        self.cap = None
        # create directory to store captured frames
        self.video_directory = config.get("iostreamer.video_frames_dir")
        if os.path.exists(self.video_directory):
            shutil.rmtree(self.video_directory)
        os.makedirs(self.video_directory)

    def get_next_data_item(self):
        ret, frame = self.cap.read()
        if ret:
            img_file_name = "image_" + str(self.index) + ".png"
            cv2.imwrite(os.path.join(self.video_directory, img_file_name), frame)
        else:
            raise Exception("Cannot capture a frame form the built-in camera.")
        self.index += 1

        return frame

    def open(self):
        """
        Start reading frames from default camera.
        :return:
        """
        self.cap = cv2.VideoCapture(0)
        if not self.cap.isOpened():
            raise Exception("Cannot open camera stream.")

    def close(self):
        """
        Stop reading from default camera.
        :return:
        """
        self.cap.release()

