import numpy
import copy
import operator
from htmproject.analyzers.single_class import SingleClassAnalyzer
from sklearn.metrics.pairwise import cosine_similarity
from scipy.stats import pearsonr


class MultipleClassesAnalyzer(SingleClassAnalyzer):
    """

    """
    def __init__(self, mapper, *args, **kwargs):
        super(MultipleClassesAnalyzer, self).__init__(*args, **kwargs)
        self.__mapper = mapper

    def _reset_history(self):
        super(MultipleClassesAnalyzer, self)._reset_history()

        self._class_similarity_history['mean_class_similarity'] = []

        self.__inter_class_similarity_history = {
            'mean_inter_class_similarity': [],
            'inter_class_similarity': []
        }

    def save_class_stats(self):
        self.__save_class_similarity()
        self.__save_inter_class_similarity()

    def __save_class_similarity(self):
        class_similarity = self.__compute_class_similarity()
        self._class_similarity_history['class_similarity'].append(class_similarity['class_similarity'])
        self._class_similarity_history['mean_class_similarity'].append(class_similarity['mean_class_similarity'])

    def __save_inter_class_similarity(self):
        inter_class_similarity = self.__compute_inter_class_similarity()
        self.__inter_class_similarity_history['mean_inter_class_similarity'].append(inter_class_similarity['mean_inter_class_similarity'])
        self.__inter_class_similarity_history['inter_class_similarity'].append(inter_class_similarity['inter_class_similarity'])

    def __class_vs_class_sim(self, class_a, class_b):
        """
        Generates similarity for pair of classes.
        :return dict :{
            'cos_similarity': cos_sim,
            'pearson_similarity': pearson_sim
        }
        """

        class_hist_data = self.get_last_chunk_histograms_by_class()
        item_a = class_hist_data[class_a]
        item_b = class_hist_data[class_b]

        cos_sim = 0
        pearson_sim = 0

        if class_a == class_b:
            sim = self._single_class_similarity(item_a)
            cos_sim = sim['cosine']
            pearson_sim = sim['pearson']
        else:
            for i in item_a:
                i = i.reshape(1, -1)
                for j in item_b:
                    j = j.reshape(1, -1)
                    cos_sim = cos_sim + cosine_similarity(i, j)
                    pearson_sim = pearson_sim + pearsonr(i[0], j[0])[0]
            cos_sim = float(cos_sim)/(len(item_a) * len(item_b))
            pearson_sim = float(pearson_sim)/(len(item_a) * len(item_b))

        return {
            'cosine': cos_sim,
            'pearson': pearson_sim
        }

    def __compute_class_similarity(self):
        """

        :return:
        """

        class_hist_data = self.get_last_chunk_histograms_by_class()

        similarity = {
            'cosine': None,
            'pearson': None
        }

        class_similarity = {}
        accumulated_cos_sim = 0
        accumulated_pearson_sim = 0
        for key, item in class_hist_data.items():
            similarity = self._single_class_similarity(item)
            accumulated_cos_sim += similarity['cosine']
            accumulated_pearson_sim += similarity['pearson']
            class_similarity[key] = (copy.deepcopy(similarity))

        # mean class similarity
        if len(class_hist_data):
            similarity['cosine'] = float(accumulated_cos_sim)/len(class_hist_data)
            similarity['pearson'] = float(accumulated_pearson_sim)/len(class_hist_data)

        mean_class_similarity = copy.deepcopy(similarity)

        return {
            'class_similarity': class_similarity,
            'mean_class_similarity': mean_class_similarity
        }

    def __compute_inter_class_similarity(self):
        """

        :return:
        {
            'mean_inter_class_similarity': { 'cosine': cosine_class_sim, 'pearson': pearson_class_sim }
            'inter_class_similarity': inter_class_sims
        }
        """
        similarity = {
            'cosine': None,
            'pearson': None
        }

        keys = self.get_last_chunk_histograms_by_class().keys()
        sims = {}
        inter_class_sims = {}
        cosine_sims_array = numpy.zeros(len(keys)*len(keys)).reshape(len(keys), len(keys))
        pearson_sims_array = numpy.zeros(len(keys)*len(keys)).reshape(len(keys), len(keys))

        for i in range(len(keys)):
            for j in range(len(keys)):
                sims[keys[j]] = self.__class_vs_class_sim(keys[i], keys[j])
                cosine_sims_array[i][j] = sims[keys[j]]['cosine']
                pearson_sims_array[i][j] = sims[keys[j]]['pearson']
            inter_class_sims[keys[i]] = copy.deepcopy(sims)

        cosine_sims_array = numpy.triu(cosine_sims_array, 1)
        cosine_sims_array[numpy.isnan(cosine_sims_array)] = 0
        non_zero = numpy.nonzero(cosine_sims_array)
        if non_zero[0].size:
            cos_sim = cosine_sims_array[non_zero]
            cos_sim = cos_sim.mean()
        else:
            cos_sim = 0.0
        similarity['cosine'] = cos_sim

        pearson_sims_array = numpy.triu(pearson_sims_array, 1)
        pearson_sims_array[numpy.isnan(pearson_sims_array)] = 0
        non_zero = numpy.nonzero(pearson_sims_array)
        if non_zero[0].size:
            pearson_sim = pearson_sims_array[non_zero]
            pearson_sim = pearson_sim.mean()
        else:
            pearson_sim = 0.0
        similarity['pearson'] = pearson_sim

        mean_inter_class_similarity = copy.deepcopy(similarity)
        inter_class_sim = copy.deepcopy(inter_class_sims)

        return {
            'mean_inter_class_similarity': mean_inter_class_similarity,
            'inter_class_similarity': inter_class_sim
        }

    def get_inter_class_similarity(self):
        return self.__inter_class_similarity_history

    def get_last_inter_class_similarity(self):
        """
        :rtype: dict
        :return:
        """
        stats = {}
        for key, history in self.__inter_class_similarity_history.items():
            if len(history):
                stats[key] = history[-1]
        return stats

    def get_chunk_histograms_by_class(self):
        histograms_by_class = {}
        data = self.__chunk_info
        for name in histograms_by_class.keys():
            histograms_by_class[name] = numpy.array(map(operator.itemgetter('histogram'), filter(
                lambda x: self.__mapper.extract_name(x['name']) == name,
                data
            )))
        return histograms_by_class

    def get_last_chunk_histograms_by_class(self, with_chunk_names=False):
        last_histograms_by_class = {}
        last_chunk_names_by_class = {}
        last_histograms_by_name = self.get_last_chunk_histograms_by_name()
        for name in last_histograms_by_class.keys():
            last_histograms_by_class[name] = numpy.array([
                histogram
                for chunk_name, histogram in last_histograms_by_name.items()
                if self.__mapper.extract_name(chunk_name) == name
            ])
            if with_chunk_names:
                last_chunk_names_by_class[name] = filter(
                    lambda x: self.__mapper.extract_name(x) == name,
                    last_histograms_by_name.keys()
                )
        if with_chunk_names:
            return last_histograms_by_class, last_chunk_names_by_class
        else:
            return last_histograms_by_class
