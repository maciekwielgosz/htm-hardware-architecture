import numpy
from sklearn.metrics.pairwise import cosine_similarity

from htmproject.analyzers.basic import BasicAnalyzer


class SingleClassAnalyzer(BasicAnalyzer):
    """

    """
    def __init__(self, htm, input_set_size, config, *args, **kwargs):
        self._htm = htm
        self._input_set_size = input_set_size
        self._layers = int(config.get("core.htm.layers"))

        super(SingleClassAnalyzer, self).__init__(config, *args, **kwargs)

    def _reset_history(self):
        super(SingleClassAnalyzer, self)._reset_history()

        self._class_similarity_history = {
            'class_similarity': []
        }

        self.__per_layer_stats_history = []
        for _ in range(self._layers):
            self.__per_layer_stats_history.append({
                'percent_of_active_columns': [],
                'inhibition_radius': [],
                'overlap': [],
                'boost': [],
                'cell_active_[%_of_layer_capacity]': [],
                'cell_predictive_[%_of_layer_capacity]': [],
                'cell_learning_[%_of_layer_capacity]': [],
                'segment_active_[%_of_layer_capacity]': [],
                'segment_sequential_[%_of_layer_capacity]': [],
                'segment_learning_[%_of_layer_capacity]': []
            })

    def save_class_stats(self):
        self.__save_class_similarity()

    def save_iteration_stats(self, encoded_input, encoded_output):
        """
        Tells analyzer that iteration has passed an it needs to update its internal state
        :return:
        """
        super(SingleClassAnalyzer, self).save_iteration_stats(encoded_input, encoded_output)

        # update per-layer stats
        for i in range(self._layers):
            layer = self.__per_layer_stats_history[i]
            layer["percent_of_active_columns"].append(self.__get_active_columns(i))
            layer["inhibition_radius"].append(self.__get_inhibition_radius(i))
            layer["overlap"].append(self.__get_overlap(i))
            layer["boost"].append(self.__get_boost(i))
            layer["cell_active_[%_of_layer_capacity]"].append(self.__get_cell_active(i))
            layer["cell_predictive_[%_of_layer_capacity]"].append(self.__get_cell_predicted(i))
            layer["cell_learning_[%_of_layer_capacity]"].append(self.__get_cell_learning(i))
            layer["segment_active_[%_of_layer_capacity]"].append(self.__get_segment_active(i))
            layer["segment_sequential_[%_of_layer_capacity]"].append(self.__get_segment_sequential(i))
            layer["segment_learning_[%_of_layer_capacity]"].append(self.__get_segment_learning(i))

    def get_iteration_stats(self):
        """
        Returns statistics from the last iteration for all the layers.
        :rtype: dict
        :return dict of dicts: Current set of statistics for all the layers.
        """

        stats = super(SingleClassAnalyzer, self).get_iteration_stats()

        per_layer_stats = []

        for layer in self.__per_layer_stats_history:
            stats = {}
            for k, v in layer.items():
                stats[k] = v[-1]
            per_layer_stats.append(stats)

        stats.update({
            'per_layer': per_layer_stats
        })

        return stats

    def get_complete_stats(self):
        """
        Returns a complete set of statistics from all the iterations.
        :rtype: dict
        :return dict of dicts: Complete set of statistics
        """
        stats = super(SingleClassAnalyzer, self).get_complete_stats()

        stats.update({
            'per_layer': self.__per_layer_stats_history
        })

        return stats

    def get_class_similarity(self):
        return self._class_similarity_history

    def get_last_class_similarity(self):
        """
        :rtype: dict
        :return:
        """
        stats = {}
        for key, history in self._class_similarity_history.items():
            if len(history):
                stats[key] = history[-1]
        return stats

    def get_set_size(self):
        return self._input_set_size

    def __save_class_similarity(self):
        class_similarity = self.__compute_class_similarity()
        self._class_similarity_history['class_similarity'].append(class_similarity['class_similarity'])

    @staticmethod
    def _single_class_similarity(item):
        similarity = {
            'cosine': None,
            'pearson': None
        }

        cos_sim = numpy.triu(cosine_similarity(item), 1)
        cos_sim[numpy.isnan(cos_sim)] = 0
        non_zero = numpy.nonzero(cos_sim)
        if non_zero[0].size:
            cos_sim = cos_sim[non_zero]
            cos_sim = cos_sim.mean()
        else:
            cos_sim = 0.0
        similarity['cosine'] = cos_sim

        pearson_sim = numpy.triu(numpy.corrcoef(item), 1)
        pearson_sim[numpy.isnan(pearson_sim)] = 0
        non_zero = numpy.nonzero(pearson_sim)
        if non_zero[0].size:
            pearson_sim = pearson_sim[non_zero]
            pearson_sim = pearson_sim.mean()
        else:
            pearson_sim = 0.0
        similarity['pearson'] = pearson_sim

        return similarity

    def __compute_class_similarity(self):
        """

        :return:
        """

        class_hist_data = self.get_last_chunk_histograms()

        return {
            'class_similarity': {
                '_': self._single_class_similarity(class_hist_data)
            }
        }

    def __get_active_columns(self, layer):
        """
        Returns % of active columns for a given layer.

        :param int layer: layer index
        :return float: % of active columns
        """
        sp_output = self._htm.get_sp().get_layer_output(self._htm.get_layers()[layer])
        return self._count_non_zero_in_percent(sp_output)

    def __get_inhibition_radius(self, layer):
        """
        Returns inhibition radius for a given layer.

        :param layer:
        :return int: inhibition radius
        """
        return self._htm.get_layers()[layer].get_sp_state().get_inhibition_radius()

    def __get_overlap(self, layer):
        """
        Returns min., max., mean and variance of overlap for a given layer.

        :param layer:
        :return:
        """
        return self.__get_column_connector_stats(layer, lambda connector: connector.get_overlap())

    def __get_boost(self, layer):
        return self.__get_column_connector_stats(layer, lambda connector: connector.get_boost())

    def __get_total_number_of_cells_and_segments(self, layer):
        """
        Gets total number of cells for a given layer
        :param layer:
        :return:
        """
        layer_all = self._htm.get_layers()[layer]
        column_amount = layer_all.get_columns_amount()
        cells_amount_in_a_single_column = \
            layer_all.get_columns()[0].get_cells_amount()

        # compute segments amount in all the cells within the layer
        total_number_of_segments = sum(cell.get_segments_amount() for column in layer_all.get_columns() for cell in column.get_cells())

        return {
            'cells_amount': column_amount * cells_amount_in_a_single_column,
            'segments_amount': total_number_of_segments
        }

    @staticmethod
    def __get_calculated_stats(stats):
        stats_vector = numpy.array(stats)

        stats_mean = None
        if stats_vector[0] is not None:
            stats_mean = stats_vector.mean()

        return stats_mean

    def __get_column_connector_stats(self, layer, extractor):
        stats = []

        for column_connector in self._htm.get_layers()[layer].get_column_connectors():
            stats.append(extractor(column_connector))

        return self.__get_calculated_stats(stats)

    def __get_cell_stats_per_column(self, layer, extractor, items_amount):
        if not items_amount:
            return 0

        all_layer = self._htm.get_layers()[layer]
        total = sum(extractor(cell) for column in all_layer.get_columns() for cell in column.get_cells())
        return (float(total)/items_amount) * 100

    def __get_cell_active(self, layer):
        """

        :param layer:
        :return:
        """
        cells_amount = self.__get_total_number_of_cells_and_segments(layer)['cells_amount']

        return self.__get_cell_stats_per_column(layer, lambda cell: cell.is_active(), cells_amount)

    def __get_cell_predicted(self, layer):
        """

        :param layer:
        :return:
        """
        cells_amount = self.__get_total_number_of_cells_and_segments(layer)['cells_amount']

        return self.__get_cell_stats_per_column(layer, lambda cell: cell.is_predicted(), cells_amount)

    def __get_cell_learning(self, layer):
        """

        :param layer:
        :return:
        """
        cells_amount = self.__get_total_number_of_cells_and_segments(layer)['cells_amount']

        return self.__get_cell_stats_per_column(layer, lambda cell: cell.is_learning(), cells_amount)

    def __get_segment_active(self, layer):
        """

        :param layer:
        :return:
        """

        segments_amount = self.__get_total_number_of_cells_and_segments(layer)['segments_amount']

        return self.__get_cell_stats_per_column(layer, lambda cell: numpy.count_nonzero([
            segment.is_active() for segment in cell.get_segments()
        ]), segments_amount)

    def __get_segment_sequential(self, layer):
        """

        :param layer:
        :return:
        """

        segments_amount = self.__get_total_number_of_cells_and_segments(layer)['segments_amount']

        # return self.__get_cell_stats_per_column(layer, lambda cell: numpy.count_nonzero([
        #     segment.is_sequential() for segment in cell.get_segments()
        # ]), segments_amount)
        return self.__get_cell_stats_per_column(layer, lambda cell: sum(
            1 for segment in cell.get_segments() if segment.is_sequential()), segments_amount)

    def __get_segment_learning(self, layer):
        """

        :param layer:
        :return:
        """
        segments_amount = self.__get_total_number_of_cells_and_segments(layer)['segments_amount']

        # return self.__get_cell_stats_per_column(layer, lambda cell: numpy.count_nonzero([
        #     segment.is_learning() for segment in cell.get_segments()
        # ]), segments_amount)
        return self.__get_cell_stats_per_column(layer, lambda cell: sum(
            1 for segment in cell.get_segments() if segment.is_learning()), segments_amount)
