import os

from htmproject.interfaces.config import Config


class RootConfig(Config):
    """

    """

    def __init__(self, root_path=None, *args, **kwargs):
        super(RootConfig, self).__init__(*args, **kwargs)
        self.__root_path = root_path if root_path is not None else os.getcwd()

    def __getitem__(self, item):
        val = super(RootConfig, self).__getitem__(item)

        if item.endswith(('file', '_dir')) and not os.path.isabs(val):
            val = os.path.abspath(os.path.join(self.__root_path, val))

        return val

    def set_root_path(self, root_path):
        self.__root_path = root_path

    def get_root_path(self):
        return self.__root_path
