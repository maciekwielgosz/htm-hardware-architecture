import inspect
import os
import sys
import pybars


class TemplatesMixin(object):
    def __init__(self):
        self._tpl_dir = os.path.join(self._get_script_dir(), 'templates')
        self._compiler = pybars.Compiler()
        self._templates = {}

    def _add_templates(self, templates):
        for n, f in templates.items():
            tpl = open(os.path.join(self._tpl_dir, f)).read().decode('utf8')
            self._templates[n] = self._compiler.compile(tpl)

    def _render_template(self, name, data, out):
        output_file = open(out, mode='w')
        output_file.write(self._templates[name](data).encode('utf8'))
        output_file.close()

    def _get_script_dir(self):
        if getattr(sys, 'frozen', False):  # py2exe, PyInstaller, cx_Freeze
            path = os.path.abspath(sys.executable)
        else:
            path = inspect.getabsfile(self._get_script_dir)
        return os.path.dirname(os.path.realpath(path))
