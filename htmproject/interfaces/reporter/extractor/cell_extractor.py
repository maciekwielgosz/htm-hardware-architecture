import numpy
from .extractor import Extractor
from .segment_extractor import SegmentExtractor


class CellExtractor(Extractor):
    @staticmethod
    def extract(cell):
        segment_extractor = SegmentExtractor()

        data = {
            'cell_index': cell.get_cell_index(),
            'column_index': cell.get_column_index(),
            'is_active': cell.is_active(),
            'is_predictive': cell.is_predicted(),
            'is_learning': cell.is_learning(),
            'segments': [segment_extractor.extract(segment) for segment in cell.get_segments()]
        }

        data['segments_count'] = len(data['segments'])

        data['segments_transposed'] = [
            {'synapses': s}
            for s in numpy.array(
                [se['synapses'] for se in data['segments']]
            ).transpose().tolist()
        ]

        return data