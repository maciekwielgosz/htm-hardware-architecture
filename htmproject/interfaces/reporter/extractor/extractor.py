class Extractor(object):
    @staticmethod
    def extract(obj):
        raise NotImplementedError()

