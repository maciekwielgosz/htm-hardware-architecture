import copy
from collections import MutableMapping

from htmproject.core.core_params import params as core


class Config(MutableMapping):
    """

    """

    def __init__(self, initial_data=None, core_defaults=True):
        self.__data = initial_data if initial_data is not None else {}
        self.__params = {}

        if core_defaults:
            self.add_default_params_section('core', core)

    def __contains__(self, name):
        keys = name.split('.')

        data = self.__data

        for key in keys[:-1]:
            if key not in data:
                return False
            data = data[key]

        if keys[-1].isdigit():
            return int(keys[-1]) < len(data)
        else:
            return keys[-1] in data

    def __getitem__(self, name):
        if name == 'all':
            return self.__data

        keys = name.split('.')

        data = self.__data
        for key in keys:
            if key.isdigit():
                key = int(key)
            data = data[key]

        return data

    def __setitem__(self, name, value):
        keys = name.split('.')

        data = self.__data

        for key in keys[:-1]:
            if key not in data:
                data[key] = {}
            data = data[key]

        last_key = int(keys[-1]) if keys[-1].isdigit() else keys[-1]
        data[last_key] = value

        # add defaults
        if isinstance(value, dict):
            self.__set_defaults()

        return value

    def __delitem__(self, name):
        keys = name.split('.')

        data = self.__data

        try:
            for key in keys[:-1]:
                data = data[key]
            del data[keys[-1]]
        except KeyError:
            pass

    def __iter__(self):
        return iter(self.__data)

    def __len__(self):
        return len(self.__data)

    def set(self, name, value, index=None):
        if index is None:
            self[name] = value
        else:
            self[name] = self[name] if name in self else []
            self[name][index] = value

    def add_default_params_section(self, name, params):
        keys = name.split('.')

        # update default params dict
        data = self.__params
        for key in keys:
            if key not in data:
                data[key] = {}
            data = data[key]
        data.update(params)

        # ensure all newly added params are in data
        self.__set_defaults()

    def __set_defaults(self):
        default_data = copy.deepcopy(self.__params)
        default_data = self.__recursive_replace_defaults(default_data)
        self.__data = self.__recursive_update(default_data, self.__data)

    def __recursive_update(self, old, new):
        for k, v in new.items():
            if isinstance(v, dict):
                r = self.__recursive_update(old.get(k, {}), v)
                old[k] = r
            else:
                old[k] = new[k]
        return old

    def __recursive_replace_defaults(self, data):
        if 'default' in data:
            data = data['default']
        else:
            for k, v in data.items():
                if isinstance(v, dict):
                    data[k] = self.__recursive_replace_defaults(v)

        return data

    def get_default_param(self, name):
        """
        Allows accessing default params configuration (properties like default value, min, max, etc)
        :param name:
        :rtype: int|float|str|list|dict
        :return: parameter property
        """
        keys = name.split('.')

        param = self.__params
        for key in keys:
            if isinstance(param, dict):
                try:
                    param = param[key]
                except KeyError:
                    if key != 'default':
                        raise KeyError(key)
            elif isinstance(param, list) and key != 'default':
                param = param[int(key)]
            elif key != 'default':
                raise KeyError(key)

        return param

    def __str__(self):
        return str(self.__data)

