class Visualizer(object):
    def __init__(self, analyzer, config, *args, **kwargs):
        self._analyzer = analyzer
        self._config = config

    def visualize_complete(self, prefix='', iterations_offset=0):
        raise NotImplementedError()
