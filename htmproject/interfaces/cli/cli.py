import logging
import copy
import operator
import pprint
import sys

from htmproject.interfaces.cli.exceptions import TooManyIterationsError, PickleError
from htmproject.utils.class_from_path import get_class_from_path
from htmproject.interfaces.cli.pickler import Pickler
from htmproject.interfaces.cli.dumper import Dumper

from htmproject.configs.json import JSONConfig
from htmproject.interfaces.cli.cli_params import params as cli_params


class CLI(object):
    def __init__(self, config_path='config.json'):
        """
        Reads configuration from external file and configures execution according to its contents
        and constructor parameters

        :param str [config_path]: HTM Configuration file name
        """
        self._logger = logging.getLogger('htm')

        self._config = JSONConfig(config_path)
        self._config.add_default_params_section('cli', cli_params)

        self.__wrapper_class = self.__get_class('cli.wrapper_path', 'HTM wrapper')
        self.__classifier_class = self.__get_class('cli.classifier_path', 'classifier')

        self._input_pickle_path = None
        self._output_pickle_path = None
        self._classifier_pickle_path = None

        self._classifier_stats_path = 'classifier_stats.json'
        self._histograms_path = None
        self._combined_histograms_path = None
        self._histograms_stats_path = None

    def __start(self, mode_handler):
        """
        Starts HTM life cycle
        """

        self._logger.info('HTM life cycle starting...')
        mode_handler()
        self._logger.info('HTM life cycle done')

    def learn(
            self,
            input_pickle_path=None,
            output_pickle_path=None,
            classifier_pickle_path=None,
            histograms_path=None,
            combined_histograms_path=None,
            histograms_stats_path=None,
            classifier_stats_path=None
    ):
        self._logger.info('MODE: LEARNING')

        self._input_pickle_path = input_pickle_path
        self._output_pickle_path = output_pickle_path
        self._classifier_pickle_path = classifier_pickle_path

        if self._input_pickle_path is not None:
            self._logger.debug('HTM module(s) input pickle: %s' % (self._input_pickle_path,))
        if self._output_pickle_path is not None:
            self._logger.debug('HTM module(s) output pickle: %s' % (self._output_pickle_path,))
        if self._classifier_pickle_path is not None:
            self._logger.debug('Classifier pickle: %s' % (self._classifier_pickle_path,))

        self.__setup_dumps(classifier_stats_path, histograms_path, combined_histograms_path, histograms_stats_path)

        self.__setup_htm_modules()
        self.__start(self.__start_learning)

    def test(self, input_pickle_path=None, classifier_pickle_path=None, histograms_path=None,
             combined_histograms_path=None, histograms_stats_path=None, classifier_stats_path=None):
        self._logger.info('MODE: TESTING')

        self._input_pickle_path = input_pickle_path
        self._classifier_pickle_path = classifier_pickle_path

        if self._input_pickle_path is not None:
            self._logger.debug('HTM module(s) input pickle: %s' % (self._input_pickle_path,))

        if self._classifier_pickle_path is not None:
            self._logger.debug('Classifier pickle: %s' % (self._classifier_pickle_path,))

        self.__setup_dumps(classifier_stats_path, histograms_path, combined_histograms_path, histograms_stats_path)

        self.__setup_htm_modules()
        self.__start(self.__start_testing)

    def work(self):
        self._logger.info('MODE: WORKING')

    def __setup_dumps(
            self,
            classifier_stats_path=None,
            histograms_path=None,
            combined_histograms_path=None,
            histograms_stats_path=None
    ):
        if classifier_stats_path is not None:
            self._classifier_stats_path = classifier_stats_path
        self._logger.debug('Classifier stats file path: %s' % (self._classifier_stats_path,))

        if histograms_path is not None:
            self._histograms_path = histograms_path
        self._logger.debug('Histograms dump file path: %s' % (self._histograms_path,))

        if combined_histograms_path is not None:
            self._combined_histograms_path = combined_histograms_path
        self._logger.debug('Combined histograms dump file path: %s' % (self._combined_histograms_path,))

        if histograms_stats_path is not None:
            self._histograms_stats_path = histograms_stats_path
        self._logger.debug('Histograms stats file path: %s' % (self._histograms_stats_path,))

    def __save_classifier(self, classifier):
        if self._classifier_pickle_path is not None:
            self._logger.info('Saving trained classifier...')
            Pickler.dump(self._classifier_pickle_path, classifier)
            self._logger.info('Classifier saved')

    def __dump_classifier_stats(self, learning_stats=None, testing_stats=None):
        dumper = Dumper()

        dumper.dump(
            self._classifier_stats_path, {
                'learning': learning_stats,
                'testing': testing_stats
            }
        )

    def __get_classifier_stats(self, classifier, histograms, mapped_classes, class_names, set_name):
        self._logger.info('Classifying %s set...' % (set_name,))

        stats = classifier.get_classification_stats(
            histograms,
            mapped_classes,
            class_names
        )

        self._logger.info('Classification done')

        self._logger.debug('Classification results:\n%s' % (pprint.pformat(stats),))

        return stats

    def __start_learning(self):
        modules_to_train = self.__htm_modules.keys()

        while len(modules_to_train):
            modules_to_train = self.__train_modules(modules_to_train)

        self._logger.info('All HTM modules reached maximum learning iterations amount.')

        learning_stats, classifier = self.__train_classifier()
        testing_stats = self.__test_modules(classifier)

        self.__dump_classifier_stats(learning_stats, testing_stats)
        self.__save_state(classifier)

    def __start_testing(self):
        if self._classifier_pickle_path is not None:
            self._logger.info('Loading classifier...')
            classifier = Pickler.load(self._classifier_pickle_path)
            self._logger.info('Classifier loaded')

            _, classifier_stats = self.__test_modules(classifier)
            self.__dump_classifier_stats(testing_stats=classifier_stats)
        else:
            self.__get_final_results('testing', True)

    def __start_working(self):
        pass

    def _get_testing_results(self, set_name):
        self._logger.info('Gathering testing results for %s set...' % (set_name,))

        results = {}
        stats = {}
        sort_list = None

        for name, wrapper in self.__htm_modules.items():
            mapper = wrapper.get_mapper()
            test_results = wrapper.test(set_name == 'testing')

            stats[name] = test_results['stats']
            sort_list = []

            for chunk_name, histogram in test_results['histograms']:
                sort_list.append(chunk_name)
                if chunk_name not in results:
                    results[chunk_name] = {
                        'class_name': mapper.extract_name(chunk_name),
                        'histograms': {}
                    }
                results[chunk_name]['histograms'][name] = histogram

        self._logger.info('Testing results gathered')

        sorted_results = [
            (chunk_name, results[chunk_name])
            for chunk_name in sort_list
        ]

        return sorted_results, stats

    def __train_modules(self, modules_to_train):
        next_to_train = copy.copy(modules_to_train)

        for name in modules_to_train:
            wrapper = self.__htm_modules[name]
            try:
                wrapper.learn()
            except TooManyIterationsError:
                next_to_train.remove(name)
        return next_to_train

    def __train_classifier(self):
        classifier = self.__classifier_class(self._config)

        results = self.__get_results_for_classifier('learning')

        self._logger.info('Training classifier...')
        classifier.train(results['histograms'], results['mapped_classes'])
        self._logger.info('Classifier trained')

        stats = self.__get_classifier_stats(classifier, results['histograms'], results['mapped_classes'],
                                            results['class_names'], 'learning')

        return stats, classifier

    def __get_results_for_classifier(self, set_name):
        final_results = self.__get_final_results(set_name, True)

        keys, values = zip(*final_results)

        classes = list(set(map(operator.itemgetter('class_name'), values)))
        mapper = self.__htm_modules.values()[0].get_mapper(classes)
        mapped_classes = mapper.map(keys)

        histograms = map(operator.itemgetter('histogram'), values)

        return {
            'mapped_classes': mapped_classes,
            'histograms': histograms,
            'class_names': classes
        }

    def __test_modules(self, classifier):
        """
        Test HTM modules using testing set

        :param Classifier classifier: Trained classifier
        :rtype: dict
        :return: Classification statistics
        """
        results = self.__get_results_for_classifier('testing')

        stats = self.__get_classifier_stats(classifier, results['histograms'], results['mapped_classes'],
                                            results['class_names'], 'testing')

        return stats

    def __log_class_done(self):
        if 0 < self._logger.getEffectiveLevel() < logging.WARNING:
            sys.stdout.write('\n')
            sys.stdout.flush()

    def __log_iterations_done(self, elapsed_iterations):
        self._logger.info('Elapsed iterations: %i' % (elapsed_iterations,))

    def __setup_htm_modules(self):
        self.__htm_modules = None
        self.__load_htm_modules()
        if self.__htm_modules is None:
            self.__create_htm_modules()

    def __load_htm_modules(self):
        if self._input_pickle_path is not None:
            self._logger.info('Loading trained HTM modules...')
            try:
                self.__htm_modules = Pickler.load(self._input_pickle_path)
                for wrapper in self.__htm_modules.values():
                    wrapper.update_config(copy.deepcopy(self._config))
                self._logger.info('HTM modules loaded')
            except EOFError:
                self._logger.critical('Empty or corrupted pickle file. Aborting.')
                raise PickleError()

    def __create_htm_modules(self):
        self.__htm_modules = {}

        module_names = self._get_module_names()

        for name in module_names:
            self.__htm_modules[name] = self.__wrapper_class(config=copy.deepcopy(self._config),
                                                            class_name=name)

    def __save_state(self, classifier):
        self.__save_htm_modules()
        self.__save_classifier(classifier)

    def __save_htm_modules(self):
        if self._output_pickle_path is not None:
            self._logger.info('Saving trained HTM modules...')
            Pickler.dump(self._output_pickle_path, self.__htm_modules)
            self._logger.info('HTM modules saved')

    def _get_module_names(self):
        raise NotImplementedError()

    def __dump_histograms(self, combined_results, testing_results, testing_stats, set_name):
        dumper = Dumper()

        if self._histograms_path is not None:
            dumper.dump(
                self._histograms_path % (set_name,),
                testing_results
            )

        if self._histograms_stats_path is not None:
            dumper.dump(
                self._histograms_stats_path % (set_name,),
                testing_stats
            )

        if self._combined_histograms_path is not None:
            dumper.dump(
                self._combined_histograms_path % (set_name,),
                combined_results
            )

    def __get_final_results(self, set_name, with_dump=False):
        testing_results, testing_stats = self._get_testing_results(set_name)
        combined_results = self._combine_results(testing_results)
        if with_dump:
            self.__dump_histograms(combined_results, testing_results, testing_stats, set_name)
        return combined_results

    def _combine_results(self, testing_results):
        raise NotImplementedError()

    def __get_class(self, key, desc):
        path = self._config.get(key)
        return get_class_from_path(path, desc)
