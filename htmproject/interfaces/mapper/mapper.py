class Mapper(object):
    def __init__(self, names):
        self._names = names

    def map(self, list_to_map):
        return [
            self._names.index(self.extract_name(v)) for v in list_to_map
        ]

    @staticmethod
    def extract_name(name):
        return name
