import importlib
from .iostreamer_params import params as iostreamer


def setup_from_config(config):
    config.add_default_params_section('iostreamer', iostreamer)

    proto_path = config.get('iostreamer.compiled_proto_path')
    proto = importlib.import_module(proto_path)

    extras_path = config.get('iostreamer.extras_proto_path')
    if extras_path:
        extras = importlib.import_module(extras_path)
    else:
        extras = None

    return proto, extras
