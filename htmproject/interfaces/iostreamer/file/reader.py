import os
import glob
import numpy
import logging
import random

from htmproject.interfaces.iostreamer.reader import Reader
from htmproject.interfaces.iostreamer.exceptions import EmptyError
from .iostreamer_params import params as iostreamer


class FileReader(Reader):
    """
    Reads frames from files.
    """

    def __init__(self, config, extension='*', shuffling=True):
        config.add_default_params_section('iostreamer', iostreamer)

        self._logger = logging.getLogger('htm')

        self._files_list = None
        self._current_file = None
        self._shuffling = shuffling

        self._frames = self._read_files()
        self.__input_dir = config.get("iostreamer.in_dir")
        self.__files_list = None

        if isinstance(extension, tuple) or isinstance(extension, list):
            self.__extensions = tuple(extension)
        else:
            self.__extensions = (extension,)

        if not os.path.exists(self.__input_dir):
            self._logger.critical("Input directory '%s' does not exist" % (self.__input_dir,))

    def _read_files(self):
        raise NotImplementedError()

    def get_next_data_item(self):
        """
        :rtype: numpy.ndarray
        :return: Video frame
        """
        return self._frames.next()

    def get_files_list(self):
        """
        :rtype: list
        :return: List of files in directory matching predefined extensions.
            Order of files does not need to match processing order
        """
        if self.__files_list is None:
            files_list = []

            for ext in self.__extensions:
                files_list.extend(glob.glob(
                    os.path.join(self.__input_dir, '*.%s' % (ext, ))
                ) + glob.glob(
                    os.path.join(self.__input_dir, '**', '*.%s' % (ext, ))
                ))

            self.__files_list = files_list

        return self.__files_list

    def get_name(self):
        return self._current_file

    def get_data_set_size(self, files_filter=lambda n: True):
        files_list = self.get_files_list()

        return int(len(files_list))

    def open(self):
        """
        Read frames from all the files in the frame folder.
        """

        files_list = self.get_files_list()

        if not len(files_list):
            self._logger.critical('There are no *.%s files in %s directory' % (self.__extensions, self.__input_dir,))
            raise EmptyError()

        if self._shuffling:
            random.shuffle(files_list)

        self._files_list = numpy.nditer([files_list])

    def close(self):
        """
        Clean up
        """
        pass

    def reset(self):
        """
        Resets (and potentially reshuffles) files list
        """
        if not self._shuffling:
            self._files_list.reset()
        else:
            files_list = self.get_files_list()
            random.shuffle(files_list)
            self._files_list = numpy.nditer([files_list])
        self._frames = self._read_files()
