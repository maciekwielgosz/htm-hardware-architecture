class Reader(object):
    """
    Abstract reader class
    """

    def get_next_data_item(self):
        """

        :return: single datum as an n-dimensional matrix of integers
        :rtype: numpy.ndarray
        """
        raise NotImplementedError()

    def open(self):
        raise NotImplementedError()

    def close(self):
        raise NotImplementedError()

    def get_data_set_size(self):
        raise NotImplementedError()

    def get_name(self):
        raise NotImplementedError()

    def reset(self):
        raise NotImplementedError()
