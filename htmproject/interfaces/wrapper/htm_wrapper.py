import pprint

from htmproject.analyzers.multiple_classes import MultipleClassesAnalyzer
from htmproject.analyzers.single_class import SingleClassAnalyzer
from htmproject.core import HTM, Iterator
from htmproject.interfaces.cli.exceptions import ConfigError, TooManyIterationsError
from htmproject.iostreamers.null import NullWriter
from htmproject.visualizers.composite import CompositeVisualizer

from .htm_wrapper_params import params as wrapper_params
from .wrapper import Wrapper


class HTMWrapper(Wrapper):
    def __init__(self, config, class_name='default', htm=None, dry_run=False):
        super(HTMWrapper, self).__init__(config, class_name)

        self._config.add_default_params_section('wrapper', wrapper_params)
        self._epochs = int(self._config.get('wrapper.epochs'))

        self._logger.debug('Learning epochs: %i' % (self._epochs,))

        self._encoder = None
        self._htm = None
        self._iterator = None

        if not dry_run:
            self._setup_encoder()
            self._setup_core(htm)

        self._learning_reader = None
        self._testing_reader = None

        self._learning_writer = NullWriter()
        self._testing_writer = NullWriter()

        self._learning_analyzer = None
        self._testing_analyzer = None

        self._learning_visualizer = None
        self._testing_visualizer = None

        self.__learning_data_set_size = None
        self.__testing_data_set_size = None

        self._learning_iterations_offset = 0
        self._elapsed_learning_iterations = 0
        self._max_learning_iterations = None

    def get_htm(self):
        return self._htm

    def get_iterator(self):
        return self._iterator

    def get_last_learning_stats(self):
        if self._learning_analyzer is None:
            return None
        else:
            stats = {}
            stats.update(self._learning_analyzer.get_last_class_similarity())
            return stats

    def get_last_testing_stats(self):
        if self._testing_analyzer is None:
            return None
        else:
            stats = {}
            stats.update(self._testing_analyzer.get_last_class_similarity())
            stats.update(self._testing_analyzer.get_last_inter_class_similarity())
            return stats

    def __getstate__(self):
        state = {
            'config': self.__dict__['_config'],
            'class_name': self.__dict__['_class_name'],
            'htm': self.__dict__['_htm']
        }
        return state

    def __setstate__(self, ndict):
        self.__init__(ndict['config'], ndict['class_name'], htm=ndict['htm'])

    def _setup_core(self, htm):
        if htm is None:
            self._setup_htm(self._encoder)
        else:
            self._htm = htm

        self._iterator = Iterator(
            config=self._config,
            encoder=self._encoder,
            htm=self._htm
        )

    def _setup_htm(self, encoder):
        self._logger.info('Creating \'%s\' HTM...' % (self._class_name,))
        try:
            self._htm = HTM(self._config, encoder.get_item_size())
            self._logger.info('HTM ready')
        except ValueError:
            self._logger.exception('HTM could not be created.')
            raise

    def _setup_encoder(self):
        raise NotImplementedError()

    def _setup_learning(self):
        self._learning_reader = self._get_learning_reader()
        self.__learning_data_set_size = self._learning_reader.get_data_set_size()

        self._max_learning_iterations = self._epochs * self.__learning_data_set_size
        self._elapsed_learning_iterations = 0

        self._learning_analyzer = SingleClassAnalyzer(
            self._htm,
            self.__learning_data_set_size,
            self._config
        )

        self._learning_visualizer = None
        if self._enable_visualizations:
            self._learning_visualizer = CompositeVisualizer(
                self._learning_analyzer,
                self._config,
                visualizers=self._get_visualizers()
            )

        if self.__learning_data_set_size > self._max_learning_iterations:
            raise ConfigError('Learning set size (%i) is bigger than maximum allowed learning iterations (%i).' % (
                self.__learning_data_set_size,
                self._max_learning_iterations
            ))

        if self._enable_reporter:
            from htmproject.reporters.html import HTMLReporter
            self._reporter = HTMLReporter(self._htm, self._config)

    def _setup_testing(self, testing_mode=True):
        self._testing_reader = self._get_testing_reader() if testing_mode else self._get_learning_reader()
        self.__testing_data_set_size = self._testing_reader.get_data_set_size()

        self._testing_analyzer = MultipleClassesAnalyzer(
            self.get_mapper(),
            self._htm,
            self.__testing_data_set_size,
            self._config
        )

        self._testing_visualizer = None
        if self._enable_visualizations:
            self._testing_visualizer = CompositeVisualizer(
                self._testing_analyzer,
                self._config,
                visualizers=self._get_visualizers()
            )

    def get_mapper(self, classes_list=None):
        raise NotImplementedError()

    def _start_learning(self):
        self._logger.info('Starting \'%s\' HTM training...' % (self._class_name,))

        self._learning_analyzer.start()

        if self._enable_reporter:
            self._reporter.start()

        too_many_iterations = False
        try:
            while True:
                self._learn()
        except TooManyIterationsError:
            self._logger.warning('\'%s\' HTM reached maximum learning iterations amount' % (self._class_name,))
            too_many_iterations = True

        self._learning_iterations_offset = self._elapsed_learning_iterations

        self._learning_analyzer.stop()

        if self._enable_reporter:
            self._reporter.stop()

        if too_many_iterations:
            raise TooManyIterationsError()
        else:
            self._logger.info('Training for \'%s\' HTM finished' % (self._class_name,))
            return self._get_testing_analyzer_results()

    def _start_testing(self):
        self._test()

        return self._get_testing_analyzer_results()

    def _get_learning_reader(self):
        raise NotImplementedError()

    def _get_testing_reader(self):
        raise NotImplementedError()

    def _get_visualizers(self):
        visualizers = ()
        if self._enable_visualizations:
            try:
                from htmproject.visualizers.plot import PlotVisualizer
                visualizers = (PlotVisualizer,)
            except ImportError:
                pass
        return visualizers

    def _learn(self):
        """
        HTM learning mode
        """
        if not self._max_learning_iterations - self._elapsed_learning_iterations:
            raise TooManyIterationsError()

        self._logger.info('\'%s\' HTM is entering learning mode...' % (self._class_name,))

        self._reset_iteration_char()

        self._htm.toggle_learning(True)

        self._iterator.set_reader(self._learning_reader)
        self._iterator.set_writer(self._learning_writer)
        self._iterator.start()

        elapsed_iterations = self._learn_loop()

        if elapsed_iterations and (not self._partial_visualizations_frequency or (
                        elapsed_iterations % self._partial_visualizations_frequency != 0
        )):
            self._log_iterations_done(elapsed_iterations)

            if self._enable_visualizations:
                self._learning_visualizer.visualize_complete(
                    '%s-learn-%i-%i' % (
                        self._class_name,
                        self._learning_iterations_offset,
                        self._elapsed_learning_iterations - 1
                    ),
                    self._learning_iterations_offset
                )

        self._logger.info('Total elapsed learning iterations: %i' % (self._elapsed_learning_iterations,))
        self._iterator.stop()

        self._logger.debug('SP profile info:\n{}'.format(pprint.pformat(self._htm.get_sp().get_profile_info())))
        self._logger.debug('TP profile info:\n{}'.format(pprint.pformat(self._htm.get_tp().get_profile_info())))

        self._logger.info('Learning mode left')

    def _test(self):
        """
        :rtype: bool
        :return: Does HTM still need learning?
        """
        self._logger.info('\'%s\' HTM is entering testing mode...' % (self._class_name,))

        self._reset_iteration_char()

        self._htm.toggle_learning(False)

        self._iterator.set_reader(self._testing_reader)
        self._iterator.set_writer(NullWriter())
        self._iterator.start()

        self._testing_analyzer.start()

        self._test_loop()

        self._testing_analyzer.save_class_stats()

        elapsed_iterations = self._iterator.get_elapsed_iterations()
        self._log_iterations_done(elapsed_iterations)
        self._iterator.stop()

        needs_learning = self._has_htm_learned()

        self._testing_analyzer.stop()

        if elapsed_iterations and self._enable_visualizations:
            self._testing_visualizer.visualize_complete(
                '%s-test' % (self._class_name,)
            )

        self._logger.debug('SP profile info:\n{}'.format(pprint.pformat(self._htm.get_sp().get_profile_info())))
        self._logger.debug('TP profile info:\n{}'.format(pprint.pformat(self._htm.get_tp().get_profile_info())))

        self._logger.info('Testing mode left')

        return needs_learning

    def _has_htm_learned(self):
        return True

    def _learn_loop(self):
        offset = 0
        elapsed_iterations = 0
        partials_counter = 0

        while self._max_learning_iterations - self._elapsed_learning_iterations > 0:
            try:
                self._iterator.iterate()
            except StopIteration:
                self._learning_reader.reset()
                self._switch_iteration_char()
            else:
                encoded_input = self._iterator.get_encoded_input()
                encoded_output = self._iterator.get_encoded_output()

                self._learning_analyzer.save_iteration_stats(encoded_input, encoded_output)

                # effectively ignore histograms
                # TODO: remove histograms from required flow altogether
                self._learning_analyzer.save_chunk_info(self._get_name(True), 1)
                self._learning_analyzer.reset_chunk_outputs()

                if self._enable_reporter:
                    self._reporter.log_iteration(encoded_input, encoded_output)

                self._log_iteration(offset)

                self._elapsed_learning_iterations += 1
                offset += 1

                elapsed_iterations = self._iterator.get_elapsed_iterations()
                if self._enable_visualizations and self._partial_visualizations_frequency and (
                                elapsed_iterations / self._partial_visualizations_frequency != partials_counter
                ):
                    partials_counter = elapsed_iterations / self._partial_visualizations_frequency
                    self._log_iterations_done(elapsed_iterations)
                    self._learning_visualizer.visualize_complete(
                        '%s-learn-%i-%i' % (
                            self._class_name,
                            self._learning_iterations_offset,
                            self._elapsed_learning_iterations - 1
                        ),
                        self._learning_iterations_offset
                    )
        return elapsed_iterations

    def _test_loop(self):
        offset = 0
        while True:
            try:
                self._iterator.iterate()
            except StopIteration:
                break
            else:
                encoded_input = self._iterator.get_encoded_input()
                encoded_output = self._iterator.get_encoded_output()

                self._testing_analyzer.save_iteration_stats(encoded_input, encoded_output)

                # effectively ignore histograms
                # TODO: remove histograms from required flow altogether
                self._testing_analyzer.save_chunk_info(self._get_name(False), 1)
                self._testing_analyzer.reset_chunk_outputs()

                self._log_iteration(offset)

                offset += 1

    def _get_testing_analyzer_results(self):
        results = {
            'histograms': [
                (item['name'], item['histogram'])
                for item in self._testing_analyzer.get_chunk_info()
            ],
            'stats': self.get_last_testing_stats()
        }

        return results

    def _get_name(self, is_learning=True):
        if is_learning:
            return self._learning_reader.get_name()
        else:
            return self._testing_reader.get_name()
