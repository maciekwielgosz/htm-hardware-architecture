from htmproject.interfaces.wrapper import HTMWrapper


class ChunkMixin(HTMWrapper):
    def __init__(self, *args, **kwargs):
        super(ChunkMixin, self).__init__(*args, **kwargs)
        self.__whole_learning_set_seen = False

    def _run_chunk(self, chunk, analyzer, offset=0, report=False):
        i = 0
        try:
            for i in xrange(chunk['size']):
                self._iterator.iterate()

                encoded_input = self._iterator.get_encoded_input()
                encoded_output = self._iterator.get_encoded_output()

                analyzer.save_iteration_stats(encoded_input, encoded_output)

                # effectively ignore histograms
                # TODO: remove histograms from required flow altogether
                analyzer.save_chunk_info(chunk['name'], 1)
                analyzer.reset_chunk_outputs()

                if report:
                    self._reporter.log_iteration(encoded_input, encoded_output)

                self._log_iteration(offset + i)
        except StopIteration:
            self._logger.debug('\nError while reading frame %i (out of %i) from %s chunk.' % (
                i,
                chunk['size'],
                chunk['name']
            ))
            raise StopIteration()

        self._switch_iteration_char()

    def _learn_loop(self):
        offset = 0
        elapsed_iterations = 0
        partials_counter = 0

        chunk = self._learning_reader.next_chunk()

        while self._max_learning_iterations - self._elapsed_learning_iterations >= chunk['size']:
            self._run_chunk(chunk, self._learning_analyzer, offset, self._enable_reporter)

            self._elapsed_learning_iterations += chunk['size']
            offset += chunk['size']

            chunk = self._learning_reader.next_chunk()
            if chunk is None:
                self.__whole_learning_set_seen = True
                self._learning_reader.reset()
                chunk = self._learning_reader.next_chunk()

            if self.__whole_learning_set_seen:
                self._learning_analyzer.save_class_stats()

            elapsed_iterations = self._iterator.get_elapsed_iterations()
            if self._enable_visualizations and self._partial_visualizations_frequency and (
                            elapsed_iterations / self._partial_visualizations_frequency != partials_counter
            ):
                partials_counter = elapsed_iterations / self._partial_visualizations_frequency
                self._log_iterations_done(elapsed_iterations)
                self._learning_visualizer.visualize_complete(
                    '%s-learn-%i-%i' % (
                        self._class_name,
                        self._learning_iterations_offset,
                        self._elapsed_learning_iterations - 1
                    ),
                    self._learning_iterations_offset
                )
        return elapsed_iterations

    def _test_loop(self):
        chunk = self._testing_reader.next_chunk()
        offset = 0
        while chunk is not None:
            self._run_chunk(chunk, self._testing_analyzer, offset)

            offset += chunk['size']
            chunk = self._testing_reader.next_chunk()
