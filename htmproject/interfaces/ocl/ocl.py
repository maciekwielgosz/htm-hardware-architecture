import pyopencl as ocl
import os
import sys
import inspect
import logging
import pprint

from htmproject.interfaces.ocl.exceptions import NoDeviceOCLException

KERNELS_LOCATION = 'ocls/kernels'
PROFILE = True


class OCL(object):
    """
    Template class for openCL applications
    """
    def __init__(self, kernel_filenames, build_defines=None):
        self._logger = logging.getLogger('htm')

        try:
            self.device = self.__get_device()
            self.__log_device_debug_info()
        except IndexError:
            raise NoDeviceOCLException()

        self._profile = PROFILE
        properties = None
        if self._profile:
            properties = ocl.command_queue_properties.PROFILING_ENABLE

        self.ctx = ocl.Context([self.device])
        self.queue = ocl.CommandQueue(self.ctx, properties=properties)

        if isinstance(kernel_filenames, str):
            kernel_filenames = [kernel_filenames]

        if build_defines is None:
            build_defines = {}

        self.build_defines = {k: v() if callable(v) else v for k, v in build_defines.items()}
        self.build_defines['NULL'] = 0

        self._logger.debug('Build defines:\n{}'.format(pprint.pformat(self.build_defines)))

        self.kernel_paths = [
            os.path.join(self._get_kernels_dir(), name)
            for name in kernel_filenames
            ]
        self._load_kernel()

        if self._profile:
            self._exec_time = [0] * len(self.program.all_kernels())
            self._dev_to_host_time = [0]
            self._host_to_dev_time = []
            self._exec_amount = 0

    def __log_device_debug_info(self):
        if self._logger.getEffectiveLevel() == logging.DEBUG:
            params = [
                'MAX_WORK_GROUP_SIZE',
                'MAX_WORK_ITEM_DIMENSIONS',
                'MAX_WORK_ITEM_SIZES',
                'MAX_COMPUTE_UNITS',
                'GLOBAL_MEM_SIZE',
                'LOCAL_MEM_SIZE',
                'MAX_CONSTANT_ARGS',
                'MAX_CONSTANT_BUFFER_SIZE',
                'PREFERRED_VECTOR_WIDTH_INT',
                'PREFERRED_VECTOR_WIDTH_FLOAT'
            ]
            info = {k: self._get_device_info(k) for k in params}
            self._logger.debug('Selected device info:\n{}'.format(pprint.pformat(info)))

    def __get_device(self):
        """
        Finds all the available devices and uses the best among them.
        :return:
        """
        devices = []
        best = [
            ocl.device_type.ACCELERATOR,
            ocl.device_type.GPU,
            ocl.device_type.CPU,
            ocl.device_type.DEFAULT,
            ocl.device_type.ALL
        ]

        if ocl.get_cl_header_version()[1] > 1:
            best.insert(0, ocl.device_type.CUSTOM)

        for platform in ocl.get_platforms():
            for device in platform.get_devices():
                devices.append((device, device.type))

        devices.sort(key=lambda x: str(x))
        devices.sort(key=lambda x: best.index(x[1]))

        choice = devices[int(os.environ.get('PYOPENCL_DEVICE_INDEX', '0'))][0]

        self._logger.debug('%s is using %s @ %s' % (self.__class__.__name__, choice.name, choice.platform.name))

        return choice

    def _get_kernels_dir(self):
        """
        Gets kernels dir location. All the kernels are located in a single dir pointed
        by KERNELS_LOCATION relative to the current file location.
        """
        if getattr(sys, 'frozen', False):
            path = os.path.abspath(sys.executable)
        else:
            path = inspect.getabsfile(self._get_kernels_dir)

        current_path = os.path.dirname(os.path.realpath(path))
        relative_kernel_dir_loc = os.path.abspath(os.path.join(current_path, os.pardir))  # move to the parent dir
        relative_kernel_dir_loc = os.path.abspath(os.path.join(
            relative_kernel_dir_loc, os.pardir))  # move to the parent dir
        kernel_dir = os.path.abspath(os.path.join(
            relative_kernel_dir_loc, KERNELS_LOCATION))  # move to the dir with kernels

        return kernel_dir

    def _load_kernel(self, show_kernel=False):
        kernel_code = ''

        for path in self.kernel_paths:
            with open(path + ".cl") as f:
                kernel_code += f.read()

        if show_kernel:
            print(kernel_code)

        self.program = ocl.Program(self.ctx, kernel_code).build("-I " + self._get_kernels_dir().replace('\\', '\\\\')
                                                                + ' ' + self._get_build_defines_as_str())
        self.__log_kernels_debug_info()

    def __log_kernels_debug_info(self):
        if self._logger.getEffectiveLevel() == logging.DEBUG:
            for kernel in self.program.all_kernels():
                params = [
                    'COMPILE_WORK_GROUP_SIZE',
                    'LOCAL_MEM_SIZE',
                    'PREFERRED_WORK_GROUP_SIZE_MULTIPLE',
                    'PRIVATE_MEM_SIZE',
                    'WORK_GROUP_SIZE',
                ]
                info = {k: self._get_kernel_info(kernel, k) for k in params}
                self._logger.debug('{} kernel info:\n{}'.format(kernel.function_name, pprint.pformat(info)))

    def _get_kernel_info(self, kernel, param):
        return kernel.get_work_group_info(getattr(ocl.kernel_work_group_info, param.upper(), None), self.device)

    def _get_device_info(self, param):
        return getattr(self.device, param.lower(), None)

    def _get_output_buf_and_vec(self, *args, **kwargs):
        raise NotImplementedError()

    def _execute_kernel(self, *args, **kwargs):
        raise NotImplementedError()

    def _post_execute_kernel(self):
        pass

    def execute_kernel(self, *args, **kwargs):
        exec_events = self._execute_kernel(*args, **kwargs)

        out_buffer, out_vec = self._get_output_buf_and_vec()

        read_evt = ocl.enqueue_read_buffer(self.queue, out_buffer, out_vec, wait_for=exec_events)
        read_evt.wait()

        self._post_execute_kernel()

        if self._profile:
            self._exec_amount += 1
            self._dev_to_host_time[0] += read_evt.profile.end - read_evt.profile.start
            for i, exec_evt in enumerate(exec_events):
                self._exec_time[i] += exec_evt.profile.end - exec_evt.profile.start

        return out_vec

    def get_profile_info(self):
        return {
            "exec_amount": self._exec_amount,
            "exec_time": sum(self._exec_time),
            "dev_to_host_time": sum(self._dev_to_host_time),
            "host_to_dev_time": sum(self._host_to_dev_time),
            "avg_exec_times": [p / self._exec_amount for p in self._exec_time],
            "avg_dev_to_host_times": [p / self._exec_amount for p in self._dev_to_host_time],
            "avg_host_to_dev_times": [p / self._exec_amount for p in self._host_to_dev_time]
        } if self._profile else None

    def _get_build_defines_as_str(self):
        return ' '.join(["-D {!s}={!s}".format(k, v) for k, v in self.build_defines.items()])
