import os

import numpy

from htmproject.interfaces.cli import CLI
from htmproject.interfaces.cli.exceptions import InputError


class ModularCLI(CLI):
    def __init__(self, *args, **kwargs):
        self.__subdirs = {}

        super(ModularCLI, self).__init__(*args, **kwargs)

        self._logger.info('CLI SETUP: Multiple HTMs')

    def _get_module_names(self):
        return self._config.get('cli.classes')

    def _combine_results(self, testing_results):
        self._logger.info('Combining results...')

        keys = testing_results.values()[0]['histograms'].keys()
        keys.sort()

        combined_results = []
        for chunk_name, item in testing_results:
            combined_results.append((chunk_name, {
                'class_name': item['class_name'],
                'histogram': self.__combine_histograms(item['histograms'], keys)
            }))

        self._logger.info('Results combined')

        return combined_results

    @staticmethod
    def __combine_histograms(histograms, keys):
        combined_histogram = numpy.array([], dtype=numpy.float64)
        for key in keys:
            combined_histogram = numpy.hstack((combined_histogram, histograms[key]))
        return combined_histogram
