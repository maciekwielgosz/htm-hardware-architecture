# from profilehooks import profile
from sklearn.svm import LinearSVC

from htmproject.interfaces.classifier import Classifier


class LinearSVMClassifier(Classifier):
    """
    Classifies output histograms to one of the predefined categories.
    """

    def __init__(self, config):
        super(LinearSVMClassifier, self).__init__(config)

        self._model = LinearSVC()

    # @profile
    def train(self, train_data, target):
        """

        :param numpy.ndarray train_data: Data to be used for training.
        :param numpy.ndarray target: Categories (e.g. [0, 0, 0, 1, 1, 2, 1, 0]); number of different values
            determines amount of classes
        """
        self._model.fit(train_data, target)

    # @profile
    def classify(self, data):
        """
        Classifies result histograms to one of the categories.
        :param numpy.ndarray data: histograms to be classified
        :rtype: numpy.ndarray
        :return: Numeric categories the items in data belong to.
        """
        return self._model.predict(data)

