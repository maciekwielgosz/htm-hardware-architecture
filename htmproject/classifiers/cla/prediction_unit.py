import numpy as np


class PredictionUnit:

    def __init__(self, class_number):
        self.hist = np.zeros(class_number)
        # __init__

    def activate(self, aClass):
        self.hist[aClass] += 1

    def density(self):
        return self.hist / np.max(self.hist)
