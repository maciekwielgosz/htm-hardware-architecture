import numpy as np
from htmproject.interfaces.classifier import Classifier

from .prediction_unit import PredictionUnit
from .classifier_params import params as classifier_params


class CLAClassifier(Classifier):
    """
    CLAClassifier

    @version
        0.1a
    @author
        Kymon
    @brief
        CLAClassifier based on Numenta CLAClassifier
    """

    def __init__(self, config):
        super(CLAClassifier, self).__init__(config)

        self._config.add_default_params_section('classifier', classifier_params)

        self.SDRs = []
        self.prediction = {}

        p_step = self._config.get('classifier.p_step')
        input_size = np.sum(np.multiply(
            np.array(self._config.get('core.htm.columns_per_layer'), dtype=int),
            np.array(self._config.get('core.htm.cells_per_column'), dtype=int))
        )
        self.__class_number = len(self._config.get('cli.classes'))

        # Generate containter for data storage
        if isinstance(p_step, list):
            iFoo = p_step
            self.maxPredStep = max(p_step)
        else:
            iFoo = range(1, p_step + 1)
            self.maxPredStep = p_step

        for i in iFoo:
            cells = []
            for j in range(input_size):
                cells.append(PredictionUnit(self.__class_number))
            self.prediction[i] = tuple(cells)


            # __init__

    def pushInfo(self, ClassID, SDR):
        self.SDRs.insert(0, SDR)
        self.currentClass = ClassID
        self.updateCells()

        # optim with flag (no checking for len)
        if len(self.SDRs) > self.maxPredStep + 4:
            self.SDRs.pop()
        pass  # pushSDR

    def updateCells(self):
        SDRLen = len(self.SDRs) - 1

        for key, value in self.prediction.items():
            if key <= SDRLen:
                indexes = np.nonzero(self.SDRs[key])[0]
                for i in indexes:
                    self.prediction[key][i].activate(self.currentClass)
                    # updateCells

    def getPrediction(self, SDR):
        prediction = {}
        indexes = np.nonzero(SDR)[0]

        for key, value in self.prediction.items():
            p = np.zeros(self.__class_number)
            for c in indexes:
                p += value[c].density()
            prediction[key] = np.argmax(p)

        return prediction  # getPrediction

    def train(self, train_data, target):
        for class_id, sdr in zip(target, train_data):
            self.pushInfo(class_id, sdr)

    def classify(self, data):
        results = []

        for sdr in data:
            results.append(self.getPrediction(sdr))

        return results

    def get_classification_stats(self, data, target, labels):
        """
        :rtype: dict
        :return: Classification statistics
        """
        results = self.classify(data)
        return zip(target, results)
