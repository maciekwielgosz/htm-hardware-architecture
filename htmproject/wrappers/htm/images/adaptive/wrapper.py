from htmproject.encoders.adaptive_frame import AdaptiveFrameEncoder
from htmproject.interfaces.wrapper import FileHTMWrapper
from htmproject.iostreamers.images import ImageFileReader


class AdaptiveImagesHTMWrapper(FileHTMWrapper):
    def _setup_encoder(self):
        self._encoder = AdaptiveFrameEncoder(self._config)

    def _get_reader_class(self):
        return ImageFileReader
