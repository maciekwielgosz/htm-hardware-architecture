from htmproject.interfaces.decoder import Decoder


class PassThroughDecoder(Decoder):
    """

    """
    def decode(self, item):
        """
        Maps from SDR to the result space.
        :param numpy.array item:
        :return: item mapped from SDR
        :rtype: numpy.array
        """
        return 0 + item  # change output to int from bool

