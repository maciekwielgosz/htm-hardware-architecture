

class LazyOCLList(object):
    def __init__(self, cls):
        self.cls = cls
        self.args = []
        self.ocls = {}

    def append(self, *args, **kwargs):
        self.args.append((args, kwargs))

    def __getitem__(self, key):
        if key not in self.ocls:
            args, kwargs = self.args[key]
            self.ocls[key] = self.cls(*args, **kwargs)
        return self.ocls[key]

    def __setitem__(self, key, value):
        self.ocls[key] = value
