import multiprocessing
import multiprocessing.pool
import math


class KernelThreadPool(multiprocessing.pool.ThreadPool):
    def _istarmap_unordered(self, func, iterable, chunksize=1):
        # return self.starmap(func, iterable, chunksize)
        task_batches = multiprocessing.pool.Pool._get_tasks(func, iterable, chunksize)
        result = multiprocessing.pool.IMapIterator(self._cache)
        self._taskqueue.put(
            (
                self._guarded_task_generation(result._job,
                                              multiprocessing.pool.starmapstar,
                                              task_batches),
                result._set_length
            ))
        return (item for chunk in result for item in chunk)

    def istarmap_unordered(self, func, iterable, chunksize=1):
        return self.imap_unordered(lambda x: func(*x), iterable, chunksize)

    def get_chunk_size(self, worksize):
        return math.ceil(float(worksize)/self._processes)




