from htmproject.ocls.numba.utils import KernelThreadPool

import numpy as np
import numba as nb


@nb.jit(nopython=True, nogil=True)
def inhibition_kernel_orig(data, boost_coeff, inhibition_range, number_of_columns, number_of_synapses, min_overlap, winners_set_size):
    r = np.zeros(number_of_columns, np.int32)
    for x in range(number_of_columns):
        # s = sum(1 for y in range(number_of_synapses) if data[x * number_of_synapses + y])
        s = 0
        for y in range(number_of_synapses):
            if data[x * number_of_synapses + y]:
                s += 1
        r[x] = s * boost_coeff[x] if s > min_overlap else 0
    threshold = 2 * inhibition_range + 1 - winners_set_size
    if threshold <= 0:
        return r
    rr = r.copy()
    for x in range(number_of_columns):
        s = r[x]
        if s > 0:
            # ss = sum(1 for i in range(x-inhibition_range, x+inhibition_range+1) if r[i % number_of_columns] < s)
            ss = 0
            for i in range(x - inhibition_range, x + inhibition_range + 1):
                if r[i % number_of_columns] < s:
                    ss += 1
            if ss <= threshold:
                rr[x] = 0
    return rr


@nb.jit(nopython=True, nogil=True)
def kernel_1(r, data, boost_coeff, number_of_synapses, min_overlap, x):
    s = 0
    for y in range(number_of_synapses):
        if data[x * number_of_synapses + y]:
            s += 1
    r[x] = s * boost_coeff[x] if s > min_overlap else 0


@nb.jit(nopython=True, nogil=True)
def kernel_2(r, rr, number_of_columns, inhibition_range, threshold, x):
    s = r[x]
    if s > 0:
        # ss = sum(1 for i in range(x-inhibition_range, x+inhibition_range+1) if r[i % number_of_columns] < s)
        ss = 0
        for i in range(x - inhibition_range, x + inhibition_range + 1):
            if r[i % number_of_columns] < s:
                ss += 1
        if ss <= threshold:
            rr[x] = 0


# @nb.jit(nopython=True, nogil=True)
def inhibition_kernel(pool, data, boost_coeff, inhibition_range, number_of_columns, number_of_synapses, min_overlap, winners_set_size):
    r = np.zeros(number_of_columns, np.int32)

    d = ((r, data, boost_coeff, number_of_synapses, min_overlap, x) for x in range(number_of_columns))
    pr = pool.istarmap_unordered(kernel_1, d, chunksize=pool.get_chunk_size(number_of_columns))
    # pr.get()
    for _ in pr:
        pass

    threshold = 2 * inhibition_range + 1 - winners_set_size
    if threshold <= 0:
        return r
    rr = r.copy()

    d = ((r, rr, number_of_columns, inhibition_range, threshold, x) for x in range(number_of_columns))
    pr = pool.istarmap_unordered(kernel_2, d, chunksize=pool.get_chunk_size(number_of_columns))
    for _ in pr:
        pass
    # pr.get()
    return rr


class SpatialPoolerOCLNumba(object):
    """
        Numba implementation of spatial pooler overlap
    """

    __slots__ = (
        'number_of_columns', 'number_of_synapses', 'min_overlap', 'winners_set_size', 'pool',
    )

    def __init__(self, number_of_columns, number_of_synapses, min_overlap, winners_set_size, threads=8):
        self.number_of_columns = number_of_columns
        self.number_of_synapses = number_of_synapses
        self.min_overlap = min_overlap
        self.winners_set_size = winners_set_size
        self.pool = KernelThreadPool(threads)

    def execute_kernel(self, data, boost_coeff, inhibition_radius):
        return inhibition_kernel(
            self.pool,
            data,
            boost_coeff,
            inhibition_radius,
            self.number_of_columns,
            self.number_of_synapses,
            self.min_overlap,
            self.winners_set_size
        )


