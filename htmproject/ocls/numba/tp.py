from htmproject.ocls.numba.functions import Data, initializeLayer, phaseZero, phaseOne, phaseTwo, phaseThree
from htmproject.ocls.numba.utils import KernelThreadPool

import numpy as np
import numba as nb


# def tp_step0_kernel(cellState, isLearningCellInColumn, columns_per_layer):
#     cellState <<= 4
#     isLearningCellInColumn[1:columns_per_layer+1] = isLearningCellInColumn[0:columns_per_layer]
#     isLearningCellInColumn[0] = 0
#     isLearningCellInColumn <<= 4
#     isLearningCellInColumn &= 0xF0


# @nb.jit(nopython=True, nogil=True, cache=Trues)
def tp_steps_kernel(pool, data, learning_enabled, work_group_size, columns_per_layer, cells_per_column, segments_per_cell,
                    synapses_per_segment, segment_activation_threshold, segment_min_activation_threshold,
                    synapse_connected_permanence, max_new_synapses, synapse_permanence_increment,
                    synapse_permanence_decrement, synapse_initial_permanence):

    big_num = columns_per_layer * cells_per_column * segments_per_cell * synapses_per_segment
    # init numpy arrays
    cellState = np.zeros(columns_per_layer * cells_per_column, np.uint8)
    isLearningCellInColumn = np.zeros(columns_per_layer + 2, np.uint8)
    hasCellQueuedChangesData = np.zeros(columns_per_layer * cells_per_column, np.uint8)
    newSegmentsQueuedCountData = np.zeros(columns_per_layer * cells_per_column, np.uint32)
    segmentsCountData = np.zeros(columns_per_layer * cells_per_column, np.uint32)
    isSegmentSequentialData = np.zeros(columns_per_layer * cells_per_column * segments_per_cell, np.uint8)
    hasSegmentQueuedChangesData = np.zeros(columns_per_layer * cells_per_column * segments_per_cell, np.uint8)
    sequenceSegmentQueuedData = np.zeros(columns_per_layer * cells_per_column * segments_per_cell, np.uint8)
    synapsesCountData = np.zeros(columns_per_layer * cells_per_column * segments_per_cell, np.uint32)
    newSynapsesQueuedCountData = np.zeros(columns_per_layer * cells_per_column * segments_per_cell, np.uint32)
    synapsePermanenceData = np.zeros(big_num, np.uint32)
    synapsePermanenceQueuedData = np.zeros(big_num, np.int32)
    synapseTargetColumnData = np.zeros(big_num, np.uint32)
    synapseTargetCellData = np.zeros(big_num, np.uint32)
    isLearningCellInColumnData = np.zeros(columns_per_layer + 2, np.uint8)
    # randomValueData = np.random.randint(0, 10 ** 6, columns_per_layer, np.uint32)
    randomValueData = np.zeros(columns_per_layer, np.uint32)
    outputData = np.zeros(columns_per_layer, np.uint8)

    data_c = Data(
        data,  # columnState=
        cellState,  # m_cellState=
        hasCellQueuedChangesData,  # m_hasCellQueuedChanges=
        newSegmentsQueuedCountData,  # m_newSegmentsQueuedCount=
        segmentsCountData,  # m_segmentsCount=
        isLearningCellInColumn,  # m_isLearningCellInColumn=
        isLearningCellInColumnData,  # m_usedLearningCells=
        isSegmentSequentialData,  # m_isSegmentSequential=
        hasSegmentQueuedChangesData,  # m_hasSegmentQueuedChanges=
        sequenceSegmentQueuedData,  # m_sequenceSegmentQueued=
        synapsesCountData,  # m_synapsesCount=
        newSynapsesQueuedCountData,  # m_newSynapsesQueuedCount=
        synapsePermanenceData,  # m_synapsePermanence=
        synapsePermanenceQueuedData,  # m_synapsePermanenceQueued=
        synapseTargetColumnData,  # m_synapseTargetColumn=
        synapseTargetCellData,  # m_synapseTargetCell=
        randomValueData,  # m_randomNumberState=
        columns_per_layer,  # MAX_COLUMNS_PER_LAYER=
        cells_per_column,  # MAX_CELLS_PER_COLUMN=
        segments_per_cell,  # MAX_SEGMENTS_PER_CELL=
        synapses_per_segment,  # MAX_SYNAPSES_PER_SEGMENT=
        segment_activation_threshold,  # SEGMENT_ACTIVATION_THRESHOLD=
        segment_min_activation_threshold,  # SEGMENT_MIN_ACTIVATION_THRESHOLD=
        synapse_connected_permanence,  # SYNAPSE_CONNECTED_PERMANENCE=
        max_new_synapses,  # MAX_NEW_SYNAPSES=
        synapse_permanence_increment,  # SYNAPSE_PERMANENCE_INCREMENT=
        synapse_permanence_decrement,  # SYNAPSE_PERMANENCE_DECREMENT=
        synapse_initial_permanence,  # SYNAPSE_INITIAL_PERMANENCE=
        learning_enabled,  # isLearningActive=
        outputData,  # outputData=
    )

    # initializeLayer(data_c)
    # phaseZero(data_c)
    # phaseOne(data_c)
    # phaseTwo(data_c)
    # phaseThree(data_c)

    d = ((data_c, x) for x in range(columns_per_layer))
    pr = pool.istarmap_unordered(initializeLayer, d, chunksize=pool.get_chunk_size(columns_per_layer))
    for _ in pr:
        pass

    d = ((data_c, x) for x in range(columns_per_layer))
    pr = pool.istarmap_unordered(phaseZero, d, chunksize=pool.get_chunk_size(columns_per_layer))
    for _ in pr:
        pass

    d = ((data_c, x) for x in range(columns_per_layer))
    pr = pool.istarmap_unordered(phaseOne, d, chunksize=pool.get_chunk_size(columns_per_layer))
    for _ in pr:
        pass

    d = ((data_c, x) for x in range(columns_per_layer))
    pr = pool.istarmap_unordered(phaseTwo, d, chunksize=pool.get_chunk_size(columns_per_layer))
    for _ in pr:
        pass

    d = ((data_c, x) for x in range(columns_per_layer))
    pr = pool.istarmap_unordered(phaseThree, d, chunksize=pool.get_chunk_size(columns_per_layer))
    for _ in pr:
        pass

    return outputData


class TemporalPoolerOCLNumba(object):
    """
        Numba implementation of spatial pooler overlap
    """

    __slots__ = (
        'work_group_size', 'cells_per_column', 'columns_per_layer', 'segments_per_cell', 'synapses_per_segment',
        'segment_activation_threshold', 'segment_min_activation_threshold', 'synapse_connected_permanence',
        'max_new_synapses', 'synapse_permanence_increment', 'synapse_permanence_decrement',
        'synapse_initial_permanence', 'pool'
    )

    def __init__(self, work_group_size, columns_per_layer, cells_per_column, segments_per_cell, synapses_per_segment,
                 segment_activation_threshold, segment_min_activation_threshold, synapse_connected_permanence,
                 max_new_synapses, synapse_permanence_increment, synapse_permanence_decrement,
                 synapse_initial_permanence, threads=8):
        self.work_group_size = work_group_size
        self.cells_per_column = cells_per_column
        self.columns_per_layer = columns_per_layer
        self.segments_per_cell = segments_per_cell
        self.synapses_per_segment = synapses_per_segment
        self.segment_activation_threshold = segment_activation_threshold
        self.segment_min_activation_threshold = segment_min_activation_threshold
        self.synapse_connected_permanence = synapse_connected_permanence
        self.max_new_synapses = max_new_synapses
        self.synapse_permanence_increment = synapse_permanence_increment
        self.synapse_permanence_decrement = synapse_permanence_decrement
        self.synapse_initial_permanence = synapse_initial_permanence
        self.pool = KernelThreadPool(threads)

    def execute_kernel(self, data, learning_enabled):
        return tp_steps_kernel(
            self.pool,  # pool=
            data,  # data=
            learning_enabled,  # learning_enabled=
            self.work_group_size,  # work_group_size=
            self.columns_per_layer,  # columns_per_layer=
            self.cells_per_column,  # cells_per_column=
            self.segments_per_cell,  # segments_per_cell=
            self.synapses_per_segment,  # synapses_per_segment=
            self.segment_activation_threshold,  # segment_activation_threshold=
            self.segment_min_activation_threshold,  # segment_min_activation_threshold=
            self.synapse_connected_permanence,  # synapse_connected_permanence=
            self.max_new_synapses,  # max_new_synapses=
            self.synapse_permanence_increment,  # synapse_permanence_increment=
            self.synapse_permanence_decrement,  # synapse_permanence_decrement=
            self.synapse_initial_permanence,  # synapse_initial_permanence=
        )


