import random
import numpy as np
import numba as nb

# Enums


ACTIVE_CELL = 0x01
PREDICTIVE_CELL = 0x02
LEARN_CELL = 0x04
NOW = 0x00
WAS = 0x01


# Structs

@nb.jitclass([
    ('columnState', nb.uint8[:]),
    ('m_cellState', nb.uint8[:]),
    ('m_hasCellQueuedChanges', nb.uint8[:]),
    ('m_newSegmentsQueuedCount', nb.uint32[:]),
    ('m_segmentsCount', nb.uint32[:]),
    ('m_isLearningCellInColumn', nb.uint8[:]),
    ('m_usedLearningCells', nb.uint8[:]),
    ('m_isSegmentSequential', nb.uint8[:]),
    ('m_hasSegmentQueuedChanges', nb.uint8[:]),
    ('m_sequenceSegmentQueued', nb.uint8[:]),
    ('m_synapsesCount', nb.uint32[:]),
    ('m_newSynapsesQueuedCount', nb.uint32[:]),
    ('m_synapsePermanence', nb.uint32[:]),
    ('m_synapsePermanenceQueued', nb.uint32[:]),
    ('m_synapseTargetColumn', nb.uint32[:]),
    ('m_synapseTargetCell', nb.uint32[:]),
    ('m_randomNumberState', nb.uint32[:]),
    ('MAX_COLUMNS_PER_LAYER', nb.uint32),
    ('MAX_CELLS_PER_COLUMN', nb.uint32),
    ('MAX_SEGMENTS_PER_CELL', nb.uint32),
    ('MAX_SYNAPSES_PER_SEGMENT', nb.uint32),
    ('SEGMENT_ACTIVATION_THRESHOLD', nb.uint32),
    ('SEGMENT_MIN_ACTIVATION_THRESHOLD', nb.uint32),
    ('SYNAPSE_CONNECTED_PERMANENCE', nb.uint32),
    ('MAX_NEW_SYNAPSES', nb.uint32),
    ('SYNAPSE_PERMANENCE_INCREMENT', nb.uint32),
    ('SYNAPSE_PERMANENCE_DECREMENT', nb.uint32),
    ('SYNAPSE_INITIAL_PERMANENCE', nb.uint32),
    ('isLearningActive', nb.uint8),
    ('outputData', nb.uint8[:]),
])
class Data(object):
    def __init__(self, columnState, m_cellState, m_hasCellQueuedChanges, m_newSegmentsQueuedCount, m_segmentsCount,
                 m_isLearningCellInColumn, m_usedLearningCells, m_isSegmentSequential, m_hasSegmentQueuedChanges,
                 m_sequenceSegmentQueued, m_synapsesCount, m_newSynapsesQueuedCount, m_synapsePermanence,
                 m_synapsePermanenceQueued, m_synapseTargetColumn, m_synapseTargetCell, m_randomNumberState,
                 MAX_COLUMNS_PER_LAYER, MAX_CELLS_PER_COLUMN, MAX_SEGMENTS_PER_CELL, MAX_SYNAPSES_PER_SEGMENT,
                 SEGMENT_ACTIVATION_THRESHOLD, SEGMENT_MIN_ACTIVATION_THRESHOLD, SYNAPSE_CONNECTED_PERMANENCE,
                 MAX_NEW_SYNAPSES, SYNAPSE_PERMANENCE_INCREMENT, SYNAPSE_PERMANENCE_DECREMENT,
                 SYNAPSE_INITIAL_PERMANENCE, isLearningActive, outputData):
        self.columnState = columnState

        self.m_cellState = m_cellState
        self.m_hasCellQueuedChanges = m_hasCellQueuedChanges
        self.m_newSegmentsQueuedCount = m_newSegmentsQueuedCount
        self.m_segmentsCount = m_segmentsCount
        self.m_isLearningCellInColumn = m_isLearningCellInColumn
        self.m_usedLearningCells = m_usedLearningCells

        self.m_isSegmentSequential = m_isSegmentSequential
        self.m_hasSegmentQueuedChanges = m_hasSegmentQueuedChanges
        self.m_sequenceSegmentQueued = m_sequenceSegmentQueued
        self.m_synapsesCount = m_synapsesCount
        self.m_newSynapsesQueuedCount = m_newSynapsesQueuedCount

        self.m_synapsePermanence = m_synapsePermanence
        self.m_synapsePermanenceQueued = m_synapsePermanenceQueued
        self.m_synapseTargetColumn = m_synapseTargetColumn
        self.m_synapseTargetCell = m_synapseTargetCell

        self.m_randomNumberState = m_randomNumberState

        self.MAX_COLUMNS_PER_LAYER = MAX_COLUMNS_PER_LAYER
        self.MAX_CELLS_PER_COLUMN = MAX_CELLS_PER_COLUMN
        self.MAX_SEGMENTS_PER_CELL = MAX_SEGMENTS_PER_CELL
        self.MAX_SYNAPSES_PER_SEGMENT = MAX_SYNAPSES_PER_SEGMENT
        self.SEGMENT_ACTIVATION_THRESHOLD = SEGMENT_ACTIVATION_THRESHOLD
        self.SEGMENT_MIN_ACTIVATION_THRESHOLD = SEGMENT_MIN_ACTIVATION_THRESHOLD
        self.SYNAPSE_CONNECTED_PERMANENCE = SYNAPSE_CONNECTED_PERMANENCE
        self.MAX_NEW_SYNAPSES = MAX_NEW_SYNAPSES
        self.SYNAPSE_PERMANENCE_INCREMENT = SYNAPSE_PERMANENCE_INCREMENT
        self.SYNAPSE_PERMANENCE_DECREMENT = SYNAPSE_PERMANENCE_DECREMENT
        self.SYNAPSE_INITIAL_PERMANENCE = SYNAPSE_INITIAL_PERMANENCE

        self.isLearningActive = isLearningActive

        self.outputData = outputData


@nb.jitclass([
    ('m_cellIdx', nb.uint32),
    ('m_segmentIdx', nb.uint32),
    ('m_anyCellFound', nb.boolean),
    ('m_anySegmentFound', nb.boolean),
])
class CellSearch(object):
    def __init__(self, m_cellIdx, m_segmentIdx, m_anyCellFound, m_anySegmentFound):
        self.m_cellIdx = m_cellIdx
        self.m_segmentIdx = m_segmentIdx
        self.m_anyCellFound = m_anyCellFound
        self.m_anySegmentFound = m_anySegmentFound


@nb.jitclass([
    ('m_segmentIdx', nb.uint32),
    ('m_activeSynapses', nb.uint32),
    ('m_anySegmentFound', nb.boolean),
])
class SegmentSearch(object):
    def __init__(self, m_segmentIdx, m_activeSynapses, m_anySegmentFound):
        self.m_segmentIdx = m_segmentIdx
        self.m_activeSynapses = m_activeSynapses
        self.m_anySegmentFound = m_anySegmentFound


@nb.jitclass([
    ('m_segmentIdx', nb.uint32),
    ('m_segmentCreated', nb.boolean),
])
class NewSegment(object):
    def __init__(self, m_segmentIdx, m_segmentCreated):
        self.m_segmentIdx = m_segmentIdx
        self.m_segmentCreated = m_segmentCreated


@nb.jitclass([
    ('m_segmentIdx', nb.uint32),
    ('m_valid', nb.boolean),
])
class ActiveSegment(object):
    def __init__(self, m_segmentIdx, m_valid):
        self.m_segmentIdx = m_segmentIdx
        self.m_valid = m_valid


# RNG


@nb.jit(nopython=True, nogil=True)
def MWC64X_NextUint(state):
    return random.getrandbits(32)


@nb.jit(nopython=True, nogil=True)
def MWC64X_SeedStreams(state, baseOffset, perStreamOffset):
    random.seed(baseOffset + perStreamOffset * 4096)


# Functions


@nb.jit(nopython=True, nogil=True)
def getCellState(data, columnIdx, cellIdx, when):
    return (data.m_cellState[columnIdx * data.MAX_CELLS_PER_COLUMN + cellIdx] >> (when * 4)) & 0x0F


@nb.jit(nopython=True, nogil=True)
def setCellState(data, columnIdx, cellIdx, state):
    data.m_cellState[columnIdx * data.MAX_CELLS_PER_COLUMN + cellIdx] |= state


@nb.jit(nopython=True, nogil=True)
def isSegmentSequential(data, m_columnIdx, m_cellIdx, m_segment):
    return data.m_isSegmentSequential[
        m_columnIdx * data.MAX_CELLS_PER_COLUMN * data.MAX_SEGMENTS_PER_CELL + m_cellIdx * data.MAX_SEGMENTS_PER_CELL + m_segment]


@nb.jit(nopython=True, nogil=True)
def segmentActive(data, columnIdx, cellIdx, segmentIdx, when, cellState):
    return segmentActivity(data, columnIdx, cellIdx, segmentIdx, when, cellState) > data.SEGMENT_ACTIVATION_THRESHOLD


@nb.jit(nopython=True, nogil=True)
def segmentActivity(data, columnIdx, cellIdx, segmentIdx, when, cellState):
    activity = 0
    synapseOffsetStart = columnIdx * data.MAX_CELLS_PER_COLUMN * data.MAX_SEGMENTS_PER_CELL * data.MAX_SYNAPSES_PER_SEGMENT + cellIdx * data.MAX_SEGMENTS_PER_CELL * data.MAX_SYNAPSES_PER_SEGMENT + segmentIdx * data.MAX_SYNAPSES_PER_SEGMENT
    for synapse in range(data.m_synapsesCount[
                             columnIdx * data.MAX_CELLS_PER_COLUMN * data.MAX_SEGMENTS_PER_CELL + cellIdx * data.MAX_SEGMENTS_PER_CELL + segmentIdx]):
        if getCellState(data, data.m_synapseTargetColumn[synapseOffsetStart + synapse],
                        data.m_synapseTargetCell[synapseOffsetStart + synapse], when) & cellState:
            if data.m_synapsePermanence[synapseOffsetStart + synapse] > data.SYNAPSE_CONNECTED_PERMANENCE:
                activity += 1
    return activity


@nb.jit(nopython=True, nogil=True)
def getBestMatchingCell(data, columnIdx, when):
    bestMatchingCell = CellSearch(0, 0, False, False)
    nbrOfActiveSynapses = 0
    for cell in range(data.MAX_CELLS_PER_COLUMN):
        bestMatchingSegment = getBestMatchingSegment(data, columnIdx, cell, when)
        if bestMatchingSegment.m_anySegmentFound and bestMatchingSegment.m_activeSynapses > nbrOfActiveSynapses:
            nbrOfActiveSynapses = bestMatchingSegment.m_activeSynapses
            bestMatchingCell.m_cellIdx = cell
            bestMatchingCell.m_segmentIdx = bestMatchingSegment.m_segmentIdx
            bestMatchingCell.m_anySegmentFound = True
            bestMatchingCell.m_anyCellFound = True

    if not bestMatchingCell.m_anyCellFound:
        nrOfSegments = 4294967295  # max value for unsigned int
        for cell in range(data.MAX_CELLS_PER_COLUMN):
            if data.m_segmentsCount[columnIdx * data.MAX_CELLS_PER_COLUMN + cell] <= nrOfSegments:
                nrOfSegments = data.m_segmentsCount[columnIdx * data.MAX_CELLS_PER_COLUMN + cell]
                bestMatchingCell.m_cellIdx = cell
                bestMatchingCell.m_anyCellFound = True

    return bestMatchingCell


@nb.jit(nopython=True, nogil=True)
def getBestMatchingSegment(data, columnIdx, cellIdx, when):
    bestMatchingSegment = SegmentSearch(0, 0, False)
    activity = 0
    synapseOffsetStart = columnIdx * data.MAX_CELLS_PER_COLUMN * data.MAX_SEGMENTS_PER_CELL * data.MAX_SYNAPSES_PER_SEGMENT + cellIdx * data.MAX_SEGMENTS_PER_CELL * data.MAX_SYNAPSES_PER_SEGMENT
    for segment in range(data.m_segmentsCount[columnIdx * data.MAX_CELLS_PER_COLUMN + cellIdx]):
        for synapse in range(data.m_synapsesCount[
                                 columnIdx * data.MAX_CELLS_PER_COLUMN * data.MAX_SEGMENTS_PER_CELL + cellIdx * data.MAX_SEGMENTS_PER_CELL + segment]):
            if getCellState(data, data.m_synapseTargetColumn[
                synapseOffsetStart + segment * data.MAX_SYNAPSES_PER_SEGMENT + synapse], data.m_synapseTargetCell[
                                synapseOffsetStart + segment * data.MAX_SYNAPSES_PER_SEGMENT + synapse],
                            when) & ACTIVE_CELL:
                activity += 1
        if activity >= data.SEGMENT_MIN_ACTIVATION_THRESHOLD:
            if activity > bestMatchingSegment.m_activeSynapses:
                bestMatchingSegment.m_activeSynapses = activity
                bestMatchingSegment.m_segmentIdx = segment
                bestMatchingSegment.m_anySegmentFound = True
        activity = 0

    return bestMatchingSegment


@nb.jit(nopython=True, nogil=True)
def getLayerLearningCells(data, columnIdx):
    isLearningCell = False
    for cell in range(data.MAX_CELLS_PER_COLUMN):
        if not isLearningCell:
            if getCellState(data, columnIdx, cell, NOW) & LEARN_CELL:
                data.m_isLearningCellInColumn[columnIdx] = data.m_isLearningCellInColumn[columnIdx] | 0x01
                isLearningCell = True
                data.m_isLearningCellInColumn[data.MAX_COLUMNS_PER_LAYER] = 1
        if isLearningCell:
            break


@nb.jit(nopython=True, nogil=True)
def queueNewSegment(data, columnIdx, cellIdx, isSegmentSequential):
    newSegment = NewSegment(0, False)
    newSegmentOffset = data.m_newSegmentsQueuedCount[columnIdx * data.MAX_CELLS_PER_COLUMN + cellIdx]
    # check if there is space for new synapse in segment
    if data.m_segmentsCount[
        columnIdx * data.MAX_CELLS_PER_COLUMN + cellIdx] + newSegmentOffset <= data.MAX_SEGMENTS_PER_CELL - 1:
        data.m_newSegmentsQueuedCount[columnIdx * data.MAX_CELLS_PER_COLUMN + cellIdx] += 1
        data.m_hasCellQueuedChanges[columnIdx * data.MAX_CELLS_PER_COLUMN + cellIdx] = True
        newSegment.m_segmentIdx = data.m_segmentsCount[
                                      columnIdx * data.MAX_CELLS_PER_COLUMN + cellIdx] + newSegmentOffset
        newSegment.m_segmentCreated = True
    return newSegment


@nb.jit(nopython=True, nogil=True)
def getActiveSegment(data, columnIdx, cellIdx, when, cellState):
    activeSequenceSegments = False
    for segment in range(data.m_segmentsCount[columnIdx * data.MAX_CELLS_PER_COLUMN + cellIdx]):
        if isSegmentSequential(data, columnIdx, cellIdx, segment):
            if segmentActive(data, columnIdx, cellIdx, segment, when, cellState):
                activeSequenceSegments = True
                break

    bestActivitySegment = ActiveSegment(0, False)
    bestActivity = 0
    for segment in range(data.m_segmentsCount[columnIdx * data.MAX_CELLS_PER_COLUMN + cellIdx]):
        if activeSequenceSegments and not isSegmentSequential(data, columnIdx, cellIdx, segment):
            continue

        activity = segmentActivity(data, columnIdx, cellIdx, segment, when, cellState)
        if activity > data.SEGMENT_ACTIVATION_THRESHOLD and activity >= bestActivity:
            bestActivity = activity
            bestActivitySegment.m_segmentIdx = segment
            bestActivitySegment.m_valid = True

    return bestActivitySegment


@nb.jit(nopython=True, nogil=True)
def applyCellsQueuedChanges(data, columnIdx, cellIdx):
    if data.m_hasCellQueuedChanges[columnIdx * data.MAX_CELLS_PER_COLUMN + cellIdx]:
        data.m_segmentsCount[columnIdx * data.MAX_CELLS_PER_COLUMN + cellIdx] += data.m_newSegmentsQueuedCount[
            columnIdx * data.MAX_CELLS_PER_COLUMN + cellIdx]
        data.m_newSegmentsQueuedCount[columnIdx * data.MAX_CELLS_PER_COLUMN + cellIdx] = 0
        data.m_hasCellQueuedChanges[columnIdx * data.MAX_CELLS_PER_COLUMN + cellIdx] = 0


@nb.jit(nopython=True, nogil=True)
def applySegmentsQueuedChanges(data, columnIdx, cellIdx, positiveReinforcement):
    segmentStart = columnIdx * data.MAX_CELLS_PER_COLUMN * data.MAX_SEGMENTS_PER_CELL + cellIdx * data.MAX_SEGMENTS_PER_CELL
    for segment in range(data.m_segmentsCount[columnIdx * data.MAX_CELLS_PER_COLUMN] + cellIdx):
        # no changes in this segment, exit
        if not data.m_hasSegmentQueuedChanges[segmentStart + segment]:
            break

        data.m_isSegmentSequential[segmentStart + segment] = data.m_sequenceSegmentQueued[segmentStart + segment]
        synapseStart = segmentStart * data.MAX_SYNAPSES_PER_SEGMENT + segment * data.MAX_SYNAPSES_PER_SEGMENT
        if positiveReinforcement:
            for synapse in range(data.m_synapsesCount[segmentStart + segment]):
                synapsePermanence = data.m_synapsePermanence[synapseStart + synapse]
                synapsePermanenceQueued = data.m_synapsePermanenceQueued[synapseStart + synapse]
                if synapsePermanenceQueued >= 0 and synapsePermanenceQueued <= 100000:
                    data.m_synapsePermanence[synapseStart + synapse] = data.m_synapsePermanenceQueued[
                        synapseStart + synapse]
                elif synapsePermanenceQueued < 0:
                    data.m_synapsePermanence[synapseStart + synapse] = 0
                elif synapsePermanenceQueued > 100000:
                    data.m_synapsePermanence[synapseStart + synapse] = 100000
                data.m_synapsePermanenceQueued[synapseStart + synapse] = 0
        else:
            for synapse in range(data.m_synapsesCount[segmentStart + segment]):
                synapsePermanence = data.m_synapsePermanence[synapseStart + synapse]
                synapsePermanenceQueued = data.m_synapsePermanenceQueued[synapseStart + synapse]
                if synapsePermanenceQueued > synapsePermanence:
                    if int(2 * synapsePermanence) - synapsePermanenceQueued >= 0:
                        data.m_synapsePermanence[synapseStart + synapse] -= data.m_synapsePermanenceQueued[
                                                                                synapseStart + synapse] - \
                                                                            data.m_synapsePermanence[
                                                                                synapseStart + synapse]
                    else:
                        data.m_synapsePermanence[synapseStart + synapse] = 0
                data.m_synapsePermanenceQueued[synapseStart + synapse] = 0

        newSynapseOffset = data.m_synapsesCount[segmentStart + segment]
        data.m_synapsesCount[segmentStart + segment] += data.m_newSynapsesQueuedCount[segmentStart + segment]
        if data.m_synapsesCount[segmentStart + segment] > data.MAX_SYNAPSES_PER_SEGMENT:
            data.m_synapsesCount[segmentStart + segment] = data.MAX_SYNAPSES_PER_SEGMENT
        for newSynapse in range(data.m_newSynapsesQueuedCount[segmentStart + segment]):
            data.m_synapsePermanence[synapseStart + newSynapseOffset + newSynapse] = data.SYNAPSE_INITIAL_PERMANENCE

        data.m_newSynapsesQueuedCount[segmentStart + segment] = 0
        data.m_hasSegmentQueuedChanges[segmentStart + segment] = 0


@nb.jit(nopython=True, nogil=True)
def getSegmentActiveSynapses(data, columnIdx, cellIdx, segmentIdx, when, makeSegmentSequential, allowNewSynapses):
    activeSynapsesCount = 0
    for localId in range(data.MAX_CELLS_PER_COLUMN):  # localId = get_local_id(0)
        segment = columnIdx * data.MAX_CELLS_PER_COLUMN * data.MAX_SEGMENTS_PER_CELL + cellIdx * data.MAX_SEGMENTS_PER_CELL + segmentIdx
        if not data.m_hasSegmentQueuedChanges[segment]:
            data.m_hasSegmentQueuedChanges[segment] = 1
            for synapse in range(data.MAX_SYNAPSES_PER_SEGMENT):
                data.m_synapsePermanenceQueued[segment * data.MAX_SYNAPSES_PER_SEGMENT + synapse] = \
                data.m_synapsePermanence[segment * data.MAX_SYNAPSES_PER_SEGMENT + synapse]

        data.m_sequenceSegmentQueued[segment] = makeSegmentSequential

        for synapse in range(data.m_synapsesCount[segment]):
            if getCellState(data, data.m_synapseTargetColumn[segment * data.MAX_SYNAPSES_PER_SEGMENT + synapse],
                            data.m_synapseTargetCell[segment * data.MAX_SYNAPSES_PER_SEGMENT + synapse],
                            when) & ACTIVE_CELL:
                data.m_synapsePermanenceQueued[
                    segment * data.MAX_SYNAPSES_PER_SEGMENT + synapse] += data.SYNAPSE_PERMANENCE_INCREMENT
                activeSynapsesCount += 1
            else:
                data.m_synapsePermanenceQueued[
                    segment * data.MAX_SYNAPSES_PER_SEGMENT + synapse] -= data.SYNAPSE_PERMANENCE_DECREMENT

        if allowNewSynapses:
            amountToCreate = data.MAX_NEW_SYNAPSES - activeSynapsesCount
            if amountToCreate <= 0:
                return
            if amountToCreate + data.m_newSynapsesQueuedCount[segment] + data.m_synapsesCount[
                segment] > data.MAX_SYNAPSES_PER_SEGMENT:
                amountToCreate = data.MAX_SYNAPSES_PER_SEGMENT - data.m_newSynapsesQueuedCount[segment] - \
                                 data.m_synapsesCount[segment]

            amountCreated = 0
            # if we have space for new synapses and there was any learning cell in layer
            if amountToCreate > 0 and data.m_isLearningCellInColumn[data.MAX_COLUMNS_PER_LAYER + when]:

                targetColumn = 0
                targetCell = 0
                found = False
                # randColumnId = 0
                # randCellId = 0
                # offset = 0
                usedCount = 0
                offsetUsedLearningCells = localId * data.MAX_NEW_SYNAPSES
                globalSegmentBlockStartIdx = columnIdx * data.MAX_CELLS_PER_COLUMN * data.MAX_SEGMENTS_PER_CELL * data.MAX_SYNAPSES_PER_SEGMENT + cellIdx * data.MAX_SEGMENTS_PER_CELL * data.MAX_SYNAPSES_PER_SEGMENT + segmentIdx * data.MAX_SYNAPSES_PER_SEGMENT
                newSynapseOffset = data.m_newSynapsesQueuedCount[segment] + data.m_synapsesCount[segment]
                for newSynapse in range(amountToCreate):
                    if newSynapse > 0 and not found:
                        break
                    found = False
                    offset = MWC64X_NextUint(data.m_randomNumberState[columnIdx]) % data.MAX_COLUMNS_PER_LAYER
                    for column in range(data.MAX_COLUMNS_PER_LAYER):
                        randColumnId = (offset + column) % data.MAX_COLUMNS_PER_LAYER
                        if (randColumnId == columnIdx):
                            continue

                        if (data.m_isLearningCellInColumn[randColumnId] >> (when * 4)) & 0x0F:
                            randCellId = MWC64X_NextUint(
                                data.m_randomNumberState[columnIdx]) % data.MAX_CELLS_PER_COLUMN
                            for cell in range(data.MAX_CELLS_PER_COLUMN):
                                if getCellState(data, randColumnId, randCellId, when) & LEARN_CELL:
                                    used = False
                                    for i in range(usedCount):
                                        if data.m_usedLearningCells[offsetUsedLearningCells + i] == randCellId and \
                                                data.m_usedLearningCells[
                                                    offsetUsedLearningCells + i + 1] == randColumnId:
                                            used = True
                                    if not used:
                                        targetColumn = randColumnId
                                        targetCell = randCellId
                                        data.m_usedLearningCells[offsetUsedLearningCells + usedCount] = targetCell
                                        data.m_usedLearningCells[offsetUsedLearningCells + usedCount + 1] = targetColumn
                                        usedCount += 1
                                        found = True
                                        break
                        if found:
                            data.m_synapseTargetColumn[
                                globalSegmentBlockStartIdx + newSynapseOffset + newSynapse] = targetColumn
                            data.m_synapseTargetCell[
                                globalSegmentBlockStartIdx + newSynapseOffset + newSynapse] = targetCell
                            amountCreated += 1
                            break

            if amountCreated:
                data.m_newSynapsesQueuedCount[segment] += amountCreated


# Kernels


@nb.jit(nopython=True, nogil=True)
def initializeLayer(data, columnIdx):
        # for columnIdx in range(data.MAX_COLUMNS_PER_LAYER):
        # initialize random number generator
        MWC64X_SeedStreams(data.m_randomNumberState[columnIdx], data.m_randomNumberState[columnIdx], 4294967296)

        # clear all global buffer data
        cellStart = columnIdx * data.MAX_CELLS_PER_COLUMN
        for cell in range(data.MAX_CELLS_PER_COLUMN):
            data.m_cellState[cellStart + cell] = 0
            data.m_hasCellQueuedChanges[cellStart + cell] = 0
            data.m_newSegmentsQueuedCount[cellStart + cell] = 0
            data.m_newSegmentsQueuedCount[cellStart + cell] = 0
            segmentStart = cellStart * data.MAX_SEGMENTS_PER_CELL + cell * data.MAX_SEGMENTS_PER_CELL
            for segment in range(data.MAX_SEGMENTS_PER_CELL):
                data.m_isSegmentSequential[segmentStart + segment] = 0
                data.m_hasSegmentQueuedChanges[segmentStart + segment] = 0
                data.m_sequenceSegmentQueued[segmentStart + segment] = 0
                data.m_synapsesCount[segmentStart + segment] = 0
                data.m_newSynapsesQueuedCount[segmentStart + segment] = 0
                synapseStart = segmentStart * data.MAX_SYNAPSES_PER_SEGMENT + segment * data.MAX_SYNAPSES_PER_SEGMENT
                for synapse in range(data.MAX_SYNAPSES_PER_SEGMENT):
                    data.m_synapsePermanence[synapseStart + synapse] = 0
                    data.m_synapsePermanenceQueued[synapseStart + synapse] = 0
                    data.m_synapseTargetColumn[synapseStart + synapse] = 0
                    data.m_synapseTargetCell[synapseStart + synapse] = 0

        if columnIdx == 0:
            data.m_isLearningCellInColumn[data.MAX_COLUMNS_PER_LAYER] = 0
            data.m_isLearningCellInColumn[data.MAX_COLUMNS_PER_LAYER + 1] = 0

        data.m_isLearningCellInColumn[columnIdx] = 0


# time update, shift states, present states becomes past states
@nb.jit(nopython=True, nogil=True)
def phaseZero(data, columnIdx):
        # for columnIdx in range(data.MAX_COLUMNS_PER_LAYER):
        cellStart = columnIdx * data.MAX_CELLS_PER_COLUMN
        for cell in range(data.MAX_CELLS_PER_COLUMN):
            data.m_cellState[cellStart + cell] = data.m_cellState[cellStart + cell] << 4
        if columnIdx == 0:
            data.m_isLearningCellInColumn[data.MAX_COLUMNS_PER_LAYER + 1] = data.m_isLearningCellInColumn[
                data.MAX_COLUMNS_PER_LAYER]
            data.m_isLearningCellInColumn[data.MAX_COLUMNS_PER_LAYER] = 0
        data.m_isLearningCellInColumn[columnIdx] = (data.m_isLearningCellInColumn[columnIdx] << 4) & 0xF0


@nb.jit(nopython=True, nogil=True)
def phaseOne(data, columnIdx):
        # for columnIdx in range(data.MAX_COLUMNS_PER_LAYER):
        buPredicted = False
        lcChosen = False
        if data.columnState[columnIdx]:
            segmentIdx = 0
            segment = ActiveSegment(0, False)
            for cell in range(data.MAX_CELLS_PER_COLUMN):
                if getCellState(data, columnIdx, cell, WAS) & PREDICTIVE_CELL:
                    segment = getActiveSegment(data, columnIdx, cell, WAS, ACTIVE_CELL)
                    if segment.m_valid:
                        if isSegmentSequential(data, columnIdx, cell, segment.m_segmentIdx):
                            buPredicted = True
                            setCellState(data, columnIdx, cell, ACTIVE_CELL)
                            if data.isLearningActive and segmentActive(data, columnIdx, cell, segment.m_segmentIdx, WAS,
                                                                       LEARN_CELL):
                                lcChosen = True
                                setCellState(data, columnIdx, cell, LEARN_CELL)

            if not buPredicted:
                for cell in range(data.MAX_CELLS_PER_COLUMN):
                    setCellState(data, columnIdx, cell, ACTIVE_CELL)

            if data.isLearningActive and not lcChosen:
                bestMatchingCell = getBestMatchingCell(data, columnIdx, WAS)
                setCellState(data, columnIdx, bestMatchingCell.m_cellIdx, LEARN_CELL)
                if bestMatchingCell.m_anySegmentFound:
                    getSegmentActiveSynapses(data, columnIdx, bestMatchingCell.m_cellIdx, bestMatchingCell.m_segmentIdx,
                                             WAS, True, True)
                else:
                    newSegment = queueNewSegment(data, columnIdx, bestMatchingCell.m_cellIdx, True)
                    if newSegment.m_segmentCreated:
                        getSegmentActiveSynapses(data, columnIdx, bestMatchingCell.m_cellIdx, newSegment.m_segmentIdx,
                                                 WAS, True, True)
                        segmentStart = columnIdx * data.MAX_CELLS_PER_COLUMN * data.MAX_SEGMENTS_PER_CELL + bestMatchingCell.m_cellIdx * data.MAX_SEGMENTS_PER_CELL
                        # remove segment, if no synapses are connected
                        if data.m_newSynapsesQueuedCount[segmentStart + newSegment.m_segmentIdx] == 0:
                            data.m_newSegmentsQueuedCount[
                                columnIdx * data.MAX_CELLS_PER_COLUMN + bestMatchingCell.m_cellIdx] -= 1


@nb.jit(nopython=True, nogil=True)
def phaseTwo(data, columnIdx):
        # for columnIdx in range(data.MAX_COLUMNS_PER_LAYER):
        # collect data about present learning cells in layer
        getLayerLearningCells(data, columnIdx)
        for cell in range(data.MAX_CELLS_PER_COLUMN):
            for segment in range(data.MAX_SEGMENTS_PER_CELL):
                if segmentActive(data, columnIdx, cell, segment, NOW, ACTIVE_CELL):
                    setCellState(data, columnIdx, cell, PREDICTIVE_CELL)
                    if data.isLearningActive:
                        getSegmentActiveSynapses(data, columnIdx, cell, segment, NOW, False, False)
                        bestMatchingSegment = getBestMatchingSegment(data, columnIdx, cell, WAS)
                        if bestMatchingSegment.m_anySegmentFound:
                            getSegmentActiveSynapses(data, columnIdx, cell, bestMatchingSegment.m_segmentIdx, WAS,
                                                     False, True)
                        else:
                            newSegment = queueNewSegment(data, columnIdx, cell, False)
                            if newSegment.m_segmentCreated:
                                getSegmentActiveSynapses(data, columnIdx, cell, newSegment.m_segmentIdx, WAS, False,
                                                         True)
                                segmentStart = columnIdx * data.MAX_CELLS_PER_COLUMN * data.MAX_SEGMENTS_PER_CELL + cell * data.MAX_SEGMENTS_PER_CELL
                                # remove segment, if no synapses are connected
                                if data.m_newSynapsesQueuedCount[segmentStart + newSegment.m_segmentIdx] == 0:
                                    data.m_newSegmentsQueuedCount[columnIdx * data.MAX_CELLS_PER_COLUMN + cell] -= 1


@nb.jit(nopython=True, nogil=True)
def phaseThree(data, columnIdx):
        # for columnIdx in range(data.MAX_COLUMNS_PER_LAYER):
        outputStateColumn = 0

        if data.isLearningActive:
            for cell in range(data.MAX_CELLS_PER_COLUMN):
                if getCellState(data, columnIdx, cell, NOW) & LEARN_CELL:
                    applyCellsQueuedChanges(data, columnIdx, cell)
                    applySegmentsQueuedChanges(data, columnIdx, cell, True)
                elif (getCellState(data, columnIdx, cell, NOW) & PREDICTIVE_CELL) == 0 and getCellState(data, columnIdx,
                                                                                                        cell,
                                                                                                        WAS) & PREDICTIVE_CELL:
                    applyCellsQueuedChanges(data, columnIdx, cell)
                    applySegmentsQueuedChanges(data, columnIdx, cell, False)
                # removes updates if cell predicts too long
                elif getCellState(data, columnIdx, cell, WAS) & PREDICTIVE_CELL and getCellState(data, columnIdx, cell,
                                                                                                 NOW) & PREDICTIVE_CELL:
                    data.m_newSegmentsQueuedCount[columnIdx * data.MAX_CELLS_PER_COLUMN + cell] = 0
                    data.m_hasCellQueuedChanges[columnIdx * data.MAX_CELLS_PER_COLUMN + cell] = False
                    segmentStart = columnIdx * data.MAX_CELLS_PER_COLUMN * data.MAX_SEGMENTS_PER_CELL + cell * data.MAX_SEGMENTS_PER_CELL
                    for segment in range(data.MAX_SEGMENTS_PER_CELL):
                        data.m_hasSegmentQueuedChanges[segmentStart + segment] = 0
                        data.m_newSynapsesQueuedCount[segmentStart + segment] = 0

        # calculate TP output
        for cell in range(data.MAX_CELLS_PER_COLUMN):
            if getCellState(data, columnIdx, cell, NOW) & (ACTIVE_CELL | PREDICTIVE_CELL):
                outputStateColumn = 1
                break
        data.outputData[columnIdx] = outputStateColumn



