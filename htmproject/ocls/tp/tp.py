import numpy as np
import pyopencl as cl
from htmproject.interfaces.ocl import OCL
import os
from time import time

os.environ['PYOPENCL_COMPILER_OUTPUT'] = '1'


class TemporalPoolerOCL(OCL):
    # TODO: Add ability to handle multiple layers of HTM!

    def __init__(self):

        super(TemporalPoolerOCL, self).__init__([
            'temporal_pooler', 'additional_temporal_pooler_functions'
        ])

        self.__settings = None

        self.__columns_per_layer = None

        self.__settingsBuffer = None
        self.__cellStateBuffer = None
        self.__hasCellQueuedChangesBuffer = None
        self.__newSegmentQueuedCountBuffer = None
        self.__segmentsCountBuffer = None
        self.__isSegmentSequentialBuffer = None
        self.__hasSegmentQueuedChangesBuffer = None
        self.__sequenceSegmentQueuedBuffer = None
        self.__synapsesCountBuffer = None
        self.__newSynapsesCountBuffer = None
        self.__synapsePermanenceBuffer = None
        self.__synapsePermanenceQueuedBuffer = None
        self.__synapseTargetColumnBuffer = None
        self.__synapseTargetCellBuffer = None

        self.__outputData = None
        self.__outputBuffer = None
        self.__valuesFromSpBuffer = None

        self.__work_group_size = 0

    def __toggle_learning(self, state=True):
        self.__settings[4] = state
        self.__settingsBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_ONLY | cl.mem_flags.COPY_HOST_PTR,
                                          hostbuf=self.__settings)

    def prepare_kernel(self, columns, settings, work_group_size):
        self.__work_group_size = work_group_size
        self.__prepare_kernel(columns, settings)

    def __prepare_kernel(self, columns, settings):
        self.__settings = settings

        self.__columns_per_layer = int(columns)
        self.__settings = np.array(settings).astype(np.uint32)

        self.__outputData = np.zeros(self.__columns_per_layer).astype(np.uint8)
        self.__outputBuffer = cl.Buffer(self.ctx, cl.mem_flags.WRITE_ONLY, self.__outputData.nbytes)

        self.__settingsBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_ONLY | cl.mem_flags.COPY_HOST_PTR,
                                          hostbuf=self.__settings)
        self.__cellStateBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_ONLY,
                                           np.dtype(np.uint8).itemsize * self.__columns_per_layer * self.__settings[1])
        self.__hasCellQueuedChangesBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_ONLY,
                                                      np.dtype(np.uint8).itemsize * self.__columns_per_layer *
                                                      self.__settings[1])
        self.__newSegmentQueuedCountBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_ONLY,
                                                       np.dtype(np.uint32).itemsize * self.__columns_per_layer *
                                                       self.__settings[1])
        self.__segmentsCountBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_ONLY,
                                               np.dtype(np.uint32).itemsize * self.__columns_per_layer *
                                               self.__settings[1])
        self.__isSegmentSequentialBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_ONLY,
                                                     np.dtype(np.uint8).itemsize * self.__columns_per_layer *
                                                     self.__settings[1] * self.__settings[2])
        self.__hasSegmentQueuedChangesBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_ONLY,
                                                         np.dtype(np.uint8).itemsize * self.__columns_per_layer *
                                                         self.__settings[1] * self.__settings[2])
        self.__sequenceSegmentQueuedBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_ONLY,
                                                       np.dtype(np.uint8).itemsize * self.__columns_per_layer *
                                                       self.__settings[1] * self.__settings[2])
        self.__synapsesCountBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_ONLY,
                                               np.dtype(np.uint32).itemsize * self.__columns_per_layer *
                                               self.__settings[1] * self.__settings[2])
        self.__newSynapsesCountBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_ONLY,
                                                  np.dtype(np.uint32).itemsize * self.__columns_per_layer *
                                                  self.__settings[1] * self.__settings[2])
        self.__synapsePermanenceBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_ONLY,
                                                   np.dtype(np.uint32).itemsize * self.__columns_per_layer *
                                                   self.__settings[1] * self.__settings[2] * self.__settings[3])
        self.__synapsePermanenceQueuedBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_ONLY,
                                                         np.dtype(np.uint8).itemsize * self.__columns_per_layer *
                                                         self.__settings[1] * self.__settings[2] * self.__settings[3])
        self.__synapseTargetColumnBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_ONLY,
                                                     np.dtype(np.uint32).itemsize * self.__columns_per_layer *
                                                     self.__settings[1] * self.__settings[2] * self.__settings[3])
        self.__synapseTargetCellBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_ONLY,
                                                   np.dtype(np.uint32).itemsize * self.__columns_per_layer *
                                                   self.__settings[1] * self.__settings[2] * self.__settings[3])
        self.isLearningCellInColumnData = np.zeros(self.__columns_per_layer + 2).astype(np.uint32)
        self._isLearningCellInColumnBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_WRITE,
                                                       np.dtype(np.uint32).itemsize * (self.__columns_per_layer + 2))

        self._usedLearningCellsBuffer = cl.LocalMemory(
            2 * self.__work_group_size * settings[8] * np.dtype(np.uint32).itemsize)

        self._mwc64x_state_Buffer = cl.Buffer(self.ctx, cl.mem_flags.READ_ONLY,
                                              2 * self.__columns_per_layer * np.dtype(np.uint32).itemsize)

        self.randomValueData = np.random.randint(0, 10 ** 6, self.__columns_per_layer, np.uint32)
        self._randomValueBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_ONLY | cl.mem_flags.COPY_HOST_PTR,
                                            hostbuf=self.randomValueData)

        exec_evt = self.program.initializeLayer(
            self.queue, (self.__columns_per_layer,), (self.__work_group_size,),
            self.__settingsBuffer, self.__cellStateBuffer,
            self.__hasCellQueuedChangesBuffer, self.__newSegmentQueuedCountBuffer,
            self.__segmentsCountBuffer,
            self.__isSegmentSequentialBuffer, self.__hasSegmentQueuedChangesBuffer,
            self.__sequenceSegmentQueuedBuffer, self.__synapsesCountBuffer,
            self.__newSynapsesCountBuffer,
            self.__synapsePermanenceBuffer, self.__synapsePermanenceQueuedBuffer,
            self.__synapseTargetColumnBuffer, self.__synapseTargetCellBuffer,
            self._isLearningCellInColumnBuffer, self._mwc64x_state_Buffer, self._randomValueBuffer)
        exec_evt.wait()

        if self._profile:
            elapsed = 1e-9 * (exec_evt.profile.end - exec_evt.profile.start)

            print("Execution time of test: %g s" % elapsed)

    def execute_kernel(self, output_values_from_sp):
        # TODO: add is_learning_enabled as a param
        # TODO: add layer_index as a param
        start = time()

        output_values_from_sp = np.array(output_values_from_sp).astype(np.uint8)
        # print "Values from SP"
        # print output_values_from_sp
        self.__valuesFromSpBuffer = cl.Buffer(self.ctx, cl.mem_flags.READ_ONLY | cl.mem_flags.COPY_HOST_PTR,
                                              hostbuf=output_values_from_sp)

        exec_evt = self.program.executeKernels(
            self.queue, (self.__columns_per_layer,), (self.__work_group_size,),
            self.__settingsBuffer, self.__cellStateBuffer,
            self.__hasCellQueuedChangesBuffer, self.__newSegmentQueuedCountBuffer,
            self.__segmentsCountBuffer,
            self.__isSegmentSequentialBuffer, self.__hasSegmentQueuedChangesBuffer,
            self.__sequenceSegmentQueuedBuffer, self.__synapsesCountBuffer,
            self.__newSynapsesCountBuffer,
            self.__synapsePermanenceBuffer, self.__synapsePermanenceQueuedBuffer,
            self.__synapseTargetColumnBuffer, self.__synapseTargetCellBuffer,
            self._isLearningCellInColumnBuffer, self._usedLearningCellsBuffer,
            self.__valuesFromSpBuffer,
            self.__outputBuffer, self._mwc64x_state_Buffer)
        exec_evt.wait()

        # print "PhaseZero END"
        # sys.stdout.flush()

        end = time()

        # print "PhaseFour END"
        # sys.stdout.flush()

        cl.enqueue_read_buffer(self.queue, self.__outputBuffer, self.__outputData).wait()

        if self._profile:
            elapsed = 1e-9 * (exec_evt.profile.end - exec_evt.profile.start)
            # print("Execution time of kernels 1 to 5 (Python time module): ", end - start, "s")
            # print("Execution time of kernels 1 to 5 (OpenCL events): %g s" % elapsed)

        self._save_data_to_file(self.__outputData, "log")
        return self.__outputData

    def _save_data_to_file(self, data, file_name, data_format="%d ", file_format=".txt"):
        logs_dir = os.path.join(self._get_kernels_dir(), "logs")

        if not os.path.exists(logs_dir):
            os.mkdir(logs_dir)

        f = open(os.path.join(logs_dir, file_name + file_format), "w")
        for data_element in data:
            f.write(data_format % data_element)
        f.write("END")
        f.close()
