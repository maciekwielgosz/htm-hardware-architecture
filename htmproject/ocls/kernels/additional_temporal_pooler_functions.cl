#include "additional_temporal_pooler_functions.h"

/** \fn uchar getCellState(global Data *data, uint columnIdx, uint cellIdx, WhenState_enum when)
 *  \brief Function to get cell state
 */
uchar getCellState(Data *data, uint columnIdx, uint cellIdx, WhenState_enum when) {
  return ((*(data->m_cellState + columnIdx*data->m_cellsPerColumn + cellIdx)) >> (when * 4)) & 0x0F;
}

/** \fn void setCellState(global Data *data, uint columnIdx, uint cellIdx, m_cellState_enum state)
 *  \brief Function to set cell state
 */
void setCellState(Data *data, uint columnIdx, uint cellIdx, CellState_enum state) {
  *(data->m_cellState + columnIdx*data->m_cellsPerColumn + cellIdx) = *(data->m_cellState + columnIdx*data->m_cellsPerColumn + cellIdx) | state;
}

/** \fn Data makeDataStructure(global uint *m_settings, global uchar *m_cellState, 
		       global uchar *m_isSegmentSequential, global uchar *m_hasSegmentQueuedChanges, 
		       global uchar *m_sequenceSegmentQueued, global uint *m_synapsesCount, global uint *m_newSynapsesCount,
		       global uint *m_synapsePermanence, global uchar *m_synapsePermanenceQueued, 
		       global uint *m_synapseTargetColumn, global uint *m_synapseTargetCell)
 *  \brief Function to make structure data from OpenCL buffers.
 */
Data makeDataStructure(constant uint *m_settings, global uchar *m_cellState,
                       global uchar *m_hasCellQueuedChanges, global uint *m_newSegmentQueuedCount, global uint *m_segmentsCount,
		       global uchar *m_isSegmentSequential, global uchar *m_hasSegmentQueuedChanges, 
		       global uchar *m_sequenceSegmentQueued, global uint *m_synapsesCount, global uint *m_newSynapsesCount, 
		       global uint *m_synapsePermanence, global uchar *m_synapsePermanenceQueued, 
		       global uint *m_synapseTargetColumn, global uint *m_synapseTargetCell, volatile global uint *m_isLearningCellInColumn,
		       local uint *m_usedLearningCells, global mwc64x_state_t *m_randomNumberState) {
  Data data;
  data.m_columnsCount = m_settings[0];
  data.m_cellsPerColumn = m_settings[1];
  data.m_segmentsPerCell = m_settings[2];
  data.m_synapsesPerSegment = m_settings[3];
  data.m_isLearningActive = m_settings[4];
  data.m_segmentActivationThreshold = m_settings[5];
  data.m_minSegmentActivationThreshold = m_settings[6];
  data.m_synapseConnectedPermanence = m_settings[7];
  data.m_maxNewSynapses = m_settings[8];
  data.m_permanenceIncrement = m_settings[9];
  data.m_permanenceDecrement = m_settings[10];
  data.m_initialPermanence = m_settings[11];
  data.m_cellState = m_cellState;
  data.m_hasCellQueuedChanges = m_hasCellQueuedChanges;
  data.m_newSegmentQueuedCount = m_newSegmentQueuedCount;
  data.m_segmentsCount = m_segmentsCount;
  data.m_isSegmentSequential = m_isSegmentSequential;
  data.m_hasSegmentQueuedChanges = m_hasSegmentQueuedChanges;
  data.m_sequenceSegmentQueued = m_sequenceSegmentQueued;
  data.m_synapsesCount = m_synapsesCount;
  data.m_newSynapsesCount = m_newSynapsesCount;
  data.m_synapsePermanence = m_synapsePermanence;
  data.m_synapsePermanenceQueued = m_synapsePermanenceQueued;
  data.m_synapseTargetColumn = m_synapseTargetColumn;
  data.m_synapseTargetCell = m_synapseTargetCell;
  data.m_isLearningCellInColumn = m_isLearningCellInColumn;
  data.m_usedLearningCells = m_usedLearningCells;
  data.m_randomNumberState = m_randomNumberState;
  
  return data;
}

/** \fn bool isSegmentSequential(Data data, uint m_columnIdx, uint m_cellIdx, uint m_segment)
 *  \brief Check whether segment is sequential.
 */
bool isSegmentSequential(Data *data, uint m_columnIdx, uint m_cellIdx, uint m_segment) {
  return data->m_isSegmentSequential[m_columnIdx * data->m_cellsPerColumn * data->m_segmentsPerCell + m_cellIdx * data->m_segmentsPerCell + m_segment];
}

/** \fn int getActiveSegment(Data data, uint columnIdx, uint cellIdx, WhenState_enum when, CellState_enum cellState)
 *  \brief For the given column c cell i, return a segment index such that 
 *  segmentActive(s,t, state) is true. If multiple segments are active, sequence segments are given preference. 
 *  Otherwise, segments with most activity are given preference.
 *  \return -1 if not found
 */
ActiveSegment getActiveSegment(Data *data, uint columnIdx, uint cellIdx, WhenState_enum when, CellState_enum cellState) {
  bool activeSequenceSegments = false;
  for (uint segment = 0; segment < data->m_segmentsCount[columnIdx * data->m_cellsPerColumn + cellIdx]; ++segment)
  {
    if (isSegmentSequential(data, columnIdx, cellIdx, segment)) {
      if(segmentActive(data, columnIdx, cellIdx, segment, when, cellState)) {
        activeSequenceSegments = true;
        break;
      }
    }
  }

  ActiveSegment bestActivitySegment = {0, false};
  int bestActivityIdx = -1;
  int bestActivity = -1;

  for (uint segment = 0; segment < data->m_segmentsCount[columnIdx * data->m_cellsPerColumn + cellIdx]; ++segment)
  {
    if (activeSequenceSegments && !isSegmentSequential(data, columnIdx, cellIdx, segment))
      continue;

    int act = segmentActivity(data, columnIdx, cellIdx, segment, when, cellState);
    if (act > data->m_segmentActivationThreshold && act > bestActivity)
    {
      bestActivity = act;
      bestActivitySegment.m_segmentIdx = segment;
      bestActivitySegment.m_valid = true;
    }
  }
  
  return bestActivitySegment;
}

bool segmentActive(Data *data, uint columnIdx, uint cellIdx, uint segmentIdx, WhenState_enum when, CellState_enum cellState) {
  return segmentActivity(data, columnIdx, cellIdx, segmentIdx, when, cellState) > data->m_segmentActivationThreshold;
}

uint segmentActivity(Data *data, uint columnIdx, uint cellIdx, uint segmentIdx, WhenState_enum when, CellState_enum cellState) {
  uint activity = 0;
  global uint *synapseTargetColumnBlockIdx = data->m_synapseTargetColumn + columnIdx * data->m_cellsPerColumn * data->m_segmentsPerCell * data->m_synapsesPerSegment + 
  cellIdx * data->m_segmentsPerCell * data->m_synapsesPerSegment + segmentIdx * data->m_synapsesPerSegment;
  global uint *synapseTargetCellBlockIdx = data->m_synapseTargetCell + columnIdx * data->m_cellsPerColumn * data->m_segmentsPerCell * data->m_synapsesPerSegment + \
                                           cellIdx * data->m_segmentsPerCell * data->m_synapsesPerSegment + segmentIdx * data->m_synapsesPerSegment;   
  global uint *synapsePermanenceCellBlockIdx = data->m_synapsePermanence + columnIdx * data->m_cellsPerColumn * data->m_segmentsPerCell * data->m_synapsesPerSegment + \
                                               cellIdx * data->m_segmentsPerCell * data->m_synapsesPerSegment + segmentIdx * data->m_synapsesPerSegment;    
                                               
  for(uint synapse = 0; synapse < data->m_synapsesCount[columnIdx * data->m_cellsPerColumn * data->m_segmentsPerCell + cellIdx * data->m_segmentsPerCell + segmentIdx]; ++synapse) {
    if(getCellState(data, *(synapseTargetColumnBlockIdx + synapse), *(synapseTargetCellBlockIdx + synapse), when) & cellState) {
      if(*(synapsePermanenceCellBlockIdx + synapse) > data->m_synapseConnectedPermanence) {
        ++activity;
      }
    }
  }

return activity;
}

CellSearch getBestMatchingCell(Data *data, uint columnIdx, WhenState_enum when) {
  CellSearch bestMatchingCell;
  uint nbrOfActiveSynapses = 0;
  bestMatchingCell.m_anyCellFound = false;
  bestMatchingCell.m_anySegmentFound = false;
  bestMatchingCell.m_cellIdx = 0; 
  bestMatchingCell.m_segmentIdx = 0;
  
  for(uint cell = 0; cell < data->m_cellsPerColumn; ++cell) {
    SegmentSearch bestMatchingSegment = getBestMatchingSegment(data, columnIdx, cell, when);

    if(bestMatchingSegment.m_anySegmentFound == true && bestMatchingSegment.m_activeSynapses > nbrOfActiveSynapses) {
      nbrOfActiveSynapses = bestMatchingSegment.m_activeSynapses;
      bestMatchingCell.m_cellIdx = cell;
      bestMatchingCell.m_segmentIdx = bestMatchingSegment.m_segmentIdx;
      bestMatchingCell.m_anySegmentFound = true;
      bestMatchingCell.m_anyCellFound = true;
    }
  }
  
  if(bestMatchingCell.m_anyCellFound == false) {
    uint nrOfSegments = 4294967295; // max value for unsigned int
    for(uint cell = 0; cell < data->m_cellsPerColumn; ++cell) {
      if(*(data->m_segmentsCount + columnIdx * data->m_cellsPerColumn + cell) <= nrOfSegments) {
        nrOfSegments = *(data->m_segmentsCount + columnIdx * data->m_cellsPerColumn + cell);
        bestMatchingCell.m_cellIdx = cell;
        bestMatchingCell.m_anyCellFound = true;
      }
    }
  }
  
  return bestMatchingCell;
}

SegmentSearch getBestMatchingSegment(Data *data, uint columnIdx, uint cellIdx, WhenState_enum when) {
  SegmentSearch bestMatchingSegment;
  bestMatchingSegment.m_anySegmentFound = false;
  bestMatchingSegment.m_activeSynapses = 0;
  bestMatchingSegment.m_segmentIdx = 0;
  
  uint activity = 0;
  global uint *synapseTargetColumnBlockIdx = data->m_synapseTargetColumn + columnIdx * data->m_cellsPerColumn * data->m_segmentsPerCell * data->m_synapsesPerSegment + \
                                            cellIdx * data->m_segmentsPerCell * data->m_synapsesPerSegment;
  global uint *synapseTargetCellBlockIdx = data->m_synapseTargetCell + columnIdx * data->m_cellsPerColumn * data->m_segmentsPerCell * data->m_synapsesPerSegment + \
                                           cellIdx * data->m_segmentsPerCell * data->m_synapsesPerSegment;  
  for(uint segment = 0; segment < data->m_segmentsCount[columnIdx * data->m_cellsPerColumn + cellIdx]; ++segment) {                                             
    for(uint synapse = 0; synapse < data->m_synapsesCount[columnIdx * data->m_cellsPerColumn * data->m_segmentsPerCell + cellIdx * data->m_segmentsPerCell + segment]; ++synapse) {
      if(getCellState(data, *(synapseTargetColumnBlockIdx + segment * data->m_synapsesPerSegment + synapse), *(synapseTargetCellBlockIdx + segment * data->m_synapsesPerSegment + synapse), when) & ACTIVE_CELL) {
        ++activity;
      }
    }
    if(activity > data->m_minSegmentActivationThreshold) {
      if(activity > bestMatchingSegment.m_activeSynapses) {
        bestMatchingSegment.m_activeSynapses = activity;
        bestMatchingSegment.m_segmentIdx = segment;
        bestMatchingSegment.m_anySegmentFound = true;
      }
    }
    activity = 0;
  }
  
  return bestMatchingSegment;
}

NewSegment queueNewSegment(Data *data, uint columnIdx, uint cellIdx, uchar isSegmentSequential) {
  NewSegment newSegment = {.m_segmentCreated = false};
  uint newSegmentOffset = *(data->m_newSegmentQueuedCount + columnIdx * data->m_cellsPerColumn + cellIdx);
  if(data->m_segmentsCount[columnIdx * data->m_cellsPerColumn + cellIdx] + newSegmentOffset <= data->m_segmentsPerCell - 1) {
    *(data->m_newSegmentQueuedCount + columnIdx * data->m_cellsPerColumn + cellIdx) += 1;
    *(data->m_hasCellQueuedChanges + columnIdx * data->m_cellsPerColumn + cellIdx) = true;
    
    *(data->m_isSegmentSequential + columnIdx * data->m_cellsPerColumn * data->m_segmentsPerCell + cellIdx * data->m_segmentsPerCell + 
      data->m_segmentsCount[columnIdx * data->m_cellsPerColumn + cellIdx] + newSegmentOffset) = isSegmentSequential;

    newSegment.m_segmentIdx = data->m_segmentsCount[columnIdx * data->m_cellsPerColumn + cellIdx] + newSegmentOffset;
    newSegment.m_segmentCreated = true;
  }
  return newSegment;
}

void getLayerLearningCells(Data *data, unsigned int columnIdx) {
  bool wasLearningCell = false;
  bool isLearningCell = false;

  barrier(CLK_GLOBAL_MEM_FENCE);

  for(unsigned int cell = 0; cell < data->m_cellsPerColumn; ++cell) {
    if(!isLearningCell) {
      if(getCellState(data, columnIdx, cell, NOW) & LEARN_CELL) {
        data->m_isLearningCellInColumn[columnIdx] = data->m_isLearningCellInColumn[columnIdx] | 0x01;
        isLearningCell = true;
        data->m_isLearningCellInColumn[get_global_size(0)] = 1;
      }
    }
    if(isLearningCell) {
      break;
    }
  }

  barrier(CLK_GLOBAL_MEM_FENCE);
}

void getSegmentActiveSynapses(Data *data, uint columnIdx, uint cellIdx, uint segmentIdx, WhenState_enum when, bool makeSegmentSequential, bool allowNewSynapses) {
  uint activeSynapsesCount = 0;
  size_t localId = get_local_id(0);
  
  data->m_sequenceSegmentQueued[columnIdx * data->m_cellsPerColumn * data->m_segmentsPerCell + cellIdx * data->m_segmentsPerCell + segmentIdx] = makeSegmentSequential;
  
  for(uint synapse = 0; synapse < data->m_synapsesCount[columnIdx * data->m_cellsPerColumn * data->m_segmentsPerCell + cellIdx * data->m_segmentsPerCell + segmentIdx]; ++synapse) {
    if(getCellState(data, data->m_synapseTargetColumn[columnIdx * data->m_cellsPerColumn * data->m_segmentsPerCell * data->m_synapsesPerSegment + 
      cellIdx * data->m_segmentsPerCell * data->m_synapsesPerSegment + segmentIdx * data->m_synapsesPerSegment + synapse], 
      data->m_synapseTargetCell[columnIdx * data->m_cellsPerColumn * data->m_segmentsPerCell * data->m_synapsesPerSegment + 
      cellIdx * data->m_segmentsPerCell * data->m_synapsesPerSegment + segmentIdx * data->m_synapsesPerSegment + synapse], when) & ACTIVE_CELL) {
        data->m_synapsePermanenceQueued[columnIdx * data->m_cellsPerColumn * data->m_segmentsPerCell * data->m_synapsesPerSegment + 
        cellIdx * data->m_segmentsPerCell * data->m_synapsesPerSegment + segmentIdx * data->m_synapsesPerSegment + synapse] = true;
        ++activeSynapsesCount;
    }
  }

  if(allowNewSynapses) {
    int amountToCreate = data->m_maxNewSynapses - activeSynapsesCount;
    if(amountToCreate < 0) return;
    if(amountToCreate + data->m_newSynapsesCount[columnIdx * data->m_cellsPerColumn * data->m_segmentsPerCell + cellIdx * data->m_segmentsPerCell + segmentIdx] + data->m_synapsesCount[columnIdx * data->m_cellsPerColumn * data->m_segmentsPerCell + cellIdx * data->m_segmentsPerCell + segmentIdx] > data->m_synapsesPerSegment) {
      amountToCreate = data->m_synapsesPerSegment - data->m_newSynapsesCount[columnIdx * data->m_cellsPerColumn * data->m_segmentsPerCell + cellIdx * data->m_segmentsPerCell + 
        segmentIdx] - data->m_synapsesCount[columnIdx * data->m_cellsPerColumn * data->m_segmentsPerCell + cellIdx * data->m_segmentsPerCell + segmentIdx];
    }
    
    uint amountCreated = 0;

    if(amountToCreate > 0 && data->m_isLearningCellInColumn[get_global_size(0) + when]) {
      
      uint targetColumn = 0;
      uint targetCell = 0;
      bool found = false;
      uint randColumnId;
      uint randCellId;
      uint offset;
      uint usedCount = 0;
      uint offsetUsedLearningCells = localId * data->m_maxNewSynapses;
      
      uint globalSegmentBlockStartIdx = columnIdx * data->m_cellsPerColumn * data->m_segmentsPerCell * data->m_synapsesPerSegment +
                                  cellIdx * data->m_segmentsPerCell * data->m_synapsesPerSegment + segmentIdx * data->m_synapsesPerSegment;
    
      uint newSynapseOffset = data->m_newSynapsesCount[columnIdx * data->m_cellsPerColumn * data->m_segmentsPerCell + cellIdx * data->m_segmentsPerCell + segmentIdx] + 
        data->m_synapsesCount[columnIdx * data->m_cellsPerColumn * data->m_segmentsPerCell + cellIdx * data->m_segmentsPerCell + segmentIdx];
      
      for(unsigned int newSynapse = 0; newSynapse < amountToCreate; ++newSynapse) {
        if(newSynapse > 0 && found == false) {
          break;
        }
        
        found = false;

        offset = MWC64X_NextUint(&data->m_randomNumberState[columnIdx]) % get_global_size(0);
        for(unsigned int column = 0; column < get_global_size(0); ++column) {

          randColumnId = (offset + column) % get_global_size(0);
          if(randColumnId == columnIdx) {
            continue;
          }

          if((data->m_isLearningCellInColumn[randColumnId] >> (when * 4)) & 0x0F) {
            randCellId = MWC64X_NextUint(&data->m_randomNumberState[columnIdx]) % data->m_cellsPerColumn;
            for(unsigned int cell = 0; cell < data->m_cellsPerColumn; ++cell) {
              if(getCellState(data, randColumnId, randCellId, when) & LEARN_CELL) {
                bool used = false;
                for(int i = 0; i < usedCount; ++i) {
                    if((data->m_usedLearningCells[offsetUsedLearningCells + i] == randCellId) && (data->m_usedLearningCells[offsetUsedLearningCells + i + 1] == randColumnId)) {
                        used = true;
                    }
                }
                if(used == false) {
                    targetColumn = randColumnId;
                    targetCell = randCellId;
                    data->m_usedLearningCells[offsetUsedLearningCells + usedCount] = targetCell;
                    data->m_usedLearningCells[offsetUsedLearningCells + usedCount + 1] = targetColumn;
                    ++usedCount;
                    found = true;
                    break;
                }
              }
            }
          }
          if(found) {
            data->m_synapseTargetColumn[globalSegmentBlockStartIdx + newSynapseOffset + newSynapse] = targetColumn;
            data->m_synapseTargetCell[globalSegmentBlockStartIdx + newSynapseOffset + newSynapse] = targetCell;
            ++amountCreated;
            break;
          }
        }
      }
    }
    
    if(amountCreated) {
      data->m_newSynapsesCount[columnIdx * data->m_cellsPerColumn * data->m_segmentsPerCell + cellIdx * data->m_segmentsPerCell + segmentIdx] += amountCreated;
    }
  }
}

void adaptSegments(Data *data, uint columnIdx, uint cellIdx, bool positiveReinforcement) {
  uint segmentBlockIdx = columnIdx * data->m_cellsPerColumn * data->m_segmentsPerCell + cellIdx * data->m_segmentsPerCell;
  for(uint segment = 0; segment < *(data->m_segmentsCount + columnIdx * data->m_cellsPerColumn + cellIdx); ++segment) {
    data->m_isSegmentSequential[segmentBlockIdx + segment] =
      data->m_sequenceSegmentQueued[segmentBlockIdx + segment];
    
    //data->m_sequenceSegmentQueued[segmentBlockIdx + segment] = false;
  
    const uint synapseBlockIdx = segmentBlockIdx * data->m_synapsesPerSegment + segment * data->m_synapsesPerSegment;
    if(positiveReinforcement) {
      for(uint synapse = 0; synapse < data->m_synapsesCount[segmentBlockIdx + segment]; ++synapse) {
        if(data->m_synapsePermanenceQueued[synapseBlockIdx + synapse]) {
            data->m_synapsePermanence[synapseBlockIdx + synapse] += data->m_permanenceIncrement;
            if(data->m_synapsePermanence[synapseBlockIdx + synapse] > 100000) {
              data->m_synapsePermanence[synapseBlockIdx + synapse] = 100000;
            }
        } else {
          if(data->m_synapsePermanence[synapseBlockIdx + synapse] > data->m_permanenceDecrement) {
            data->m_synapsePermanence[synapseBlockIdx + synapse] -= data->m_permanenceDecrement;
          } else {
            data->m_synapsePermanence[synapseBlockIdx + synapse] = 0;
          }
        }
        data->m_synapsePermanenceQueued[synapseBlockIdx + synapse] = false;
      }
    } else {
      for(uint synapse = 0; synapse < data->m_synapsesCount[segmentBlockIdx + segment]; ++synapse) {
        if(data->m_synapsePermanenceQueued[synapseBlockIdx + synapse]) {
          if(data->m_synapsePermanence[synapseBlockIdx + synapse] > data->m_permanenceDecrement) {
            data->m_synapsePermanence[synapseBlockIdx + synapse] -= data->m_permanenceDecrement;
          } else {
            data->m_synapsePermanence[synapseBlockIdx + synapse] = 0;
          }
        }
        data->m_synapsePermanenceQueued[synapseBlockIdx + synapse] = false;
      }
    }
    
    
    uint newSynapseOffset = data->m_synapsesCount[segmentBlockIdx + segment];
    data->m_synapsesCount[segmentBlockIdx + segment] += data->m_newSynapsesCount[segmentBlockIdx + segment];
    if(data->m_synapsesCount[segmentBlockIdx + segment] > data->m_synapsesPerSegment) {
      data->m_synapsesCount[segmentBlockIdx + segment] = data->m_synapsesPerSegment;
    }

    if(data->m_synapsesCount[segmentBlockIdx + segment] == 0) {
        *(data->m_segmentsCount + columnIdx * data->m_cellsPerColumn + cellIdx) -= 1;
    }
    
    for(uint newSynapse = 0; newSynapse < data->m_newSynapsesCount[segmentBlockIdx + segment]; ++newSynapse) {
      data->m_synapsePermanence[synapseBlockIdx + newSynapseOffset + newSynapse] = data->m_initialPermanence;
    }
    
    data->m_newSynapsesCount[segmentBlockIdx + segment] = 0;
  }
}

void adaptCell(Data *data, uint columnIdx, uint cellIdx) {
  if(*(data->m_hasCellQueuedChanges + columnIdx * data->m_cellsPerColumn + cellIdx) == true) {
    *(data->m_segmentsCount + columnIdx * data->m_cellsPerColumn + cellIdx) += *(data->m_newSegmentQueuedCount + columnIdx * data->m_cellsPerColumn + cellIdx);
    *(data->m_newSegmentQueuedCount + columnIdx * data->m_cellsPerColumn + cellIdx) = 0;
    *(data->m_hasCellQueuedChanges + columnIdx * data->m_cellsPerColumn + cellIdx) = false;
  }
}