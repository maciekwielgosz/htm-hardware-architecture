#include "additional_temporal_pooler_functions.h"

/** \fn kernel void initializeLayer(constant uint *m_settings, global uchar *m_cellState,
 *                                  global uchar *m_hasCellQueuedChanges, global uint *m_newSegmentQueuedCount, global uint *m_segmentsCount,
 *                                  global uchar *m_isSegmentSequential, global uchar *m_hasSegmentQueuedChanges,
 *                                  global uchar *m_sequenceSegmentQueued, global uint *m_synapsesCount, global uint *m_newSynapsesCount,
 *                                  global uint *m_synapsePermanence, global uchar *m_synapsePermanenceQueued,
 *                                  global uint *m_synapseTargetColumn, global uint *m_synapseTargetCell)
 *  \brief Initialize layer state. Random synapse connections.
 */
kernel void initializeLayer(constant uint *m_settings, global uchar *m_cellState,
                            global uchar *m_hasCellQueuedChanges, global uint *m_newSegmentQueuedCount, global uint *m_segmentsCount,
                            global uchar *m_isSegmentSequential, global uchar *m_hasSegmentQueuedChanges, 
                            global uchar *m_sequenceSegmentQueued, global uint *m_synapsesCount, global uint *m_newSynapsesCount,
                            global uint *m_synapsePermanence, global uchar *m_synapsePermanenceQueued, 
                            global uint *m_synapseTargetColumn, global uint *m_synapseTargetCell, volatile global uint *m_isLearningCellInColumn,
                            global mwc64x_state_t *m_randomNumberState, global uint *initRandomValues) {
  size_t columnIdx = get_global_id(0);
  
//   printf("KERNEL (initializeLayer), ID: %d\n", columnIdx);
  
  Data data = makeDataStructure(m_settings, m_cellState, 
                                m_hasCellQueuedChanges, m_newSegmentQueuedCount, m_segmentsCount,
				m_isSegmentSequential, m_hasSegmentQueuedChanges, 
				m_sequenceSegmentQueued, m_synapsesCount, m_newSynapsesCount,
				m_synapsePermanence, m_synapsePermanenceQueued, 
				m_synapseTargetColumn, m_synapseTargetCell, m_isLearningCellInColumn, 0, m_randomNumberState);

  MWC64X_SeedStreams(&m_randomNumberState[columnIdx], *(initRandomValues + columnIdx), 4294967296);

  if(columnIdx == 0) {
    m_isLearningCellInColumn[get_global_size(0)] = 0;
    m_isLearningCellInColumn[get_global_size(0) + 1] = 0;
  }

  *(m_isLearningCellInColumn + columnIdx)  = 0;

  uint globalCellBlockStartIdx = columnIdx * data.m_cellsPerColumn;
  global uchar *cellState = m_cellState + globalCellBlockStartIdx;
  global uchar *hasCellQueuedChanges = m_hasCellQueuedChanges + globalCellBlockStartIdx;
  global uint *newSegmentQueuedCount = m_newSegmentQueuedCount + globalCellBlockStartIdx;
  global uint *segmentsCount = m_segmentsCount + globalCellBlockStartIdx;

  for(uint cell = 0; cell < data.m_cellsPerColumn; ++cell) {
    *(cellState + cell) = 0;
    *(hasCellQueuedChanges + cell) = 0;
    *(newSegmentQueuedCount + cell) = 0;
    *(segmentsCount + cell) = 0;
    
    uint globalSegmentBlockStartIdx = globalCellBlockStartIdx * data.m_segmentsPerCell + cell * data.m_segmentsPerCell;
    global uchar *isSegmentSequential = m_isSegmentSequential + globalSegmentBlockStartIdx;
    global uchar *hasSegmentQueuedChanges = m_hasSegmentQueuedChanges + globalSegmentBlockStartIdx;
    global uchar *sequenceSegmentQueued = m_sequenceSegmentQueued + globalSegmentBlockStartIdx;
    global uint *synapsesCount = m_synapsesCount + globalSegmentBlockStartIdx;
    global uint *newSynapsesCount = m_newSynapsesCount + globalSegmentBlockStartIdx;
    
    for(uint segment = 0; segment < data.m_segmentsPerCell; ++segment) {
      *(isSegmentSequential + segment) = 0;
      *(hasSegmentQueuedChanges + segment) = 0;
      *(sequenceSegmentQueued + segment) = 0;
      *(synapsesCount + segment) = 0;
      *(newSynapsesCount + segment) = 0;
      
      uint globalSynapseBlockStartIdx = globalSegmentBlockStartIdx * data.m_synapsesPerSegment + segment * data.m_synapsesPerSegment;
      global uint *synapsePermanence = data.m_synapsePermanence + globalSynapseBlockStartIdx;
      global uchar *synapsePermanenceQueued = data.m_synapsePermanenceQueued + globalSynapseBlockStartIdx;
      global uint *synapseTargetColumn = data.m_synapseTargetColumn + globalSynapseBlockStartIdx;
      global uint *synapseTargetCell = data.m_synapseTargetCell + globalSynapseBlockStartIdx;

      for(uint synapse = 0; synapse < data.m_synapsesPerSegment; ++synapse) {
        *(synapseTargetColumn + synapse) = 0;
        *(synapseTargetCell + synapse) = 0;
        *(synapsePermanence + synapse) = 0;//data.m_initialPermanence;
        *(synapsePermanenceQueued + synapse) = 0;
      }
    }
  }
}

/** \fn kernel void phaseZero(Data *data)
 *  \brief Time steps kernel
 */
void phaseZero(Data *data) {
  size_t columnIdx = get_global_id(0);

//   printf("KERNEL (phaseZero), ID: %d\n", columnIdx);

  global uchar *cellState = data->m_cellState + columnIdx * data->m_cellsPerColumn;

  if(columnIdx == 0) {
    data->m_isLearningCellInColumn[get_global_size(0) + 1] = data->m_isLearningCellInColumn[get_global_size(0)];
    data->m_isLearningCellInColumn[get_global_size(0)] = 0;
  }
  *(data->m_isLearningCellInColumn + columnIdx) = ((*(data->m_isLearningCellInColumn + columnIdx)) << 4) & 0xF0;;

  for(uint cell = 0; cell < data->m_cellsPerColumn; ++cell) {
    *(cellState + cell) = (*(cellState + cell)) << 4;
  }

  mem_fence(CLK_GLOBAL_MEM_FENCE);
}

/** \fn kernel void phaseOne(Data *data, global uchar *m_columnState)
 *  \brief Compute active state for each cell.
 */
void phaseOne(Data *data, constant uchar *m_columnState) {
  size_t columnIdx = get_global_id(0);

//   printf("KERNEL (phaseOne), ID: %d\n", columnIdx);
  CellSearch bestMatchingCell;
  bool buPredicted = false, lcChoosen = false;
  if(m_columnState[columnIdx]) {
    uint segmentIdx;
    ActiveSegment segment;
    
    for(uint cell = 0; cell < data->m_cellsPerColumn; ++cell) {
      if(getCellState(data, columnIdx, cell, WAS) & PREDICTIVE_CELL) {
	    segment = getActiveSegment(data, columnIdx, cell, WAS, ACTIVE_CELL);
        if(segment.m_valid == false) {
          // error
          return;
        }
        if(isSegmentSequential(data, columnIdx, cell, segment.m_segmentIdx)) {
          buPredicted = true;
          setCellState(data, columnIdx, cell, ACTIVE_CELL);
          if(segmentActive(data, columnIdx, cell, segment.m_segmentIdx, WAS, LEARN_CELL) && data->m_isLearningActive) {
            lcChoosen = true;
            setCellState(data, columnIdx, cell, LEARN_CELL);
          }
        }
      }
    }
    
    if(!buPredicted) {
      for(uint cell = 0; cell < data->m_cellsPerColumn; ++cell) {
	setCellState(data, columnIdx, cell, ACTIVE_CELL);
      }
    }

    if(!lcChoosen && data->m_isLearningActive) {
      bestMatchingCell = getBestMatchingCell(data, columnIdx, WAS);

      setCellState(data, columnIdx, bestMatchingCell.m_cellIdx, LEARN_CELL);
    }
    }
      mem_fence(CLK_GLOBAL_MEM_FENCE);
      getLayerLearningCells(data, columnIdx);
      mem_fence(CLK_GLOBAL_MEM_FENCE);
    if(m_columnState[columnIdx]) {
      if(!lcChoosen && data->m_isLearningActive) {
      if(bestMatchingCell.m_anySegmentFound == false) {
        NewSegment newSegment = queueNewSegment(data, columnIdx, bestMatchingCell.m_cellIdx, true);
        
        if(newSegment.m_segmentCreated == true) {
          getSegmentActiveSynapses(data, columnIdx, bestMatchingCell.m_cellIdx, newSegment.m_segmentIdx, WAS, true, true);
          *(data->m_sequenceSegmentQueued + columnIdx * data->m_cellsPerColumn * data->m_segmentsPerCell + bestMatchingCell.m_cellIdx * data->m_segmentsPerCell + newSegment.m_segmentIdx) = 1;
        }
      } else {
        getSegmentActiveSynapses(data, columnIdx, bestMatchingCell.m_cellIdx, bestMatchingCell.m_segmentIdx, WAS, true, true);
        *(data->m_sequenceSegmentQueued + columnIdx * data->m_cellsPerColumn * data->m_segmentsPerCell + bestMatchingCell.m_cellIdx * data->m_segmentsPerCell + bestMatchingCell.m_segmentIdx) = 1;
      }
    }
  }
}

/** \fn kernel void phaseTwo(Data *data)
 *  \brief Calculate predictive state for each cell.
 */
void phaseTwo(Data *data) {
  size_t columnIdx = get_global_id(0);
  
//   printf("KERNEL (phaseTwo), ID: %d\n", columnIdx);
  
  for(uint cell = 0; cell < data->m_cellsPerColumn; ++cell) {
    for(uint segment = 0; segment < data->m_segmentsPerCell; ++segment) {
      if(segmentActive(data, columnIdx, cell, segment, NOW, ACTIVE_CELL)) {
        setCellState(data, columnIdx, cell, PREDICTIVE_CELL);
        
        if(data->m_isLearningActive) {
          getSegmentActiveSynapses(data, columnIdx, cell, segment, NOW, false, false);
          
          SegmentSearch bestMatchingSegment = getBestMatchingSegment(data, columnIdx, cell, WAS);
          if(bestMatchingSegment.m_anySegmentFound == false) {
            NewSegment newSegment = queueNewSegment(data, columnIdx, cell, false);
            
            if(newSegment.m_segmentCreated == true) {
              getSegmentActiveSynapses(data, columnIdx, cell, newSegment.m_segmentIdx, WAS, false, true);
            }
          } else {
            getSegmentActiveSynapses(data, columnIdx, cell, bestMatchingSegment.m_segmentIdx, WAS, false, true);
          }
        }
      }
    }
  }
}

/** \fn kernel void phaseThree(Data *data)
 *  \brief Update segments.
 */
void phaseThree(Data *data) {
  size_t columnIdx = get_global_id(0);
  
//   printf("KERNEL (phaseThree), ID: %d\n", columnIdx);

  if(data->m_isLearningActive) {
    for(uint cell = 0; cell < data->m_cellsPerColumn; ++cell) {
      if(getCellState(data, columnIdx, cell, NOW) & LEARN_CELL) {
        adaptCell(data, columnIdx, cell);
        adaptSegments(data, columnIdx, cell, true);
      } else if(((getCellState(data, columnIdx, cell, NOW) & PREDICTIVE_CELL) == 0) && (getCellState(data, columnIdx, cell, WAS) & PREDICTIVE_CELL)) {
        adaptCell(data, columnIdx, cell);
        adaptSegments(data, columnIdx, cell, false);
      }
    }
  }
}

/** \fn kernel void phaseFour(Data *data, global uchar *m_outputData)
 *  \brief Returns logic OR of active and predictive cells' states.
 */
void phaseFour(Data *data, global uchar *m_outputData) {
  size_t columnIdx = get_global_id(0);
  
//   printf("KERNEL (phaseFour), ID: %d\n", columnIdx);
  uchar outputStateColumn = 0;

  for(uint cell = 0; cell < data->m_cellsPerColumn; ++cell) {
    if(getCellState(data, columnIdx, cell, NOW) & (ACTIVE_CELL | PREDICTIVE_CELL)) {
      outputStateColumn = 1;
      break;
    }
  }
  
  *(m_outputData + columnIdx) = outputStateColumn;
}

kernel void executeKernels(constant uint *m_settings, global uchar *m_cellState, \
                          global uchar *m_hasCellQueuedChanges, global uint *m_newSegmentQueuedCount, global uint *m_segmentsCount,
                          global uchar *m_isSegmentSequential, global uchar *m_hasSegmentQueuedChanges, \
                          global uchar *m_sequenceSegmentQueued, global uint *m_synapsesCount, global uint *m_newSynapsesCount,\
                          global uint *m_synapsePermanence, global uchar *m_synapsePermanenceQueued, \
                          global uint *m_synapseTargetColumn, global uint *m_synapseTargetCell, volatile global uint *m_isLearningCellInColumn,
                          local uint *m_usedLearningCells, constant uchar *m_columnState, global uchar *m_outputData,
                          global mwc64x_state_t *m_randomNumberState) {
  Data data = makeDataStructure(m_settings, m_cellState,
                                m_hasCellQueuedChanges, m_newSegmentQueuedCount, m_segmentsCount,
                                m_isSegmentSequential, m_hasSegmentQueuedChanges,
                                m_sequenceSegmentQueued, m_synapsesCount, m_newSynapsesCount,
                                m_synapsePermanence, m_synapsePermanenceQueued,
                                m_synapseTargetColumn, m_synapseTargetCell, m_isLearningCellInColumn,
                                m_usedLearningCells, m_randomNumberState);

  #ifndef PHASEZERO
  phaseZero(&data);
  #endif
  #ifndef PHASEONE
  phaseOne(&data, m_columnState);
  #endif
  #ifndef PHASETWO
  phaseTwo(&data);
  #endif
  #ifndef PHASETHREE
  phaseThree(&data);
  #endif
  #ifndef PHASEFOUR
  phaseFour(&data, m_outputData);
  #endif
}
