#include "additional_temporal_pooler_functions_2.h"

uchar getCellState(Data *data, uint columnIdx, uint cellIdx, WhenState when) {
    return ((*(data->m_cellState + columnIdx * MAX_CELLS_PER_COLUMN + cellIdx)) >> (when * 4)) & 0x0F;
}

void setCellState(Data *data, uint columnIdx, uint cellIdx, CellState state) {
    *(data->m_cellState + columnIdx * MAX_CELLS_PER_COLUMN + cellIdx) = *(data->m_cellState + columnIdx * MAX_CELLS_PER_COLUMN + cellIdx) | state;
}

Data makeDataStructure(global uchar *m_cellState,
    global uchar *m_hasCellQueuedChanges, global uint *m_newSegmentsQueuedCount, global uint *m_segmentsCount,
    global uchar *m_isSegmentSequential, global uchar *m_hasSegmentQueuedChanges,
    global uchar *m_sequenceSegmentQueued, global uint *m_synapsesCount, global uint *m_newSynapsesQueuedCount,
    global uint *m_synapsePermanence, global int *m_synapsePermanenceQueued,
    global uint *m_synapseTargetColumn, global uint *m_synapseTargetCell, global uchar *m_isLearningCellInColumn,
    local uint *m_usedLearningCells, global mwc64x_state_t *m_randomNumberState) {

    Data data;
    data.m_cellState = m_cellState;
    data.m_hasCellQueuedChanges = m_hasCellQueuedChanges;
    data.m_newSegmentsQueuedCount = m_newSegmentsQueuedCount;
    data.m_segmentsCount = m_segmentsCount;
    data.m_isSegmentSequential = m_isSegmentSequential;
    data.m_hasSegmentQueuedChanges = m_hasSegmentQueuedChanges;
    data.m_sequenceSegmentQueued = m_sequenceSegmentQueued;
    data.m_synapsesCount = m_synapsesCount;
    data.m_newSynapsesQueuedCount = m_newSynapsesQueuedCount;
    data.m_synapsePermanence = m_synapsePermanence;
    data.m_synapsePermanenceQueued = m_synapsePermanenceQueued;
    data.m_synapseTargetColumn = m_synapseTargetColumn;
    data.m_synapseTargetCell = m_synapseTargetCell;
    data.m_isLearningCellInColumn = m_isLearningCellInColumn;
    data.m_usedLearningCells = m_usedLearningCells;
    data.m_randomNumberState = m_randomNumberState;

    return data;
}

bool isSegmentSequential(Data *data, uint m_columnIdx, uint m_cellIdx, uint m_segment) {
    return data->m_isSegmentSequential[m_columnIdx * MAX_CELLS_PER_COLUMN * MAX_SEGMENTS_PER_CELL + m_cellIdx * MAX_SEGMENTS_PER_CELL + m_segment];
}

bool segmentActive(Data *data, uint columnIdx, uint cellIdx, uint segmentIdx, WhenState when, CellState cellState) {
    return segmentActivity(data, columnIdx, cellIdx, segmentIdx, when, cellState) > SEGMENT_ACTIVATION_THRESHOLD;
}

uint segmentActivity(Data *data, uint columnIdx, uint cellIdx, uint segmentIdx, WhenState when, CellState cellState) {
    uint activity = 0;
    uint synapseOffsetStart = columnIdx * MAX_CELLS_PER_COLUMN * MAX_SEGMENTS_PER_CELL * MAX_SYNAPSES_PER_SEGMENT +
        cellIdx * MAX_SEGMENTS_PER_CELL * MAX_SYNAPSES_PER_SEGMENT + segmentIdx * MAX_SYNAPSES_PER_SEGMENT;
    global uint *synapseTargetColumnStart = data->m_synapseTargetColumn + synapseOffsetStart;
    global uint *synapseTargetCellStart = data->m_synapseTargetCell + synapseOffsetStart;
    global uint *synapsePermanenceStart = data->m_synapsePermanence + synapseOffsetStart;

    for(uint synapse = 0; synapse < data->m_synapsesCount[columnIdx * MAX_CELLS_PER_COLUMN * MAX_SEGMENTS_PER_CELL + cellIdx * MAX_SEGMENTS_PER_CELL + segmentIdx]; ++synapse) {
        if(getCellState(data, *(synapseTargetColumnStart + synapse), *(synapseTargetCellStart + synapse), when) & cellState) {
            if(*(synapsePermanenceStart + synapse) > SYNAPSE_CONNECTED_PERMANENCE) {
                ++activity;
            }
        }
    }

    return activity;
}

CellSearch getBestMatchingCell(Data *data, uint columnIdx, WhenState when) {
    CellSearch bestMatchingCell;
    uint nbrOfActiveSynapses = 0;
    bestMatchingCell.m_anyCellFound = false;
    bestMatchingCell.m_anySegmentFound = false;
    bestMatchingCell.m_cellIdx = 0;
    bestMatchingCell.m_segmentIdx = 0;

    for(uint cell = 0; cell < MAX_CELLS_PER_COLUMN; ++cell) {
        SegmentSearch bestMatchingSegment = getBestMatchingSegment(data, columnIdx, cell, when);

        if(bestMatchingSegment.m_anySegmentFound == true && bestMatchingSegment.m_activeSynapses > nbrOfActiveSynapses) {
            nbrOfActiveSynapses = bestMatchingSegment.m_activeSynapses;
            bestMatchingCell.m_cellIdx = cell;
            bestMatchingCell.m_segmentIdx = bestMatchingSegment.m_segmentIdx;
            bestMatchingCell.m_anySegmentFound = true;
            bestMatchingCell.m_anyCellFound = true;
        }
    }

    if(bestMatchingCell.m_anyCellFound == false) {
        uint nrOfSegments = 4294967295; // max value for unsigned int
        for(uint cell = 0; cell < MAX_CELLS_PER_COLUMN; ++cell) {
            if(*(data->m_segmentsCount + columnIdx * MAX_CELLS_PER_COLUMN + cell) <= nrOfSegments) {
                nrOfSegments = *(data->m_segmentsCount + columnIdx * MAX_CELLS_PER_COLUMN + cell);
                bestMatchingCell.m_cellIdx = cell;
                bestMatchingCell.m_anyCellFound = true;
            }
        }
    }

    return bestMatchingCell;
}

SegmentSearch getBestMatchingSegment(Data *data, uint columnIdx, uint cellIdx, WhenState when) {
    SegmentSearch bestMatchingSegment;
    bestMatchingSegment.m_anySegmentFound = false;
    bestMatchingSegment.m_activeSynapses = 0;
    bestMatchingSegment.m_segmentIdx = 0;

    uint activity = 0;
    uint synapseOffsetStart = columnIdx * MAX_CELLS_PER_COLUMN * MAX_SEGMENTS_PER_CELL * MAX_SYNAPSES_PER_SEGMENT +
        cellIdx * MAX_SEGMENTS_PER_CELL * MAX_SYNAPSES_PER_SEGMENT;
    global uint *synapseTargetColumnStart = data->m_synapseTargetColumn + synapseOffsetStart;
    global uint *synapseTargetCellStart = data->m_synapseTargetCell + synapseOffsetStart;
    for(uint segment = 0; segment < data->m_segmentsCount[columnIdx * MAX_CELLS_PER_COLUMN + cellIdx]; ++segment) {
        for(uint synapse = 0; synapse < data->m_synapsesCount[columnIdx * MAX_CELLS_PER_COLUMN * MAX_SEGMENTS_PER_CELL + cellIdx * MAX_SEGMENTS_PER_CELL + segment]; ++synapse) {
            if(getCellState(data, *(synapseTargetColumnStart + segment * MAX_SYNAPSES_PER_SEGMENT + synapse), *(synapseTargetCellStart + segment * MAX_SYNAPSES_PER_SEGMENT + synapse), when) & ACTIVE_CELL) {
                ++activity;
            }
        }
        if(activity > SEGMENT_MIN_ACTIVATION_THRESHOLD) {
            if(activity > bestMatchingSegment.m_activeSynapses) {
                bestMatchingSegment.m_activeSynapses = activity;
                bestMatchingSegment.m_segmentIdx = segment;
                bestMatchingSegment.m_anySegmentFound = true;
            }
        }
        activity = 0;
    }

    return bestMatchingSegment;
}

void getLayerLearningCells(Data *data, unsigned int columnIdx) {
    bool isLearningCell = false;

    for(uint cell = 0; cell < MAX_CELLS_PER_COLUMN; ++cell) {
        if(!isLearningCell) {
            if(getCellState(data, columnIdx, cell, NOW) & LEARN_CELL) {
                data->m_isLearningCellInColumn[columnIdx] = data->m_isLearningCellInColumn[columnIdx] | 0x01;
                isLearningCell = true;
                data->m_isLearningCellInColumn[get_global_size(0)] = 1;
            }
        }
        if(isLearningCell) {
            break;
        }
    }
}

NewSegment queueNewSegment(Data *data, uint columnIdx, uint cellIdx, uchar isSegmentSequential) {
    NewSegment newSegment = {.m_segmentCreated = false};
    uint newSegmentOffset = *(data->m_newSegmentsQueuedCount + columnIdx * MAX_CELLS_PER_COLUMN + cellIdx);
    // check if there is space for new synapse in segment
    if(data->m_segmentsCount[columnIdx * MAX_CELLS_PER_COLUMN + cellIdx] + newSegmentOffset <= MAX_SEGMENTS_PER_CELL - 1) {
        *(data->m_newSegmentsQueuedCount + columnIdx * MAX_CELLS_PER_COLUMN + cellIdx) += 1;
        *(data->m_hasCellQueuedChanges + columnIdx * MAX_CELLS_PER_COLUMN + cellIdx) = true;

        newSegment.m_segmentIdx = data->m_segmentsCount[columnIdx * MAX_CELLS_PER_COLUMN + cellIdx] + newSegmentOffset;
        newSegment.m_segmentCreated = true;
    }
    return newSegment;
}

ActiveSegment getActiveSegment(Data *data, uint columnIdx, uint cellIdx, WhenState when, CellState cellState) {
    bool activeSequenceSegments = false;
    for (uint segment = 0; segment < data->m_segmentsCount[columnIdx * MAX_CELLS_PER_COLUMN + cellIdx]; ++segment) {
        if (isSegmentSequential(data, columnIdx, cellIdx, segment)) {
            if(segmentActive(data, columnIdx, cellIdx, segment, when, cellState)) {
                activeSequenceSegments = true;
                break;
            }
        }
    }

    ActiveSegment bestActivitySegment = {0, false};
    uint bestActivity = 0;

    for (uint segment = 0; segment < data->m_segmentsCount[columnIdx * MAX_CELLS_PER_COLUMN + cellIdx]; ++segment)
    {
        if (activeSequenceSegments && !isSegmentSequential(data, columnIdx, cellIdx, segment))
            continue;

        uint activity = segmentActivity(data, columnIdx, cellIdx, segment, when, cellState);
        if (activity > SEGMENT_ACTIVATION_THRESHOLD && activity >= bestActivity)
        {
            bestActivity = activity;
            bestActivitySegment.m_segmentIdx = segment;
            bestActivitySegment.m_valid = true;
        }
    }

    return bestActivitySegment;
}

void applyCellsQueuedChanges(Data *data, uint columnIdx, uint cellIdx) {
    if(*(data->m_hasCellQueuedChanges + columnIdx * MAX_CELLS_PER_COLUMN + cellIdx) == true) {
        *(data->m_segmentsCount + columnIdx * MAX_CELLS_PER_COLUMN + cellIdx) += *(data->m_newSegmentsQueuedCount + columnIdx * MAX_CELLS_PER_COLUMN + cellIdx);
        *(data->m_newSegmentsQueuedCount + columnIdx * MAX_CELLS_PER_COLUMN + cellIdx) = 0;
        *(data->m_hasCellQueuedChanges + columnIdx * MAX_CELLS_PER_COLUMN + cellIdx) = 0;
    }
}

void applySegmentsQueuedChanges(Data *data, uint columnIdx, uint cellIdx, bool positiveReinforcement) {
    uint segmentStart = columnIdx * MAX_CELLS_PER_COLUMN * MAX_SEGMENTS_PER_CELL + cellIdx * MAX_SEGMENTS_PER_CELL;
    for(uint segment = 0; segment < *(data->m_segmentsCount + columnIdx * MAX_CELLS_PER_COLUMN + cellIdx); ++segment) {

        // no changes in this segment, exit
        if(!data->m_hasSegmentQueuedChanges[segmentStart + segment]) {
            break;
        }

        data->m_isSegmentSequential[segmentStart + segment] = data->m_sequenceSegmentQueued[segmentStart + segment];

        uint synapseStart = segmentStart * MAX_SYNAPSES_PER_SEGMENT + segment * MAX_SYNAPSES_PER_SEGMENT;
        if(positiveReinforcement) {
            for(uint synapse = 0; synapse < data->m_synapsesCount[segmentStart + segment]; ++synapse) {
                uint synapsePermanence = data->m_synapsePermanence[synapseStart + synapse];
                int synapsePermanenceQueued = data->m_synapsePermanenceQueued[synapseStart + synapse];
                if((synapsePermanenceQueued >= 0) && (synapsePermanenceQueued <= 100000)) {
                    data->m_synapsePermanence[synapseStart + synapse] = data->m_synapsePermanenceQueued[synapseStart + synapse];
                } else if(synapsePermanenceQueued < 0) {
                    data->m_synapsePermanence[synapseStart + synapse] = 0;
                } else if(synapsePermanenceQueued > 100000) {
                    data->m_synapsePermanence[synapseStart + synapse] = 100000;
                }
                data->m_synapsePermanenceQueued[synapseStart + synapse] = 0;
            }
        } else {
            for(uint synapse = 0; synapse < data->m_synapsesCount[segmentStart + segment]; ++synapse) {
                uint synapsePermanence = data->m_synapsePermanence[synapseStart + synapse];
                int synapsePermanenceQueued = data->m_synapsePermanenceQueued[synapseStart + synapse];
                if(synapsePermanenceQueued > synapsePermanence) {
                    if(((int)(2 * synapsePermanence) - synapsePermanenceQueued) >= 0) {
                        data->m_synapsePermanence[synapseStart + synapse] -= data->m_synapsePermanenceQueued[synapseStart + synapse] - data->m_synapsePermanence[synapseStart + synapse];
                    } else {
                        data->m_synapsePermanence[synapseStart + synapse] = 0;
                    }
                data->m_synapsePermanenceQueued[synapseStart + synapse] = 0;
                }
            }
        }

        uint newSynapseOffset = data->m_synapsesCount[segmentStart + segment];
        data->m_synapsesCount[segmentStart + segment] += data->m_newSynapsesQueuedCount[segmentStart + segment];
        if(data->m_synapsesCount[segmentStart + segment] > MAX_SYNAPSES_PER_SEGMENT) {
            data->m_synapsesCount[segmentStart + segment] = MAX_SYNAPSES_PER_SEGMENT;
        }

        for(uint newSynapse = 0; newSynapse < data->m_newSynapsesQueuedCount[segmentStart + segment]; ++newSynapse) {
            data->m_synapsePermanence[synapseStart + newSynapseOffset + newSynapse] = SYNAPSE_INITIAL_PERMANENCE;
        }

        data->m_newSynapsesQueuedCount[segmentStart + segment] = 0;

        data->m_hasSegmentQueuedChanges[segmentStart + segment] = 0;
    }
}

void getSegmentActiveSynapses(Data *data, uint columnIdx, uint cellIdx, uint segmentIdx, WhenState when, bool makeSegmentSequential, bool allowNewSynapses) {
  uint activeSynapsesCount = 0;
  size_t localId = get_local_id(0);
  uint segment = columnIdx * MAX_CELLS_PER_COLUMN * MAX_SEGMENTS_PER_CELL + cellIdx * MAX_SEGMENTS_PER_CELL + segmentIdx;

  if(!data->m_hasSegmentQueuedChanges[segment]) {
    data->m_hasSegmentQueuedChanges[segment] = 1;
    for(uint synapse = 0; synapse < MAX_SYNAPSES_PER_SEGMENT; ++synapse) {
      data->m_synapsePermanenceQueued[segment * MAX_SYNAPSES_PER_SEGMENT + synapse] = data->m_synapsePermanence[segment * MAX_SYNAPSES_PER_SEGMENT + synapse];
    }
  }

  data->m_sequenceSegmentQueued[segment] = makeSegmentSequential;

  for(uint synapse = 0; synapse < data->m_synapsesCount[segment]; ++synapse) {
    if(getCellState(data, data->m_synapseTargetColumn[segment * MAX_SYNAPSES_PER_SEGMENT + synapse],
      data->m_synapseTargetCell[segment * MAX_SYNAPSES_PER_SEGMENT + synapse], when) & ACTIVE_CELL) {
        data->m_synapsePermanenceQueued[segment * MAX_SYNAPSES_PER_SEGMENT + synapse] += SYNAPSE_PERMANENCE_INCREMENT;
        ++activeSynapsesCount;
    } else {
        data->m_synapsePermanenceQueued[segment * MAX_SYNAPSES_PER_SEGMENT + synapse] -= SYNAPSE_PERMANENCE_DECREMENT;
    }
  }

  if(allowNewSynapses) {
    int amountToCreate = MAX_NEW_SYNAPSES - activeSynapsesCount;
    if(amountToCreate <= 0) return;
    if(amountToCreate + data->m_newSynapsesQueuedCount[segment] + data->m_synapsesCount[segment] > MAX_SYNAPSES_PER_SEGMENT) {
      amountToCreate = MAX_SYNAPSES_PER_SEGMENT - data->m_newSynapsesQueuedCount[segment] - data->m_synapsesCount[segment];
    }

    uint amountCreated = 0;

    // if we have space for new synapses and there was any learning cell in layer
    if(amountToCreate > 0 && data->m_isLearningCellInColumn[get_global_size(0) + when]) {

      uint targetColumn = 0;
      uint targetCell = 0;
      bool found = false;
      uint randColumnId;
      uint randCellId;
      uint offset;
      uint usedCount = 0;
      uint offsetUsedLearningCells = localId * MAX_NEW_SYNAPSES;

      uint globalSegmentBlockStartIdx = columnIdx * MAX_CELLS_PER_COLUMN * MAX_SEGMENTS_PER_CELL * MAX_SYNAPSES_PER_SEGMENT +
                                  cellIdx * MAX_SEGMENTS_PER_CELL * MAX_SYNAPSES_PER_SEGMENT + segmentIdx * MAX_SYNAPSES_PER_SEGMENT;

      uint newSynapseOffset = data->m_newSynapsesQueuedCount[segment] +
        data->m_synapsesCount[segment];

      for(unsigned int newSynapse = 0; newSynapse < amountToCreate; ++newSynapse) {
        if(newSynapse > 0 && found == false) {
          break;
        }

        found = false;

        offset = MWC64X_NextUint(&data->m_randomNumberState[columnIdx]) % get_global_size(0);
        for(unsigned int column = 0; column < get_global_size(0); ++column) {

          randColumnId = (offset + column) % get_global_size(0);
          if(randColumnId == columnIdx) {
            continue;
          }

          if((data->m_isLearningCellInColumn[randColumnId] >> (when * 4)) & 0x0F) {
            randCellId = MWC64X_NextUint(&data->m_randomNumberState[columnIdx]) % MAX_CELLS_PER_COLUMN;
            for(unsigned int cell = 0; cell < MAX_CELLS_PER_COLUMN; ++cell) {
              if(getCellState(data, randColumnId, randCellId, when) & LEARN_CELL) {
                bool used = false;
                for(int i = 0; i < usedCount; ++i) {
                    if((data->m_usedLearningCells[offsetUsedLearningCells + i] == randCellId) && (data->m_usedLearningCells[offsetUsedLearningCells + i + 1] == randColumnId)) {
                        used = true;
                    }
                }
                if(used == false) {
                    targetColumn = randColumnId;
                    targetCell = randCellId;
                    data->m_usedLearningCells[offsetUsedLearningCells + usedCount] = targetCell;
                    data->m_usedLearningCells[offsetUsedLearningCells + usedCount + 1] = targetColumn;
                    ++usedCount;
                    found = true;
                    break;
                }
              }
            }
          }
          if(found) {
            data->m_synapseTargetColumn[globalSegmentBlockStartIdx + newSynapseOffset + newSynapse] = targetColumn;
            data->m_synapseTargetCell[globalSegmentBlockStartIdx + newSynapseOffset + newSynapse] = targetCell;
            ++amountCreated;
            break;
          }
        }
      }
    }

    if(amountCreated) {
      data->m_newSynapsesQueuedCount[segment] += amountCreated;
    }
  }
}