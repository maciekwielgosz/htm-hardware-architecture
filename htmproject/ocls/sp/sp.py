import numpy as np
import math
import pyopencl as cl
from htmproject.interfaces.ocl import OCL


class SpatialPoolerOCL(OCL):
    """
    OpenCl implementation of spatial pooler overlap
    """
    def __init__(self, number_of_columns, number_of_synapses, min_overlap, winners_set_size):
        in_data_size = np.zeros(number_of_columns*number_of_synapses).astype(np.bool).nbytes
        boost_data_size = np.zeros(number_of_columns).astype(np.float).nbytes

        build_defines = {
            'NUMBER_OF_COLUMNS': number_of_columns,
            'NUMBER_OF_SYNAPSES': number_of_synapses,
            'MIN_OVERLAP': min_overlap,
            'WINNERS_SET_SIZE': winners_set_size,
            'TOTAL_SYNAPSES': number_of_columns*number_of_synapses,
            'CONSTANT_OR_GLOBAL_IN_DATA':
                lambda: '__global' if in_data_size > self.device.max_constant_buffer_size else '__constant',
            'CONSTANT_OR_GLOBAL_BOOST_DATA':
                lambda: '__global' if boost_data_size > self.device.max_constant_buffer_size else '__constant',
            'OVERLAP_WORK_GROUPS_PER_COLUMN':
                lambda: int(math.ceil(number_of_synapses / min(number_of_synapses, self.device.max_work_group_size)))
        }

        super(SpatialPoolerOCL, self).__init__('spatial_pooler', build_defines)

        # we have two arrays to transfer
        self._host_to_dev_time = [0, 0]

    def __prepare_kernel(self, data, boost_coefficients, inhibition_radius):
        """
        Prepares data structures for the kernel.
        :return:
        """
        number_of_columns = self.build_defines['NUMBER_OF_COLUMNS']
        number_of_synapses = self.build_defines['NUMBER_OF_SYNAPSES']
        total_synapses = self.build_defines['TOTAL_SYNAPSES']

        data = np.array(data).astype(np.bool)
        boost_coefficients = np.array(boost_coefficients).astype(np.float32)

        self.__prepare_overlap_work_group(data, number_of_synapses, total_synapses)
        self.__prepare_overlap_check_work_group(number_of_columns)

        self.__prepare_input_data_buffers(boost_coefficients, data)
        self.__prepare_intermediate_data_buffers(number_of_columns)
        self.__prepare_output_data_buffers(number_of_columns)

        self.__prepare_inhibition_work_group(inhibition_radius, number_of_columns)

    def __prepare_overlap_check_work_group(self, number_of_columns):
        self.__overlap_check_work_group_size = min(number_of_columns,
                                                   self._get_kernel_info(self.program.sp_overlap_check, 'WORK_GROUP_SIZE'))

    def __prepare_inhibition_work_group(self, inhibition_radius, number_of_columns):
        self.__inhibition_work_group_size = min(number_of_columns,
                                                self._get_kernel_info(self.program.sp_inhibition, 'WORK_GROUP_SIZE'))
        inhibition_item_size = int(self.__overlap_vector.nbytes / number_of_columns)
        self.__local_inhibition_buf = cl.LocalMemory(
            inhibition_item_size * (self.__inhibition_work_group_size + inhibition_radius*2 + 1))

    def __prepare_overlap_work_group(self, data, number_of_synapses, total_synapses):
        self.__overlap_work_group_size = min(number_of_synapses,
                                             self._get_kernel_info(self.program.sp_overlap, 'WORK_GROUP_SIZE'))
        self.__overlap_work_groups_per_column = int(math.ceil(number_of_synapses / self.__overlap_work_group_size))
        overlap_item_size = int(data.nbytes / total_synapses)
        self.__local_overlap_buf = cl.LocalMemory(overlap_item_size * self.__overlap_work_group_size)

    def __prepare_input_data_buffers(self, boost_coefficients, data):
        self.__data_buf = cl.Buffer(self.ctx, cl.mem_flags.READ_ONLY, size=data.nbytes)
        self.__data_write_evt = cl.enqueue_write_buffer(self.queue, self.__data_buf, data)

        self.__boost_buf = cl.Buffer(self.ctx, cl.mem_flags.READ_ONLY, size=boost_coefficients.nbytes)
        self.__boost_write_evt = cl.enqueue_write_buffer(self.queue, self.__boost_buf, boost_coefficients)

    def __prepare_intermediate_data_buffers(self, number_of_columns):
        self.__overlap_vector = np.zeros(number_of_columns * self.__overlap_work_groups_per_column).astype(np.int32)
        self.__overlap_buf = cl.Buffer(self.ctx,
                                       cl.mem_flags.READ_WRITE,
                                       self.__overlap_vector.nbytes)

    def __prepare_output_data_buffers(self, number_of_columns):
        self.__output_vector = np.zeros(number_of_columns).astype(np.bool)
        self.__output_buf = cl.Buffer(self.ctx,
                                      cl.mem_flags.WRITE_ONLY,
                                      self.__output_vector.nbytes)

    def _get_output_buf_and_vec(self):
        return self.__output_buf, self.__output_vector

    def _execute_kernel(self, data, boost_coeff, inhibition_radius):
        number_of_columns = self.build_defines['NUMBER_OF_COLUMNS']

        self.__prepare_kernel(data, boost_coeff, inhibition_radius)

        overlap_evt = self.program.sp_overlap(
            self.queue,  # queue
            (np.int32(self.__overlap_work_group_size * self.__overlap_work_groups_per_column * number_of_columns), ),  # global_size
            (np.int32(self.__overlap_work_group_size), ),  # local_size
            self.__data_buf, self.__overlap_buf, self.__local_overlap_buf,
            wait_for=[self.__data_write_evt]
        )

        overlap_check_evt = self.program.sp_overlap_check(
            self.queue,
            (number_of_columns, ),
            (self.__overlap_check_work_group_size, ),
            self.__boost_buf, self.__overlap_buf,
            wait_for=[self.__boost_write_evt, overlap_evt]
        )

        inhibition_evt = self.program.sp_inhibition(
            self.queue,
            (number_of_columns, ),
            (self.__inhibition_work_group_size, ),
            self.__overlap_buf, self.__output_buf,
            np.int32(inhibition_radius), np.uint32(inhibition_radius * 2 + 1),
            self.__local_inhibition_buf,
            wait_for=[overlap_check_evt]
        )

        return [overlap_evt, overlap_check_evt, inhibition_evt]

    def _post_execute_kernel(self):
        if self._profile:
            self._host_to_dev_time[0] += self.__data_write_evt.profile.end - self.__data_write_evt.profile.start
            self._host_to_dev_time[1] += self.__boost_write_evt.profile.end - self.__boost_write_evt.profile.start
