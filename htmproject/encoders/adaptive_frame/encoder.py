import numpy
from skimage.transform import resize
from skimage.color import rgb2gray
from skimage.filters import threshold_otsu

from htmproject.interfaces.encoder import Encoder
from .encoder_params import params as encoder


class AdaptiveFrameEncoder(Encoder):
    def __init__(self, config):
        config.add_default_params_section('encoder', encoder)

        self._target_frame_size = config.get("encoder.frame_size")

    def encode(self, frame):
        """
        Encoding procedure.
        :param frame: frame frame to be encoded.
        :return:
        """

        if frame.shape[1] != self._target_frame_size[0] or \
                        frame.shape[0] != self._target_frame_size[1]:
            frame = resize(frame, self._target_frame_size, mode='edge')

        return self._convert_to_binary(frame)

    def _convert_to_binary(self, frame):
        gray = rgb2gray(frame)
        try:
            thresh = threshold_otsu(gray)
        except TypeError:
            thresh = numpy.full_like(gray, 0.5)
        binary = gray > thresh

        frame = binary.flatten()
        return frame

    def get_item_size(self):
        return self._target_frame_size[0] * self._target_frame_size[1]
