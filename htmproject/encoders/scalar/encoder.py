import random
import numpy

from htmproject.interfaces.encoder import Encoder
from .encoder_params import params as encoder_params


class ScalarEncoder(Encoder):
    """

    Scalar encoder

    @author
        Kymon
    @brief
        Scalar encoder for 1D distributed representation of scalar number
    @version
        1.0
    @variables
        @TODO
    @API

    """

    def __init__(self, config):
        """
        @brief
            Initialize encoder with configuration. See example configuration
            file examples/config/config.py (ScalarEncoder).
        @params
            aConfig
                configuration of the encoder
        @return
            None
        """
        self.config = config
        self.config.add_default_params_section('encoder', encoder_params)

        self.__window_size = int(self.config.get('encoder.window_size'))
        self.__width = int(self.config.get('encoder.width'))

    def get_item_size(self):
        return self.__width

    def spaceSize(self):
        """
        @breif
            Returns amount of available different distributed representation.
        @params
            None
        @return
            Number of possible representations
        """
        return self.__width - (self.__window_size - 1)
        # spaceSize()

    def encode(self, aVal, **kwargs):
        """
        @brief
            Encode aVal value to distributed representation.
        @params
            aVal
                Scalar value to encode
            kwargs
                @TODO
        @return
            bool[] Encoded value suitable as HTM input
        @detailed
            if aVal is too low or too high is automatically converted
            to min or max value for specified parameters.
        """
        aVal = int(aVal)
        if aVal < 0 :
            aVal = 0
        else:
            maxSize = self.spaceSize() - 1
            if aVal > maxSize :
                aVal = maxSize

        vector = []
        for i in range(self.__window_size):
            vector.append(i+aVal)

        # Add noise
        if bool(kwargs) is not False:
            for key, value in kwargs.items():
                if key == 'noise':
                    noiseBits = int(value * self.__window_size)
                    for i in range(noiseBits):
                        vector.remove( random.choice(vector) )
                    while noiseBits > 0:
                        newBit = random.randint(0, self.__window_size + 1)
                        if newBit in vector:
                            continue
                        else:
                            vector.append(newBit)
                            noiseBits -= 1

        out = numpy.zeros(self.get_item_size(), dtype=numpy.ubyte)
        out[vector] = 1

        return out > 0
        # encode()

    def print_e(self, aVal, addNoise = False):
        """
            @brief
                Print to stdout encoded vector aVal (debug)
            @params
                aVal
                    Set containing indexes of bits turned on
            @return
                None
        """
        dr = self.encode(aVal, noise=addNoise)
        string = ''.join(dr + 0)
        print(string)
        # printf()

