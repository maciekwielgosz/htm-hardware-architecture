params = {
    'htm': {
        'layers': {
            'min': 0,
            'max': 6,
            'default': 1
        },
        'columns_per_layer': {
            'min': 1,
            'max': 65536,
            'default': [
                128, 64, 32, 16, 8, 4
            ]
        },
        'cells_per_column': {
            'min': 0,
            'max': 64,
            'default': [
                1, 4, 4, 4, 4, 4
            ]
        }
    },
    'sp': {
        'overlap_history_buffer_length': {
            'min': 0,
            'max': 4096,
            'default': 128
        },
        'active_history_buffer_length': {
            'min': 0,
            'max': 4096,
            'default': 128
        },
        'min_overlap': {
            'min': 1,
            'max': 4096,
            'default': [2, 2, 2, 2, 2, 2]
        },
        'boost': {
            'min': 0,
            'max': 128,
            'default': 1
        },
        'perm_inc': {
            'min': 0,
            'max': 1,
            'step': 0.001,
            'default': 0.005
        },
        'perm_dec': {
            'min': 0,
            'max': 1,
            'step': 0.0001,
            'default': 0.0005
        },
        'init_perm': {
            'min': 0,
            'max': 1,
            'step': 0.01,
            'default': 0.5
        },
        'connected_perm': {
            'min': 0,
            'max': 1,
            'step': 0.01,
            'default': 0.5
        },
        'init_inhibition_radius': {
            'min': 1,
            'max': 128,
            'default': 3
        },
        'winners_set_size': {
            'min': 1,
            'max': 128,
            'default': [2, 2, 2, 2, 2, 2]
        },
        'synapses_per_column': {
            'min': 1,
            'max': 65536,
            'default': [
                256, 256, 256, 256, 256, 256
            ]
        }
    },
    'tp': {
        'max_number_of_segments': {
            'min': 1,
            'max': 128,
            'default': [16, 16, 16, 16, 16, 16]
        },
        'max_number_of_synapses_per_segment': {
            'min': 1,
            'max': 128,
            'default': [32, 32, 32, 32, 32, 32]
        },
        'segment_activation_threshold': {
            'min': 1,
            'max': 128,
            'default': [2, 2, 2, 2, 2, 2]
        },
        'min_segment_activation_threshold': {
            'min': 1,
            'max': 128,
            'default': [1, 1, 1, 1, 1, 1]
        },
        'max_number_of_new_connections': {
            'min': 1,
            'max': 128,
            'default': [4, 4, 4, 4, 4, 4]
        },
        'perm_inc': {
            'min': 0,
            'max': 1,
            'step': 0.001,
            'default': 0.005
        },
        'perm_dec': {
            'min': 0,
            'max': 1,
            'step': 0.0001,
            'default': 0.0005
        },
        'init_perm': {
            'min': 0,
            'max': 1,
            'step': 0.01,
            'default': 0.5
        },
        'connected_perm': {
            'min': 0,
            'max': 1,
            'step': 0.01,
            'default': 0.5
        },
        'work_group_size': {
            'min': 1,
            'max': 2048,
            'default': [256, 256, 256, 256, 256, 256]
        }
    }
}
