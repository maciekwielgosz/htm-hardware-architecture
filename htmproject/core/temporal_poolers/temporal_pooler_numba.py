from htmproject.core.temporal_poolers import TemporalPoolerBasic
from htmproject.ocls.numba import TemporalPoolerOCLNumba
from htmproject.ocls.lazy import LazyOCLList

import numpy as np
import numba as nb


class TemporalPoolerNumba(TemporalPoolerBasic):
    def __init__(self, *args, **kwargs):
        super(TemporalPoolerNumba, self).__init__(*args, **kwargs)

        self.layers = int(self._config.get('core.htm.layers'))
        self.columns_per_layer = [int(c) for c in self._config.get('core.htm.columns_per_layer')]
        self.cells_per_column = [int(c) for c in self._config.get('core.htm.cells_per_column')]
        self.segments_per_cell = [int(c) for c in self._config.get('core.tp.max_number_of_segments')]
        self.synapses_per_segment = [int(c) for c in self._config.get('core.tp.max_number_of_synapses_per_segment')]
        self.activation_threshold = [int(c) for c in self._config.get('core.tp.segment_activation_threshold')]
        self.min_activation_threshold = [int(c) for c in self._config.get('core.tp.min_segment_activation_threshold')]
        self.synapse_connected_permanence = float(self._config.get('core.tp.connected_perm'))
        self.max_new_synapses = [int(c) for c in self._config.get('core.tp.max_number_of_new_connections')]
        self.permanence_increment = float(self._config.get('core.tp.perm_inc'))
        self.permanence_decrement = float(self._config.get('core.tp.perm_dec'))
        self.initial_permanence = float(self._config.get('core.tp.init_perm'))
        self.work_group_size = [int(c) for c in self._config.get('core.tp.work_group_size')]

        self.__ocls = LazyOCLList(TemporalPoolerOCLNumba)
        for layer_index in range(0, self.layers):
                self.__ocls.append(
                    work_group_size=self.work_group_size[layer_index],
                    columns_per_layer=self.columns_per_layer[layer_index],
                    cells_per_column=self.cells_per_column[layer_index],
                    segments_per_cell=self.segments_per_cell[layer_index],
                    synapses_per_segment=self.synapses_per_segment[layer_index],
                    segment_activation_threshold=self.activation_threshold[layer_index],
                    segment_min_activation_threshold=self.min_activation_threshold[layer_index],
                    synapse_connected_permanence=self.synapse_connected_permanence * 100000,
                    max_new_synapses=self.max_new_synapses[layer_index],
                    synapse_permanence_increment=self.permanence_increment * 100000,
                    synapse_permanence_decrement=self.permanence_decrement * 100000,
                    synapse_initial_permanence=self.initial_permanence * 100000,
                )

        self.__output_from_tp = [None] * self.layers

    def update_layer_state(self, layer):
        """
        GPU accelerated temporal pooler.
        """
        layer_index = layer.get_layer_index()
        output_from_sp = np.array([column.is_active() for column in layer.get_columns()], np.uint8)
        ocl = self.__ocls[layer_index]
        self.__output_from_tp[layer_index] = ocl.execute_kernel(output_from_sp, layer.is_learning_enabled())

    def get_layer_output(self, layer):
        return self.__output_from_tp[layer.get_layer_index()]
