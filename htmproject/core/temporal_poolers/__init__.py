import logging
from .temporal_pooler_basic import TemporalPoolerBasic

try:
    from .temporal_pooler_multi_gpu import TemporalPoolerMultiGPU
    from .temporal_pooler_gpu_2 import TemporalPoolerGPU2
    from .temporal_pooler_gpu import TemporalPoolerGPU
except ImportError:
    logging.getLogger('htm').exception('No TemporalPoolerGPU')

try:
    from .temporal_pooler_numba import TemporalPoolerNumba
except ImportError:
    logging.getLogger('htm').exception('No TemporalPoolerNumba')

