from time import time

from htmproject.core.temporal_poolers import TemporalPoolerBasic
from htmproject.interfaces.ocl.exceptions import NoDeviceOCLException
from htmproject.ocls.tp import TemporalPoolerOCL
from htmproject.ocls.tp import TemporalPoolerOCL2


class TemporalPoolerGPU2(TemporalPoolerBasic):
    # TODO: Add ability to handle multiple layers of HTM!

    def __init__(self, *args, **kwargs):
        super(TemporalPoolerGPU2, self).__init__(*args, **kwargs)

        self.__layers = int(self._config.get('core.htm.layers'))
        self.__columns_per_layer = [int(c) for c in self._config.get('core.htm.columns_per_layer')]
        self.__cells_per_column = [int(c) for c in self._config.get('core.htm.cells_per_column')]
        self.__segments_per_cell = [int(c) for c in self._config.get('core.tp.max_number_of_segments')][0]
        self.__synapses_per_segment = [int(c) for c in self._config.get('core.tp.max_number_of_synapses_per_segment')][0]
        self.__activation_threshold = [int(c) for c in self._config.get('core.tp.segment_activation_threshold')][0]
        self.__min_activation_threshold = [int(c) for c in self._config.get('core.tp.min_segment_activation_threshold')][0]
        self.__synapse_connected_permanence = float(self._config.get('core.tp.connected_perm'))
        self.__max_new_synapses = [int(c) for c in self._config.get('core.tp.max_number_of_new_connections')][0]
        self.__permanence_increment = float(self._config.get('core.tp.perm_inc'))
        self.__permanence_decrement = float(self._config.get('core.tp.perm_dec'))
        self.__initial_permanence = float(self._config.get('core.tp.init_perm'))
        self.__work_group_size = [int(c) for c in self._config.get('core.tp.work_group_size')][0]

        self.settings = {
            'MAX_COLUMNS_PER_LAYER': self.__columns_per_layer[0],
            'MAX_CELLS_PER_COLUMN': self.__cells_per_column[0],
            'MAX_SEGMENTS_PER_CELL': self.__segments_per_cell,
            'MAX_SYNAPSES_PER_SEGMENT': self.__synapses_per_segment,
            'SEGMENT_ACTIVATION_THRESHOLD': self.__activation_threshold,
            'SEGMENT_MIN_ACTIVATION_THRESHOLD': self.__min_activation_threshold,
            'SYNAPSE_CONNECTED_PERMANENCE': self.__synapse_connected_permanence * 100000,
            'MAX_NEW_SYNAPSES': self.__max_new_synapses,
            'SYNAPSE_PERMANENCE_INCREMENT': self.__permanence_increment * 100000,
            'SYNAPSE_PERMANENCE_DECREMENT': self.__permanence_decrement * 100000,
            'SYNAPSE_INITIAL_PERMANENCE': self.__initial_permanence * 100000
        }

        self.build_defines = self.settings

        try:
            self.__ocl = TemporalPoolerOCL2(build_defines=self.build_defines, settings=self.settings, work_group_size=self.__work_group_size)
        except NoDeviceOCLException:
            self._logger.warning("There is no hardware accelerator.")
            self.update_layer_state = super(TemporalPoolerGPU2, self).update_layer_state
            self.get_layer_output = super(TemporalPoolerGPU2, self).get_layer_output

            self.__output_from_tp = [None] * self.__layers

            # TODO: only single layer handling is implemented in OCL
            settings = self.__get_settings(0, True)
            self.__ocl.prepare_kernel(self.__columns_per_layer[0], settings, self.__work_group_size)

    def __get_settings(self, layer_index, is_learning_active):
        return [self.__columns_per_layer[layer_index],
                self.__cells_per_column[layer_index],
                self.__segments_per_cell,
                self.__synapses_per_segment,
                is_learning_active,
                self.__activation_threshold,
                self.__min_activation_threshold,
                self.__synapse_connected_permanence * 100000,
                self.__max_new_synapses,
                self.__permanence_increment * 100000,
                self.__permanence_decrement * 100000,
                self.__initial_permanence * 100000]

    def update_layer_state(self, layer):
        """
        GPU accelerated temporal pooler.
        """
        layer_index = layer.get_layer_index()
        output_from_sp = [column.is_active() for column in layer.get_columns()]

        self.__output_from_tp[layer_index] = self.__ocl.execute_kernel(output_from_sp, layer.is_learning_enabled())

    def get_layer_output(self, layer):
        return self.__output_from_tp[layer.get_layer_index()]

    def get_total_elapsed_time(self):
        return self.__ocl.get_total_elapsed_time()
