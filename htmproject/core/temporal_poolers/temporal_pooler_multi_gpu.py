from htmproject.core.temporal_poolers import TemporalPoolerBasic
from htmproject.interfaces.ocl.exceptions import NoDeviceOCLException
from htmproject.ocls.tp import TemporalPoolerOCL2
from htmproject.ocls.lazy import LazyOCLList


class TemporalPoolerMultiGPU(object):
    def __init__(self, config):
        self._config = config
        self.__layers = int(self._config.get('core.htm.layers'))
        self.__columns_per_layer = [int(c) for c in self._config.get('core.htm.columns_per_layer')]
        self.__cells_per_column = [int(c) for c in self._config.get('core.htm.cells_per_column')]
        self.__segments_per_cell = [int(c) for c in self._config.get('core.tp.max_number_of_segments')]
        self.__synapses_per_segment = [int(c) for c in self._config.get('core.tp.max_number_of_synapses_per_segment')]
        self.__activation_threshold = [int(c) for c in self._config.get('core.tp.segment_activation_threshold')]
        self.__min_activation_threshold = [int(c) for c in self._config.get('core.tp.min_segment_activation_threshold')]
        self.__synapse_connected_permanence = float(self._config.get('core.tp.connected_perm'))
        self.__max_new_synapses = [int(c) for c in self._config.get('core.tp.max_number_of_new_connections')]
        self.__permanence_increment = float(self._config.get('core.tp.perm_inc'))
        self.__permanence_decrement = float(self._config.get('core.tp.perm_dec'))
        self.__initial_permanence = float(self._config.get('core.tp.init_perm'))
        self.__work_group_size = [int(c) for c in self._config.get('core.tp.work_group_size')]

        self.__ocls = LazyOCLList(self._create_ocl)

        for layer_index in range(0, self.__layers):
            self.settings = {
                'MAX_COLUMNS_PER_LAYER': self.__columns_per_layer[layer_index],
                'MAX_CELLS_PER_COLUMN': self.__cells_per_column[layer_index],
                'MAX_SEGMENTS_PER_CELL': self.__segments_per_cell[layer_index],
                'MAX_SYNAPSES_PER_SEGMENT': self.__synapses_per_segment[layer_index],
                'SEGMENT_ACTIVATION_THRESHOLD': self.__activation_threshold[layer_index],
                'SEGMENT_MIN_ACTIVATION_THRESHOLD': self.__min_activation_threshold[layer_index],
                'SYNAPSE_CONNECTED_PERMANENCE': self.__synapse_connected_permanence * 100000,
                'MAX_NEW_SYNAPSES': self.__max_new_synapses[layer_index],
                'SYNAPSE_PERMANENCE_INCREMENT': self.__permanence_increment * 100000,
                'SYNAPSE_PERMANENCE_DECREMENT': self.__permanence_decrement * 100000,
                'SYNAPSE_INITIAL_PERMANENCE': self.__initial_permanence * 100000
            }
            self.build_defines = self.settings

            self.__ocls.append(
                layer_index=layer_index,
                build_defines=self.build_defines,
                settings=self.settings,
                work_group_size=self.__work_group_size[layer_index]
            )

        self.__output_from_tp = [None] * self.__layers

    def _create_ocl(self, layer_index, *args, **kwargs):
        ocl = TemporalPoolerOCL2(*args, **kwargs)
        settings = self.__get_settings(layer_index, True)
        ocl.prepare_kernel(self.__columns_per_layer[layer_index], settings, self.__work_group_size[layer_index])
        return ocl

    def __get_settings(self, layer_index, is_learning_active):
        return [self.__columns_per_layer[layer_index],
                self.__cells_per_column[layer_index],
                self.__segments_per_cell[layer_index],
                self.__synapses_per_segment[layer_index],
                is_learning_active,
                self.__activation_threshold[layer_index],
                self.__min_activation_threshold[layer_index],
                self.__synapse_connected_permanence * 100000,
                self.__max_new_synapses[layer_index],
                self.__permanence_increment * 100000,
                self.__permanence_decrement * 100000,
                self.__initial_permanence * 100000]

    def update_layer_state(self, layer):
        """
        GPU accelerated temporal pooler.
        """
        layer_index = layer.get_layer_index()
        output_from_sp = [column.is_active() for column in layer.get_columns()]

        self.__output_from_tp[layer_index] = self.__ocls[layer_index].execute_kernel(output_from_sp, layer.is_learning_enabled())

    def get_layer_output(self, layer):
        return self.__output_from_tp[layer.get_layer_index()]

    def get_profile_info(self):
        return [ocl.get_total_elapsed_time() for ocl in self.__ocls]

