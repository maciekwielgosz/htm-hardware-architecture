from htmproject.core.object_oriented.column import Column
from htmproject.core.vectorized.column_connector import ColumnConnectorVectorized


class ColumnVectorized(Column):
    """Class ColumnVectorized
        """
    __slots__ = Column.__slots__

    def _init_for_sp(self, config, input_length, layer):
        self._connector = ColumnConnectorVectorized(self, config, input_length, layer.get_columns_amount(),
                                                    layer.get_synapses_amount(), layer.get_min_overlap())
