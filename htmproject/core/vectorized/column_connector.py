from htmproject.core.object_oriented.column_connector import ColumnConnector
import numpy


class ColumnConnectorVectorized(ColumnConnector):
    __slots__ = ('_config', '_column', '_input_length', '_columns_amount', '_overlap_history_buffer_length',
                 '_active_history_buffer_length', '_boost', '_inhibition_radius', '_number_of_synapses',
                 '_active_after_inhibition', '_overlap', '_min_overlap', '_max_duty_cycle', '_min_duty_cycle',
                 '_overlap_duty_history', '_active_duty_history', '_overlap_duty_cycle', '_active_duty_cycle',
                 '_init_perm', '_perm_inc', '_perm_dec', '_connected_perm', '_perm_boost', '_existing_connections',
                 '_perm_values')

    def __init__(self, column, config, *args, **kwargs):
        self._init_perm = float(config.get('core.sp.init_perm'))
        self._perm_inc = float(config.get('core.sp.perm_inc'))
        self._perm_dec = float(config.get('core.sp.perm_dec'))
        self._connected_perm = float(config.get('core.sp.connected_perm'))
        self._perm_boost = 1 + 0.1 * self._connected_perm

        super(ColumnConnectorVectorized, self).__init__(column, config, *args, **kwargs)

    def _generate_connections(self):
        self._existing_connections = numpy.nonzero(self._get_connected_indexes())
        self._perm_values = numpy.random.normal(
            loc=self._init_perm,
            scale=0.025,
            size=(self._number_of_synapses, )
        )

    def __get_zero_activity(self):
        return numpy.zeros((self._number_of_synapses,), dtype=bool)

    def boost_perm_values(self):
        if self._overlap_duty_cycle <= self._min_duty_cycle:
            # noinspection PyAttributeOutsideInit
            self._perm_values = numpy.minimum(self._perm_values * self._perm_boost, 1)

    def update_inhibition_radius(self):
        connected_synapses = numpy.nonzero(self._perm_values >= self._connected_perm)[0]

        if connected_synapses.shape[0]:
            reception_field = connected_synapses[-1] - connected_synapses[0]
            self._inhibition_radius = max(int(round(
                (self._columns_amount * reception_field) / (3 * self._number_of_synapses)
            )), 1)

    def update_synapses(self, input_item):
        received_input = input_item[self._existing_connections]
        to_increase = numpy.nonzero(received_input)
        to_decrease = numpy.nonzero(1 - received_input)

        self._perm_values[to_increase] = numpy.minimum(self._perm_values[to_increase] + self._perm_inc, 1)
        self._perm_values[to_decrease] = numpy.maximum(self._perm_values[to_decrease] - self._perm_dec, 0)

    def calculate_overlap(self, input_item):
        self._overlap = numpy.count_nonzero(self.get_synapses_activity(input_item))

    def get_synapses_activity(self, input_item):
        received_input = input_item[self._existing_connections]

        return numpy.logical_and(received_input, self._perm_values >= self._connected_perm)
