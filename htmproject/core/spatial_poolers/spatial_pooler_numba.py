from htmproject.core.spatial_poolers import SpatialPoolerBasic
from htmproject.ocls.numba import SpatialPoolerOCLNumba
from htmproject.ocls.lazy import LazyOCLList

import numpy as np
import numba as nb


class SpatialPoolerNumba(SpatialPoolerBasic):
    """

    """
    def __init__(self, config):
        super(SpatialPoolerNumba, self).__init__(config)
        self.__ocls = LazyOCLList(SpatialPoolerOCLNumba)
        for i in range(0, self._number_of_layers):
            self.__ocls.append(
                self._number_of_columns[i],
                self._number_of_synapses[i],
                self._min_overlap[i],
                self._winners_set_size[i]
            )

    def _inhibition(self, column_connectors, item, number_of_columns, layer_index, inhibition_radius):
        """
        Numba accelerated inhibition.
        """
        number_of_synapses = self._number_of_synapses[layer_index]
        synapses_activity = np.zeros(number_of_columns * number_of_synapses, np.int8)
        boost_coefficients = np.zeros(number_of_columns, np.int32)
        for x in range(number_of_columns):
            connector = column_connectors[x]
            boost_coefficients[x] = connector.get_boost()
            synapses_activity[number_of_synapses * x: number_of_synapses * (x+1)] = connector.get_synapses_activity(item)

        active_after_inhibition = self.__ocls[layer_index].execute_kernel(
            synapses_activity,
            boost_coefficients,
            inhibition_radius
        )

        for i in range(number_of_columns):
            if active_after_inhibition[i]:
                column_connectors[i].set_active_after_inhibition()

        return None

    def get_profile_info(self):
        return None
