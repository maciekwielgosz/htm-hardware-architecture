from htmproject.core.spatial_poolers import SpatialPoolerBasic
from htmproject.interfaces.ocl.exceptions import NoDeviceOCLException
from htmproject.ocls.sp import SpatialPoolerOCL
from htmproject.ocls.lazy import LazyOCLList


class SpatialPoolerGPU(SpatialPoolerBasic):
    """

    """
    def __init__(self, config):
        super(SpatialPoolerGPU, self).__init__(config)

        self.__ocls = LazyOCLList(SpatialPoolerOCL)
        try:
            for i in range(0, self._number_of_layers):
                self.__ocls.append(
                    self._number_of_columns[i],
                    self._number_of_synapses[i],
                    self._min_overlap[i],
                    self._winners_set_size[i]
                )
        except NoDeviceOCLException:
            self._logger.warning("There is no hardware accelerator.")
            self._inhibition = super(SpatialPoolerGPU, self)._inhibition

    def _inhibition(self, column_connectors, item, number_of_columns, layer_index, inhibition_radius):
        """
        GPU accelerated inhibition.
        :param number_of_columns:
        :param layer_index:
        :param inhibition_radius:
        """
        synapses_activity = []
        boost_coefficients = []

        for connector in column_connectors:
            boost_coefficients.append(connector.get_boost())
            synapses_activity += list(connector.get_synapses_activity(item))

        active_after_inhibition = self.__ocls[layer_index].execute_kernel(
            synapses_activity,
            boost_coefficients,
            inhibition_radius
        )

        for i in range(len(active_after_inhibition)):
            if active_after_inhibition[i]:
                column_connectors[i].set_active_after_inhibition()

        return None

    def get_profile_info(self):
        return [ocl.get_profile_info() for ocl in self.__ocls]
