import logging

import math
import numpy


class SpatialPoolerBasic(object):
    """

    """

    def __init__(self, config):
        self._config = config

        self._number_of_layers = int(config.get('core.htm.layers'))
        self._number_of_columns = config.get('core.htm.columns_per_layer')
        self._number_of_synapses = config.get('core.sp.synapses_per_column')
        self._min_overlap = config.get('core.sp.min_overlap')
        self._winners_set_size = config.get('core.sp.winners_set_size')

        self._logger = logging.getLogger('htm')

    def update_layer_state(self, layer, item):
        """function update_layer_state

        returns
        """
        layer_index = layer.get_layer_index()
        column_connectors = layer.get_column_connectors()
        number_of_columns = self._number_of_columns[layer_index]
        inhibition_radius = layer.get_sp_state().get_inhibition_radius()
        indexes_of_columns_in_inhibition_range = self._inhibition(column_connectors, item, number_of_columns,
                                                                  layer_index, inhibition_radius)

        for column_connector in column_connectors:
            column_connector.get_column().set_active(column_connector.is_active_after_inhibition())

        if layer.is_learning_enabled():
            if indexes_of_columns_in_inhibition_range is None:
                indexes_of_columns_in_inhibition_range = self.__get_all_indexes_of_columns_in_inhibition_range(
                    inhibition_radius, number_of_columns)
            self._learning(layer, column_connectors, item, indexes_of_columns_in_inhibition_range)

    @staticmethod
    def get_layer_output(layer):
        """
        Return active columns from a current layer.
        :param layer:
        :return numpy.array:
        """
        return numpy.array([c.is_active() for c in layer.get_columns()])

    def _get_columns_before_inhibition(self, column_connectors, item):
        """

        :param column_connectors:
        :param item:
        :return:
        """
        columns_boosted_overlaps = []
        for connector in column_connectors:
            connector.calculate_overlap(item)
            columns_boosted_overlaps.append(connector.get_boosted_overlap())

        return numpy.array(columns_boosted_overlaps)

    def _inhibition(self, column_connectors, item, number_of_columns, layer_index, inhibition_radius):
        """
        :param number_of_columns:
        :param layer_index:
        :param inhibition_radius:

        """
        columns_boosted_overlaps = self._get_columns_before_inhibition(column_connectors, item)
        indexes_of_columns_in_inhibition_range = self.__get_all_indexes_of_columns_in_inhibition_range(
            inhibition_radius, number_of_columns)

        threshold = 2*inhibition_radius + 1 - self._winners_set_size[layer_index]
        if threshold <= 0:
            for i in range(0, number_of_columns):
                if columns_boosted_overlaps[i]:
                    column_connectors[i].set_active_after_inhibition()
        else:
            for i in range(0, number_of_columns):
                if columns_boosted_overlaps[i]:
                    indexes = indexes_of_columns_in_inhibition_range[i]
                    counter = numpy.count_nonzero(columns_boosted_overlaps[indexes] < columns_boosted_overlaps[i])

                    if counter > threshold:
                        column_connectors[i].set_active_after_inhibition()

        return indexes_of_columns_in_inhibition_range

    def __get_all_indexes_of_columns_in_inhibition_range(self, inhibition_radius, number_of_columns):
        return [
            self.__get_indexes_of_columns_in_inhibition_range(i, inhibition_radius, number_of_columns)
            for i in range(0, number_of_columns)
        ]

    @staticmethod
    def _learning(layer, column_connectors, item, indexes_of_columns_in_inhibition_range):
        """
        This function performs all the learning steps. Updates permValues.

        :return:
        """
        sp_state = layer.get_sp_state()

        for column_connector in column_connectors:
            if column_connector.is_active_after_inhibition():
                column_connector.update_synapses(item)  # change permValues
            column_connector.update_active_duty_cycle()

        # iterate over inhibition radius and find max duty cycle
        active_duty_cycle_values = numpy.array([column_connector.get_active_duty_cycle()
                                                for column_connector in column_connectors])
        for i, column_connector in enumerate(column_connectors):
            indexes = indexes_of_columns_in_inhibition_range[i]
            neighbouring_max_active_duty_cycle = numpy.amax(active_duty_cycle_values[indexes])

            column_connector.set_max_and_min_duty_cycle(neighbouring_max_active_duty_cycle)

        inhibition_radiuses = []
        for column_connector in column_connectors:
            column_connector.update_boost()
            column_connector.update_overlap_duty_cycle()
            column_connector.boost_perm_values()
            column_connector.update_inhibition_radius()
            ir = column_connector.get_inhibition_radius()
            if ir is not None:
                inhibition_radiuses.append(ir)

        sp_state.set_inhibition_radius(int(round(numpy.average(inhibition_radiuses))))

    @staticmethod
    def __get_indexes_of_columns_in_inhibition_range(column_index, inhibition_radius, number_of_columns):
        indexes = numpy.mod(
            numpy.arange(
                column_index - inhibition_radius + number_of_columns,
                column_index + inhibition_radius + number_of_columns + 1
            ),
            number_of_columns
        )
        return indexes

    def get_profile_info(self):
        return None
