import logging
from .spatial_pooler_basic import SpatialPoolerBasic

try:
    from .spatial_pooler_gpu import SpatialPoolerGPU
except ImportError as e:
    logging.getLogger('htm').exception('No SpatialPoolerGPU')

try:
    from .spatial_pooler_numba import SpatialPoolerNumba
except ImportError:
    logging.getLogger('htm').exception('No SpatialPoolerNumba')

