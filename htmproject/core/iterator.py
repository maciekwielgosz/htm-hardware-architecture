# from profilehooks import profile

from .htm import HTM
from .exceptions import SetError
from htmproject.decoders.pass_through import PassThroughDecoder
from htmproject.encoders.pass_through import PassThroughEncoder
from htmproject.iostreamers.null import NullWriter


class Iterator(object):
    """Class Iterator
    """

    def __init__(self, config, reader=None, writer=None, encoder=None, decoder=None, htm=None):
        """

        :param Config config: Configuration object
        :param Reader reader: Object implementing Reader interface
        :param Writer writer: Object implementing Writer interface
        :param Encoder encoder: Object implementing Encoder interface
        :param Decoder decoder: Object implementing Decoder interface
        :param HTM htm: HTM instance (if not provided, empty HTM is created)
        :return:
        """
        self.__running = False

        self.__reader = reader
        self.__writer = writer if writer is not None else NullWriter()
        self.__encoder = encoder if encoder is not None else PassThroughEncoder(config)
        self.__decoder = decoder if decoder is not None else PassThroughDecoder()
        self.__htm = htm if htm is not None else HTM(config, self.__encoder.get_item_size())

        self.__item = None
        self.__encoded_item = None
        self.__encoded_output = None
        self.__output = None

        self.__elapsed_iterations = 0

    def get_htm(self):
        """

        :return:
        """
        return self.__htm

    def start(self):
        """
        Starts HTM iterations
        :return:
        """

        if self.__reader is None:
            raise ValueError('Reader is not set')

        self.__running = True
        self.__elapsed_iterations = 0

        self.__reader.open()
        self.__writer.open()

    def iterate(self):
        """
        Learning and testing iterations separated for profiling purposes.
        """
        if self.__htm.is_learning_enabled():
            self.__iterate_learning()
        else:
            self.__iterate_testing()

    def __iterate(self):
        self.iterate_input()
        self.iterate_output()

        self.__elapsed_iterations += 1

    # @profile
    def __iterate_learning(self):
        self.__iterate()

    # @profile
    def __iterate_testing(self):
        self.__iterate()

    def iterate_input(self):
        """
        Prepares input for iteration
        :return:
        """
        self.__item = self.__reader.get_next_data_item()
        self.__encoded_item = self.__encoder.encode(self.__item)

    def iterate_output(self):
        """
        Gets prepared input through HTM and saves output
        :return:
        """
        self.__encoded_output = self.__htm.get_output(self.__encoded_item)
        self.__output = self.__decoder.decode(self.__encoded_output)
        self.__writer.save_data_item(self.__output)

    def stop(self):
        """

        :return:
        """
        self.__reader.close()
        self.__writer.close()
        self.__running = False

    def get_input(self):
        """
        :return: Not-yet-encoded HTM input
        """
        return self.__item

    def get_output(self):
        """
        :return: HTM output after it passed through decoder
        """
        return self.__output

    def get_encoded_input(self):
        """
        :return: HTM input after it passed through encoder
        """
        return self.__encoded_item

    def get_encoded_output(self):
        """
        :return: Still-encoded HTM output
        """
        return self.__encoded_output

    def set_reader(self, reader):
        if not self.__running:
            self.__reader = reader
        else:
            raise SetError('Cannot set Reader while Iterator is running')

    def set_writer(self, writer):
        if not self.__running:
            self.__writer = writer
        else:
            raise SetError('Cannot set Writer while Iterator is running')

    def get_elapsed_iterations(self):
        return self.__elapsed_iterations
