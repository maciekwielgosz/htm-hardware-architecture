from htmproject.core.object_oriented.cell import Cell
from htmproject.core.object_oriented.column_connector import ColumnConnector


class Column(object):
    __slots__ = ('_connector', '_layer_index', '_column_index', 'cells', '_cells_amount',
                 '_previous_state', '_current_state')

    def __init__(self, layer, column_index, config, input_length, cells_amount=8):
        """

        :param int cells_amount: amount of cells per column
        """
        self._layer_index = layer.get_layer_index()
        self._column_index = column_index

        self._init_states()
        self._init_for_sp(config, input_length, layer)
        self._init_for_tp(cells_amount, config)

    def _init_for_tp(self, cells_amount, config):
        self.cells = []
        self._cells_amount = cells_amount
        for i in range(0, cells_amount):
            self.cells.append(Cell(self.get_column_index(), i, config))

    def _init_for_sp(self, config, input_length, layer):
        self._connector = ColumnConnector(self, config, input_length,
                                          layer.get_columns_amount(), layer.get_synapses_amount(),
                                          layer.get_min_overlap())

    def _init_states(self):
        self._previous_state = False
        self._current_state = False

    def set_active(self, state):
        """

        :param bool state:
        """
        self._current_state = state

    def is_active(self):
        """

        :rtype: bool
        :return:
        """
        return self._current_state

    def was_active(self):
        """

        :rtype: bool
        :return:
        """
        return self._previous_state

    def get_cells(self):
        """

        :rtype: Cell[]
        :return:
        """
        return self.cells

    def get_previous_best_matching_cell(self, layer):
        """

        :rtype: Cell
        :return:
        """

        tmp_segments = []

        for cell in self.get_cells():
            tmp_segments.append((cell, cell.get_previous_best_matching_segment(layer)))

        tmp_segments.sort(key=lambda s: s[1].get_current_active_synapses_count(True) if s[1] is not None else 0)

        return tmp_segments[0][0]

    def get_current_learning_cell(self):
        """

        :rtype: Cell
        :return:
        """
        for cell in self.get_cells():
            if cell.is_learning():
                return cell

    def get_previous_learning_cell(self):
        """

        :rtype: Cell
        :return:
        """
        for cell in self.get_cells():
            if cell.was_learning():
                return cell

    def shift_states(self):
        """

        """
        # noinspection PyAttributeOutsideInit
        self._previous_state, self._current_state = self._current_state, False
        # noinspection PyAttributeOutsideInit
        self.get_connector().shift_states()

        for cell in self.get_cells():
            cell.shift_states()

    def get_connector(self):
        """

        :rtype: ColumnConnector
        :return:
        """
        return self._connector

    # logging / debugging
    def get_cell_index(self, cell):
        """

        :param Cell cell:
        :rtype: int
        :return:
        """
        return self.cells.index(cell)

    def get_column_index(self):
        """

        :rtype: int
        :return:
        """
        return self._column_index

    def get_cells_amount(self):
        """
        Returns a number of cells in the column.
        :return: int
        """
        return self._cells_amount

    def get_current_state(self):
        """
        Returns current state of column.
        :return:
        """
        return self._current_state

    def get_previous_state(self):
        """
        Returns previous column state.
        :return:
        """
        return self._previous_state
