import numpy

from htmproject.core.object_oriented.synapse import Synapse


class SPSynapse(Synapse):
    """
    SP synapse implementation.
    """
    __slots__ = Synapse.__slots__

    def __init__(self, config, input_index):
        super(SPSynapse, self).__init__(config, 'sp')

        self._current_state.set_perm_value(numpy.random.normal(
            loc=self._current_state.get_perm_value(),
            scale=0.025
        ))
        self._input_index = input_index

    def is_active(self, input_item):
        """
        Synapse is active if connected input state is '1' and permValue > connectedPerm

        :param [] input_item:
        :return:
        """
        return bool(input_item[self._input_index]) and self._current_state.is_connected()

    def boost_perm_value(self):
        """
        Boost permValue of the synapse by 10% * connectedPerm.
        """
        self._current_state.boost_perm_value()

    def get_input_index(self):
        return self._input_index
