import numpy

from htmproject.core.object_oriented.sp_synapse import SPSynapse


class ColumnConnector(object):
    __slots__ = ('_config', '_column', '_input_length', '_columns_amount', '_overlap_history_buffer_length',
                 '_active_history_buffer_length', '_boost', '_inhibition_radius', '_number_of_synapses',
                 '_active_after_inhibition', '_overlap', '_min_overlap', '_max_duty_cycle', '_min_duty_cycle',
                 '_overlap_duty_history', '_active_duty_history', '_overlap_duty_cycle', '_active_duty_cycle',
                 '_synapses')
    """
    Holds synapses for each column.
    To be used with SP.
    """
    def __init__(self, column, config, input_length, columns_amount, no_of_synapses, min_overlap):
        self._column = column
        self._config = config
        self._input_length = input_length
        self._columns_amount = columns_amount

        self._overlap_history_buffer_length = int(self._config.get('core.sp.overlap_history_buffer_length'))
        self._active_history_buffer_length = int(self._config.get('core.sp.active_history_buffer_length'))
        self._boost = self._config.get('core.sp.boost')
        self._inhibition_radius = int(self._config.get('core.sp.init_inhibition_radius'))
        self._number_of_synapses = no_of_synapses

        self._active_after_inhibition = False
        self._overlap = 0  # None instead of 0, so it throws exception when trying to use uninitialized
        self._max_duty_cycle = 0
        self._min_duty_cycle = 0
        self._overlap_duty_history = []
        self._active_duty_history = []
        self._overlap_duty_cycle = 0
        self._active_duty_cycle = 0
        self._min_overlap = min_overlap

        self._generate_connections()

    def _generate_connections(self):
        """
        Generates input connections. Synapses are connected to some of the input bits.
        The input may come from both encoder and TP output.
        n - length of input data
        """
        input_indexes = self._get_connected_indexes()

        self._synapses = []
        for i in range(0, len(input_indexes)):
            if input_indexes[i]:
                self._synapses.append(SPSynapse(self._config, i))

    def _get_connected_indexes(self):
        # TODO: add locality bias - local inhibition ranges do not make much sense otherwise
        # maybe try 80-20 rule: 80% of synapses are in the 20% closest sector of input
        ones = numpy.ones(self._number_of_synapses, dtype=bool)
        zeros = numpy.zeros(self._input_length - self._number_of_synapses, dtype=bool)
        input_indexes = numpy.hstack((ones, zeros))
        numpy.random.shuffle(input_indexes)
        return input_indexes

    def get_boost(self):
        """
        :rtype: float
        :return: boost value
        """
        return self._boost

    def update_boost(self):
        """
        Calculates and set new boost value for column
        """
        adc = self.get_active_duty_cycle()
        mdc = self.get_min_duty_cycle()
        if not adc or not mdc or adc > mdc:
            self._boost = 1
        else:
            self._boost = mdc / adc

    def calculate_overlap(self, input_item):
        self._overlap = sum([int(y.is_active(input_item)) for y in self._synapses])

    def is_active_before_inhibition(self):
        """
        :rtype: bool
        :return: overlap > minOverlap
        """

        return self._overlap > self._min_overlap

    def get_boosted_overlap(self):
        """

        :return:
        """
        return self._overlap * self._boost if self.is_active_before_inhibition() else 0

    def set_active_after_inhibition(self):
        """
        Once the inhibition phase is done this is to be set if
        the column was chosen.
        """
        self._active_after_inhibition = True

    def shift_states(self):
        """
        Resets the flag after the current iteration is done.
        :return:
        """
        self._active_after_inhibition = False

    def is_active_after_inhibition(self):
        """
        The column was selected as active during inhibition phase.
        :rtype: bool
        :return: If the column is active after inhibition phase.
        """
        return self._active_after_inhibition

    def update_synapses(self, input_item):
        """
        Increments synapses which are connected to '1' and decrements the
        other ones.
        """
        for synapse in self._synapses:
            if input_item[synapse.get_input_index()]:
                synapse.inc_perm_value()
            else:
                synapse.dec_perm_value()

    def update_active_duty_cycle(self):
        """

        :return:
        """
        # TODO: implement _active_duty_history as collections.deque or WrappedQueue
        # update the history buffer
        if len(self._active_duty_history) < self._active_history_buffer_length:
            self._active_duty_history.append(self.is_active_after_inhibition())
        else:
            self._active_duty_history.remove(self._active_duty_history[0])
            self._active_duty_history.append(self.is_active_after_inhibition())

        # compute activeDutyCycle
        self._active_duty_cycle = sum(self._active_duty_history)

    def get_active_duty_cycle(self):
        """
        ActiveDutyCycle - number of activation of a given column in the decided time
        range.

        :return:
        """
        return self._active_duty_cycle

    def set_max_and_min_duty_cycle(self, max_duty_cycle):
        """

        :return:
        """
        self._max_duty_cycle = max_duty_cycle
        self._min_duty_cycle = 0.01 * self._max_duty_cycle

    def get_min_duty_cycle(self):
        """
        :return:
        """
        return self._min_duty_cycle

    def update_overlap_duty_cycle(self):
        """
        Updates the history buffer and computes overlapDutyCycle
        :return:
        """
        # TODO: implement _overlap_duty_history as collections.deque or WrappedQueue
        # update the history buffer
        if len(self._overlap_duty_history) < self._overlap_history_buffer_length:
            self._overlap_duty_history.append(self._overlap > self._min_overlap)
        else:
            self._overlap_duty_history.remove(self._overlap_duty_history[0])
            self._overlap_duty_history.append(self._overlap > self._min_overlap)

        # compute overlapDutyCycle
        self._overlap_duty_cycle = sum(self._overlap_duty_history)

    def get_overlap_duty_cycle(self):
        """

        :return:
        """
        return self._overlap_duty_cycle

    def boost_perm_values(self):
        """
        Increment permValues of all the synapses if overlapDutyCycle < minDutyCycle
        by 10%*connectedPerm. It is used to equalize activity of all the connected synapses.
        """
        if self._overlap_duty_cycle <= self._min_duty_cycle:
            for synapse in self._synapses:
                synapse.boost_perm_value()

    def update_inhibition_radius(self):
        """
        Computes new inhibition radius.
        """

        # find min value
        min_value = None
        for i in range(self._number_of_synapses):
            if self._synapses[i].is_connected():
                min_value = i
                break

        # find max value
        max_value = None
        for i in range(self._number_of_synapses - 1, 0, -1):
            if self._synapses[i].is_connected():
                max_value = i
                break

        if min_value is not None and max_value is not None:
            reception_field = max_value - min_value
            self._inhibition_radius = max(int(round(
                (self._columns_amount * reception_field) / (3 * self._number_of_synapses)
            )), 1)

    def get_inhibition_radius(self):
        """

        :rtype: int
        :return:
        """
        return self._inhibition_radius

    def get_max_duty_cycle(self):
        return self._max_duty_cycle

    def get_overlap(self):
        return self._overlap

    def get_active_duty_history(self):
        return self._active_duty_history

    def get_overlap_duty_history(self):
        return self._overlap_duty_history

    def get_column(self):
        """

        :rtype: Column
        :return:
        """
        return self._column

    def get_synapses(self):
        """

        :rtype: SPSynapse[]
        :return:
        """

        return self._synapses

    def get_synapses_amount(self):
        """

        :return:
        """
        return self._number_of_synapses

    def get_min_overlap(self):
        return self._min_overlap

    def get_synapses_activity(self, item):
        return [synapse.is_active(item) for synapse in self._synapses]









