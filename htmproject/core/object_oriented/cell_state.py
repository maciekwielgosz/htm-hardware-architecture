
class CellState(object):
    """

    """
    __slots__ = ('predictive', 'active', 'learning')

    def __init__(self):
        self.predictive = False  # (boolean)
        self.active = False  # (boolean)
        self.learning = False  # (boolean)
    
    # Operations
    def set_predictive(self, state):
        """

        :param state: bool
        :return:
        """
        self.predictive = state

    def is_predictive(self):
        """

        :return:
        """
        return self.predictive

    def set_active(self, state):
        """

        :param state: bool
        :return:
        """
        self.active = state
    
    def is_active(self):
        """

        :return:
        """

        return self.active

    def set_learning(self, state):
        """

        :param state:
        :return:
        """
        self.learning = state

    def is_learning(self):
        """

        :return:
        """
        return self.learning
