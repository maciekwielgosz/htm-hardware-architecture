from htmproject.core.object_oriented.synapse import Synapse


class TPSynapse(Synapse):
    """
    Synapse used in TP
    """
    __slots__ = Synapse.__slots__

    def __init__(self, segment_index, synapse_index, config, attached_cell):
        super(TPSynapse, self).__init__(config, 'tp')
        self._attached_cell = attached_cell
        self._previous_state = self._state_factory()
        self._segment_index = segment_index
        self._synapse_index = synapse_index

    def is_active(self, ignore_connected_perm=False):
        """

        :return:
        """
        return self._attached_cell.is_active() and (ignore_connected_perm or self._current_state.is_connected())

    def was_active(self, ignore_connected_perm=False):
        """

        :return:
        """
        return self._attached_cell.was_active() and (ignore_connected_perm or self._previous_state.is_connected())

    def get_attached_cell(self):
        """

        :return:
        """
        return self._attached_cell

    def was_connected(self):
        """

        :return:
        """
        return self._previous_state.is_connected()

    def shift_states(self):
        """

        """
        # self._previous_state, self._current_state = self._current_state, self._state_factory()
        self._previous_state, self._current_state = self._current_state, self._previous_state
        self._state_factory.reset(self._current_state)

    def is_learning(self, ignore_connected_perm=False):
        """

        :return:
        """
        return self._attached_cell.is_learning() and (ignore_connected_perm or self._current_state.is_connected())

    def was_learning(self, ignore_connected_perm=False):
        """

        :return:
        """
        return self._attached_cell.was_learning() and (ignore_connected_perm or self._previous_state.is_connected())

    # logging / debugging
    def get_synapse_index(self):
        return self._synapse_index

    def get_segment_index(self):
        return self._segment_index
