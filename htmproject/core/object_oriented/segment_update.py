from htmproject.core.object_oriented.update import Update


class SegmentUpdate(Update):
    """

    """

    def __init__(self,
                 synapses_to_be_updated=None,
                 cells_to_be_connected=None,
                 make_segment_sequential=False
                 ):
        super(SegmentUpdate, self).__init__()
        self.__synapses_to_be_updated = synapses_to_be_updated if synapses_to_be_updated is not None else []
        self.__cells_to_be_connected = cells_to_be_connected if cells_to_be_connected is not None else []
        self.__make_segment_sequential = make_segment_sequential

    def get_active_synapses_to_be_updated(self):
        """

        :return:
        """
        return self.__synapses_to_be_updated

    def get_cells_to_be_connected(self):
        """

        :return:
        """
        return self.__cells_to_be_connected

    def get_make_segment_sequential(self):
        """

        :return:
        """
        return self.__make_segment_sequential



