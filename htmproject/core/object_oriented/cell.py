from htmproject.core.object_oriented.cell_state import CellState
from htmproject.core.object_oriented.cell_update import CellUpdate


class Cell(object):
    """

    """
    __slots__ = ('config', 'cell_updates_queue', 'state', 'segments',
                 'min_activation_threshold', '_column_index', '_cell_index')

    def __init__(self, column_index, cell_index, config):
        """

        """
        self.config = config
        self.cell_updates_queue = []
        self.state = 0x00
        self.segments = []
        self.min_activation_threshold = [int(c) for c in self.config.get('core.tp.min_segment_activation_threshold')]
        # for logging/debugging purposes
        self._column_index = column_index
        self._cell_index = cell_index

    def get_segments(self):
        """

        :rtype: Segment[]
        :return:
        """
        return self.segments

    def was_predicted(self):
        """

        :rtype: bool
        :return:
        """

        return self.state & 0x20

    def is_predicted(self):
        """

        :rtype: bool
        :return:
        """
        return self.state & 0x02

    def set_predictive(self, state):
        """

        :param state:
        :return:
        """
        if state:
            self.state |= 0x02
        else:
            self.state &= 0xFD

    def was_active(self):
        """

        :rtype: bool
        :return:
        """
        return self.state & 0x10

    def is_active(self):
        """

        :rtype: bool
        :return:
        """
        return self.state & 0x01

    def set_active(self, state):
        """

        :param state:
        :return:
        """
        if state:
            self.state |= 0x01
        else:
            self.state &= 0xFE

    def was_learning(self):
        """

        :rtype: bool
        :return:
        """
        return self.state & 0x40

    def is_learning(self):
        """

        :rtype: bool
        :return:
        """
        return self.state & 0x04

    def set_learning(self, state):
        """

        :param state:
        :return:
        """
        if state:
            self.state |= 0x04
        else:
            self.state &= 0xFB

    def get_previous_active_segment(self):
        """

        :return:
        """
        active_segments = []

        for segment in self.segments:
            if segment.was_active():
                active_segments.append(segment)

        active_segments.sort(key=lambda s: s.get_current_active_synapses_count())

        for segment in active_segments:
            if segment.is_sequential():
                return segment
            else:
                active_segments.remove(segment)

        return active_segments[0] if len(active_segments) else None

    def get_previous_best_matching_segment(self, layer):
        """

        :return:
        """

        if self.segments:
            segment = max(self.segments, key=lambda s: s.get_current_active_synapses_count(True))
            if segment.get_current_active_synapses_count() > self.min_activation_threshold[layer.get_layer_index()]:
                return segment

        return None

    def empty_queue(self):
        """

        """
        self.cell_updates_queue = []
        # self.segments = self.segments[:255]

    def queue_new_segment(self, make_segment_sequential=False):
        """

        :param make_segment_sequential:
        :rtype: Segment
        :return:
        """
        update = CellUpdate(self.config, make_segment_sequential)
        self.cell_updates_queue.append(update)
        return update.get_segment()

    def apply_queued_changes(self, reinforcement=True, max_number_of_segments=1024, max_number_of_synapses=1024):
        """
        Increases or decreases permValues of synapses.
        If reinforcement is true active synapses have their permValues increased
        while all the other synapses have their values decreased.
        If reinforcement is false active synapses have their permValues decreased
        and all the other synapses remain the same.
        After permValues of the existing synapses are updated the queued new synapses
        are created with standard permValue.
        :param reinforcement: Boolean, True means positive reinforcement
        """

        for update in self.cell_updates_queue:
            segment = update.get_segment()
            i = len(self.segments)
            segment.set_segment_index(i)
            if i <= max_number_of_segments:
                self.segments.append(segment)
            else:
                pass  # TODO: add warning about filling up the network

        for segment in self.segments:
            segment.apply_queued_changes(reinforcement, max_number_of_synapses)

        self.empty_queue()

    def update_queue(self):
        """
        Decrease TTL (Time To Live) in queued updates and removes updates for
        which TTL reaches zero.
        :return:
        """
        new_cell_updates_queue = []

        for update in self.cell_updates_queue:
            update.decrease_TTL()
            if update.get_TTL() > 0:
                new_cell_updates_queue.append(update)

        self.cell_updates_queue = new_cell_updates_queue

        for segment in self.segments:
            segment.update_queue()

    def clear_queued_updates(self):
        self.empty_queue()

        for segment in self.segments:
            segment.empty_queue()

    def shift_states(self):
        """

        """
        self.state = (self.state << 4) & 0xFF

        for segment in self.segments:
            segment.shift_states()

    # logging / debugging
    def get_cell_index(self):
        """

        :return:
        """
        return self._cell_index

    def get_column_index(self):
        """

        :return:
        """
        return self._column_index

    def get_segment_index(self, segment):
        """

        :param segment:
        :return:
        """
        return self.segments.index(segment)

    def get_segments_amount(self):
        """
        Returns a number of segments in the cell.
        :return int: number of segments in the cell.
        """
        return len(self.segments)

    def get_current_state(self):
        """
        Returns current state of cell.
        :return:
        """
        return self.state

    def get_previous_state(self):
        """
        Returns previous state of cell.
        :return:
        """
        return self.state

    def __getstate__(self):
        state = [getattr(self, attr_name, None) for attr_name in self.__slots__]
        state[self.__slots__.index('segments')] = None  # It will be restored by Layer
        return state

    def __setstate__(self, state):
        for attr_name, attr_value in zip(self.__slots__, state):
            setattr(self, attr_name, attr_value)
