import random

from htmproject.core.object_oriented.segment_update import SegmentUpdate
from htmproject.core.object_oriented.tp_synapse import TPSynapse


class Segment(object):
    __slots__ = ('config', 'segment_updates_queue', 'sequential', 'synapses', 'activation_threshold',
                 'max_new_synapses', '_segment_index')
    """

    """

    def __init__(self, config, make_segment_sequential=False):

        self.config = config
        self.segment_updates_queue = []
        self.sequential = make_segment_sequential
        self.synapses = []
        self.activation_threshold = [int(c) for c in self.config.get('core.tp.segment_activation_threshold')][0]
        self.max_new_synapses = [int(c) for c in self.config.get('core.tp.max_number_of_new_connections')]
        self._segment_index = -1

    # Operations
    def get_current_active_synapses_count(self, ignore_connected_perm=False):
        """

        :param ignore_connected_perm:
        :return:
        """
        return sum((1 for synapse in self.synapses if synapse.is_active(ignore_connected_perm)))

    def __get_previous_active_synapses_count(self, ignore_connected_perm=False):
        """

        :param ignore_connected_perm:
        :return:
        """
        return sum((1 for synapse in self.synapses if synapse.was_active(ignore_connected_perm)))

    def get_current_learning_synapses_count(self, ignore_connected_perm=False):
        """

        :param ignore_connected_perm:
        :return:
        """
        return sum((1 for synapse in self.synapses if synapse.is_learning(ignore_connected_perm)))

    def __get_previous_learning_synapses_count(self, ignore_connected_perm=False):
        """

        :param ignore_connected_perm:
        :return:
        """
        # return sum((1 for _ in self.__get_previous_learning_synapses(ignore_connected_perm)))
        return sum((1 for synapse in self.synapses if synapse.was_learning(ignore_connected_perm)))

    def __get_current_active_synapses(self, ignore_connected_perm=False):
        """

        :param ignore_connected_perm:
        :return:
        """
        return (synapse for synapse in self.synapses if synapse.is_active(ignore_connected_perm))

    def __get_previous_active_synapses(self, ignore_connected_perm=False):
        """

        :param ignore_connected_perm:
        :return:
        """
        return (synapse for synapse in self.synapses if synapse.was_active(ignore_connected_perm))

    def __get_current_learning_synapses(self, ignore_connected_perm=False):
        """

        :param ignore_connected_perm:
        :return:
        """
        return (synapse for synapse in self.synapses if synapse.is_learning(ignore_connected_perm))

    def __get_previous_learning_synapses(self, ignore_connected_perm=False):
        """

        :param ignore_connected_perm:
        :return:
        """
        return (synapse for synapse in self.synapses if synapse.was_learning(ignore_connected_perm))

    def get_synapses(self):
        return self.synapses

    def is_active(self):
        """

        :rtype: bool
        :return:
        """

        return self.get_current_active_synapses_count() > self.activation_threshold

    def was_active(self):
        """

        :rtype: bool
        :return:
        """
        return self.__get_previous_active_synapses_count() > self.activation_threshold

    def is_sequential(self):
        """

        :return:
        """
        return self.sequential

    def set_sequential(self, state):
        """

        :param state: bool
        """
        self.sequential = state

    def was_learning(self):
        """

        :rtype: bool
        :return:
        """
        return self.__get_previous_learning_synapses_count() > self.activation_threshold

    def is_learning(self):
        """

        :rtype: bool
        :return:
        """
        return self.get_current_learning_synapses_count() > self.activation_threshold

    def queue_current_step_segment_updates(self,
                                           layer,
                                           make_segment_sequential=False,
                                           allow_new_synapses=False,
                                           cells_in_learning_state=None):
        """

        :param bool make_segment_sequential:
        :param bool allow_new_synapses:
        :param Cell[] cells_in_learning_state:
        """
        cells_in_learning_state = cells_in_learning_state if cells_in_learning_state is not None else []
        cells_to_be_connected = []
        active_synapses = list(self.__get_current_active_synapses(True))

        if allow_new_synapses:
            amount_to_create = self.max_new_synapses[layer.get_layer_index()] - len(active_synapses)
            amount_in_learning_state = len(cells_in_learning_state)
            if amount_to_create > 0 and amount_in_learning_state:
                for i in range(0, min(amount_to_create, amount_in_learning_state)):
                    random_index = random.randrange(amount_in_learning_state)
                    random_cell = cells_in_learning_state[random_index]
                    cells_to_be_connected.append(random_cell)
                    cells_in_learning_state.remove(random_cell)
                    amount_in_learning_state = len(cells_in_learning_state)

        self.segment_updates_queue.append(SegmentUpdate(
            active_synapses,
            cells_to_be_connected,
            make_segment_sequential)
        )

    def queue_previous_step_segment_updates(self,
                                            layer,
                                            make_segment_sequential=False,
                                            allow_new_synapses=False,
                                            cells_in_learning_state=None):
        """

        :param bool make_segment_sequential:
        :param bool allow_new_synapses:
        :param Cell[] cells_in_learning_state:
        """
        cells_in_learning_state = cells_in_learning_state if cells_in_learning_state is not None else []
        cells_to_be_connected = []
        active_synapses = list(self.__get_previous_active_synapses(True))

        if allow_new_synapses:
            amount_to_create = self.max_new_synapses[layer.get_layer_index()] - len(active_synapses)
            amount_in_learning_state = len(cells_in_learning_state)
            if amount_to_create > 0 and amount_in_learning_state:
                for i in range(0, min(amount_to_create, amount_in_learning_state)):
                    random_index = random.randrange(amount_in_learning_state)
                    random_cell = cells_in_learning_state[random_index]
                    cells_to_be_connected.append(random_cell)
                    cells_in_learning_state.remove(random_cell)
                    amount_in_learning_state = len(cells_in_learning_state)

        self.segment_updates_queue.append(SegmentUpdate(
            active_synapses,
            cells_to_be_connected,
            make_segment_sequential)
        )

    def empty_queue(self):
        """

        """
        self.segment_updates_queue = []

    def update_queue(self):
        """

        """

        new_segment_updates_queue = []

        for update in self.segment_updates_queue:
            update.decrease_TTL()
            if update.get_TTL() > 0:
                new_segment_updates_queue.append(update)

        self.segment_updates_queue = new_segment_updates_queue

    def apply_queued_changes(self, reinforcement=True, max_number_of_synapses=1024):
        """

        """
        for update in self.segment_updates_queue:
            # change segment function
            if update.get_make_segment_sequential():
                self.set_sequential(True)

            # update existing synapses
            active_synapses = update.get_active_synapses_to_be_updated()
            if reinforcement:
                for synapse in self.synapses:
                    if synapse in active_synapses:
                        synapse.inc_perm_value()
                    else:
                        synapse.dec_perm_value()
            else:
                for synapse in active_synapses:
                    synapse.dec_perm_value()

            # create new synapses
            for cell in update.get_cells_to_be_connected():
                i = len(self.synapses)
                if i <= max_number_of_synapses:
                    self.synapses.append(TPSynapse(self._segment_index, i, self.config, cell))
                else:
                    pass  # TODO: add warning about filling up the network

        self.empty_queue()

    def shift_states(self):
        """

        """
        for synapse in self.synapses:
            synapse.shift_states()


    # logging / debugging
    def get_synapse_index(self, synapse):
        return self.synapses.index(synapse)

    def set_segment_index(self, value):
        self._segment_index = value

    def get_segment_index(self):
        return self._segment_index
