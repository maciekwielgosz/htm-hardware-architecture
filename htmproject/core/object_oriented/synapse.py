from htmproject.core.object_oriented.synapse_state_factory import SynapseStateFactory


class Synapse(object):
    """

    """
    __slots__ = ('_config', '_state_factory', '_current_state', '_previous_state', '_attached_cell',
                 '_segment_index', '_synapse_index', '_input_index')

    def __init__(self, config, namespace):
        self._config = config
        self._state_factory = SynapseStateFactory(self._config, namespace)
        self._current_state = self._state_factory()

    def inc_perm_value(self):
        """

        :return:
        """
        self._current_state.inc_perm_value()

    def dec_perm_value(self):
        """

        :return:
        """
        self._current_state.dec_perm_value()

    def get_perm_value(self):
        return self._current_state.get_perm_value()

    def is_connected(self):
        return self._current_state.is_connected()
