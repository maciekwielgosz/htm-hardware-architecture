
class SynapseState(object):
    __slots__ = ('_perm_value', '_perm_inc', '_perm_dec', '_connected_perm')  # Reduce RAM usage and increase speed

    def __init__(self, perm_value=0.0, perm_inc=0.0, perm_dec=0.0, connected_perm=0.0):
        self._perm_value = perm_value
        self._perm_inc = perm_inc
        self._perm_dec = perm_dec
        self._connected_perm = connected_perm

    def get_perm_value(self):
        return self._perm_value

    def set_perm_value(self, value):
        self._perm_value = value

    def inc_perm_value(self):
        self._perm_value = min(self._perm_value + self._perm_inc, 1)  # perm_value does not exceed 1

    def dec_perm_value(self):
        self._perm_value = max(self._perm_value - self._perm_dec, 0)  # perm_value does not drop below 0

    def is_connected(self):
        return self._perm_value >= self._connected_perm

    def boost_perm_value(self):
        """
        It is used solely by SP to boost synapses' connections.
        Increment permValue by 10 % * connectedPerm
        """
        # TODO: decide between two options
        # increase perm value by a percent of max possible perm value (==1)
        # self.perm_value = min(self.perm_value + 0.1 * self.connected_perm, 1)
        # alternative: increase perm value by a percent of current value
        self._perm_value = min(self._perm_value * (1 + 0.1 * self._connected_perm), 1)

    def __getstate__(self):
        """
        Pickling helper
        """
        return [getattr(self, attribute_name, None) for attribute_name in self.__slots__]

    def __setstate__(self, data):
        """
        Unpickling helper
        """
        for attribute_name, attribute_value in zip(self.__slots__, data):
            setattr(self, attribute_name, attribute_value)
