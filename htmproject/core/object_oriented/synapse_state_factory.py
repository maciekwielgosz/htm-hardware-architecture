from htmproject.core.object_oriented.synapse_state import SynapseState


class SynapseStateFactory(object):
    __slots__ = ('_perm_value', '_perm_inc', '_perm_dec', '_connected_perm', '_args')

    def __init__(self, config, namespace):
        self._perm_value = float(config.get('core.%s.init_perm' % (namespace,)))

        self._perm_inc = float(config.get('core.%s.perm_inc' % (namespace,)))
        self._perm_dec = float(config.get('core.%s.perm_dec' % (namespace,)))
        self._connected_perm = float(config.get('core.%s.connected_perm' % (namespace,)))
        self._args = (self._perm_value, self._perm_inc, self._perm_dec, self._connected_perm)

    def __call__(self):
        return SynapseState(*self._args)

    def reset(self, state):
        state._perm_value = self._perm_value
        state._perm_inc = self._perm_inc
        state._perm_dec = self._perm_dec
        state._connected_perm = self._connected_perm
