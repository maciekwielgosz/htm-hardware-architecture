from htmproject.core.object_oriented.update import Update
from htmproject.core.object_oriented.segment import Segment


class CellUpdate(Update):
    """

    """
    def __init__(self, config, make_segment_sequential=False):
        """

        :return:
        """
        super(CellUpdate, self).__init__()
        self.segment = Segment(config, make_segment_sequential)

    def get_segment(self):
        """

        :return:
        """
        return self.segment
