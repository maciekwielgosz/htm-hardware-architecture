# from htmproject.core.object_oriented.column import Column
from htmproject.core.vectorized.column import ColumnVectorized as Column


from htmproject.core.spatial_pooler_state import SpatialPoolerState
import math


class Layer(object):
    """
    """

    def __init__(self, config, input_length, layer_index):
        self.__layer_index = layer_index
        self.__config = config

        self.__cells_per_column = int(config.get('core.htm.cells_per_column')[self.__layer_index])
        self.__columns_amount = int(config.get('core.htm.columns_per_layer')[self.__layer_index])

        self.__synapses_amount = int(config.get('core.sp.synapses_per_column')[self.__layer_index])
        self.__min_overlap = int(config.get('core.sp.min_overlap')[self.__layer_index])
        self.__winners_set_size = int(config.get('core.sp.winners_set_size')[self.__layer_index])
        self.__max_number_of_segments = int(config.get('core.tp.max_number_of_segments')[self.__layer_index])
        self.__max_number_of_synapses_per_segment = int(config.get('core.tp.max_number_of_synapses_per_segment')[self.__layer_index])

        # check if a number of columns is well defined i.e.
        # num_of_columns >= 0.5 * (num_inputs / num_of_synapses_per_column)
        threshold = 0.5 * (float(input_length) / self.__synapses_amount)

        if self.__columns_amount < threshold:
            raise ValueError("Layer %d: "
                             "Num. of columns is too low. "
                             "Should follow the formula: "
                             "num_of_columns >= 0.5 * (num_inputs / num_of_synapses_per_column) which is: %s"
                             % (self.__layer_index, int(math.ceil(threshold)),))
        self.columns = []
        self.__enable_learning = True

        self.__sp_state = SpatialPoolerState(self.__columns_amount)

        self.__enable_tp = self.__cells_per_column > 0

        for i in range(0, self.__columns_amount):
            self.columns.append(Column(self, i, config, input_length, self.__cells_per_column))

    def get_columns(self):
        """
        :rtype: Column[]
        :return: list of columns
        """

        return self.columns

    def get_columns_amount(self):
        """
        Returns a number of columns in the current layer.
        :return:
        """
        return self.__columns_amount

    def get_synapses_amount(self):
        return self.__synapses_amount

    def get_min_overlap(self):
        return self.__min_overlap

    def get_winners_set_size(self):
        return self.__winners_set_size

    def get_current_active_columns(self):
        """

        :rtype: Column[]
        :return:
        """
        active_columns = []

        for column in self.get_columns():
            if column.is_active():
                active_columns.append(column)

        return active_columns

    def get_previous_active_columns(self):
        """

        :rtype: Column[]
        :return:
        """
        active_columns = []

        for column in self.get_columns():
            if column.was_active():
                active_columns.append(column)

        return active_columns

    def toggle_learning(self, state):
        """
        Enable/disable learning of HTM layer.

        :param bool state: True if HTM layer should learn
        """
        self.__enable_learning = state

        # forget queued changes
        if not self.__enable_learning:
            for column in self.get_columns():
                for cell in column.get_cells():
                    cell.clear_queued_updates()

    def is_learning_enabled(self):
        """
        Checks if the HTM layer is currently learning.

        :rtype: bool
        :return:
        """

        return self.__enable_learning

    def is_tp_enabled(self):
        return self.__enable_tp

    def get_previous_learning_cells(self):
        """

        :rtype: Cell[]
        :return:
        """
        learning_cells = []
        for column in self.get_previous_active_columns():
            cell = column.get_previous_learning_cell()
            if cell is not None:
                learning_cells.append(cell)

        return learning_cells

    def get_current_learning_cells(self):
        """

        :rtype: Cell[]
        :return:
        """
        learning_cells = []
        for column in self.get_current_active_columns():
            cell = column.get_current_learning_cell()
            if cell is not None:
                learning_cells.append(cell)

        return learning_cells

    def shift_states(self):
        """

        """
        for column in self.get_columns():
            column.shift_states()

    def get_sp_state(self):
        """

        :rtype: SpatialPoolerState
        :return:
        """
        return self.__sp_state

    def get_column_connectors(self):
        """

        :rtype: ColumnConnector[]
        :return:
        """
        return [c.get_connector() for c in self.get_columns()]

    def get_maximum_numbers(self):
        return self.__max_number_of_segments, self.__max_number_of_synapses_per_segment

    # logging / debugging
    def get_column_index(self, column):
        """

        :param Column column:
        :return:
        """
        return self.columns.index(column)

    def get_layer_index(self):
        """

        :return:
        """
        return self.__layer_index

    def __getstate__(self):
        dct = {}
        dct.update(self.__dict__)
        column_cell_segments = []
        for column in self.columns:
            column_cell_segments.append([cell.segments for cell in column.cells])
        return dct, column_cell_segments  # Break graph cycle to enable pickling big layers

    def __setstate__(self, state):
        dct, column_cell_segments = state
        self.__dict__.update(dct)
        for column, cell_segments in zip(self.columns, column_cell_segments):
            for cell, segments in zip(column.cells, cell_segments):
                cell.segments = segments
