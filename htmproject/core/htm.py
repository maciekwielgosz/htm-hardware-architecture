import logging

from .layer import Layer

try:
    from .spatial_poolers import SpatialPoolerBasic as SpatialPooler
    # from .spatial_poolers import SpatialPoolerGPU as SpatialPooler
    from .temporal_poolers import TemporalPoolerBasic as TemporalPooler
    # from .temporal_poolers import TemporalPoolerGPU2 as TemporalPooler
    # from .temporal_poolers import TemporalPoolerMultiGPU as TemporalPooler
except ImportError:
    logger = logging.getLogger('htm')
    logger.warn('Cannot import hardware-accelerated poolers, using software ones instead.')
    from .spatial_poolers import SpatialPoolerBasic as SpatialPooler
    from .temporal_poolers import TemporalPoolerBasic as TemporalPooler


class HTM(object):
    """
    Class HTM
    """

    def __init__(self, config, input_length):
        self.__config = config
        self.__spatial_pooler, self.__temporal_pooler = self.__create_poolers(self.__config)

        self.__layers_amount = int(config.get('core.htm.layers'))
        self.__layers = []
        self.__enable_learning = True

        for i in range(0, self.__layers_amount):
            self.__layers.append(
                Layer(config, input_length, i)
            )
            input_length = self.__layers[-1].get_columns_amount()

    @staticmethod
    def __create_poolers(config):
        return SpatialPooler(config), TemporalPooler(config)

    def __getstate__(self):
        # state = self.__dict__
        state = {}
        state.update(self.__dict__)
        del state['_HTM__spatial_pooler']
        del state['_HTM__temporal_pooler']
        return state

    def __setstate__(self, state):
        state['_HTM__spatial_pooler'], state['_HTM__temporal_pooler'] = self.__create_poolers(state['_HTM__config'])
        self.__dict__ = state

    def get_output(self, encoded_item):
        """

        :param encoded_item: Input to the bottom layer
        :return: Output from the top layer
        :rtype: numpy.ndarray
        """
        output = encoded_item

        for index, layer in enumerate(self.__layers):
            layer.shift_states()
            self.__spatial_pooler.update_layer_state(layer, output)
            if layer.is_tp_enabled():
                self.__temporal_pooler.update_layer_state(layer)
                output = self.__temporal_pooler.get_layer_output(layer)
            else:
                output = self.__spatial_pooler.get_layer_output(layer)

            # print(index, output.sum())

        return output

    def get_output_size(self):
        """
        Returns a number of columns in the top layer.
        :return: int
        """
        self.get_layers()[-1].get_columns()

    def get_layers(self):
        """

        :rtype: Layer[]
        :return:
        """
        return self.__layers

    def get_sp(self):
        """
        :rtype: SpatialPooler
        :return:
        """
        return self.__spatial_pooler

    def get_tp(self):
        """
        :rtype: TemporalPooler
        :return:
        """
        return self.__temporal_pooler

    def toggle_learning(self, state):
        """
        Enable/disable learning of HTM.

        :param state: Bool True if HTM should learn
        :return:
        """
        self.__enable_learning = state

        for layer in self.__layers:
            layer.toggle_learning(state)

    def is_learning_enabled(self):
        """
        Checks if the whole HTM is currently learning.

        :rtype: Bool
        :return:
        """

        return self.__enable_learning

    # logging / debugging
    def get_layer_index(self, layer):
        """

        :param layer:
        :return:
        """
        return self.__layers.index(layer)

    def get_total_elapsed_time(self):
        time = []
        for layer in self.__layers:
            time.append(self.__temporal_pooler.get_total_elapsed_time(layer))
        return time
