import unittest
from tests.UnitTests.tp_unit_test import OclTp
import numpy as np
import pyopencl as cl
from settings import Settings

class TestPhaseZero(unittest.TestCase):
    def setUp(self):
        print "Test case: ", self.__module__, self._testMethodName

        self.settings_manager = Settings('configs/test_config.json')

    def tearDown(self):
        pass

    def test_basic(self):
        self.ocl_temporalpooler = OclTp('temporal_pooler', "-D PHASEONE -D PHASETWO -D PHASETHREE -D PHASEFOUR", work_group_size=self.settings_manager.work_group_size)

        self.ocl_temporalpooler.prepare_buffers(self.settings_manager.columns_amount[0], self.settings_manager.get_settings())
        self.ocl_temporalpooler.cellStateData = np.ones(self.ocl_temporalpooler.cellStateData.size).astype(np.uint8)
        self.ocl_temporalpooler._cellStateBuffer = cl.Buffer(self.ocl_temporalpooler.ctx, cl.mem_flags.READ_WRITE | cl.mem_flags.COPY_HOST_PTR, hostbuf=self.ocl_temporalpooler.cellStateData)

        self.ocl_temporalpooler.execute_kernel([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1])
        self.ocl_temporalpooler.collect_data()
        print self.ocl_temporalpooler.cellStateData
        self.assertTrue(np.all(self.ocl_temporalpooler.cellStateData == 16))

