from __future__ import print_function

import unittest
import random

import sys, os  # Fixes
P = '/home/robertsitko/htm-hardware-architecture'
if P in sys.path:
    sys.path.remove(P)
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))


from htmproject.configs.json import JSONConfig
from htmproject.core import HTM
from htmproject.core.spatial_poolers import SpatialPoolerBasic
from htmproject.core.spatial_poolers import SpatialPoolerNumba
from htmproject.core.spatial_poolers import SpatialPoolerGPU
import numpy as np
import os

np.set_printoptions(threshold=np.nan)


class TestSP(unittest.TestCase):
    def setUp(self):
        print("Test case: ", self.__module__, self._testMethodName)
        self.settings_manager = JSONConfig(os.path.join(os.path.dirname(__file__), 'configs', 'test_config2.json'))

    def tearDown(self):
        pass

    def _reset_random(self):
        np.random.seed(42)
        random.seed(42)

    def test_basic(self):
        self._reset_random()
        htm1 = HTM(self.settings_manager, 960*540)
        layer1 = htm1.get_layers()[0]
        spb = SpatialPoolerBasic(self.settings_manager)
        htm1._HTM__spatial_pooler = spb
        self._reset_random()
        htm2 = HTM(self.settings_manager, 960*540)
        layer2 = htm2.get_layers()[0]
        spn = SpatialPoolerNumba(self.settings_manager)
        htm2._HTM__spatial_pooler = spn
        self._reset_random()
        htm3 = HTM(self.settings_manager, 960*540)
        layer3 = htm3.get_layers()[0]
        spg = SpatialPoolerGPU(self.settings_manager)
        htm3._HTM__spatial_pooler = spg
        items = [np.array([random.random() < (x / 20.0) for _ in range(960*540)], np.bool) for x in range(1, 11)]
        self._reset_random()
        for item in items:
            print('~', end='')
            spb.update_layer_state(layer1, item)
            self._reset_random()
            out1 = spb.get_layer_output(layer1)
            self._reset_random()
            spn.update_layer_state(layer2, item)
            self._reset_random()
            out2 = spb.get_layer_output(layer2)
            self._reset_random()
            spg.update_layer_state(layer3, item)
            self._reset_random()
            out3 = spn.get_layer_output(layer3)
            self._reset_random()
            # self.assertTrue(np.array_equal(out1, out3),'\n%r\n%r\n%r' % (1, out1.astype(np.int32), out3.astype(np.int32)))
            #self.assertTrue(np.array_equal(out2, out1), '\n%r\n%r\n%r' % (1, out2.astype(np.int32), out1.astype(np.int32)))  # item.astype(np.int32)
            self.assertTrue(np.array_equal(out2, out3), '\n%r\n%r\n%r' % (1, out2.astype(np.int32), out3.astype(np.int32)))
            # raise Exception


if __name__ == '__main__':
    unittest.main()
