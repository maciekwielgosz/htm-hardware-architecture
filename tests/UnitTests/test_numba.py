from __future__ import print_function
import sys, os  # Fixes
P = '/home/robertsitko/htm-hardware-architecture'
if P in sys.path:
    sys.path.remove(P)
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))


from htmproject.configs.json import JSONConfig
from htmproject.core import HTM
from htmproject.core.spatial_poolers import SpatialPoolerBasic
from htmproject.core.spatial_poolers import SpatialPoolerNumba
from htmproject.core.spatial_poolers import SpatialPoolerGPU
from htmproject.core.temporal_poolers import TemporalPoolerBasic
from htmproject.core.temporal_poolers import TemporalPoolerNumba
from htmproject.core.temporal_poolers import TemporalPoolerMultiGPU
import os
import unittest
import random
import gc
import logging
import numpy as np
import pyopencl.tools


np.set_printoptions(threshold=np.nan)


class TestNumba(unittest.TestCase):
    def setUp(self):
        pyopencl.tools.clear_first_arg_caches()
        gc.collect()
        print("Test case: ", self.__module__, self._testMethodName)
        self.settings_manager = JSONConfig(os.path.join(os.path.dirname(__file__), 'configs', 'test_config3.json'))
        f = open(os.path.join(os.path.dirname(__file__), 'test_numba.dat'), 'r')
        g = {}
        g.update(np.__dict__)
        g.update(globals())
        self.reference_data = eval(f.read(), g, {})
        f.close()

    def tearDown(self):
        pyopencl.tools.clear_first_arg_caches()
        gc.collect()

    def _reset_random(self):
        np.random.seed(42)
        random.seed(42)

    def _compare_arrays(self, a, b, errors=0, warning=0, msg=''):
        c = (a == b).astype(np.int32)
        s = (1 - c).sum()
        self.assertLessEqual(s, errors, '%s\n%r\n%r\n%r' % (msg, a.astype(np.int32), b.astype(np.int32), c))
        if warning < s <= errors:
            print('%s: Difference warning: %r' % (msg, s), file=sys.stderr)

    def test_1_tp(self):
        self._reset_random()
        htm1 = HTM(self.settings_manager, 960*540)
        layer1 = htm1.get_layers()[0]
        spb = SpatialPoolerBasic(self.settings_manager)
        htm1._HTM__spatial_pooler = spb
        tpb = TemporalPoolerBasic(self.settings_manager)
        htm1._HTM__temporal_pooler = tpb
        self._reset_random()
        htm2 = HTM(self.settings_manager, 960*540)
        layer2 = htm2.get_layers()[0]
        # spn = SpatialPoolerNumba(self.settings_manager)
        spn = SpatialPoolerBasic(self.settings_manager)
        htm2._HTM__spatial_pooler = spn
        tpn = TemporalPoolerNumba(self.settings_manager)
        htm2._HTM__temporal_pooler = tpn
        self._reset_random()
        htm3 = HTM(self.settings_manager, 960*540)
        layer3 = htm3.get_layers()[0]
        # spg = SpatialPoolerGPU(self.settings_manager)
        spg = SpatialPoolerBasic(self.settings_manager)
        htm3._HTM__spatial_pooler = spg
        tpg = TemporalPoolerMultiGPU(self.settings_manager)
        htm3._HTM__temporal_pooler = tpg

        self._reset_random()
        items = [np.array([random.random() < ((x + 1) / 20.0) for _ in range(960 * 540)], np.bool) for x in range(20)]
        self._reset_random()
        for i, item in enumerate(items):
            print(i % 10, end='')
            self._reset_random()

            layer1.shift_states()
            spb.update_layer_state(layer1, item)
            tpb.update_layer_state(layer1)
            self._reset_random()
            out1 = np.array(tpb.get_layer_output(layer1))
            self._reset_random()

            layer2.shift_states()
            spn.update_layer_state(layer2, item)
            tpn.update_layer_state(layer2)
            self._reset_random()
            out2 = np.array(tpn.get_layer_output(layer2))
            self._reset_random()

            layer3.shift_states()
            spg.update_layer_state(layer3, item)
            tpg.update_layer_state(layer3)
            self._reset_random()
            out3 = np.array(tpg.get_layer_output(layer3))
            self._reset_random()

            out4 = self.reference_data[0][i][0]
            out5 = self.reference_data[0][i][1]
            self._compare_arrays(out1, out3, 0, 0, '%d) TP Basic GPU  ' % i)
            self._compare_arrays(out2, out1, 0, 0, '%d) TP Numba Basic' % i)
            self._compare_arrays(out2, out3, 0, 0, '%d) TP Numba GPU  ' % i)
            self._compare_arrays(out1, out4, 0, 0, '%d) TP Basic Ref  ' % i)
            self._compare_arrays(out3, out5, 0, 0, '%d) TP GPU   Ref  ' % i)
        print()

    def test_2_sp(self):
        self._reset_random()
        htm1 = HTM(self.settings_manager, 960 * 540)
        layer1 = htm1.get_layers()[0]
        spb = SpatialPoolerBasic(self.settings_manager)
        htm1._HTM__spatial_pooler = spb
        self._reset_random()
        htm2 = HTM(self.settings_manager, 960 * 540)
        layer2 = htm2.get_layers()[0]
        spn = SpatialPoolerNumba(self.settings_manager)
        htm2._HTM__spatial_pooler = spn
        self._reset_random()
        htm3 = HTM(self.settings_manager, 960 * 540)
        layer3 = htm3.get_layers()[0]
        spg = SpatialPoolerGPU(self.settings_manager)
        htm3._HTM__spatial_pooler = spg

        self._reset_random()
        items = [np.array([random.random() < ((x + 1) / 20.0) for _ in range(960 * 540)], np.bool) for x in range(20)]
        self._reset_random()

        for i, item in enumerate(items):
            print(i % 10, end='')
            self._reset_random()

            layer1.shift_states()
            spb.update_layer_state(layer1, item)
            self._reset_random()
            out1 = np.array(spb.get_layer_output(layer1))
            self._reset_random()

            layer2.shift_states()
            spn.update_layer_state(layer2, item)
            self._reset_random()
            out2 = np.array(spn.get_layer_output(layer2))
            self._reset_random()

            layer3.shift_states()
            spg.update_layer_state(layer3, item)
            self._reset_random()
            out3 = np.array(spg.get_layer_output(layer3))
            self._reset_random()

            out4 = self.reference_data[1][i][0]
            out5 = self.reference_data[1][i][1]
            self._compare_arrays(out1, out3, 5, 0, '%d) SP Basic GPU  ' % i)
            self._compare_arrays(out2, out1, 5, 0, '%d) SP Numba Basic' % i)
            self._compare_arrays(out2, out3, 0, 0, '%d) SP Numba GPU  ' % i)
            self._compare_arrays(out1, out4, 0, 0, '%d) SP Basic Ref  ' % i)
            self._compare_arrays(out3, out5, 0, 0, '%d) SP GPU   Ref  ' % i)
        print()

    def test_3_combo(self):
        self._reset_random()
        htm1 = HTM(self.settings_manager, 960*540)
        layer1 = htm1.get_layers()[0]
        spb = SpatialPoolerBasic(self.settings_manager)
        htm1._HTM__spatial_pooler = spb
        tpb = TemporalPoolerBasic(self.settings_manager)
        htm1._HTM__temporal_pooler = tpb
        self._reset_random()
        htm2 = HTM(self.settings_manager, 960*540)
        layer2 = htm2.get_layers()[0]
        spn = SpatialPoolerNumba(self.settings_manager)
        htm2._HTM__spatial_pooler = spn
        tpn = TemporalPoolerNumba(self.settings_manager)
        htm2._HTM__temporal_pooler = tpn
        self._reset_random()
        htm3 = HTM(self.settings_manager, 960*540)
        layer3 = htm3.get_layers()[0]
        spg = SpatialPoolerGPU(self.settings_manager)
        htm3._HTM__spatial_pooler = spg
        tpg = TemporalPoolerMultiGPU(self.settings_manager)
        htm3._HTM__temporal_pooler = tpg

        self._reset_random()
        items = [np.array([random.random() < ((x + 1) / 20.0) for _ in range(960 * 540)], np.bool) for x in range(20)]
        self._reset_random()
        for i, item in enumerate(items):
            print(i % 10, end='')
            self._reset_random()

            layer1.shift_states()
            spb.update_layer_state(layer1, item)
            tpb.update_layer_state(layer1)
            self._reset_random()
            out1 = np.array(tpb.get_layer_output(layer1))
            self._reset_random()

            layer2.shift_states()
            spn.update_layer_state(layer2, item)
            tpn.update_layer_state(layer2)
            self._reset_random()
            out2 = np.array(tpn.get_layer_output(layer2))
            self._reset_random()

            layer3.shift_states()
            spg.update_layer_state(layer3, item)
            tpg.update_layer_state(layer3)
            self._reset_random()
            out3 = np.array(tpg.get_layer_output(layer3))
            self._reset_random()

            out4 = self.reference_data[2][i][0]
            out5 = self.reference_data[2][i][1]
            self._compare_arrays(out1, out3, 5, 0, '%d) Combo Basic GPU  ' % i)
            self._compare_arrays(out2, out1, 5, 0, '%d) Combo Numba Basic' % i)
            self._compare_arrays(out2, out3, 0, 0, '%d) Combo Numba GPU  ' % i)
            self._compare_arrays(out1, out4, 0, 0, '%d) Combo Basic Ref  ' % i)
            self._compare_arrays(out3, out5, 0, 0, '%d) Combo GPU   Ref  ' % i)
        print()

    def test_4_stack(self):
        self._reset_random()
        htm1 = HTM(self.settings_manager, 960*540)
        htm1._HTM__spatial_pooler = SpatialPoolerBasic(self.settings_manager)
        htm1._HTM__temporal_pooler = TemporalPoolerBasic(self.settings_manager)
        self._reset_random()
        htm2 = HTM(self.settings_manager, 960*540)
        htm2._HTM__spatial_pooler = SpatialPoolerNumba(self.settings_manager)
        htm2._HTM__temporal_pooler = TemporalPoolerNumba(self.settings_manager)
        self._reset_random()
        htm3 = HTM(self.settings_manager, 960*540)
        htm3._HTM__spatial_pooler = SpatialPoolerGPU(self.settings_manager)
        htm3._HTM__temporal_pooler = TemporalPoolerMultiGPU(self.settings_manager)

        self._reset_random()
        items = [np.array([random.random() < ((x + 1) / 20.0) for _ in range(960 * 540)], np.bool) for x in range(20)]
        self._reset_random()
        for i, item in enumerate(items):
            print(i % 10, end='')
            self._reset_random()
            out1 = np.array(htm1.get_output(item))
            self._reset_random()
            out2 = np.array(htm2.get_output(item))
            self._reset_random()
            out3 = np.array(htm3.get_output(item))
            self._reset_random()

            out4 = self.reference_data[3][i][0]
            out5 = self.reference_data[3][i][1]
            self._compare_arrays(out1, out3, 0, 0, '%d) Stack Basic GPU  ' % i)
            self._compare_arrays(out2, out1, 0, 0, '%d) Stack Numba Basic' % i)
            self._compare_arrays(out2, out3, 0, 0, '%d) Stack Numba GPU  ' % i)
            self._compare_arrays(out1, out4, 0, 0, '%d) Stack Basic Ref  ' % i)
            self._compare_arrays(out3, out5, 0, 0, '%d) Stack GPU   Ref  ' % i)
        print()


if __name__ == '__main__':
    # logging.basicConfig(level=logging.DEBUG)
    # os.environ.update(PYOPENCL_DEVICE_INDEX='2')
    unittest.main()
