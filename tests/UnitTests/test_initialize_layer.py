import unittest
import numpy as np
from tp_unit_test import OclTp
from settings import Settings

class TestInitializeLayer(unittest.TestCase):
    def setUp(self):
        print "Test case: ", self.__module__, self._testMethodName

        self.settings_manager = Settings('configs/basic.json')

    def tearDown(self):
        pass

    def test_basic(self):
        self.ocl_temporalpooler = OclTp('temporal_pooler', work_group_size=self.settings_manager.work_group_size)

        self.ocl_temporalpooler.prepare_buffers(self.settings_manager.columns_amount[0],
                                                self.settings_manager.get_settings())

        self.ocl_temporalpooler.prepare_kernel(0, 0)

        self.ocl_temporalpooler.collect_data()

        self.assertEqual(np.count_nonzero(self.ocl_temporalpooler.cellStateData), 0)
        self.assertEqual(np.count_nonzero(self.ocl_temporalpooler.hasCellQueuedChangesData), 0)
        self.assertEqual(np.count_nonzero(self.ocl_temporalpooler.newSegmentQueuedCountData), 0)
        self.assertEqual(np.count_nonzero(self.ocl_temporalpooler.segmentsCountData), 0)
        self.assertEqual(np.count_nonzero(self.ocl_temporalpooler.isSegmentSequentialData), 0)
        self.assertEqual(np.count_nonzero(self.ocl_temporalpooler.hasSegmentQueuedChangesData), 0)
        self.assertEqual(np.count_nonzero(self.ocl_temporalpooler.sequenceSegmentQueuedData), 0)
        self.assertEqual(np.count_nonzero(self.ocl_temporalpooler.synapsesCountData), 0)
        self.assertEqual(np.count_nonzero(self.ocl_temporalpooler.newSynapsesCountData), 0)
        self.assertEqual(np.count_nonzero(self.ocl_temporalpooler.synapsePermanenceData), 0)
        self.assertEqual(np.count_nonzero(self.ocl_temporalpooler.synapsePermanenceQueuedData), 0)
        self.assertEqual(np.count_nonzero(self.ocl_temporalpooler.synapseTargetColumnData), 0)
        self.assertEqual(np.count_nonzero(self.ocl_temporalpooler.synapseTargetCellData), 0)
