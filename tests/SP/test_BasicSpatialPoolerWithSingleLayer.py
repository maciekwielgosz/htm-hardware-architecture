import unittest

from htmproject.core.layer import Layer
from htmproject.core.spatial_poolers import SpatialPoolerBasic
from tests.SP.mock_config import mock_config


class BasicSpatialPoolerWithSingleLayerTestCase(unittest.TestCase):
    def setUp(self):
        self.layer = Layer(mock_config, 20, 0)
        self.sp = SpatialPoolerBasic(mock_config)

    def test_initially_has_no_active_columns(self):
        expected = [False] * 10
        actual = list(self.sp.get_layer_output(self.layer))
        self.assertListEqual(expected, actual)


suite = unittest.TestLoader().loadTestsFromTestCase(BasicSpatialPoolerWithSingleLayerTestCase)
