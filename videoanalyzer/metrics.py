import time
import numpy as np

from .datasets import Dataset


# Use? http://scikit-learn.org/stable/modules/model_evaluation.html


class Metric(object):
    """
    Base class for the metrics
    """

    name = ''  # type: str  # Human readable name of the metric

    def __init__(self):
        pass

    def start(self, dataset):
        """
        Reset internal state and prepare for new tests

        :param Dataset dataset: dataset that will be processed by adapter
        """

    def add_test(self, data, expected_output, output):
        """
        Store test result

        :param str data: tested data
        :param int expected_output: true label of the data
        :param np.array output: array of classification certainties to corresponding labels
        """

    def get_value(self):
        """
        Return value for the metric
        """
        return 0.0

    @classmethod
    def as_function(cls, *args, **kwargs):
        """
        Create function that instantiates the metric
        """
        def metric_creator():
            return cls(*args, **kwargs)
        return metric_creator


class DurationMetric(Metric):
    name = 'Duration [s]'

    def __init__(self):
        super(DurationMetric, self).__init__()
        self.started = 0.0

    def start(self, dataset):
        self.started = time.time()

    def get_value(self):
        return time.time() - self.started


class AccuracyMetric(Metric):
    name = 'Accuracy [%]'

    def __init__(self):
        super(AccuracyMetric, self).__init__()
        self.labels = []
        self.correct = 0
        self.incorrect = 0

    def start(self, dataset):
        self.labels = dataset.labels
        self.correct = 0
        self.incorrect = 0

    def add_test(self, data, expected_output, output):
        true_output = output.argsort()[-1]
        if true_output == expected_output:
            self.correct += 1
        else:
            self.incorrect += 1

    def get_value(self):
        s = self.correct + self.incorrect
        if s:
            return (self.correct / float(s)) * 100.0
        return 0.0


class TopKAccuracyMetric(AccuracyMetric):
    @property
    def name(self):
        return 'Top-%d Accuracy [%%]' % self.k

    def __init__(self, k):
        super(TopKAccuracyMetric, self).__init__()
        self.k = k

    def add_test(self, data, expected_output, output):
        if expected_output in output.argsort()[-self.k:]:
            self.correct += 1
        else:
            self.incorrect += 1


class ConfusionMatrixMetric(Metric):
    name = 'Confusion matrix'

    def __init__(self, normalized=False):
        super(ConfusionMatrixMetric, self).__init__()
        self.matrix = np.zeros((1, 1), np.uint32)
        self.sum = 0
        self.matrix_size = 1
        self.normalized = normalized

    def start(self, dataset):
        self.matrix_size = len(dataset.labels)
        self.matrix = np.zeros((self.matrix_size, self.matrix_size), np.uint32)
        self.sum = 0

    def add_test(self, data, expected_output, output):
        true_output = output[:self.matrix_size].argsort()[-1]
        self.matrix[expected_output, true_output] += 1
        self.sum += 1

    def get_value(self):
        matrix = self.matrix
        if self.normalized:
            matrix = matrix / float(self.sum or 1)
        return '\n%s' % np.array2string(matrix, max_line_width=np.nan, threshold=np.nan)


class F1ScoreMetric(ConfusionMatrixMetric):
    name = 'F1 score'

    def __init__(self):
        super(F1ScoreMetric, self).__init__(False)

    def _get_scores(self):
        scores = []
        for x in range(self.matrix_size):
            tp = self.matrix[x, x]
            fp = np.sum(self.matrix[:, x]) - tp
            fn = np.sum(self.matrix[x, :]) - tp
            pr = tp / float(tp + fp) if tp + fp else 0.0
            re = tp / float(tp + fn) if tp + fp else 0.0
            f1 = 2.0 * (re * pr) / (re + pr) if re + pr else 0.0
            # if np.isnan(f1):
            #     f1 = 0.0
            scores.append((x, f1))
        return scores

    def get_value(self):
        scores = self._get_scores()
        scores_f = [s[1] for s in scores]
        return '%r\n%s' % (sum(scores_f) / len(scores_f), '\n'.join([repr(s) for s in scores]))





