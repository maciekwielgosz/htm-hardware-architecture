import multiprocessing

from .utils import queue, get_mp_logger
from .profile import global_profiler


class EventfulQueue(object):
    """
    Queue wrapper that provides wait function.
    """
    def __init__(self, maxsize=1, write_event=None):
        self.queue = multiprocessing.Queue(maxsize)
        self.write_event = write_event if write_event is not None else multiprocessing.Event()

    def empty(self):
        try:
            return self.queue.empty()
        except OSError:
            return True

    @global_profiler.decorate('queue_put')
    def put(self, obj, block=True, timeout=None):
        r = self.queue.put(obj, block, timeout)
        self.write_event.set()
        return r

    @global_profiler.decorate('queue_get')
    def get(self, block=True, timeout=None):
        r = self.queue.get(block, timeout)
        self.write_event.clear()
        return r

    def put_force(self, obj, timeout=1):
        """
        Always puts obj on the queue, unblocks every timeout
        """
        while True:
            try:
                return self.put(obj, block=True, timeout=timeout)
            except (queue.Full, multiprocessing.TimeoutError):
                get_mp_logger().debug('EventfulQueue: Put timeout')

    def get_force(self, timeout=1):
        """
        Always gets obj from the queue, unblocks every timeout
        """
        while True:
            try:
                return self.get(block=True, timeout=timeout)
            except (queue.Empty, multiprocessing.TimeoutError):
                get_mp_logger().debug('EventfulQueue: Get timeout')

    @global_profiler.decorate('queue_wait')
    def wait(self, timeout=1):
        """
        Waits for data in the queue. Blocks for maximum of timeout. Returns True if there is data in the queue
        """
        try:
            return (not self.empty()) or self.write_event.wait(timeout=timeout)
        except (queue.Empty, multiprocessing.TimeoutError):
            get_mp_logger().debug('EventfulQueue: Wait timeout')
            return False

    def clear(self):
        """
        Discard all queue elements
        """
        while not self.empty():
            try:
                self.get(block=False)
            except (queue.Empty, multiprocessing.TimeoutError):
                get_mp_logger().debug('EventfulQueue: Clear timeout')
            except OSError:
                get_mp_logger().debug('EventfulQueue: Clear error')
