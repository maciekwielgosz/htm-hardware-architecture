import inspect
import logging
import multiprocessing
import multiprocessing.connection
import cProfile
import functools
import os
import sys


try:
    import queue
except ImportError:  # Python2
    import Queue as queue

try:
    import faulthandler
except ImportError:  # Python2
    logging.getLogger('videoanalyzer').warning('No faulthandler')
    faulthandler = None


def get_object_size(obj, seen=None):
    """Recursively finds size of objects in bytes"""
    size = sys.getsizeof(obj)
    if seen is None:
        seen = set()
    obj_id = id(obj)
    if obj_id in seen:
        return 0
    # Important mark as seen *before* entering recursion to gracefully handle
    # self-referential objects
    seen.add(obj_id)
    if hasattr(obj, '__dict__'):
        for cls in obj.__class__.__mro__:
            if '__dict__' in cls.__dict__:
                d = cls.__dict__['__dict__']
                if inspect.isgetsetdescriptor(d) or inspect.ismemberdescriptor(d):
                    size += get_object_size(obj.__dict__, seen)
                break
    if isinstance(obj, dict):
        size += sum((get_object_size(v, seen) for v in obj.values()))
        size += sum((get_object_size(k, seen) for k in obj.keys()))
    elif hasattr(obj, '__iter__') and not isinstance(obj, (str, bytes, bytearray)):
        size += sum((get_object_size(i, seen) for i in obj))
    return size


def profile_process(name='process'):
    """
    Decorator function for saving profiling data of a process
    """
    def decorator(f):
        @functools.wraps(f)
        def wrapper(*args, **kwargs):
            pr = cProfile.Profile()
            pr.enable()
            r = f(*args, **kwargs)
            # exc = None
            # try:
            #     return f(*args, **kwargs)
            # except BaseException as e:
            #     exc = e
            #     raise
            # finally:
            #     if isinstance(exc, Exception):  # BaseExceptions like KeyboardInterrupt cause cProfile to crash
            pr.disable()
            pr.dump_stats('%s_%s.pstat' % (name, multiprocessing.current_process().name))
            return r
        return wrapper
    return decorator


def handle_process_fault(name='fault'):
    """
    Decorator function for debugging python crashes in multiprocessing module
    """
    def decorator(f):
        @functools.wraps(f)
        def wrapper(*args, **kwargs):
            if not faulthandler.is_enabled():
                fh = open('%s_%s.log' % (name, multiprocessing.current_process().name), 'w')
                faulthandler.enable(file=fh, all_threads=True)
            return f(*args, **kwargs)
        return wrapper
    return decorator


def get_mp_logger():
    """
    Get and configure multiprocessor logger for every process

    :rtype: logging.Logger
    """
    logger = multiprocessing.get_logger()
    if getattr(logger, '_process', None) is None:
        logger._process = multiprocessing.current_process().pid
        logger = multiprocessing.log_to_stderr()
        logger.setLevel(logging.DEBUG)
        logger.setLevel(int(os.environ.get('MP_LOGGER_LEVEL', logging.WARNING)))
        # fh = logging.FileHandler('pipeline.log')
        # fh.setLevel(logging.DEBUG)
        # formatter = logging.Formatter('[%(levelname)s/%(processName)s] %(asctime)s - %(message)s')
        # fh.setFormatter(formatter)
        # logger.addHandler(fh)
        logger.debug('Connected logger')
        # atexit.register(lambda: logger.debug('Exiting'))

    return logger


def dispatch_function(prefix, dispatcher, default, sufix, *args, **kwargs):
    handler_name = '%s%s' % (prefix, sufix)
    try:
        handler = getattr(dispatcher, handler_name)
    except AttributeError:
        return default(sufix, *args, **kwargs)
    else:
        return handler(*args, **kwargs)

