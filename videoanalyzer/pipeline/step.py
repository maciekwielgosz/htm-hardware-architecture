import uuid
import functools

from ..utils import pickle
from .utils import get_mp_logger, dispatch_function


class BaseStep(object):
    """
    Base class for pipeline steps. Encapsulates data and state passing
    """
    state_exclude_fields = []
    state_default_fields = {}

    def __init__(self, name=''):
        self.name = name or self.__class__.__name__
        self.uuid = uuid.uuid4().int

    def process(self, data, return_data):
        """
        Process data from previous step. Call return_data to pass data to the next step
        """
        raise NotImplemented

    def command(self, command):
        """
        Process command from the pipeline (on worker).
        """
        pass

    def init_worker(self):
        """
        Initialize on worker. Called once, after unpickling in worker process
        """
        pass

    def get_state_main(self):
        """
        Return self state, that will be pickled and passed to worker process.
        Called on main process, every send_steps_state.
        """
        pass

    def update_worker(self, state):
        """
        Update self to given state (returned from get_state_main).
        Called on worker process, every send_steps_state.
        """
        pass

    def get_state_worker(self):
        """
        Return self state, that will be pickled and passed to main process.
        Called on worker process, every fetch_steps_state.
        Defaults to self.get_state_main.
        """
        return self.get_state_main()

    def update_main(self, state):
        """
        Update self to given state (returned from get_state_worker).
        Called on worker process, every fetch_steps_state.
        Defaults to self.update_worker.
        """
        return self.update_worker(state)

    def __getstate__(self):
        state = {}
        state.update(self.__dict__)
        for field in self.state_exclude_fields:
            state.pop(field, None)
            # if field in state:
            #     del state[field]
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)
        for field, value in self.state_default_fields.items():
            if field not in self.__dict__:
                self.__dict__[field] = value()

    @staticmethod
    def pickle_state(f):
        """
        Decorator, that pickles function output. Can be used on get_state_main and get_state_worker
        """
        @functools.wraps(f)
        def wrapper(*args, **kwargs):
            r = f(*args, **kwargs)
            return pickle.dumps(r, protocol=pickle.HIGHEST_PROTOCOL)
        return wrapper

    @staticmethod
    def unpickle_state(f):
        """
        Decorator, that unpickles function input. Can be used on update_main and update_worker
        """
        @functools.wraps(f)
        def wrapper(self, state):
            state = pickle.loads(state)
            return f(self, state)
        return wrapper


def _return_data(return_data):
    def data_returner(data_type, data=None):
        return return_data((data_type, data))
    return data_returner


class HandlerStep(BaseStep):
    """
    Utility class that provides easy way of passing different data types.
    """
    def process(self, data, return_data):
        data_type, data = data
        return dispatch_function('process_', self, self._process_default, data_type, data, _return_data(return_data))

    def _process_default(self, data_type, data, return_data):
        get_mp_logger().warning('Unknown step data_type: %r', data_type)

    def command(self, data):
        command_type, data = data
        return dispatch_function('command_', self, self._command_default, command_type, data)

    def _command_default(self, command_type, data):
        get_mp_logger().warning('Unknown step command_type: %r', command_type)

