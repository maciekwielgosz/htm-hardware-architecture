import random

import numpy as np
import threading

from .step import HandlerStep
from .pipeline import Pipeline
from ..utils import typing, Summer, convert_to_binary, read_video_frames

from htmproject.core import spatial_poolers
from htmproject.core import temporal_poolers
from htmproject.core.layer import Layer


class HTMStep(HandlerStep):
    DATA = 'data'
    END_CLIP = 'end_clip'
    END_DATASET = 'end_dataset'
    TOGGLE_LEARNING = 'toggle_learning'

    def process_data(self, data, return_data):
        """
        Handle next data item (clip frame)

        :param dict data: data from previous step
        :param (str, Any) -> None return_data: callback function for passing data to the next step
        :rtype: None
        """
        return return_data(self.DATA, data)

    def process_end_clip(self, data, return_data):
        """
        Handle next end of clip

        :param None data: None
        :param (str, Any) -> None return_data: callback function for passing data to the next step
        :rtype: None
        """
        return return_data(self.END_CLIP, data)

    def process_end_dataset(self, data, return_data):
        """
        Handle next end of dataset

        :param None data: None
        :param (str, Any) -> None return_data: callback function for passing data to the next step
        :rtype: None
        """
        return return_data(self.END_DATASET, data)

    def command_toggle_learning(self, data):
        """
        Handle toggle_learning command

        :param bool data: argument to toggle_learning
        :rtype: None
        """
        pass

    def init_worker(self):
        # For result reproducibility
        np.random.seed(42)
        random.seed(42)


class DatasetReadStep(HTMStep):
    def process_data(self, data, return_data):
        labels = len(data.labels)
        for i, d in enumerate(data):
            data, label = d
            return_data(self.DATA, {
                'label': label,
                'source': data,
                'data': None,
                'frame': -1,
                'prediction': [],
                'clip_number': i,
                'labels': labels,
            })
        return return_data(self.END_DATASET, None)


class VideoReadStep(HTMStep):
    def __init__(self, frame_limit=1, frame_size=(1, 1), name=''):
        super(VideoReadStep, self).__init__(name=name)
        self.frame_limit = frame_limit
        self.frame_size = frame_size

    def process_data(self, data, return_data):
        for frame_num, frame in read_video_frames(data['source'], self.frame_size, self.frame_limit):
            frame = convert_to_binary(frame)
            data['data'] = frame
            data['frame'] = frame_num
            return_data(self.DATA, data)
        return return_data(self.END_CLIP, None)


class LayerProcessStep(HTMStep):
    state_exclude_fields = ['spatial_pooler', 'temporal_pooler']

    def __init__(self, layer, config, sp_str='SpatialPoolerBasic', tp_str='TemporalPoolerBasic'):
        super(LayerProcessStep, self).__init__('LayerProcess_%s' % layer.get_layer_index())
        self.layer = layer  # type: Layer
        self.config = config
        self.spatial_pooler = None
        self.temporal_pooler = None
        self.sp_str = sp_str
        self.tp_str = tp_str

    def init_worker(self):
        super(LayerProcessStep, self).init_worker()
        cls = getattr(spatial_poolers, self.sp_str)
        self.spatial_pooler = cls(self.config)
        cls = getattr(temporal_poolers, self.tp_str)
        self.temporal_pooler = cls(self.config)

    @HandlerStep.pickle_state
    def get_state_main(self):
        return self.layer

    @HandlerStep.unpickle_state
    def update_worker(self, state):
        state._Layer__config = self.layer._Layer__config  # copy original config
        self.layer.__dict__ = state.__dict__  # update original layer (to keep references)

    def process_data(self, data, return_data):
        self.layer.shift_states()
        self.spatial_pooler.update_layer_state(self.layer, data['data'])
        if self.layer.is_tp_enabled():
            self.temporal_pooler.update_layer_state(self.layer)
            data['data'] = self.temporal_pooler.get_layer_output(self.layer)
        else:
            data['data'] = self.spatial_pooler.get_layer_output(self.layer)
        return return_data(self.DATA, data)

    def command_toggle_learning(self, data):
        self.layer.toggle_learning(data)
        return super(LayerProcessStep, self).command_toggle_learning(data)


class ClassifyStep(HTMStep):
    def __init__(self, classifier):
        super(ClassifyStep, self).__init__('Classify')
        self.classifier = classifier
        self.learning = True
        self.summer = Summer()
        self.last_data = {}
        self.x_data = []
        self.y_data = []

    @HandlerStep.pickle_state
    def get_state_main(self):
        return self.classifier, self.learning

    @HandlerStep.unpickle_state
    def update_worker(self, state):
        classifier, learning = state
        self.classifier.__dict__ = classifier.__dict__  # update original classifier (to keep references)
        self.learning = learning

    def classify(self, data):
        if hasattr(self.classifier, 'coef_') and self.classifier.coef_ is not None:
            return self.classifier.decision_function([data['data']])[0]
        else:
            return np.array([0.0 for _ in range(data['labels'])])

    def process_data(self, data, return_data):
        self.last_data = data
        if self.learning:
            self.x_data.append(data['data'])
            self.y_data.append(data['label'])
        rr = self.classify(data)
        self.summer.add(rr)

    def process_end_clip(self, data, return_data):
        self.last_data['prediction'] = self.summer.sum
        self.summer.clear()
        return return_data(self.DATA, self.last_data)

    def process_end_dataset(self, data, return_data):
        if self.x_data:
            self.classifier.fit(self.x_data, self.y_data)
        self.x_data = []
        self.y_data = []
        return super(ClassifyStep, self).process_end_dataset(data, return_data)

    def command_toggle_learning(self, data):
        self.learning = data
        return super(ClassifyStep, self).command_toggle_learning(data)


def create_pipelines(htm, frame_limit, frame_size, config, classifier):
    """
    Create pipeline for HTM

    :param HTM htm:
    :param int frame_limit:
    :param typing.Tuple[int, int] frame_size:
    :param Config config:
    :param Classifier classifier:
    :rtype: typing.Tuple[Pipeline, Pipeline]
    """
    steps = []
    steps.append(DatasetReadStep())
    steps.append(VideoReadStep(frame_limit, frame_size))
    sp = htm.get_sp().__class__.__name__
    tp = htm.get_tp().__class__.__name__
    for layer in htm.get_layers():
        steps.append(LayerProcessStep(layer, config, sp, tp))
    steps.append(ClassifyStep(classifier))
    pipeline_train = Pipeline(prefix='Train')
    pipeline_test = Pipeline(prefix='Test')
    for step in steps:
        pipeline_train.append(step)
        pipeline_test.append(step)
    return pipeline_train, pipeline_test


class PrefetcherThread(threading.Thread):
    def __init__(self, pipeline):
        super(PrefetcherThread, self).__init__()
        self.pipeline = pipeline  # type: Pipeline
        self.finish = threading.Event()
        self.start()
        self.data = []

    def run(self):
        for data in self.pipeline.fetch_data():
            self.data.append(data)
            if self.finish.is_set():
                return

    def fetch_data(self):
        self.finish.set()
        self.join()
        for data in self.data:
            yield data
        for data in self.pipeline.fetch_data():
            yield data
