
from __future__ import print_function

import unittest
import logging
import time
import random

import os

from .pipeline import Pipeline
from .step import HandlerStep
from .utils import get_mp_logger
from .profile import global_profiler, TimeProfiler


class CreateRangeStep(HandlerStep):
    def __init__(self, sleep):
        super(CreateRangeStep, self).__init__()
        self.sleep = sleep

    def process_size(self, data, return_data):
        for x in range(data):
            if random.randint(0, 100) <= 4 and self.sleep:
                time.sleep(2)
            return_data('x', x)
        return_data('_warning_')  # should raise warning
        return_data('end')

    def command_test(self, data):
        return self.sleep * data


class ComputeStep(HandlerStep):
    def __init__(self, sleep):
        super(ComputeStep, self).__init__()
        self.sleep = sleep
        self.total = 0
        self.sum = 0

    def init_worker(self):
        self.total = 3

    @HandlerStep.pickle_state
    def get_state_main(self):
        return self.sum

    @HandlerStep.unpickle_state
    def update_worker(self, state):
        self.sum = state

    def process_x(self, data, return_data):
        if random.randint(0, 1000) <= 4 and self.sleep:
            time.sleep(2)
        self.sum += data
        return_data('x', data**2)

    def process_end(self, data, return_data):
        self.sum **= 2
        return_data('x', self.sum)
        return_data('sum', -self.sum)

    def command_test(self, data):
        return self.sleep + data


class TestPipeline(unittest.TestCase):
    maxDiff = None

    def _test(self, n, sleep):
        t = TimeProfiler('_test %s' % n)

        with t.add('creating'):
            p = Pipeline()
            p.append(CreateRangeStep(sleep))
            p.append(ComputeStep(sleep))

        with t.add('starting'):
            p.start()
            ss = 0

        for y in range(3):
            with t.add('sending_steps'):
                p.send_steps()

            for z in range(3):
                with t.add('sending_state'):
                    p.send_steps_state()

                with t.add('sending_data'):
                    p.send_data(('size', n))

                with t.add('fetching_data'):
                    output = list(p.fetch_data())

                with t.add('fetching_state'):
                    state = p.fetch_steps_state()

                s = sum(range(n))
                ss = (ss + s) ** 2
                self.assertEqual(output[:-2], [('x', x ** 2) for x in range(n)])
                self.assertEqual(p.steps[1].sum, ss)
                self.assertEqual(p.steps[1].total, 0)
                self.assertEqual(output[-2], ('x', ss))
                self.assertEqual(output[-1], ('sum', -ss))
                print('%s.%s.%s' % (n, y, z))

            with t.add('fetching_steps'):
                steps = p.fetch_steps()

        with t.add('executing_command'):
            r = p.command(('test', 100))
        self.assertEqual(r, {
            p.steps[0].uuid: sleep * 100,
            p.steps[1].uuid: sleep + 100,
        })

        with t.add('executing_command'):
            r = p.command(('warning', 100))  # should raise warning
        self.assertEqual(r, {
            p.steps[0].uuid: None,
            p.steps[1].uuid: None,
        })

        with t.add('stopping'):
            p.stop()

        t.display()

    def test_pipeline_very_fast(self):
        self._test(1000, False)

    def test_pipeline_fast(self):
        for x in range(5, 100, 5):
            with self.subTest(x=x):
                self._test(x, False)

    def test_pipeline(self):
        for x in range(5, 100, 5):
            with self.subTest(x=x):
                self._test(x, True)


def main():
    logging.basicConfig(level=logging.DEBUG, format="%(asctime)-15s:%(levelname)s:%(name)s:%(message)s")
    os.environ.setdefault('PIPELINE_TIME_PROFILER', '1')
    global_profiler.enable()
    global_profiler.clear()
    # unittest.main()
    suite = unittest.TestSuite()
    suite.addTest(TestPipeline('test_pipeline_very_fast'))  # 2s on i7 3632qm
    suite.addTest(TestPipeline('test_pipeline_fast'))
    suite.addTest(TestPipeline('test_pipeline'))
    runner = unittest.TextTestRunner()
    runner.run(suite)
    print('Done')
    global_profiler.display()



