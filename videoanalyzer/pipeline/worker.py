import multiprocessing
import multiprocessing.connection
import threading

from .eventfulqueue import EventfulQueue
from .step import BaseStep
from .utils import queue, get_mp_logger, profile_process, handle_process_fault, dispatch_function
from .profile import global_profiler


class StepWorker(multiprocessing.Process):
    DATA = 'data'
    TERMINATOR = 'terminator'

    def __init__(self, in_queue, out_queue, ctrl_queue, name=None, timeout=1):
        # multiprocessing.Process.__init__(self, name=name)
        super(StepWorker, self).__init__(name=name)
        self.timeout = timeout
        self.input_queue = in_queue  # type: EventfulQueue
        self.output_queue = out_queue  # type: EventfulQueue
        self.control_queue = ctrl_queue  # type: EventfulQueue
        self.running_event = multiprocessing.Event()
        self.running_event.set()
        self.step = None  # type: BaseStep
        self.started = False

    def start(self):
        if not self.started:
            self.started = True
            super(StepWorker, self).start()

    # @handle_process_fault('fault')
    # @profile_process('worker')
    @global_profiler.time_profile_process('time')
    def run(self):
        threading.current_thread().name = self.name  # For easier debugging & profiling
        self._run()
        # threading.Thread(target=self._run).start()

    @global_profiler.decorate('worker_run')
    def _run(self):
        logger = get_mp_logger()
        logger.debug('Worker %s started', self.name)
        try:
            while self.running_event.is_set():
                try:
                    data_type, data = self._get_input()
                except (queue.Empty, multiprocessing.TimeoutError):
                    logger.debug('Worker %s timeout', self.name)
                else:
                    self._handle(data_type, data)

            logger.debug('Worker %s exited', self.name)
        except Exception:
            logger.exception('Worker %s error:', self.name)
            raise

    # @global_profiler.decorate('worker_get_input')
    def _get_input(self):
        return self.input_queue.get(True, timeout=self.timeout)

    # @global_profiler.decorate('worker_handle')
    def _handle(self, data_type, data):
        return dispatch_function('_handle_', self, self._unknown_data_type, data_type, data)

    def _unknown_data_type(self, data_type, *args, **kwargs):
        get_mp_logger().warning('Unknown worker data type: %r %r %r', data_type, args, kwargs)

    # Step handling

    @global_profiler.decorate('worker_update')
    def _handle_step_update(self, data):
        self.step = data
        r = self.step.init_worker()
        return self.control_queue.put_force((self.step.uuid, r), self.timeout)

    @global_profiler.decorate('worker_dump')
    def _handle_step_dump(self, data):
        return self.control_queue.put_force((self.step.uuid, self.step), self.timeout)

    @global_profiler.decorate('worker_update_state')
    def _handle_step_update_state(self, data):
        r = self.step.update_worker(data)
        return self.control_queue.put_force((self.step.uuid, r), self.timeout)

    @global_profiler.decorate('worker_dump_state')
    def _handle_step_dump_state(self, data):
        state = self.step.get_state_worker()
        return self.control_queue.put_force((self.step.uuid, state), self.timeout)

    @global_profiler.decorate('worker_command')
    def _handle_step_command(self, data):
        result = self.step.command(data)
        return self.control_queue.put_force((self.step.uuid, result), self.timeout)

    # Data handling

    @global_profiler.decorate('worker_handle_data')
    def _handle_data(self, data):
        return self.step.process(data, self._return_data)

    def _handle_terminator(self, data):
        return self.output_queue.put_force((self.TERMINATOR, data), self.timeout)

    def _return_data(self, data):
        return self.output_queue.put_force((self.DATA, data), self.timeout)

    # Public / main process API

    def update_step(self, step):
        return self.input_queue.put_force(('step_update', step), self.timeout)

    def request_step(self):
        return self.input_queue.put_force(('step_dump', None), self.timeout)

    def update_step_state(self, state):
        return self.input_queue.put_force(('step_update_state', state), self.timeout)

    def request_step_state(self):
        return self.input_queue.put_force(('step_dump_state', None), self.timeout)

    def request_command(self, data):
        return self.input_queue.put_force(('step_command', data), self.timeout)

    def return_control_data(self):
        return self.control_queue.get_force(self.timeout)

    def stop(self):
        self.running_event.clear()


