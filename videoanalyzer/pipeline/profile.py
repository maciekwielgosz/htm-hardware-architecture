from __future__ import print_function

import datetime
import multiprocessing
import time

import functools
import collections

import os

import sys


class Section(object):
    def __init__(self, profiler, name, multiplier=1.0, exclude_children=True):
        self.profiler = profiler  # type: TimeProfiler
        self.name = name
        self.multiplier = multiplier
        self.exclude_children = exclude_children
        self.start = 0
        self.total = 0
        self.parent = None  # type: Section

    def __enter__(self):
        # if self.profiler.enabled:
        self.parent = self.profiler.enter(self)
        if self.parent and self.parent.exclude_children:
            self.parent.pause()
        self.start = time.time()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        # if self.profiler.enabled:
        self.profiler.exit(self)
        self.profiler.add_time(self.name, (time.time() - self.start) * self.multiplier)
        if self.parent and self.parent.exclude_children:
            self.parent.unpause()

    def pause(self):
        self.total = time.time() - self.start

    def unpause(self):
        self.start = time.time() - self.total


class TimeProfiler(object):
    def __init__(self, name='', enabled=True):
        self.name = name
        self.enabled = enabled
        self.sections = collections.OrderedDict()
        self.stack = collections.deque()
        self.started = time.time()

    def enable(self):
        self.enabled = True

    def disable(self):
        self.enabled = False

    def clear(self):
        self.sections = collections.OrderedDict()
        self.started = time.time()

    def add_time(self, name, amount):
        # self.sections[name] = self.sections.get(name, 0.0) + amount
        self.sections.setdefault(name, collections.deque())
        self.sections[name].append(amount)

    def add(self, name, exclude_children=True):
        return Section(self, name, 1.0, exclude_children)

    def subtract(self, name, exclude_children=True):
        return Section(self, name, -1.0, exclude_children)

    def enter(self, section):
        r = None
        if self.stack:
            r = self.stack[-1]
        self.stack.append(section)
        return r

    def exit(self, section):
        try:
            s = self.stack.pop()
            if s is not section:
                self.stack.append(s)
                self.stack.remove(section)
        except (ValueError, IndexError):
            pass

    def _just(self, value):
        if isinstance(value, float):
            return ('%.8f' % value).rjust(16)
        if isinstance(value, int):
            return ('%d' % value).rjust(16)
        value = str(value)
        return ('%s' % value).rjust(min(16, len(value)+6)).ljust(16)

    def _display(self, name, width, *values, **kwargs):
        print(name.ljust(width), *[self._just(value) for value in values], **kwargs)

    def display(self, file=sys.stdout):
        summ = sum(sum(map(list, self.sections.values()), [])) or 0.000000000000001
        print('Time profiler stats (%s): # %s' % (self.name, datetime.datetime.now()), file=file)
        width = max(map(len, list(self.sections.keys()) + ['Section']))
        self._display('Section', width, 'Sum', 'Min', 'Max', 'Avg', 'Median', '%', 'Count'.rjust(16), file=file)
        for name, value in self.sections.items():
            sm = sum(value)
            avg = sm / len(value)
            med = sorted(value)[int(len(value)/2.0)]
            pc = 100 * sm / summ
            self._display(name, width, sm, min(value), max(value), avg, med, pc, len(value), file=file)
        self._display('Sum', width, summ, file=file)
        self._display('Total', width, time.time() - self.started, file=file)

    def decorate(self, name=None, exclude_children=True):
        """
        Decorator function for saving time profiling data of a function
        """
        def decorator(f):
            @functools.wraps(f)
            def wrapper(*args, **kwargs):
                if self.enabled:
                    with self.add(name or f.__name__, exclude_children):
                        return f(*args, **kwargs)
                else:
                    return f(*args, **kwargs)
            # if self.enabled:
            return wrapper
            # else:
            #     return f
        return decorator

    def time_profile_process(self, name='time'):
        """
        Decorator function for saving time profiling data of a process
        """
        def decorator(f):
            @functools.wraps(f)
            def wrapper(*args, **kwargs):
                # self.enable()
                if self.enabled:
                    with self.add('process_root'):
                        r = f(*args, **kwargs)
                    self.display(open('%s_%s.log' % (name, multiprocessing.current_process().name), 'a'))
                    return r
                else:
                    return f(*args, **kwargs)

            return wrapper

        return decorator


global_profiler = TimeProfiler('global', bool(os.environ.get('PIPELINE_TIME_PROFILER', False)))



