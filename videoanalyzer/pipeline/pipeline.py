import multiprocessing


from .profile import global_profiler
from .eventfulqueue import EventfulQueue
from .worker import StepWorker
from .step import BaseStep
from .utils import queue, get_mp_logger
from ..utils import typing


class Pipeline(object):
    """
    Class that stores and manages steps. Creates queues, workers, etc.
    """
    def __init__(self, queue_size=1, prefix=''):
        self.queue_size = queue_size
        self.prefix = prefix
        self.steps = []  # type: typing.List[BaseStep]
        self.workers = []  # type: typing.List[StepWorker]
        self.in_queue = EventfulQueue(self.queue_size)
        self.out_queue = self.in_queue
        self.ctrl_queue = EventfulQueue(self.queue_size)
        self._terminated = False

    @global_profiler.decorate('pipeline_append')
    def append(self, step):
        """
        Add new step to the pipeline

        :param BaseStep step:
        :rtype: BaseStep
        """
        self.steps.append(step)
        out_queue = EventfulQueue(self.queue_size)
        worker = StepWorker(self.out_queue, out_queue, self.ctrl_queue, name='%s%s' % (self.prefix, step.name))
        # worker.start()
        self.workers.append(worker)
        self.out_queue = out_queue
        return step

    @global_profiler.decorate('pipeline_start')
    def start(self):
        """
        Start all worker processes
        """
        for worker in self.workers:
            worker.start()

    @global_profiler.decorate('pipeline_send_steps')
    def send_steps(self):
        """
        Pickle steps and send them to corresponding workers
        """
        return self._worker_cmd(
            request=lambda worker, step: worker.update_step(step),
        )

    @global_profiler.decorate('pipeline_fetch_steps')
    def fetch_steps(self):
        """
        Fetch steps from workers: {step.uuid: step for self.steps}

        :rtype: dict
        """
        return self._worker_cmd(
            request=lambda worker, step: worker.request_step(),
        )

    @global_profiler.decorate('pipeline_send_steps_state')
    def send_steps_state(self, steps_state=None):
        """
        Get state from every step not present in steps_state, pickle and send to corresponding worker.
        Returns completed dict of states (steps_state).

        :param dict steps_state: {step.uuid: step.get_state_main() for step in steps}
        :rtype: dict
        """
        if steps_state is None:
            steps_state = {}

        def send_step_state(worker, step):
            if step.uuid not in steps_state:
                steps_state[step.uuid] = step.get_state_main()
            return worker.update_step_state(steps_state[step.uuid])

        self._worker_cmd(
            request=send_step_state,
        )
        return steps_state

    @global_profiler.decorate('pipeline_fetch_steps_state')
    def fetch_steps_state(self):
        """
        Fetch steps state from workers.
        Returns {step.uuid: step.get_state_worker() for step in steps}
        :rtype: dict
        """
        def receive_step_state(worker, step, data):
            get_mp_logger().debug('Received step state - %s %s', step.name, step.uuid)
            step.update_main(data)
            return data

        return self._worker_cmd(
            request=lambda worker, step: worker.request_step_state(),
            receive=receive_step_state,
        )

    @global_profiler.decorate('pipeline_send_data')
    def send_data(self, data, send_terminator=True):
        """
        Send data into pipeline

        :param typing.Any data: data to send
        :param bool send_terminator: whenever to send terminator after data
        """
        self._terminated = False
        self.in_queue.put_force((StepWorker.DATA, data))
        if send_terminator:
            self.send_terminator()

    @global_profiler.decorate('pipeline_send_terminator')
    def send_terminator(self):
        """
        Send terminator into pipeline
        """
        self.in_queue.put_force((StepWorker.TERMINATOR, None))

    @global_profiler.decorate('pipeline_fetch_data')
    def fetch_data(self, timeout=1):
        """
        Generator yielding data, coming out of the last step

        :param float timeout: unblock every timeout
        :rtype typing.Any
        """
        while not self._terminated:
            try:
                data_type, data = self.out_queue.get(block=True, timeout=timeout)
            except (queue.Empty, multiprocessing.TimeoutError):
                get_mp_logger().debug('FAD timeout')
            else:
                if data_type == StepWorker.DATA:
                    yield data
                elif data_type == StepWorker.TERMINATOR:
                    self._terminated = True
                else:
                    get_mp_logger().warning('Unknown worker data type %r', data_type)

    @global_profiler.decorate('pipeline_command')
    def command(self, data):
        """
        Execute command on workers.

        Returns {step.uuid: step.command(data) for step in steps}
        :rtype: dict
        """
        return self._worker_cmd(
            request=lambda worker, step: worker.request_command(data),
        )

    def _worker_cmd(self, request, receive=lambda worker, step, data: data):
        """
        Call request(worker, step) for every step.
        Wait for responses in any order and get them with receive(worker, step).
        Return {step.uuid: receive(worker, step) for worker, step in zip(self.workers, self.steps)}

        :param (StepWorker, BaseStep)->typing.Any request: function to request data from worker
        :param (StepWorker, BaseStep, typing.Any)->typing.Any receive: function to process data from worker
        :rtype: dict

        """
        step_map = dict([(step.uuid, (worker, step)) for worker, step in zip(self.workers, self.steps)])
        steps_returns = {}
        for worker, step in zip(self.workers, self.steps):
            request(worker, step)
        for worker, step in zip(self.workers, self.steps):  # receive returns out of order
            step_uuid, data = worker.return_control_data()
            worker, step = step_map[step_uuid]
            steps_returns[step_uuid] = receive(worker, step, data)
        return steps_returns

    def stop(self):
        """
        Stop all pipeline processes. After that, pipeline is unusable
        """
        for worker in self.workers:
            worker.stop()
            worker.input_queue.clear()

        for worker in self.workers:
            # worker.input_queue.clear()
            worker.output_queue.clear()
            worker.control_queue.clear()

    def terminate(self):
        """
        Terminate all worker processes
        """
        for worker in self.workers:
            try:
                worker.terminate()
            except AttributeError:  # Not started
                pass

    def join(self, timeout=0.0):
        """
        Wait for all workers to stop and check if they did
        """
        for worker in self.workers:
            try:
                worker.join(timeout)
            except AssertionError:  # Not started
                pass
        return any([worker.is_alive() if worker else False for worker in self.workers])

    def __del__(self):
        self.stop()
        if not self.join(1.0):
            self.terminate()


