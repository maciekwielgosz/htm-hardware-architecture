import logging
import traceback

from .base import Adapter
from .synthetic import SyntheticAdapter, PerfectAdapter, WrongAdapter, RandomAdapter


__logger = logging.getLogger('videoanalyzer.adapters')


def __spoof(*args, **kwargs):
    pass


try:
    ModuleNotFoundError
except NameError:
    ModuleNotFoundError = ImportError


__errors = (ImportError, ModuleNotFoundError, SyntaxError)


try:
    from .htm import HTMAdapter
    from .htm import BasicHTMAdapter, GPUHTMAdapter, NumbaHTMAdapter
except __errors as e:
    __logger.debug(traceback.format_exc())
    __logger.info('Spoofing HTM')
    HTMAdapter = __spoof
    BasicHTMAdapter = GPUHTMAdapter = NumbaHTMAdapter = __spoof

try:
    from .htm_pipeline import HTMPipelineAdapter
    from .htm_pipeline import BasicHTMPipelineAdapter, GPUHTMPipelineAdapter, NumbaHTMPipelineAdapter
except __errors as e:
    __logger.debug(traceback.format_exc())
    __logger.info('Spoofing HTM_Pipeline')
    HTMPipelineAdapter = __spoof
    BasicHTMPipelineAdapter = GPUHTMPipelineAdapter = NumbaHTMPipelineAdapter = __spoof


try:
    from .c3d import C3DPreTrainedAdapter, C3DDenseAdapter, C3DSVMAdapter
except __errors as e:
    __logger.debug(traceback.format_exc())
    __logger.info('Spoofing C3D')
    C3DPreTrainedAdapter = C3DDenseAdapter = C3DSVMAdapter = __spoof

try:
    from .numenta import NupicAdapter
except __errors as e:
    __logger.debug(traceback.format_exc())
    __logger.info('Spoofing Nupic')
    NupicAdapter = __spoof

