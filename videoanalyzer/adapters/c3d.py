from __future__ import print_function

import os
import cv2
import numpy as np
from sklearn.svm import LinearSVC
from ._suppress_keras import keras  # don't print used backend
import keras.backend as K
from keras.layers import Convolution3D, MaxPooling3D, Dense, Dropout, ZeroPadding3D, Flatten
from keras.models import model_from_json, load_model, Sequential

from .base import Adapter
from ..utils import dump_zipped_pickle, load_zipped_pickle, Summer, convert_to_binary, read_video_frames


class C3DPreTrainedAdapter(Adapter):
    store_suffix = '.pickle.gz'

    def __init__(self, caffe_model_dir, frame_limit=16):
        super(C3DPreTrainedAdapter, self).__init__()
        self.caffe_model_dir = caffe_model_dir
        self.frame_limit = frame_limit
        self.frame_size = [171, 128]
        self.model = None
        dim_ordering = K.image_dim_ordering()
        # print("[Info] image_dim_order (from default ~/.keras/keras.json)={}".format(dim_ordering))
        self.backend = dim_ordering

    def create(self):
        if self.backend == 'th':
            model_weight_filename = os.path.join(self.caffe_model_dir, 'sports1M_weights_th.h5')
            model_json_filename = os.path.join(self.caffe_model_dir, 'sports1M_weights_th.json')
        else:
            model_weight_filename = os.path.join(self.caffe_model_dir, 'sports1M_weights_tf.h5')
            model_json_filename = os.path.join(self.caffe_model_dir, 'sports1M_weights_tf.json')
        # if self.backend == 'tf':
        #     input_shape = (16, 112, 112, 3)  # l, h, w, c
        # else:
        #     input_shape = (3, 16, 112, 112)  # c, l, h, w
        self.model = model_from_json(open(model_json_filename, 'r').read())
        self.model.load_weights(model_weight_filename)
        for layer in self.model.layers:
            layer.trainable = False  # Already trained

    def encode(self, data):
        vid = []
        for frame_num, frame in read_video_frames(data, self.frame_size, self.frame_limit):
            vid.append(frame)  # frame.transpose((1, 0, 2))
            if len(vid) == 16:
                X = np.array(vid, dtype=np.float32)[:, 8:120, 30:142, :]  # input_shape = (16, 112, 112, 3)
                if self.backend == 'th':
                    X = np.transpose(X, (3, 0, 1, 2))  # input_shape = (3,16,112,112)
                yield np.array([X])
                vid = []

    def test_one(self, data):
        s = Summer()
        for frame in self.encode(data):
            out = self.model.predict_on_batch(frame)[0]
            s.add(out)
        return s.sum

    def train_one(self, data, expected_output):
        s = Summer()
        Y = np.array([[1.0 if x == expected_output else 0.0 for x in range(self.model.layers[-1].output_shape[1])]])
        for frame in self.encode(data):
            out = self.model.train_on_batch(frame, Y)[-1][0]
            s.add(out)
        return s.sum

    def compile(self):
        # self.model.compile(loss='mean_squared_error', optimizer='sgd', metrics=['accuracy', 'top_k_categorical_accuracy'])
        self.model.compile(loss='mean_squared_error', optimizer='sgd', metrics=[])
        self.model.metrics_tensors += self.model.outputs  # hack

    def get_dump_data(self):
        return {
            'model': self.model.to_json(),
            'weights': self.model.get_weights(),
            'frame_limit': self.frame_limit,
            'epoch': self.epoch,
        }

    def dump(self, path):
        # self.model.save(path)w
        return dump_zipped_pickle(self.get_dump_data(), path)

    def load(self, path):
        # self.model = load_model(path)
        d = load_zipped_pickle(path)
        self.model = model_from_json(d['model'])
        self.model.set_weights(d['weights'])
        self.frame_limit = d['frame_limit']
        self.epoch = d['epoch']
        return d


class C3DDenseAdapter(C3DPreTrainedAdapter):
    def create(self):
        super(C3DDenseAdapter, self).create()
        # self.model.add(Dropout(.5, name='dropout_5'))
        # self.model.add(Dense(4096, activation='relu', name='fc9'))
        self.model.add(Dense(101, activation='softmax', name='fc10'))


class C3DSVMAdapter(C3DPreTrainedAdapter):
    def __init__(self, caffe_model_dir, frame_limit=16):
        super(C3DSVMAdapter, self).__init__(caffe_model_dir=caffe_model_dir, frame_limit=frame_limit)
        self.classifier = None
        self._x_data = []
        self._y_data = []

    def create(self):
        super(C3DSVMAdapter, self).create()
        self.classifier = LinearSVC()

    def classify(self, out):
        if hasattr(self.classifier, 'coef_') and self.classifier.coef_ is not None:
            return self.classifier.decision_function([out])[0]
        return np.array([0.0 for _ in range(self.model.layers[-1].output_shape[1])])

    def train_one(self, data, expected_output):
        s = Summer()
        Y = np.array([[1.0 if x == expected_output else 0.0 for x in range(self.model.layers[-1].output_shape[1])]])
        for frame in self.encode(data):
            out = self.model.train_on_batch(frame, Y)[-1][0]
            self._x_data.append(out.copy())
            self._y_data.append(expected_output)
            r = self.classify(out)
            s.add(r)
        return s.sum

    def train(self, dataset, callback):
        self._x_data = []
        self._y_data = []
        r = super(C3DSVMAdapter, self).train(dataset, callback)
        self.classifier.fit(self._x_data, self._y_data)
        return r

    def test_one(self, data):
        s = Summer()
        for frame in self.encode(data):
            out = self.model.predict_on_batch(frame)[0]
            r = self.classify(out)
            s.add(r)
        return s.sum

    def get_dump_data(self):
        d = super(C3DSVMAdapter, self).get_dump_data()
        d['classifier'] = self.classifier
        return d

    def load(self, path):
        d = super(C3DSVMAdapter, self).load(path)
        self.classifier = d.get('classifier', None)
        if self.classifier is None:
            self.classifier = LinearSVC()
        return d


