
from ..datasets import Dataset
from ..utils import typing
import numpy as np


class Adapter(object):
    """
    Base class for encapsulating a video classification model
    """

    store_suffix = '.txt'
    START_TRAIN = 'strain'
    START_TEST = 'stest'
    END_TRAIN = 'etrain'
    END_READY = 'eready'
    END_TEST = 'etest'

    def __init__(self):
        self.epoch = 0

    def run(self, train, test, iterations, train_dataset, test_dataset, train_callback, test_callback):
        """
        Run the adapter for iterations. Yield correct values to signal corresponding events

        :param bool train: train the adapter
        :param bool test: test the adapter
        :param int iterations: number of iterations to run (train &/ test)
        :param Dataset train_dataset: dataset for training
        :param Dataset test_dataset: dataset for testing
        :param typing.Callable[[str, int, np.array], None]] train_callback: function to call with training results (for every data in dataset)
        :param typing.Callable[[str, int, np.array], None]] test_callback: function to call with testing results (for every data in dataset)
        """
        for x in range(iterations):
            if train:
                yield self.START_TRAIN  # Report start of training
                self.train(train_dataset, train_callback)
                yield self.END_TRAIN  # Report end of training
                yield self.END_READY  # Report adapter readiness to be saved
            if test:
                yield self.START_TEST  # Report start of testing
                self.test(test_dataset, test_callback)
                yield self.END_TEST  # Report end of testing

    def train(self, dataset, callback):
        """
        Train the adapter for one iteration

        :param Dataset dataset: training dataset
        :param typing.Callable[[str, int, np.array], None]] callback: function to call with training results (for every data in dataset)
        """
        for data, expected_output in dataset:
            output = self.train_one(data, expected_output)
            callback(data, expected_output, output)
        self.epoch += 1

    def train_one(self, data, expected_output):
        """
        Train the adapter with single data sample

        :param str data: data sample (path to the video file)
        :param int expected_output: video category
        :return: np.array representing confidence of data classification to each category
        """
        raise NotImplemented

    def test(self, dataset, callback):
        """
        Test the adapter for one iteration

        :param Dataset dataset: testing dataset
        :param typing.Callable[[str, int, np.array], None]] callback: function to call with testing results (for every data in dataset)
        """
        for data, expected_output in dataset:
            output = self.test_one(data)
            callback(data, expected_output, output)

    def test_one(self, data):
        """
        Test the adapter with single data sample

        :param str data: data sample (path to the video file)
        :return: np.array representing confidence of data classification to each category
        """
        raise NotImplemented

    def dump(self, path):
        """
        Save the adapter state

        :param str path: path to storage file
        """
        open(path, 'w').write(str(self.epoch))

    def load(self, path):
        """
        Load the adapter state

        :param path: path to storage file
        """
        self.epoch = int(open(path, 'r').read())

    def create(self):
        """
        Create fresh, clean (empty) adapter (initialize the model)
        """

    def compile(self):
        """
        Compile the adapter (make it usable). Called after creating and loading the adapter
        """

    def stop(self):
        """
        Cleanup after using the adapter
        """


