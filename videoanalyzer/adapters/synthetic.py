
from .base import Adapter

import numpy as np
import random


class SyntheticAdapter(Adapter):
    def train(self, dataset, callback):
        label_number = len(dataset.labels)
        for data, expected_output in dataset:
            output = self.get_output(data, expected_output, label_number)
            callback(data, expected_output, output)

    def test(self, dataset, callback):
        self.train(dataset, callback)
        self.epoch += 1

    def get_output(self, data, expected_output, label_number):
        raise NotImplemented


class PerfectAdapter(SyntheticAdapter):
    def get_output(self, data, expected_output, label_number):
        return np.array([1.0 if x == expected_output else 0.0 for x in range(label_number)])


class WrongAdapter(SyntheticAdapter):
    def get_output(self, data, expected_output, label_number):
        return np.array([0.0 if x == expected_output else 1.0 for x in range(label_number)])


class RandomAdapter(SyntheticAdapter):
    def get_output(self, data, expected_output, label_number):
        random_output = random.randrange(label_number)
        return np.array([1.0 if x == random_output else 0.0 for x in range(label_number)])

