
import logging
import numpy as np
import sklearn

from .base import Adapter
from ..histogram import HistogramAnalyzer
from ..utils import dump_zipped_pickle, load_zipped_pickle, Summer, convert_to_binary, read_video_frames

from htmproject.core import HTM
from htmproject.core import spatial_poolers
from htmproject.core import temporal_poolers


class HTMAdapter(Adapter):
    store_suffix = '.pickle.gz'

    def __init__(self):
        super(HTMAdapter, self).__init__()
        self.config = None
        self.frame_limit = 0
        self.frame_size = [0, 0]
        self.classifier = None  # type: sklearn.svm.LinearSVC
        self.htm = None   # type: HTM
        self._x_data = []
        self._y_data = []
        self._label_number = 1
        self.pooler_sp = None
        self.pooler_tp = None

    @property
    def logger(self):
        return logging.getLogger('videoanalyzer.htm')

    def _override_poolers(self):
        if self.pooler_sp:
            self.htm._HTM__spatial_pooler = self.pooler_sp
        if self.pooler_tp:
            self.htm._HTM__temporal_pooler = self.pooler_tp

    def create(self):
        from ._config import config
        self.config = config
        self.frame_limit = self.config.get("encoder.frame_limit")
        self.frame_size = self.config.get("encoder.frame_size")
        self.htm = HTM(self.config, self.frame_size[0] * self.frame_size[1])
        self.classifier = sklearn.svm.LinearSVC()

    def compile(self):
        self._override_poolers()

    def classify(self, htm_out):
        if hasattr(self.classifier, 'coef_') and self.classifier.coef_ is not None:
            return self.classifier.decision_function([htm_out])[0]
        else:
            return np.array([0.0 for _ in range(self._label_number)])

    def encode(self, data):
        for frame_num, frame in read_video_frames(data, self.frame_size, self.frame_limit):
            yield convert_to_binary(frame)

    def test(self, dataset, callback):
        self._label_number = len(dataset.labels)
        self.htm.toggle_learning(False)
        return super(HTMAdapter, self).test(dataset, callback)

    def test_one(self, data):
        s = Summer()
        for frame in self.encode(data):
            out = self.htm.get_output(frame)
            r = self.classify(out)
            s.add(r)
        return s.sum

    def train(self, dataset, callback):
        self._x_data = []
        self._y_data = []
        self._label_number = len(dataset.labels)
        self.htm.toggle_learning(True)
        r = super(HTMAdapter, self).train(dataset, callback)
        if self._x_data:
            self.classifier.fit(self._x_data, self._y_data)
        return r

    def train_one(self, data, expected_output):
        s = Summer()
        for frame in self.encode(data):
            out = self.htm.get_output(frame)
            self._x_data.append(out.copy())
            self._y_data.append(expected_output)
            r = self.classify(out)
            s.add(r)
        return s.sum

    def dump(self, path):
        dump_zipped_pickle({
            'config': self.config,
            'frame_limit': self.frame_limit,
            'frame_size': self.frame_size,
            'classifier': self.classifier,
            'htm': self.htm,
            'epoch': self.epoch,
        }, path)

    def load(self, path):
        d = load_zipped_pickle(path)
        self.config = d['config']
        self.frame_limit = d['frame_limit']
        self.frame_size = d['frame_size']
        self.classifier = d['classifier']
        self.htm = d['htm']
        self.epoch = d['epoch']
        self._override_poolers()


class BasicHTMAdapter(HTMAdapter):
    def _override_poolers(self):
        self.pooler_sp = spatial_poolers.SpatialPoolerBasic(self.config)
        self.pooler_tp = temporal_poolers.TemporalPoolerBasic(self.config)
        super(BasicHTMAdapter, self)._override_poolers()


class GPUHTMAdapter(HTMAdapter):
    def _override_poolers(self):
        self.pooler_sp = spatial_poolers.SpatialPoolerGPU(self.config)
        self.pooler_tp = temporal_poolers.TemporalPoolerMultiGPU(self.config)
        # self.pooler_tp = temporal_poolers.TemporalPoolerGPU(self.config)
        # self.pooler_tp = temporal_poolers.TemporalPoolerGPU2(self.config)
        super(GPUHTMAdapter, self)._override_poolers()


class NumbaHTMAdapter(HTMAdapter):
    def _override_poolers(self):
        self.pooler_sp = spatial_poolers.SpatialPoolerNumba(self.config)
        self.pooler_tp = temporal_poolers.TemporalPoolerNumba(self.config)
        super(NumbaHTMAdapter, self)._override_poolers()
