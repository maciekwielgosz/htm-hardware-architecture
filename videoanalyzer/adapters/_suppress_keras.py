"""
Imports keras while overriding stdout and stderr to prevent displaying "Using X backend"
"""

try:  # Python 2
    from StringIO import StringIO
except ImportError:
    from io import StringIO

import sys
stderr = sys.stderr
stdout = sys.stdout
sys.stderr = StringIO('')
sys.stdout = StringIO('')
try:
    import keras
finally:
    sys.stderr = stderr
    sys.stdout = stdout
