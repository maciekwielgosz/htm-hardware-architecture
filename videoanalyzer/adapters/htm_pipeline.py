
from .base import Adapter
from .htm import HTMAdapter, BasicHTMAdapter, GPUHTMAdapter, NumbaHTMAdapter

from ..pipeline.htm import create_pipelines, HTMStep, Pipeline, PrefetcherThread


class HTMPipelineAdapter(HTMAdapter):
    def __init__(self):
        super(HTMPipelineAdapter, self).__init__()
        self.pipeline_train = None  # type: Pipeline
        self.pipeline_test = None  # type: Pipeline

    def compile(self):
        super(HTMPipelineAdapter, self).compile()
        self.logger.debug('Creating pipelines')
        self.pipeline_train, self.pipeline_test = create_pipelines(
            htm=self.htm,
            frame_limit=self.frame_limit,
            frame_size=self.frame_size,
            config=self.config,
            classifier=self.classifier,
        )
        self.logger.debug('Creating pipelines done')

    def run(self, train, test, iterations, train_dataset, test_dataset, train_callback, test_callback):
        state = {}

        if train:  # Prepare for training
            self.logger.debug('Starting training')
            self.pipeline_train.start()
            self.logger.debug('Sending training steps')
            self.pipeline_train.send_steps()
            self.logger.debug('Sending training state')
            state = self.pipeline_train.send_steps_state(state)
            self.logger.debug('Sending toggle_learning command')
            self.pipeline_train.command((HTMStep.TOGGLE_LEARNING, True))
            self.logger.debug('Sending training done')

        if test:  # Prepare for testing
            self.logger.debug('Starting testing')
            self.pipeline_test.start()
            self.logger.debug('Sending testing steps')
            self.pipeline_test.send_steps()
            self.logger.debug('Sending testing done')

        train_fetcher = None
        test_fetcher = None
        ready = False
        for x in range(iterations+1):  # Run for iterations+1 because testing will lag 1 iteration
            if test and x != 0:  # Send testing data (without first iteration)
                yield self.START_TEST
                self.logger.debug('Sending testing state')
                self.pipeline_test.send_steps_state(state)
                self.logger.debug('Sending toggle_learning command')
                self.pipeline_test.command((HTMStep.TOGGLE_LEARNING, False))
                self.logger.debug('Sending testing data')
                self.pipeline_test.send_data((HTMStep.DATA, test_dataset))
                test_fetcher = PrefetcherThread(self.pipeline_test)
                self.logger.debug('Sending testing done')

            if train and x < iterations:  # Send training data (without last iteration)
                yield self.START_TRAIN
                self.logger.debug('Sending training data')
                self.pipeline_train.send_data((HTMStep.DATA, train_dataset))
                train_fetcher = PrefetcherThread(self.pipeline_train)
                self.logger.debug('Sending training done')

            if ready:
                ready = False
                self.logger.debug('Iteration done')
                yield self.END_READY  # Report adapter readiness to be saved

            if test_fetcher:  # Fetch testing data (before training data, because often testing is faster)
                self.logger.debug('Fetching testing results')
                for data_type, data in test_fetcher.fetch_data():
                    if data_type == HTMStep.DATA:
                        test_callback(data['source'], data['label'], data['prediction'])
                # test_fetcher = None
                self.logger.debug('Testing done')
                yield self.END_TEST  # Report end of testing

            if train_fetcher:  # Fetch training data
                self.logger.debug('Fetching training results')
                for data_type, data in train_fetcher.fetch_data():
                    if data_type == HTMStep.DATA:
                        train_callback(data['source'], data['label'], data['prediction'])
                self.logger.debug('Fetching training state')
                state = self.pipeline_train.fetch_steps_state()
                train_fetcher = None
                self.epoch += 1
                self.logger.debug('Training done')
                yield self.END_TRAIN  # Report end of training
                ready = True

    def stop(self):
        if self.pipeline_train:
            self.pipeline_train.stop()
        if self.pipeline_test:
            self.pipeline_test.stop()


class BasicHTMPipelineAdapter(BasicHTMAdapter, HTMPipelineAdapter):
    pass


class GPUHTMPipelineAdapter(GPUHTMAdapter, HTMPipelineAdapter):
    pass


class NumbaHTMPipelineAdapter(NumbaHTMAdapter, HTMPipelineAdapter):
    pass




