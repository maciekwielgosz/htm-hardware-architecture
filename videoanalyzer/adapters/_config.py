
from htmproject.interfaces.config import Config


config = Config({
    "core": {
        "htm": {
            "layers": 5,
            "columns_per_layer": [
                600, 512, 512, 256, 128
            ],
            "cells_per_column": [
                16,    32,    64,   64,   128
            ]
        },

        "sp": {
            "synapses_per_column": [64, 32, 32, 64, 128],
            "min_overlap": [4, 2, 2, 1, 1],
            "perm_inc": 0.002,
            "perm_dec": 0.002,
            "winners_set_size": [20, 20, 16, 16, 16]
        },

        "tp": {
            "perm_inc": 0.002,
            "perm_dec": 0.002,
            "work_group_size": [2, 1, 1, 1, 1],
            "init_perm": 0.40,
            "connected_perm": 0.50,
            "min_segment_activation_threshold": [4, 2, 1, 1, 1],
            "max_number_of_segments": [32, 32, 32, 32, 64],
            "max_number_of_synapses_per_segment": [32, 32, 32, 32, 64],
            "is_learning_active": 1,
            "segment_activation_threshold": [5, 3, 2, 1, 1],
            "max_number_of_new_connections": [2, 2, 2, 1, 1],
        }
    },
    "encoder": {
        # "frame_size": [960, 540]
        "frame_size": [320, 240],
        "frame_limit": 4,
    },
})
