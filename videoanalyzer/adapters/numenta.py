
import cv2
import math
import numpy as np
from io import BytesIO

from htmproject.encoders.adaptive_frame import AdaptiveFrameEncoder
from htmproject.interfaces.config import Config
from htmproject.classifiers.linear_svm import LinearSVMClassifier

# from nupic.data.inference_shifter import InferenceShifter
# from nupic.frameworks.opf.model_factory import ModelFactory
# from nupic.algorithms.sdr_classifier_factory import SDRClassifierFactory
from nupic.algorithms.spatial_pooler import SpatialPooler
from nupic.algorithms.temporal_memory import TemporalMemory

from .base import Adapter
from ..utils import pickle


# TODO: The implementation is outdated. Use newer API and utils
class NupicLayer(object):
    def __init__(self, input_length, sp_params, tm_params):
        self.input_length = input_length
        self.column_count = sp_params["columnCount"]
        self.cells_per_column = tm_params["cellsPerColumn"]
        self.sp = SpatialPooler(
            inputDimensions=(input_length,),
            columnDimensions=(self.column_count,),
            potentialRadius=input_length,
            potentialPct=sp_params["potentialPct"],
            globalInhibition=sp_params["globalInhibition"],
            localAreaDensity=sp_params["localAreaDensity"],
            numActiveColumnsPerInhArea=sp_params["numActiveColumnsPerInhArea"],
            stimulusThreshold=sp_params["stimulusThreshold"],
            synPermInactiveDec=sp_params["synPermInactiveDec"],
            synPermActiveInc=sp_params["synPermActiveInc"],
            synPermConnected=sp_params["synPermConnected"],
            minPctOverlapDutyCycle=sp_params["minPctOverlapDutyCycle"],
            dutyCyclePeriod=sp_params["dutyCyclePeriod"],
            boostStrength=sp_params["boostStrength"],
            seed=sp_params["seed"],
            spVerbosity=sp_params["spVerbosity"],
            wrapAround=sp_params["wrapAround"],
        )
        self.tm = TemporalMemory(
            columnDimensions=(self.column_count,),
            cellsPerColumn=self.cells_per_column,
            activationThreshold=tm_params["activationThreshold"],
            initialPermanence=tm_params["initialPermanence"],
            connectedPermanence=sp_params["synPermConnected"],
            minThreshold=tm_params["minThreshold"],
            maxNewSynapseCount=tm_params["newSynapseCount"],
            permanenceIncrement=tm_params["permanenceIncrement"],
            permanenceDecrement=tm_params["permanenceDecrement"],
            predictedSegmentDecrement=tm_params["predictedSegmentDecrement"],
            maxSegmentsPerCell=tm_params["maxSegmentsPerCell"],
            maxSynapsesPerSegment=tm_params["maxSynapsesPerSegment"],
            seed=tm_params["seed"],
        )

    def get_output(self, input_data, learn=True):
        active_columns = np.zeros(self.column_count)
        self.sp.compute(input_data, activeArray=active_columns, learn=learn)
        active_column_indices = np.nonzero(active_columns)[0]
        self.tm.compute(active_column_indices, learn=learn)
        active_cells_indices = self.tm.getActiveCells()
        predictive_cell_indices = self.tm.getPredictiveCells()
        return self._cells_to_columns(active_cells_indices, predictive_cell_indices)

    def _cells_to_columns(self, active_cells_indices, predictive_cell_indices):
        cells = np.zeros(self.column_count * self.cells_per_column, np.bool)
        cells[active_cells_indices] = True
        cells[predictive_cell_indices] = True
        columns = np.amax(cells.reshape((self.column_count, self.cells_per_column)), axis=1)
        return columns


class NupicHTMWrapper(object):
    def __init__(self, config, input_length):
        self.config = config
        self.layers = []
        for i in range(int(config.get('core.htm.layers'))):
            columns_per_layer = int(config.get('core.htm.columns_per_layer')[i])
            cells_per_column = int(config.get('core.htm.cells_per_column')[i])
            synapses_per_column = int(config.get('core.sp.synapses_per_column')[i])
            # active_columns = int(config.get('core.sp.active_columns')[i])
            min_overlap = int(config.get('core.sp.min_overlap')[i])
            winners_set_size = int(config.get('core.sp.winners_set_size')[i])
            sp_perm_inc = float(config.get('core.sp.perm_inc'))
            sp_perm_dec = float(config.get('core.sp.perm_dec'))
            sp_init_perm = float(config.get('core.sp.init_perm'))
            sp_connected_perm = float(config.get('core.sp.connected_perm'))
            sp_boost = float(config.get('core.sp.boost'))

            tp_init_perm = float(config.get('core.tp.init_perm'))
            tp_perm_inc = float(config.get('core.tp.perm_inc'))
            tp_perm_dec = float(config.get('core.tp.perm_dec'))
            max_number_of_segments = int(config.get('core.tp.max_number_of_segments')[i])
            max_number_of_synapses_per_segment = int(config.get('core.tp.max_number_of_synapses_per_segment')[i])
            segment_activation_threshold = int(config.get('core.tp.segment_activation_threshold')[i])
            min_segment_activation_threshold = int(config.get('core.tp.min_segment_activation_threshold')[i])
            max_number_of_new_connections = int(config.get('core.tp.max_number_of_new_connections')[i])

            self.layers.append(
                NupicLayer(
                    input_length=input_length,
                    sp_params={
                        'inputWidth': input_length,
                        'columnCount': columns_per_layer,
                        'globalInhibition': True,
                        'localAreaDensity': -1.0,
                        'numActiveColumnsPerInhArea': winners_set_size,  # math.ceil(0.02 * columns_per_layer),
                        'stimulusThreshold': min_overlap,
                        'seed': 42,
                        'potentialPct': 0.5,
                        'synPermConnected': sp_connected_perm,
                        'synPermActiveInc': sp_perm_inc,
                        'synPermInactiveDec': sp_perm_dec,
                        'minPctOverlapDutyCycle': 0.001,
                        'dutyCyclePeriod': 1000,
                        'boostStrength': sp_boost,
                        'spVerbosity': 0,
                        'wrapAround': True,
                    },
                    tm_params={
                        'columnCount': columns_per_layer,
                        'cellsPerColumn': cells_per_column,
                        'inputWidth': columns_per_layer,
                        'seed': 42,
                        'newSynapseCount': max_number_of_new_connections,
                        'initialPermanence': tp_init_perm,
                        'permanenceIncrement': tp_perm_inc,
                        'permanenceDecrement': tp_perm_dec,
                        'predictedSegmentDecrement': 0.0,
                        'maxSynapsesPerSegment': max_number_of_synapses_per_segment,
                        'maxSegmentsPerCell': max_number_of_segments,
                        'minThreshold': min_segment_activation_threshold,
                        'activationThreshold': segment_activation_threshold,
                    }
                )
            )
            input_length = columns_per_layer
            # input_length = columns_per_layer * cells_per_column

    def get_output(self, input_data, learn=True):
        for layer in self.layers:
            input_data = layer.get_output(input_data, learn=learn)
        return input_data


class NupicAdapter(Adapter):
    def __init__(self):
        super(NupicAdapter, self).__init__()
        self.config = None
        self.frame_limit = 0
        self.encoder = None
        self.wrapper = None
        self.classifier = None
        self._x_data = []
        self._y_data = []

    def create(self):
        from ._config import config
        self.config = config
        self.frame_limit = self.config.get("encoder.frame_limit")
        if self.wrapper is None:
            self.wrapper = NupicHTMWrapper(self.config, 320*240)
        self.classifier = LinearSVMClassifier(self.config)

    def compile(self):
        self.encoder = AdaptiveFrameEncoder(self.config)

    def test_one(self, data):
        inp = self.get_input(data)
        # result = self.shifter.shift(self.model.run({"token": inp}))
        result = self.wrapper.get_output(inp, learn=False)
        return self.classify(result)

    def train_one(self, data, expected_output):
        inp = self.get_input(data)
        # result = self.shifter.shift(self.model.run({"token": expected_output}))
        # result = self.model.run({"token": expected_output})
        # result = self.model.run({"token": [expected_output]})
        # rr = result.inferences["multiStepPredictions"][1]
        # print(rr)
        # return np.array([nfind(rr, x) if rr else 0.0 for x in range(5)])
        result = self.wrapper.get_output(inp, learn=True)
        self._x_data.append(result)
        self._y_data.append(expected_output)
        return self.classify(result)

    def train(self, dataset, callback):
        self._x_data = []
        self._y_data = []
        super(NupicAdapter, self).train(dataset, callback)
        self.classifier.fit(self._x_data, self._y_data)
        self._x_data = []
        self._y_data = []

    def get_input(self, path):
        frame_num = 0
        capture = cv2.VideoCapture(path)
        while True:
            flag, frame = capture.read()
            frame_num += 1
            # if (not flag) or (frame_num > self.frame_limit and self.frame_limit != -1):
            #     break
            ei = self.encoder.encode(frame)
            return ei

    def classify(self, htm_out):
        if hasattr(self.classifier._model, 'coef_') and self.classifier._model.coef_ is not None:
            rr = self.classifier.classify([htm_out])
        else:
            rr = [-1]
        return np.array([1.0 if x == rr[0] else 0.0 for x in range(5)])

    def dump(self, path):
        pickle.dump((self.epoch, self.wrapper, self.classifier), open(path, 'wb'), pickle.HIGHEST_PROTOCOL)

    def load(self, path):
        self.epoch, self.wrapper, self.classifier = pickle.load(open(path, 'rb'))
