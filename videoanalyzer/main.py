from __future__ import print_function

from .logging_setup import logging  # configure logging before anything is imported


import random
import sys
import os

import numpy as np

from .pipeline.utils import profile_process, handle_process_fault
from .pipeline.profile import global_profiler


# @handle_process_fault('fault')
# @profile_process('main')
# @global_profiler.time_profile_process('time')
def main():
    """
    Run videoanalyzer using predefined cli
    """
    # Init
    # os.environ.update(PYOPENCL_DEVICE_INDEX='-1')  # Use the slowest device
    np.random.seed(42)
    random.seed(42)

    from .predefined import get_predefined_cli
    cli = get_predefined_cli()
    # Run
    r = cli.run()
    return r  # sys.exit(r or 0)


def main_optimize():
    """
    Run optimizer
    """
    from .optimizer.main import main as main_
    return main_()


def main_test():
    """
    Run tests
    """
    from .test import main as main_
    return main_()


def main_speed():
    """
    Run speed test
    """
    from .speed_test import main as main_
    main_()
