
from .pipeline.test import main as pipeline_test


def main():
    pipeline_test()


if __name__ == '__main__':
    main()
