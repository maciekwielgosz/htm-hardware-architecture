import logging

try:
    import openpyxl
except ImportError:
    logging.exception('No openpyxl')


class BaseLogger(object):
    def __init__(self):
        pass

    def log_progress(self, value):
        raise NotImplemented

    def log_metric(self, stage, epoch, name, value):
        raise NotImplemented

    def log_result(self, stage, expected_output, output, index, total):
        raise NotImplemented

    def finish(self, path):
        pass


class LoggingLogger(BaseLogger):
    @property
    def metrics_logger(self):
        return logging.getLogger('videoanalyzer.progress')

    @property
    def progress_logger(self):
        return logging.getLogger('videoanalyzer.metrics')

    def log_progress(self, value):
        self.progress_logger.info(value)

    def log_metric(self, stage, epoch, name, value):
        self.metrics_logger.info('%s) %s: %s', epoch, name, value)

    def log_result(self, stage, expected_output, output, index, total):
        self.progress_logger.info('%s %s/%s: %s -> %s', stage, index, total, expected_output, output)


class XLSXLogger(LoggingLogger):
    def __init__(self, filename=None):
        super(XLSXLogger, self).__init__()
        self.filename = filename
        self.data = []

    def log_metric(self, stage, epoch, name, value):
        super(XLSXLogger, self).log_metric(stage, epoch, name, value)
        self.data.append([stage, epoch, name, self._process_value(value)])

    def _process_value(self, value):
        try:
            return float(str(value).split()[0])
        except ValueError:
            return value

    def finish(self, path):
        try:
            wb = openpyxl.load_workbook(path)
        except Exception:
            wb = openpyxl.Workbook()
            wb.active.append(['Stage', 'Epoch', 'Metric', 'Value'])

        ws = wb.active
        for d in self.data:
            ws.append(d)

        # TODO: add auto filtering & charts

        self.data = []
        wb.save(path)
