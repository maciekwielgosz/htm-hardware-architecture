
from .cli import CLI
# from .configuration import RunConfiguration
from .metrics import DurationMetric, AccuracyMetric, TopKAccuracyMetric, ConfusionMatrixMetric, F1ScoreMetric
from .adapters import PerfectAdapter, WrongAdapter, RandomAdapter
from .adapters import BasicHTMAdapter, GPUHTMAdapter, NumbaHTMAdapter
from .adapters import BasicHTMPipelineAdapter, GPUHTMPipelineAdapter, NumbaHTMPipelineAdapter
from .adapters import C3DPreTrainedAdapter, C3DDenseAdapter, C3DSVMAdapter
from .adapters import NupicAdapter
from .datasets import DirectoryDataset, FileSplitDataset, SimpleDataset, split_dataset
from .loggers import LoggingLogger, XLSXLogger


# Adapters
perfect = PerfectAdapter()
wrong = WrongAdapter()
random = RandomAdapter()

c3d_pretrained = C3DPreTrainedAdapter('./c3d-keras/models')
c3d_dense = C3DDenseAdapter('./c3d-keras/models')
c3d_svm = C3DSVMAdapter('./c3d-keras/models')

htm_basic = BasicHTMAdapter()
htm_gpu = GPUHTMAdapter()
htm_numba = NumbaHTMAdapter()
htm_basic_pipeline = BasicHTMPipelineAdapter()
htm_gpu_pipeline = GPUHTMPipelineAdapter()
htm_numba_pipeline = NumbaHTMPipelineAdapter()

nupic = NupicAdapter()


# Datasets
no = SimpleDataset()

ucf101 = DirectoryDataset('./UCF-101/', shuffle=True)  # 13 320
ucf101_train, ucf101_test = split_dataset(ucf101, 10820)  # 10820, 2500
ucf101_train1 = FileSplitDataset('./UCF-101/classInd.txt', './UCF-101/trainlist01.txt', shuffle=True)  # 9537
ucf101_test1 = FileSplitDataset('./UCF-101/classInd.txt', './UCF-101/testlist01.txt', shuffle=True)  # 3783
ucf101_train2 = FileSplitDataset('./UCF-101/classInd.txt', './UCF-101/trainlist02.txt')
ucf101_test2 = FileSplitDataset('./UCF-101/classInd.txt', './UCF-101/testlist02.txt')
ucf101_train3 = FileSplitDataset('./UCF-101/classInd.txt', './UCF-101/trainlist03.txt')
ucf101_test3 = FileSplitDataset('./UCF-101/classInd.txt', './UCF-101/testlist03.txt')

# ucf5 = DirectoryDataset('./UCF-5/', shuffle=True)  # 698
ucf5_i = [ucf101.labels.index(l) for l in ('Basketball', 'Biking', 'Diving', 'HorseRiding', 'VolleyballSpiking')]
ucf5 = SimpleDataset([(k, ucf5_i.index(v)) for k, v in ucf101.all_data if v in ucf5_i])  # 698
ucf5_train, ucf5_test = split_dataset(ucf5, 578)  # 578, 120

test = SimpleDataset([ucf5.all_data[0], ucf5.all_data[174], ucf5.all_data[349], ucf5.all_data[523], ucf5.all_data[-1]])


# Metrics
duration = DurationMetric.as_function()
accuracy = AccuracyMetric.as_function()
accuracy3 = TopKAccuracyMetric.as_function(3)
accuracy5 = TopKAccuracyMetric.as_function(5)
f1score = F1ScoreMetric.as_function()
confusion_matrix = ConfusionMatrixMetric.as_function()


# Loggers
log = LoggingLogger()
xlsx = XLSXLogger()


def get_predefined_cli():
    cli = CLI()

    cli.adapters['perfect'] = perfect
    cli.adapters['wrong'] = wrong
    cli.adapters['random'] = random
    cli.adapters['c3d_pretrained'] = c3d_pretrained
    cli.adapters['c3d_dense'] = c3d_dense
    cli.adapters['c3d_svm'] = c3d_svm
    cli.adapters['htm_basic'] = htm_basic
    cli.adapters['htm_gpu'] = htm_gpu
    cli.adapters['htm_numba'] = htm_numba
    cli.adapters['htm_basic_pipeline'] = htm_basic_pipeline
    cli.adapters['htm_gpu_pipeline'] = htm_gpu_pipeline
    cli.adapters['htm_numba_pipeline'] = htm_numba_pipeline
    cli.adapters['nupic'] = nupic

    cli.datasets['no'] = no
    cli.datasets['5'] = test
    cli.datasets['ucf5'] = ucf5
    cli.datasets['ucf5_train'] = ucf5_train
    cli.datasets['ucf5_test'] = ucf5_test
    cli.datasets['ucf101'] = ucf101
    cli.datasets['ucf101_train'] = ucf101_train
    cli.datasets['ucf101_test'] = ucf101_test
    cli.datasets['ucf101_train1'] = ucf101_train1
    cli.datasets['ucf101_test1'] = ucf101_test1
    cli.datasets['ucf101_train2'] = ucf101_train2
    cli.datasets['ucf101_test2'] = ucf101_test2
    cli.datasets['ucf101_train3'] = ucf101_train3
    cli.datasets['ucf101_test3'] = ucf101_test3

    cli.metrics['duration'] = duration
    cli.metrics['accuracy'] = accuracy
    cli.metrics['accuracy3'] = accuracy3
    cli.metrics['accuracy5'] = accuracy5
    cli.metrics['f1score'] = f1score
    cli.metrics['confusion_matrix'] = confusion_matrix

    cli.loggers['log'] = log
    cli.loggers['xlsx'] = xlsx

    return cli

