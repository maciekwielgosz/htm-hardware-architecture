from __future__ import print_function
import logging
import random
import os

from .utils import typing


def _load(x):
    return x


def split_dataset(dataset, train_num, shuffle=42, shuffle_train=True, shuffle_test=False):
    """
    Split given dataset into two datasets where the first one has train_num first elements and the second has the rest

    :param Dataset dataset: dataset to split
    :param int train_num: number of elements in the first dataset
    :param bool shuffle: shuffle given dataset data before splitting
    :param bool shuffle_train: shuffle the first dataset
    :param bool shuffle_test: shuffle the second dataset
    :return: two datasets
    :rtype typing.Tuple[SimpleDataset, SimpleDataset]
    """
    data = dataset.all_data[:] if hasattr(dataset, 'all_data') else list(dataset)
    if shuffle:
        random.Random(shuffle).shuffle(data)
    train = SimpleDataset(data[:train_num], dataset.labels, shuffle=shuffle_train)
    test = SimpleDataset(data[train_num:], dataset.labels, shuffle=shuffle_test)
    return train, test


class Dataset(object):
    """
    Base class for all dataset types
    """
    def __init__(self):
        self.labels = self.get_all_labels()
        self.logger = self._logger()

    def _logger(self):
        return logging.getLogger('videoanalyzer.dataset')

    def __len__(self):
        return 0

    def __iter__(self):
        raise NotImplemented()

    def get_all_labels(self):
        return []

    def __getstate__(self):
        state = {}
        state.update(self.__dict__)
        del state['logger']
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)
        self.logger = self._logger()


class SimpleDataset(Dataset):
    """
    Dataset initialized with list of data and some additional functionality
    """
    def __init__(self, all_data=None, all_labels=None, load=_load, shuffle=False, limit=-1):
        """
        :param typing.Iterable[typing.Tuple[typing.Any, int]] all_data: iterable of tuples (data, label) representing the whole dataset
        :param typing.Iterable[int] all_labels: iterable of labels or None
        :param typing.Callable[[typing.Any], typing.Any] load: function converting data to actual data content
        :param bool shuffle: shuffle the dataset after every iteration
        :param int limit: limit the dataset to the given length (-1 for no limit)
        """
        if all_data is None:
            all_data = []
        self.all_data = list(all_data)
        if all_labels is None:
            if all_data is None:
                all_labels = []
            else:
                all_labels = sorted(set([label for data, label in self.all_data]))
        self.labels = list(all_labels)
        self.shuffle = shuffle
        self.load = load
        self.limit = limit
        super(SimpleDataset, self).__init__()

    def get_all_labels(self):
        return self.labels

    def __len__(self):
        return len(self.all_data) if self.limit == -1 else self.limit

    def __iter__(self):
        all_data = self.all_data
        if self.limit >= 0:
            all_data = self.all_data[:self.limit]
        if self.shuffle:
            random.shuffle(all_data)
        l = len(all_data)
        for i, _data in enumerate(all_data, 1):
            data, label = _data
            self.logger.info('%d/%d %s' % (i, l, data))
            yield self.load(data), label


class DirectoryDataset(SimpleDataset):
    """
    Dataset created from a filesystem where directory names are labels
    """
    def __init__(self, path, load=_load, shuffle=False, limit=-1):
        self.path = path
        all_labels = sorted([p for p in os.listdir(self.path) if os.path.isdir(os.path.join(self.path, p))])
        all_data = []
        for label in all_labels:
            for f in os.listdir(os.path.join(path, label)):
                p = os.path.join(path, label, f)
                if os.path.isfile(p):
                    all_data.append((p, all_labels.index(label)))
        super(DirectoryDataset, self).__init__(all_data, all_labels, load, shuffle, limit)


class FileSplitDataset(SimpleDataset):
    def __init__(self, classes_path, split_path, load=_load, shuffle=False, limit=-1):
        # all_labels = ['Unknown']
        all_labels = []
        for d in open(classes_path, 'r').readlines():
            if d:
                all_labels.append(d.strip().split(' ', 1)[-1].strip())
        all_data = []
        dir_path = os.path.dirname(split_path)
        for d in open(split_path, 'r').readlines():
            if d:
                data = d.strip().rsplit(' ', 1)[0].strip()
                all_data.append((os.path.join(dir_path, data), all_labels.index(data.split('/', 1)[0])))

        super(FileSplitDataset, self).__init__(all_data, all_labels, load, shuffle, limit)

