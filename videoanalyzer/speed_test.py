
from __future__ import print_function

import random
import timeit
import math
import numpy as np


class NoSlots(object):
    def __init__(self):
        self.a, self.b, self.c = 1, 2, 3

    def get_a(self):
        return self.a


class Slots(object):
    __slots__ = ('a', 'b', 'c')

    def __init__(self):
        self.a, self.b, self.c = 1, 2, 3

    def reset(self):
        self.a, self.b, self.c = 1, 2, 3

    def get_a(self):
        return self.a


def f1(a, b):
    return a + b


def f2(a, b):
    return f1(a, b)


def ilen(i):
    l = 0
    for _ in i:
        l += 1
    return l


def py2_timeit_repeat(stmt, repeat, number, globals):  # python2 hack
    import __builtin__
    bdict = __builtin__.__dict__.copy()
    __builtin__.__dict__.update(globals)  # update  __builtin__

    timer = timeit.repeat(stmt, repeat=repeat, number=number)

    for k in globals.keys():  # restore __builtin__
        if k in bdict:
            __builtin__.__dict__[k] = bdict[k]
        else:
            if k in __builtin__.__dict__:
                del __builtin__.__dict__[k]

    return timer


def time_it(stmt, repeat=100, number=100000, *dicts):
    gl = {}
    for d in dicts:
        gl.update(d)
    try:
        timer = timeit.repeat(stmt, repeat=repeat, number=number, globals=gl)
    except TypeError:
        timer = py2_timeit_repeat(stmt, repeat=repeat, number=number, globals=gl)
    r = sum(timer)
    print(stmt.replace('\n', '\\n').ljust(70, ' '), '%.10fs' % r)
    return r


def ab_improvement(slower, faster, end='\n'):
    print(' '*70, '%.2f%%%s' % (100.0*(1.0-faster/slower), end))


def main():
    print('Heating up')
    time_it('sum(map(math.sqrt, (x*(x+1) for x in range(3000000))))', 1, 1, {'math': math})
    print('Done\n')

    ns = NoSlots()
    ns2 = NoSlots()
    s = Slots()
    s2 = Slots()
    ns2.s = s
    d1 = [random.random() for _ in range(10)]
    d2 = [random.random() for _ in range(1000)]
    d3 = [random.random() for _ in range(100000)]

    a = time_it('ns.a = ns.b*ns.c', 250, 100000, {'ns': ns})
    b = time_it('s.a = s.b*s.c', 250, 100000, {'s': s})
    ab_improvement(a, b)

    a = time_it('NoSlots()', 100, 100000, {'NoSlots': NoSlots})
    b = time_it('Slots()', 100, 100000, {'Slots': Slots})
    ab_improvement(a, b)

    a = time_it('ns2.s = Slots()', 150, 100000, {'s': s, 'ns2': ns2, 'Slots': Slots})
    b = time_it('ns2.s.a,ns2.s.b,ns2.s.c = 1,2,3', 150, 100000, {'ns': ns, 'ns2': ns2})
    ab_improvement(a, b, '')
    c = time_it('ns2.s.reset()', 150, 100000, {'s': s, 'ns2': ns2, 'Slots': Slots})
    ab_improvement(a, c)

    rep, cou = 100, 10000
    a = time_it('s=np.count_nonzero([x > 0.5 for x in d1])', rep, cou, {'d1': d1, 'np': np})
    f = time_it('d=np.asarray(d1)\ns=len(d[d>0.5])', rep, cou, {'d1': d1, 'np': np})
    ab_improvement(a, f, '')
    g = time_it('s=np.sum(np.asarray(d1)>0.5)', rep, cou, {'d1': d1, 'np': np})
    ab_improvement(a, g, '')
    h = time_it('s=np.count_nonzero(np.asarray(d1)>0.5)', rep, cou, {'d1': d1, 'np': np})
    ab_improvement(a, h, '')
    b = time_it('s=0\nfor x in d1:\n if x > 0.5:\n  s+=1', rep, cou, {'d1': d1})
    ab_improvement(a, b, '')
    c = time_it('s=sum([1 for x in d1 if x > 0.5])', rep, cou, {'d1': d1})
    ab_improvement(a, c, '')
    d = time_it('s=sum((1 for x in d1 if x > 0.5))', rep, cou, {'d1': d1})
    ab_improvement(a, d, '')
    f = time_it('s=ilen((1 for x in d1 if x > 0.5))', rep, cou, {'d1': d1, 'ilen': ilen})
    ab_improvement(a, f, '')
    e = time_it('s=len([1 for x in d1 if x > 0.5])', rep, cou, {'d1': d1})
    ab_improvement(a, e)

    rep, cou = 50, 1000
    a = time_it('s=np.count_nonzero([x > 0.5 for x in d2])', rep, cou, {'d2': d2, 'np': np})
    f = time_it('d=np.asarray(d2)\ns=len(d[d>0.5])', rep, cou, {'d2': d2, 'np': np})
    ab_improvement(a, f, '')
    g = time_it('s=np.sum(np.asarray(d2)>0.5)', rep, cou, {'d2': d2, 'np': np})
    ab_improvement(a, g, '')
    h = time_it('s=np.count_nonzero(np.asarray(d2)>0.5)', rep, cou, {'d2': d2, 'np': np})
    ab_improvement(a, h, '')
    b = time_it('s=0\nfor x in d2:\n if x > 0.5:\n  s+=1', rep, cou, {'d2': d2})
    ab_improvement(a, b, '')
    c = time_it('s=sum([1 for x in d2 if x > 0.5])', rep, cou, {'d2': d2})
    ab_improvement(a, c, '')
    d = time_it('s=sum((1 for x in d2 if x > 0.5))', rep, cou, {'d2': d2})
    ab_improvement(a, d, '')
    f = time_it('s=ilen((1 for x in d2 if x > 0.5))', rep, cou, {'d2': d2, 'ilen': ilen})
    ab_improvement(a, f, '')
    e = time_it('s=len([1 for x in d2 if x > 0.5])', rep, cou, {'d2': d2})
    ab_improvement(a, e)

    rep, cou = 1, 500
    a = time_it('s=np.count_nonzero([x > 0.5 for x in d3])', rep, cou, {'d3': d3, 'np': np})
    f = time_it('d=np.asarray(d3)\ns=len(d[d>0.5])', rep, cou, {'d3': d3, 'np': np})
    ab_improvement(a, f, '')
    g = time_it('s=np.sum(np.asarray(d3)>0.5)', rep, cou, {'d3': d3, 'np': np})
    ab_improvement(a, g, '')
    h = time_it('s=np.count_nonzero(np.asarray(d3)>0.5)', rep, cou, {'d3': d3, 'np': np})
    ab_improvement(a, h, '')
    b = time_it('s=0\nfor x in d3:\n if x > 0.5:\n  s+=1', rep, cou, {'d3': d3})
    ab_improvement(a, b, '')
    c = time_it('s=sum([1 for x in d3 if x > 0.5])', rep, cou, {'d3': d3})
    ab_improvement(a, c, '')
    d = time_it('s=sum((1 for x in d3 if x > 0.5))', rep, cou, {'d3': d3})
    ab_improvement(a, d, '')
    f = time_it('s=ilen((1 for x in d3 if x > 0.5))', rep, cou, {'d3': d3, 'ilen': ilen})
    ab_improvement(a, f, '')
    e = time_it('s=len([1 for x in d3 if x > 0.5])', rep, cou, {'d3': d3})
    ab_improvement(a, e)

    a = time_it('s=f1(2, 3)', 100, 100000, {'f1': f1})
    b = time_it('s=f2(2, 3)', 100, 100000, {'f2': f2})
    ab_improvement(a, b)

    a = time_it('a=ns.get_a()', 100, 100000, {'ns': ns})
    b = time_it('a=ns.a', 100, 100000, {'ns': ns})
    ab_improvement(a, b)
    a = time_it('a=s.get_a()', 100, 100000, {'s': s})
    b = time_it('a=s.a', 100, 100000, {'s': s})
    ab_improvement(a, b)


if __name__ == '__main__':
    main()

