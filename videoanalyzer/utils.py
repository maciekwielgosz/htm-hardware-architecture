
import gzip
import gc
import logging

import numpy
import cv2
from skimage.transform import resize
from skimage.color import rgb2gray
from skimage.filters import threshold_otsu


try:
    import cPickle as pickle
except ImportError:
    import pickle

try:
    import typing
except ImportError:  # Python2
    # logging.getLogger('videoanalyzer').warning('No typing')
    typing = None


def dump_zipped_pickle(obj, path, protocol=pickle.HIGHEST_PROTOCOL, compression=1, disable_gc=True, *args, **kwargs):
    with gzip.open(path, 'wb', compression) as f:  # compression = 1 gives 1.5s overhead for 96% compression
        p = pickle.Pickler(f, protocol, *args, **kwargs)
        if disable_gc:
            gc.disable()
        try:
            return p.dump(obj)
        finally:
            if disable_gc:
                gc.enable()
                gc.collect()


def load_zipped_pickle(path, disable_gc=True, *args, **kwargs):
    with gzip.open(path, 'rb') as f:
        p = pickle.Unpickler(f, *args, **kwargs)
        if disable_gc:
            gc.disable()
        try:
            return p.load()
        finally:
            if disable_gc:
                gc.enable()
                gc.collect()


class Summer(object):
    def __init__(self):
        self.sum = None
        self.num = 0

    def clear(self):
        self.sum = None
        self.num = 0

    def add(self, a):
        if self.sum is None:
            self.sum = a
        else:
            self.sum += a
        self.num += 1
        return self.sum

    def normalized_sum(self):
        s = self.sum.sum()
        return (self.sum / s) if s != 0 else self.sum

    def average(self):
        return self.sum / self.num


def convert_to_binary(frame):
        gray = rgb2gray(frame)
        try:
            thresh = threshold_otsu(gray)
        except (TypeError, ValueError):
            thresh = 0.5
        binary = gray > thresh
        frame = binary.flatten()
        return frame


def read_video_frames(path, target_frame_size, frame_limit=-1):
    frame_num = 0
    capture = cv2.VideoCapture(path)
    while True:
        flag, frame = capture.read()
        if (not flag) or (frame_num >= frame_limit and frame_limit != -1):
            break
        # if frame.shape[0] != target_frame_size[1] or frame.shape[1] != target_frame_size[0]:
        frame = resize(frame, (target_frame_size[1], target_frame_size[0]), mode='edge', preserve_range=True)
        yield frame_num, frame
        frame_num += 1
