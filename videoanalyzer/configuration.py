
from __future__ import print_function
import os
import time

from .loggers import BaseLogger, LoggingLogger
from .adapters.base import Adapter
from .datasets import Dataset, SimpleDataset
from .metrics import Metric
from .utils import typing


class StopException(Exception):
    pass


class RunConfiguration(object):
    """
    Connects and runs adapter, datasets, metrics and logger
    """
    def __init__(self, adapter, training_dataset=None, testing_dataset=None, training_metrics=None, testing_metrics=None, logger=None):
        self.adapter = adapter  # type: Adapter
        self.training_dataset = training_dataset if training_dataset is not None else SimpleDataset()  # type: Dataset
        self.testing_dataset = testing_dataset if testing_dataset is not None else SimpleDataset()  # type: Dataset
        self.training_metrics = training_metrics if training_metrics is not None else []  # type: typing.List[Metric]
        self.testing_metrics = testing_metrics if testing_metrics is not None else []  # type: typing.List[Metric]
        self.training_metrics_values = []  # type: typing.List(typing.List(typing.Any))
        self.testing_metrics_values = []  # type: typing.List(typing.List(typing.Any))
        self.logger = logger if logger is not None else LoggingLogger()  # type: BaseLogger
        # self._index = 0
        # self._total = 0

    def clear(self):
        self.training_metrics_values = []
        self.testing_metrics_values = []

    def _create(self):
        self.logger.log_progress('Creating...')
        self.adapter.create()

    def _load(self, path):
        self.logger.log_progress('Loading...')
        self.adapter.load(path)
        self.logger.log_progress('Loaded adapter with epoch %s' % self.adapter.epoch)

    def _save(self, path):
        self.logger.log_progress('Saving...')
        self.adapter.dump(path)

    def _compile(self):
        self.logger.log_progress('Compiling...')
        self.adapter.compile()

    def _end_train(self, iteration, iterations):
        self.logger.log_progress('Metrics for training epoch %s (%s/%s):' % (self.adapter.epoch, iteration, iterations))
        values = []
        for index, metric in enumerate(self.training_metrics):
            value = metric.get_value()
            values.append(value)
            self.logger.log_metric('Train', self.adapter.epoch, metric.name, value)
            # metric.start(self.training_dataset)
        self.training_metrics_values.append(values)

    def _end_test(self, iteration, iterations):
        self.logger.log_progress('Metrics for testing epoch %s (%s/%s):' % (self.adapter.epoch, iteration, iterations))
        values = []
        for index, metric in enumerate(self.testing_metrics):
            value = metric.get_value()
            values.append(value)
            self.logger.log_metric('Test', self.adapter.epoch, metric.name, value)
            # metric.start(self.training_dataset)
        self.testing_metrics_values.append(values)

    def run(self, create, train, test, load_path, save_path, iterations, save_end, save_epoch, save_error, log_path):
        """
        Run the configuration

        :param bool create: create fresh adapter
        :param bool train: train adapter
        :param bool test: test adapter
        :param str load_path: adapter load path
        :param str save_path: adapter save path
        :param int iterations: number of iterations to run
        :param bool save_end: save on the end
        :param bool save_epoch: save each epoch
        :param bool save_error: save on error
        :param str log_path: path for saving the logs (xlsx)
        """
        self.logger.log_progress('Running %s%s%s--iterations %s' % (
            '--create ' if create else '',
            '--train ' if train else '',
            '--test ' if test else '',
            iterations,
        ))

        if create:
            self._create()
        else:
            self._load(load_path)
        self._compile()
        if create and (not train) and save_end:
            self._save(save_path)

        training_i = 0
        testing_i = 0
        try:
            trcb = self._make_callback(self.training_metrics, len(self.training_dataset), 'Training')
            tecb = self._make_callback(self.testing_metrics, len(self.testing_dataset), 'Testing')
            gen = self.adapter.run(train, test, iterations, self.training_dataset, self.testing_dataset, trcb, tecb)
            for ret in gen:
                if ret == Adapter.START_TRAIN:
                    for metric in self.training_metrics:
                        metric.start(self.training_dataset)
                elif ret == Adapter.START_TEST:
                    for metric in self.testing_metrics:
                        metric.start(self.testing_dataset)
                elif ret == Adapter.END_TRAIN:
                    self._end_train(training_i+1, iterations)
                    training_i += 1
                elif ret == Adapter.END_READY:
                    if save_epoch or (save_end and training_i == iterations):
                        self._save(save_path)
                elif ret == Adapter.END_TEST:
                    self._end_test(testing_i+1, iterations)
                    testing_i += 1
                else:
                    raise ValueError('Wrong run yield value: %r' % ret)
        except Exception as e:
            self.logger.log_progress('Error: %s' % e)
            if save_error and train:
                self._save(save_path)
            if isinstance(e, StopException):
                self.logger.log_progress('Forced stop')
                return -254
            raise
        finally:
            self.logger.finish(log_path)
            self.adapter.stop()

        self.logger.log_progress('Exiting')
        return 0

    def _callback(self, metrics, index, total, stage, data, expected_output, output):
        if output is not None:
            self.logger.log_result(stage, expected_output, output.argsort()[-1], index, total)
            for metric in metrics:
                metric.add_test(data, expected_output, output)
        self._check_stop()

    def _make_callback(self, metrics, total, stage):
        index = [0]  # TODO: Maybe something less hackish to count tests?

        def _callback(data, expected_output, output):
            index[0] += 1
            if index[0] > total:
                index[0] = 1
            return self._callback(metrics, index[0], total, stage, data, expected_output, output)

        return _callback

    def _check_stop(self):  # utility for stopping or pausing processes
        if os.path.exists('_stop_'):
            self.logger.log_progress('Force stopping')
            os.remove('_stop_')
            raise StopException()
        if os.path.exists('_pause_'):
            self.logger.log_progress('Pausing')
            while os.path.exists('_pause_'):
                time.sleep(0.1)
            self.logger.log_progress('Unpausing')

