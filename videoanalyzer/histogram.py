import collections
import openpyxl


try:
    FileNotFoundError
except NameError:
    FileNotFoundError = IOError


class HistogramAnalyzer(object):
    def __init__(self, path, max_v=2048, prefix=''):
        self.path = path
        self.max_v = max_v
        self.prefix = prefix
        self.data = []

    def set_prefix(self, prefix):
        self.prefix = prefix

    def add_array(self, name, array):
        self.data.append(('%s%s' % (self.prefix, name), self.histogram(array)))

    def save(self, print_=False):
        if print_:
            for name, hist in self.data:
                s = sum((v*n for symbol, h in hist.items() for v, n in h.items()))
                print(name, s)

        try:
            max_v, data = self._load(self.path)
            self.max_v = max(self.max_v, max_v)
            self.data = data + self.data
        except FileNotFoundError as e:
            pass

        wb = openpyxl.Workbook()
        wb.active.append(['#'] + ['%s [%s]' % (name, symbol) for name, hist in self.data for symbol in sorted(hist.keys())])
        for x in range(1, self.max_v+1):
            wb.active.append([x] + [hist[symbol][x] for name, hist in self.data for symbol in sorted(hist.keys())])
        wb.save(self.path)
        self.data = []

    @staticmethod
    def _load(path):
        max_v = 0
        data = []
        wb = openpyxl.load_workbook(path)
        ws = wb.active
        name = None
        hist = collections.defaultdict(lambda: collections.defaultdict(int))
        for col in list(ws.columns)[1:]:
            col = [c.value for c in col]
            max_v = max(max_v, len(col) - 2)
            n_name, n_sym = col[0].split(' [', 1)
            n_sym = n_sym[:-1]
            if name != n_name:
                if name is not None:
                    data.append(0, (name, hist))
                name = n_name
                hist = collections.defaultdict(lambda: collections.defaultdict(int))

            for x, v in enumerate(col[1:]):
                hist[n_sym][x + 1] = v

        if name is not None:
            data.append(0, (name, hist))

        return max_v, data

    @staticmethod
    def histogram(array, process=bool):
        counts = collections.defaultdict(lambda: collections.defaultdict(int))
        symbol = None
        count = 0
        for x in array:
            x = process(x)
            if x == symbol:
                count += 1
            else:
                if count:
                    counts[symbol][count] += 1
                symbol = x
                count = 1
        counts[symbol][count] += 1
        return counts

