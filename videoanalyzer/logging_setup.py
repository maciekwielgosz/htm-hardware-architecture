
# Logging setup
import warnings
import logging
import os


warnings.filterwarnings('ignore', message='The objective has been evaluated at this point before.')


logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s:%(levelname)s:%(name)s:%(message)s",
    handlers=[
        logging.FileHandler(os.environ.get('VIDEOANALYZER_LOG_PATH', "videoanalyzer.log")),
        logging.StreamHandler(),
    ],
)
logging.getLogger('videoanalyzer').setLevel(logging.DEBUG)
logging.getLogger('videoanalyzer.dataset').setLevel(logging.WARNING)
logging.getLogger('videoanalyzer.progress').setLevel(logging.DEBUG)
logging.getLogger('videoanalyzer.metrics').setLevel(logging.DEBUG)
logging.getLogger('videoanalyzer.adapters').setLevel(logging.INFO)
logging.getLogger('videoanalyzer.optimizer').setLevel(logging.INFO)
logging.getLogger('htm').setLevel(logging.CRITICAL)
logging.getLogger('pytools').setLevel(logging.WARNING)
logging.getLogger('pyopencl').setLevel(logging.WARNING)
logging.getLogger('PIL').setLevel(logging.WARNING)
logging.getLogger('matplotlib').setLevel(logging.WARNING)


