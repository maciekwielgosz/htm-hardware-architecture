# VideoAnalyzer

## What this project is for?

Creating easy to understand and modify set of tools that provide uniform interface for video analysis.
Framework should allow evaluation of different approaches in context of the same metrics.
First milestone would be comparing HTM and C3D networks on UCF-101 dataset.

## How to install?

1. Clone and checkout repository  
    `git clone https://bitbucket.org/maciekwielgosz/htm-hardware-architecture.git`  
    `cd htm-hardware-architecture`  
    `git checkout videoanalyzer`  

2. Follow steps of HTMProject installation (optional)

3. Install requirements  
    `pip install -r requirements.txt`  
    `pip install -r opt-requirements.txt`  
    `pip install -r dev-requirements.txt`  
    Installing nupic will fail under Python 3 and installing tensorflow will fail under Python 2.

4. Download the UCF-101 dataset and unpack it
    `wget http://crcv.ucf.edu/data/UCF101/UCF101.rar`  
    `wget http://crcv.ucf.edu/data/UCF101/UCF101TrainTestSplits-RecognitionTask.zip`  
    `apt-get install unrar unzip` # install unpacking software  
    `unrar x UCF101.rar`
    `unzip UCF101TrainTestSplits-RecognitionTask.zip`
    `mv ucfTrainTestlist/* UCF-101/`

## Running the analyzer

Displaying help message describing all available parameters:  
`python -m videoanalyzer --help`

### C3D
Creating adapter:  
`python -m videoanalyzer --adapter c3d_svm --create`

Training it on the UCF-101 dataset:  
`python -m videoanalyzer --adapter c3d_svm --train ucf101_train1`

Testing it on the UCF-101 dataset:  
`python -m videoanalyzer --adapter c3d_svm --test ucf101_test1`

### HTM
Creating, training and testing adapter on the UCF-5 dataset:  
`python -m videoanalyzer --adapter htm_basic --create --train ucf5_train --test ucf5_test --iterations 100`

## Running the optimizer

Displaying help message describing all available parameters:  
`python -m videoanalyzer.optimizer --help`

Optimizing number of frames used by the HTM adapter in context of accuracy on the UCF-5 dataset.   
`python -m videoanalyzer.optimizer -a htm_basic --train ucf5_train --test ucf5_test -i 100 --optimize encoder.frame_limit --parallel_evaluations 8`


## Running tests
Unit tests:  
`python -m videoanalyzer.test`

Speed tests:  
`python -m videoanalyzer.speed_test`

