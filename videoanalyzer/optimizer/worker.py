
import multiprocessing
import threading
import logging
import random

import numpy as np

from ..pipeline.eventfulqueue import EventfulQueue, queue
from ..pipeline.utils import handle_process_fault, profile_process, get_mp_logger


class EvaluatorWorker(multiprocessing.Process):
    """
    Runs evaluator in separate process
    """
    def __init__(self, name=None, timeout=1):
        # multiprocessing.Process.__init__(self, name=name)
        super(EvaluatorWorker, self).__init__(name=name)
        self.timeout = timeout
        self.input_queue = EventfulQueue()
        self.output_queue = EventfulQueue()
        self.running_event = multiprocessing.Event()
        self.running_event.set()

    # @handle_process_fault('fault')
    # @profile_process('worker')
    def run(self):
        np.random.seed(42)
        random.seed(42)  # radnom state

        threading.current_thread().name = self.name  # For easier debugging & profiling

        # from ..logging_setup import logging
        logging.basicConfig(
            level=logging.DEBUG,
            format="%(asctime)s:%(levelname)s:%(name)s:%(message)s",
            filename="%s.log" % (''.join([(c if c.isalnum() else '') for c in self.name])[:200]),
        )

        logger = get_mp_logger()
        logger.debug('Worker %s started', self.name)
        try:
            while self.running_event.is_set():
                try:
                    evaluator = self.input_queue.get(True, timeout=self.timeout)
                except (queue.Empty, multiprocessing.TimeoutError):
                    logger.debug('Worker %s timeout', self.name)
                else:
                    result = evaluator()
                    self.output_queue.put_force(result)

            logger.debug('Worker %s exited', self.name)
        except Exception:
            logger.exception('Worker %s error:', self.name)
            raise

    @classmethod
    def run_evaluator(cls, evaluator):
        runner = cls(name='Evaluator %s' % evaluator)
        runner.start()  # Start worker process
        runner.input_queue.put_force(evaluator)  # Put input data
        # TODO: add timeout for eternal-running configurations
        result = runner.output_queue.get_force()  # Fetch output data
        runner.running_event.clear()  # Stop worker process
        return result

