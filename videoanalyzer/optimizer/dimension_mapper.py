
import collections

from ..adapters._config import config as adapters_config
from htmproject.core.core_params import params as core_params
from htmproject.interfaces.config import Config


params = {
    'core': core_params,
    'encoder': {  # Sort of hack to allow optimizing the encoder. Maybe move it to some place in the htmproject
        "frame_size": {
            0: {
                'min': 1,
                'max': 1024,
                'default': 320,
            },
            1: {
                'min': 1,
                'max': 1024,
                'default': 240,
            },
        },
        "frame_limit": {
            'min': 1,
            'max': 256,  # Not really
            'default': 4,
        },
    }
}


def _get(d, k, default=None):
    try:
        return d[k]
    except (IndexError, KeyError):
        return default


class DimensionMapper(object):
    """
    Maps between HTM config parameters and dimensions passed to skopt module (optimisation space)
    """
    def __init__(self, max_layers=None, keys=None):
        self.params = params
        self.dimensions = collections.OrderedDict()
        if max_layers is None:
            max_layers = core_params['htm']['layers']['max']
        self.max_layers = max_layers
        if keys is None:
            keys = self.get_default_keys()
        self.fill_dimensions(keys)

    def get_default_keys(self, key=None, params=None):
        keys = []
        if key is None:
            key = []
        if params is None:
            params = self.params
        for k, v in params.items():
            if isinstance(v, dict):
                if 'min' in v and 'max' in v and 'default' in v:
                    keys.append(tuple(key + [k]))
                else:
                    keys.extend(self.get_default_keys(key + [k], v))
        return keys

    def fill_dimensions(self, keys):
        for key in keys:
            index = key[-1] if isinstance(key[-1], int) else None  # list attrs
            base_key = key if index is None else key[:-1]
            param = self.recursive_get(self.params, base_key)
            if not ('min' in param and 'max' in param and 'default' in param):
                index = None
                base_key = key
                param = self.recursive_get(self.params, base_key)
            iterable, r = self.process_param(param, index)
            if (not iterable) or index is not None:
                self.dimensions[key] = r
            else:
                for i in range(self.max_layers):
                    self.dimensions[tuple(list(key) + [i])] = r

    @staticmethod
    def process_param(param, index=None):
        min_ = param['min']
        max_ = param['max']
        default = param['default']
        iterable = isinstance(default, collections.Iterable)
        if not iterable:
            default = [default]
        true_int = all([isinstance(o, int) for o in list(default) + [min_, max_]])
        if true_int:
            return iterable, (min_, max_)
        else:
            return iterable, (float(min_), float(max_))  # (float(min_), float(max_), "uniform")

    @staticmethod
    def recursive_get(obj, keys, create_dicts=False):
        for key in keys:
            try:
                obj = obj[key]
            except KeyError:
                if not create_dicts:
                    raise
                obj[key] = {}
                obj = obj[key]
        return obj

    @classmethod
    def recursive_set(cls, obj, keys, value, default_len=None):
        nobj = cls.recursive_get(obj, keys[:-1], create_dicts=True)
        try:
            if isinstance(keys[-1], int) and nobj == {}:
                raise IndexError()  # Replace dict with list
            nobj[keys[-1]] = value
        except (IndexError, KeyError):
            if default_len is None:
                raise
            l = keys[-1] + 1 if isinstance(keys[-1], int) else default_len
            data = [_get(nobj, k, None) for k in range(l)]
            data[keys[-1]] = value
            cls.recursive_get(obj, keys[:-2])[keys[-2]] = data

    def get_dimensions(self):
        """
        Get list of dimensions to optimize
        """
        return list(self.dimensions.values())

    def update_config(self, config=None, values=[]):
        """
        Update passed config with passed values vector of matching shape with the one returned from get_dimensions
        """
        if config is None:
            config = Config()
        for key, value in zip(self.dimensions.keys(), values):
            self.recursive_set(config, key, value, default_len=self.max_layers)
        return config
