
from ..logging_setup import logging  # configure logging before anything is imported

import random

import numpy as np

from .cli import CLI


def main():
    """
    Run optimizer
    """
    # Init
    np.random.seed(42)
    random.seed(42)

    cli = CLI()
    r = cli.run()
    return r  # sys.exit(r or 0)
