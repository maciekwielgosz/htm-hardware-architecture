
import logging
import copy
# import threading
import math
import multiprocessing
import multiprocessing.pool
import skopt
import numpy as np

from ..adapters._config import config as adapters_config
from ..configuration import RunConfiguration
from .dimension_mapper import DimensionMapper
from .evaluator import Evaluator
from .worker import EvaluatorWorker


class ConfigOptimizer(object):
    def __init__(self, configuration, keys=None, iterations=100, avg_iterations=10, metrics_weights=(0, -1),
                 error_metric_value=1024.0, n_points=1000, n_restarts_optimizer=5, n_jobs=1, xi=0.01, kappa=1.96,
                 n_initial_points=10, acq_func='gp_hedge', acq_optimizer='auto', parallel_evaluations=2):
        self.dimensions = DimensionMapper(keys=keys)
        self.configuration = configuration  # type: RunConfiguration
        self.iterations = iterations  # type: int
        self.avg_iterations = avg_iterations
        self.metrics_weights = metrics_weights
        self.error_metric_value = error_metric_value  # type: float
        self.n_points = n_points
        self.n_restarts_optimizer = n_restarts_optimizer
        self.n_jobs = n_jobs
        self.xi = xi
        self.kappa = kappa
        self.n_initial_points = n_initial_points
        self.acq_func = acq_func
        self.acq_optimizer = acq_optimizer
        self.parallel_evaluations = parallel_evaluations

    @property
    def logger(self):
        return logging.getLogger('videoanalyzer.optimizer')

    def run(self):
        keys = ['.'.join(map(str, k)) for k in self.dimensions.dimensions.keys()]
        self.logger.info('Running optimization for %s with %s evaluations', keys, self.n_points)
        res = self.minimize()
        self.logger.debug("Optimisation result: %r", res)
        self.dimensions.update_config(adapters_config, res.x)
        self.logger.info("Minimum value of %s for params %s", res.fun, self.dimensions.update_config({}, res.x))
        cfg = self.dimensions.update_config(copy.deepcopy(adapters_config), res.x)
        self.logger.debug("Minimum value of %s for config %s", res.fun, cfg)
        return 0

    def evaluate(self, values):
        evaluator = Evaluator(
            configuration=self.configuration,
            dimensions=self.dimensions,
            config_values=values,
            iterations=self.iterations,
            avg_iterations=self.avg_iterations,
            metrics_weights=self.metrics_weights,
            error_metric_value=self.error_metric_value,
        )
        self.logger.info('Evaluating with values %s', self.dimensions.update_config({}, values))
        conf = self.dimensions.update_config(copy.deepcopy(adapters_config), values)
        self.logger.debug('Evaluating with config %s', conf)
        r = EvaluatorWorker.run_evaluator(evaluator)
        self.logger.info('Evaluation score %s for values %s', r, self.dimensions.update_config({}, values))
        conf = self.dimensions.update_config(copy.deepcopy(adapters_config), values)
        self.logger.debug('Evaluation score %s for config %s', r, conf)
        return r

    def minimize(self):
        """
        Parallel version of skopt.gp_minimize
        """
        # return skopt.gp_minimize(
        #     self.evaluate, self.dimensions.get_dimensions(),
        #     base_estimator=None, n_calls=100, n_random_starts=10, acq_func='gp_hedge',
        #     acq_optimizer='lbfgs', x0=None, y0=None, random_state=42, verbose=True, callback=None,
        #     n_points=10000, n_restarts_optimizer=5, xi=0.01, kappa=1.96, noise='gaussian', n_jobs=1,
        # )

        dimensions = self.dimensions.get_dimensions()
        from sklearn.utils import check_random_state
        from skopt.utils import normalize_dimensions, cook_estimator

        rng = check_random_state(42)
        space = normalize_dimensions(dimensions)
        base_estimator = cook_estimator("GP", space=space, random_state=rng.randint(0, np.iinfo(np.int32).max), noise='gaussian')

        acq_optimizer_kwargs = {
            "n_points": self.n_points,
            "n_restarts_optimizer": self.n_restarts_optimizer,
            "n_jobs": self.n_jobs,
        }
        acq_func_kwargs = {"xi": self.xi, "kappa": self.kappa}

        optimizer = skopt.Optimizer(
            dimensions=self.dimensions.get_dimensions(),
            base_estimator=base_estimator,
            n_initial_points=self.n_initial_points,
            acq_func=self.acq_func,
            acq_optimizer=self.acq_optimizer,  # lbfgs
            acq_optimizer_kwargs=acq_optimizer_kwargs,
            acq_func_kwargs=acq_func_kwargs,
            random_state=42,
        )
        # TODO: more dynamic ask-tell with custom pool and always correct n_points
        for i in range(int(math.ceil(self.n_points / float(self.parallel_evaluations)))):
            next_x = optimizer.ask(n_points=self.parallel_evaluations)
            next_y = multiprocessing.pool.ThreadPool(self.parallel_evaluations).map(self.evaluate, next_x)
            res = optimizer.tell(next_x, next_y)

        return res
