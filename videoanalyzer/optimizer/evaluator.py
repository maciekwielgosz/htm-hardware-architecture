
import logging
from collections import OrderedDict

from ..adapters._config import config as adapters_config
from .dimension_mapper import DimensionMapper
from ..configuration import RunConfiguration


class Evaluator(object):
    """
    Class that allows evaluating an adapter in context of given config and metrics
    """
    def __init__(self, configuration, dimensions, config_values, iterations, avg_iterations, metrics_weights=(0, -1),
                 error_metric_value=1024):
        self.dimensions = dimensions  # type: DimensionMapper
        self.configuration = configuration  # type: RunConfiguration
        self.config_values = config_values  # type: list
        self.iterations = iterations  # type: int
        self.avg_iterations = avg_iterations  # type: int
        self.metrics_weights = metrics_weights
        self.error_metric_value = error_metric_value  # High metric value for error

    def __call__(self):
        """
        Evaluate the adapter
        """
        self.logger.debug('Updating config: %s', self.dimensions.update_config({}, self.config_values))
        self.dimensions.update_config(adapters_config, self.config_values)
        self.logger.debug('Updated config: %s', adapters_config)
        metric = self.error_metric_value
        try:
            self.configuration.run(True, True, True, '_tmp', '_tmp', self.iterations, False, False, False, '_tmp.log')
            metric = self.get_metric()
        except Exception as e:
            self.logger.exception('Error with values %r and config %s', self.config_values, adapters_config)

        self.logger.debug('Function value %r', metric)
        return metric

    def get_metric(self):
        """
        Get metric value for the run. Result will be used for optimisation (lower is better)
        """
        # last_metrics = self.configuration.training_metrics_values[-self.avg_iterations:]
        last_metrics = self.configuration.testing_metrics_values[-self.avg_iterations:]
        # duration = last_metrics[-1][0]
        # accuracy = last_metrics[-1][1]
        # return 100.0 - accuracy
        total = 0.0
        for mm in last_metrics:
            total += sum([(m * w) if w else 0.0 for m, w in zip(mm, self.metrics_weights)])
        return (total / len(last_metrics)) if last_metrics else 0.0

    def __str__(self):
        return str(self.dimensions.update_config({}, self.config_values))

    @property
    def logger(self):
        return logging.getLogger('videoanalyzer.optimizer')
