from __future__ import print_function

import argparse

from ..adapters._config import config as adapters_config
from ..predefined import get_predefined_cli
from ..configuration import RunConfiguration
from .config_optimizer import ConfigOptimizer
from .dimension_mapper import DimensionMapper


def _isnumeric(s):
    try:
        return s.isnumeric()
    except AttributeError:  # PY2
        return s.decode().isnumeric()


class CLI(object):
    """
    Command line interface for the videoanalyzer.optimizer
    """
    def __init__(self):
        self.cli = get_predefined_cli()
        self.default_keys = ['.'.join(map(str, k)) for k in DimensionMapper().get_default_keys()]
        self.parser = argparse.ArgumentParser(add_help=False)
        self.parser.add_argument("--help", "-h", help="Show this help message", action="store_true")
        self.parser.add_argument("--adapter", "-a", type=str, help="Select adapter", default='')
        self.parser.add_argument("--weights", "-w", type=str, help="Comma separated metric weights. Defaults to accuracy (0,-1,0,0,0,0) ", default='0,-1,0,0,0,0')
        self.parser.add_argument("--train", type=str, help="Train on dataset", default='')
        self.parser.add_argument("--test", type=str, help="Test on dataset", default='')
        self.parser.add_argument("--iterations", "-i", type=int, help="Number of iterations", default=100)
        self.parser.add_argument("--average_iterations", type=int, help="Number of iterations to average metrics over", default=10)
        self.parser.add_argument("--error_metric", type=float, help="Value for metric in case of error", default=1024.0)
        self.parser.add_argument("--optimize", "-o", type=str, help="Config key[s] to optimize. Defaults to all available", action='append')

        self.parser.add_argument("--n_points", type=int, help="Total number of evaluations", default=20)
        self.parser.add_argument("--n_restarts_optimizer", type=int, help="n_restarts_optimizer", default=5)
        self.parser.add_argument("--n_jobs", type=int, help="n_jobs", default=4)
        self.parser.add_argument("--xi", type=float, help="xi", default=0.01)
        self.parser.add_argument("--kappa", type=float, help="kappa", default=1.96)
        self.parser.add_argument("--n_initial_points", type=int, help="Number of random starts", default=10)
        self.parser.add_argument("--acq_func", type=str, help="acq_func", default='gp_hedge')
        self.parser.add_argument("--acq_optimizer", type=str, help="acq_optimizer", default='auto')
        self.parser.add_argument("--parallel_evaluations", "-p", type=int, help="Number of parallel processes evaluating the adapter", default=2)

    def help(self, msg):
        self.parser.print_help()
        print()
        print(msg)
        print("Available adapters: %s" % (', '.join(self.cli.adapters.keys())))
        print("Available datasets: %s" % (', '.join(self.cli.datasets.keys())))
        print("Available config keys: %s" % (', '.join(self.default_keys)))

    def run(self, args=None):
        ns, argv = self.parser.parse_known_args(args)
        if argv:
            self.help('Unrecognized arguments: %s' % (' '.join(argv)))
            return -1

        if ns.help:
            self.help("Run videoanalyzer.optimizer.")
            return 0

        if ns.adapter not in self.cli.adapters:
            self.help("Unrecognised adapter (%s)!" % ns.adapter)
            return -2

        if ns.train not in self.cli.datasets:
            self.help("Unrecognised train dataset (%s)!" % ns.train)
            return -4

        if ns.test not in self.cli.datasets:
            self.help("Unrecognised test dataset (%s)!" % ns.test)
            return -5

        try:
            weights = [float(w) for w in ns.weights.split(',')]
        except TypeError:
            self.help("Wrong value for the metric weight!")
            return -6

        keys = []
        for k in (ns.optimize or self.default_keys):
            kk = tuple([int(e) if _isnumeric(e) else e for e in k.split('.')])
            try:
                DimensionMapper(keys=[kk])
            except (KeyError, IndexError):
                self.help("Wrong value for optimize (%s)!" % k)
                return -7
            keys.append(kk)

        conf = RunConfiguration(
            adapter=self.cli.adapters[ns.adapter],
            training_dataset=self.cli.datasets.get(ns.train, None),
            testing_dataset=self.cli.datasets.get(ns.test, None),
            training_metrics=self.cli.get_metrics([]),
            testing_metrics=self.cli.get_metrics([]),
            logger=None,
        )
        opt = ConfigOptimizer(
            configuration=conf,
            keys=keys,
            iterations=ns.iterations,
            avg_iterations=ns.average_iterations,
            metrics_weights=weights,
            error_metric_value=ns.error_metric,
            n_points=ns.n_points,
            n_restarts_optimizer=ns.n_restarts_optimizer,
            n_jobs=ns.n_jobs,
            xi=ns.xi,
            kappa=ns.kappa,
            n_initial_points=ns.n_initial_points,
            acq_func=ns.acq_func,
            acq_optimizer=ns.acq_optimizer,
            parallel_evaluations=ns.parallel_evaluations,
        )
        return opt.run()




