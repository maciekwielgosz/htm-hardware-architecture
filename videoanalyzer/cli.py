from __future__ import print_function

from collections import OrderedDict
import argparse

from .configuration import RunConfiguration
from .adapters import Adapter
from .datasets import Dataset
from .metrics import Metric
from .loggers import BaseLogger
from .utils import typing


class CLI(object):
    """
    Command line interface for the videoanalyzer
    """
    def __init__(self):
        self.adapters = OrderedDict()  # type: typing.Dict[str, Adapter]
        self.datasets = OrderedDict()  # type: typing.Dict[str, Dataset]
        self.metrics = OrderedDict()  # type: typing.Dict[str, typing.Callable[[], Metric]]
        self.loggers = OrderedDict()  # type: typing.Dict[str, BaseLogger]

        self.parser = argparse.ArgumentParser(add_help=False)
        self.parser.add_argument("--help", "-h", help="Show this help message", action="store_true")
        self.parser.add_argument("--adapter", "-a", type=str, help="Select adapter", default='')
        self.parser.add_argument("--metric", "-m", type=str, help="Use metric", action='append')
        self.parser.add_argument("--train", type=str, help="Train on dataset", default='')
        self.parser.add_argument("--test", type=str, help="Test on dataset", default='')
        self.parser.add_argument("--logger", "-l", type=str, help="Select logger", default='')
        self.parser.add_argument("--log_path", help="Logger save path (xlsx)", default='')
        self.parser.add_argument("--name", "-n", type=str, help="Name used in default filenames", default='')
        self.parser.add_argument("--iterations", "-i", type=int, help="Iterations", default=1)
        self.parser.add_argument("--create", help="Create new adapter", action="store_true")
        self.parser.add_argument("--load", help="Adapter load path", default='')
        self.parser.add_argument("--save", help="Adapter save path", default='')
        self.parser.add_argument("--no_save_end", help="Do not save on the end", action="store_true")
        self.parser.add_argument("--no_save_epoch", help="Do not save every epoch", action="store_true")
        self.parser.add_argument("--no_save_error", help="Do not save on error", action="store_true")

    def help(self, msg):
        self.parser.print_help()
        print()
        print(msg)
        print("Available adapters: %s" % (', '.join(self.adapters.keys())))
        print("Available metrics:  %s" % (', '.join(self.metrics.keys())))
        print("Available datasets: %s" % (', '.join(self.datasets.keys())))
        print("Available loggers:  %s" % (', '.join(self.loggers.keys())))

    def get_metrics(self, metrics):
        if not metrics:
            metrics = self.metrics.keys()
        return [self.metrics[metric]() for metric in metrics]

    def run(self, args=None):
        ns, argv = self.parser.parse_known_args(args)
        if argv:
            self.help('Unrecognized arguments: %s' % (' '.join(argv)))
            return -1

        if ns.help:
            self.help("Run videoanalyzer.")
            return 0

        if ns.adapter not in self.adapters:
            self.help("Unrecognised adapter (%s)!" % ns.adapter)
            return -2

        metrics = ns.metric if ns.metric else []
        for metric in metrics:
            if metric not in self.metrics:
                self.help("Unrecognised metric (%s)!" % metric)
                return -3

        if ns.train != '' and ns.train not in self.datasets:
            self.help("Unrecognised train dataset (%s)!" % ns.train)
            return -4

        if ns.test != '' and ns.test not in self.datasets:
            self.help("Unrecognised test dataset (%s)!" % ns.test)
            return -5

        if ns.logger != '' and ns.logger not in self.loggers:
            self.help("Unrecognised logger (%s)!" % ns.logger)
            return -6

        name = ns.name or ns.adapter
        logger = self.loggers.get(ns.logger, list(self.loggers.values())[-1] if self.loggers else None)
        conf = RunConfiguration(
            adapter=self.adapters[ns.adapter],
            training_dataset=self.datasets.get(ns.train, None),
            testing_dataset=self.datasets.get(ns.test, None),
            training_metrics=self.get_metrics(metrics),
            testing_metrics=self.get_metrics(metrics),
            logger=logger,
        )
        return conf.run(
            create=ns.create,
            train=ns.train != '',
            test=ns.test != '',
            load_path=ns.load or ('%s%s' % (name, conf.adapter.store_suffix)),
            save_path=ns.save or ('%s%s' % (name, conf.adapter.store_suffix)),
            iterations=ns.iterations,
            save_end=not ns.no_save_end,
            save_epoch=not ns.no_save_epoch,
            save_error=not ns.no_save_error,
            log_path=ns.log_path or ('%s.xlsx' % name),
        )
